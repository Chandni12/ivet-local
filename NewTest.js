import React, { Component } from 'react';
import { Picker, View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Dimensions, FlatList, Platform, Modal, ActivityIndicator, Alert } from 'react-native';
import { lang } from 'moment';


export default class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            language: ''
        }
        this.arrayTmp = ['a', 'b', 'c']
    }
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <Text>{this.state.language}</Text>

                <Picker
                    selectedValue={this.state.language}
                    style={{ height: 50, width: 100 }}
                    onValueChange={(itemValue, itemIndex) =>
                        this.setState({ language: itemValue })
                    }>
                    {this.arrayTmp.map((item, i) => {

                        return <Picker.Item label={item} value={item} key={i} />
                    })}
                </Picker>
            </View>
        )
    }
}