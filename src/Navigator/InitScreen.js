import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Image,
    TouchableOpacity,
    AsyncStorage,
    Platform
} from "react-native";
import wordConstants from '../constants/WordConstants';

export default class InitScreen extends Component{

    componentWillMount = ()=>{
       // this.createNotificationListenersIos(); 
       
      this._getAuthToken();
    }

    async _getAuthToken() {
        const auth_token = await AsyncStorage.getItem('auth_token');
        const userData = await AsyncStorage.getItem('userData');

        console.log("************", auth_token);
        if (auth_token == null) {
            this.props.navigation.navigate(Platform.OS === 'ios' ? 'Welcome' : 'SplashScreenAndroid');
        } else {
         
          //  return auth_token
           // Need to work for checking category exist or not
          this.props.navigation.navigate('MainContainer', { auth_token: auth_token, arrayCategory: [],userData:userData});
        }
      }

    render() {
      return (
       null
      )
    };
}