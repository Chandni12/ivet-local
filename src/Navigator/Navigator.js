import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Image,
    TouchableOpacity,
    AsyncStorage
} from "react-native";

import { Platform } from 'react-native';
import { Provider } from 'react-redux';
import store from '../store/configureStore';
import WelcomeAndroid from '../components/WelcomeAndroid';
import WelcomeIos from '../components/WelcomeIos';
import SignupContainer from '../containers/Signup';
import LoginContainer from '../containers/Login';
import CategorySelection from '../containers/Category';
import NoteDetailPageContainer from '../containers/NoteDetailPage';
//import CategorySelection from '../components/CategorySelection';
import wordConstants from '../constants/WordConstants'
import MainContainer from '../containers/MainContainer'

import TagsContainer from '../containers/Tags';

import LoginProgress from '../containers/LoginProgress';

import DashboardContainer from '../containers/Dashboard';
import Questions from '../containers/Questions';
import Answers from '../components/Answer/Answers';
import PostAnswer from '../components/Answer/PostAnswer';

import Notification from '../containers/Notification';
import FollowersPageContainer from '../containers/Follow';
import EditRequest from '../containers/EditRequest';
import MyProfileContainer from '../containers/MyProfile';
import TagContainer from '../containers/TagContainer';
import CommunityScore from '../components/communityScore/CommunityScore';
import IconEntypo from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconMaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconFeather from 'react-native-vector-icons/Feather';
import PostQuestionOrNotesContainer from '../containers/PostQuestionOrNotes';
import PostBasicInfo from '../components/Question/PostBasicInfo';
import PostPhotoSection from '../components/Question/PostPhotoSection';
import PostCategorySection from '../components/Question/PostCategorySection';
import Others from '../components/Others';
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer, DrawerItems, StackNavigator, DrawerNavigator, TabNavigator } from 'react-navigation';
//import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import PostMenu from '../components/PostMenu';
import DetailPage from '../containers/DetailPage';
import Forgotpassword from '../containers/ForgotPassword';
//import Forgotpassword from '../components/Forgotpassword';
import SplashScreenAndroid from '../components/SplashScreenAndroid';
import DrawerComponent from '../components/DrawerComponent';
import EditAnswerContainer from "../containers/EditAnswer";
import HistoryContainer from '../containers/HistoryContainer';

import ViewWithDropDownRowContainer from '../containers/ViewWithDropDownRow'
import { createDrawerNavigator } from 'react-navigation-drawer';
import EditWorkExperience from '../components/profile/EditWorkExperience';
import EditBasicInfo from '../components/profile/EditBasicInfo';
import EditEducationDetail from '../components/profile/EditEducationDetail';
import DraftContainer from '../containers/Draft'
import InitScreen from './InitScreen';


////************ Tab navigator *********////
var viewName = Platform.OS === 'ios' ? 'Welcome' : 'SplashScreenAndroid';
var dataToPass = {};


const getAuthToken = () => {
    const auth_token = AsyncStorage.getItem('auth_token');
    console.log("** navigatore **********", auth_token);
    if (auth_token == null) {
        viewName = Platform.OS === 'ios' ? 'Welcome' : 'SplashScreenAndroid'
    } else {

        viewName = 'MainContainer';
        //  return auth_token
        // Need to work for checking category exist or not
        //   this.props.navigation.navigate('MainContainer', { auth_token: this.state.auth_token, arrayCategory: [] });
    }
}

const tabNavigator = createBottomTabNavigator(
    {
        ホーム: DashboardContainer,
        回答する: Questions,
        質問する: PostQuestionOrNotesContainer,
        その他: PostMenu
        // その他: () =>
        //     {
        //         console.log(' props tab *********=---', this.props);
        //         return DashboardContainer;
        //      //  DashboardContainer//routeName === 'ホーム' ? DashboardContainer: routeName == '回答する'? Questions: PostQuestionOrNotesContainer

        // }
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarOnPress: ({ navigation, defaultHandler }) => {
                defaultHandler();
            },

            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let IconComponent;
                let iconName;
                if (routeName === 'ホーム' || routeName === 'Dashboard') {
                    IconComponent = IconEntypo;
                    iconName = `home${focused ? '' : ''}`;

                } else if (routeName == '回答する' || routeName === 'Question') {
                    IconComponent = IconMaterialCommunityIcon;
                    iconName = 'account-question'
                }
                else if (routeName === '質問する' || routeName === 'Answer') {
                    IconComponent = IconFontAwesome5;
                    iconName = `edit`;

                } else if (routeName == 'その他' || routeName === 'Others') {
                    IconComponent = IconFeather;
                    iconName = 'more-horizontal';
                }

                // You can return any component that you like here!
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }),
        // bottomTabs: {
        //     barStyle: 'default' | 'black',
        //     translucent: true,
        //     hideShadow: false
        // },
        // overlay: {
        //     interceptTouchOutside: true,
        //     handleKeyboardEvents: true
        // },
        // modal: {
        //     swipeToDismiss: true
        // },
        tabBarOptions: {
            activeTintColor: 'rgb(2, 132, 254)',
            inactiveTintColor: 'rgba(73, 73, 73, 255);',
        },
    }
);

const appStackNavigator = new createStackNavigator({
    Tags: {
        screen: TagsContainer
    },
    EditBasicInfo: {
        screen: EditBasicInfo
    },
    EditEducationDetail: {
        screen: EditEducationDetail
    },
    EditWorkExperience: {
        screen: EditWorkExperience
    },
    ViewWithDropDownRow: {
        screen: ViewWithDropDownRowContainer
    },
    SplashScreenAndroid: {
        screen: SplashScreenAndroid
    },
    Login: {
        screen: LoginContainer
    },
    Signup: {
        screen: SignupContainer
    },
    Welcome: {
        screen: Platform.OS === 'android' ? WelcomeAndroid : WelcomeIos
    },
    LoginProgress: {
        screen: LoginProgress
    },
    CategorySelection: {
        screen: CategorySelection,
    },
    
    Dashboard: {
        screen: DashboardContainer,
    },
    Questions: {
        screen: Questions
    },
    PostAnswer: {
        screen: PostAnswer, // Post detail / Click on Question  search 
    },
    MainContainer: {
        screen: MainContainer,
        navigationOptions: {
            gesturesEnabled: false,
        },
    },
    // tabNavigator: {
    //     screen: tabNavigator
    // },
    notification: {
        screen: Notification
    },
    follow: {
        screen: FollowersPageContainer
    },
    EditRequest: {
        screen: EditRequest
    },
    profile: {
        screen: MyProfileContainer
    },
    PostQuestionOrNotes: {
        screen: PostQuestionOrNotesContainer // Post a question/ Clinical notes 
    },
    PostBasicInfo: {
        screen: PostBasicInfo
    },
    PostCategorySection: {
        screen: PostCategorySection
    },
    PostPhotoSection: {
        screen: PostPhotoSection
    },
    communityScore: {
        screen: CommunityScore
    },
    others: {
        screen: Others
    },
    postMenu: {
        screen: PostMenu
    },
    DetailPage: {
        screen: DetailPage
    },
    NoteDetailPage: {
        screen: NoteDetailPageContainer
    },
    Forgotpassword: {
        screen: Forgotpassword
    },
    EditAnswer: {
        screen: EditAnswerContainer
    },
    ViewHistory: {
        screen: HistoryContainer
    },
    TagContainer: {
        screen: TagContainer
    },
    InitScreen: {
        screen: InitScreen
    },
    DraftContainer: {
        screen: DraftContainer
    }
    // NotificationService: {
    //     screen: NotificationService
    // }

}, {
    //initialRouteName:'Login',
    initialRouteName: 'InitScreen',
    headerMode: 'none',
    navigationOptions: {
        gesturesEnabled: false,
    },
})

const RootNavigator = createAppContainer(appStackNavigator);


////************ Drawer navigator *********////

const DrawerNavigatorExample = createDrawerNavigator({
    //Drawer Optons and indexing
    RootNavigator: { //Title
        screen: RootNavigator,
        navigationOptions: {
            drawerLabel: "ホーム"
        }
    }
},
    {
        drawerOpenRoute: 'DrawerOpen',
        drawerCloseRoute: 'DrawerClose',
        drawerToggleRoute: 'DrawerToggle',
        drawerWidth: 250,
        useNativeAnimations: true,
        contentComponent: DrawerComponent
    });

export const DrawerContainer = createAppContainer(DrawerNavigatorExample)

export class IVetApp extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        console.log('in render  props tab *********=---', this.props);
        return (
            <Provider store={store}>
                <DrawerContainer />
            </Provider>
        );
    }
}
const styles = {
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        backgroundColor: "white",
        height: 100,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 50
    },
    name: {
        fontSize: 16,
        color: 'black',

    },
    body: {
        marginTop: 70,
        marginBottom: 20,
        alignItems: 'center',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
    },

}
export default DrawerNavigatorExample;



