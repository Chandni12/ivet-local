import axios from 'axios';
import { END_POINT_ADD_REMOVE_TAG_IN_TAG_FEED, END_POINT_POST_GET_SELECTED_TAG_DATA , END_POINT_FOLLOWING_TAGS, END_POINT_FOLLOWING_TAGS_DETAIL_BASED_ON_CATEGORY_NAME} from "../api/endPoints";

import {
  GET_ALL_TAGS_REQUEST,
  GET_ALL_TAGS_SUCCESS,
  GET_ALL_TAGS_FAILURE, 

  ADD_REMOVE_TAG_IN_TAG_FEED_REQUEST,
  ADD_REMOVE_TAG_IN_TAG_FEED_SUCCESS,
  ADD_REMOVE_TAG_IN_TAG_FEED_FAILURE,

  GET_FOLLOWING_TAGS_REQUEST,
  GET_FOLLOWING_TAGS_SUCCESS,
  GET_FOLLOWING_TAGS_FAILURE, 

  GET_FOLLOWING_TAGS_BASED_ON_CATEGORY_REQUEST,
  GET_FOLLOWING_TAGS_BASED_ON_CATEGORY_SUCCESS,
  GET_FOLLOWING_TAGS_BASED_ON_CATEGORY_FAILURE, 
  
} from "../constants";

const getAllTagsRequest = () => ({
  type: GET_ALL_TAGS_REQUEST
});

const getAllTagsSuccess = data => ({
  type: GET_ALL_TAGS_SUCCESS,
  payload: {
    data: data
  }
});

const getAllTagsFailure = error => ({
  type: GET_ALL_TAGS_FAILURE,
  payload: {
    error
  }
});


export const getAllTagsData = (token, postData) => {

  return dispatch => {

   const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

   dispatch(getAllTagsRequest());

    return axios({
      method:'post',
      url:END_POINT_POST_GET_SELECTED_TAG_DATA,
      data: postData,
      headers:headers
    })
      .then(res => {
        console.log(" all tags res--",res)
        dispatch(getAllTagsSuccess(res.data));
        return res.data;
      })
      .catch(err => {
        console.log(" all tags Err--", err.response)
        dispatch(getAllTagsFailure(err.message));
      });
  };
};

//// FOLLLOWING TAGS 

const getFollowingTagsRequest = () => ({
    type: GET_FOLLOWING_TAGS_REQUEST
  });
  
  const getFollowingTagsSuccess = data => ({
    type: GET_FOLLOWING_TAGS_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const getFollowingTagsFailure = error => ({
    type: GET_FOLLOWING_TAGS_FAILURE,
    payload: {
      error
    }
  });
  
  
  export const getFollowingTagsData = (token) => {
  
    return dispatch => {
  
     const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': token
      };
  
     dispatch(getFollowingTagsRequest());
  
      return axios({
        method:'get',
        url:END_POINT_FOLLOWING_TAGS,
        headers:headers
      })
        .then(res => {
          
          dispatch(getFollowingTagsSuccess(res.data));
          return res.data;
        })
        .catch(err => {
          dispatch(getFollowingTagsFailure(err.message));
        });
    };
  };
  
  // FOLLOWING TAGS BASED ON CATEGORY 

  const getFollowingTagsBasedOnCategoryRequest = () => ({
    type: GET_FOLLOWING_TAGS_BASED_ON_CATEGORY_REQUEST
  });
  
  const getFollowingTagsBasedOnCategorySuccess = data => ({
    type: GET_FOLLOWING_TAGS_BASED_ON_CATEGORY_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const getFollowingTagsBasedOnCategoryFailure = error => ({
    type: GET_FOLLOWING_TAGS_BASED_ON_CATEGORY_FAILURE,
    payload: {
      error
    }
  });
  

  export const getFollowingTagsBasedOnCategoryData = (token, categoryName) => {
  
    return dispatch => {
  
     const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': token
      };
  let url = END_POINT_FOLLOWING_TAGS_DETAIL_BASED_ON_CATEGORY_NAME+categoryName;

     dispatch(getFollowingTagsBasedOnCategoryRequest());
  
      return axios({
        method:'get',
        url:url,
        headers:headers
      })
        .then(res => {
          
          dispatch(getFollowingTagsBasedOnCategorySuccess(res.data));
          return res.data;
        })
        .catch(err => {
          dispatch(getFollowingTagsBasedOnCategoryFailure(err.message));
        });
    };
  };

  // ADD/ REMOVE TAG IN TAG FEED. 

  const addRemoveTagFromTagfeedRequest = () => ({
    type: ADD_REMOVE_TAG_IN_TAG_FEED_REQUEST
  });
  
  const addRemoveTagFromTagfeedSuccess = data => ({
    type: ADD_REMOVE_TAG_IN_TAG_FEED_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const addRemoveTagFromTagfeedFailure = error => ({
    type: ADD_REMOVE_TAG_IN_TAG_FEED_FAILURE,
    payload: {
      error
    }
  });
  
  export const addRemoveTagFromTagfeedData = (token, tagId) => {
  
    return dispatch => {
  
     const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': token
      };
  let url = END_POINT_ADD_REMOVE_TAG_IN_TAG_FEED+tagId;

  console.log(" url is to add tag---", url); 
     dispatch(addRemoveTagFromTagfeedRequest());
  
      return axios({
        method:'get',
        url:url,
        headers:headers
      })
        .then(res => {
          dispatch(addRemoveTagFromTagfeedSuccess(res.data));
          return res.data;
        })
        .catch(err => {
          dispatch(addRemoveTagFromTagfeedFailure(err.message));
        });
    };
  };