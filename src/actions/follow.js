import axios from 'axios';
import { END_POINT_GET_FOLLOWERS_DATA } from "../api/endPoints";

import {
  FOLLOW_REQUEST,
  FOLLOW_SUCCESS,
  FOLLOW_FAILURE
} from "../constants";

const followRequest = () => ({
  type: FOLLOW_REQUEST
});

const followSuccess = data => ({
  type: FOLLOW_SUCCESS,
  payload: {
    data: data
  }
});

const followFailure = error => ({
  type: FOLLOW_FAILURE,
  payload: {
    error
  }
});

  export const getFollowersData = (data) => {
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': data.auth_token
      };
      dispatch(followRequest());
      
      return axios({
        method: 'post',
        url: END_POINT_GET_FOLLOWERS_DATA,
        headers: headers
    }).then(res => {
      console.log(" rresponse -- followers ")
      dispatch(followSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(followFailure(err.message));
      });
    };
  };
