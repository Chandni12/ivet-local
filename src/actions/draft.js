import axios from 'axios';
import { END_POINT_GET_DRAFT_DATA } from "../api/endPoints";

import {
  DRAFT_REQUEST,
  DRAFT_SUCCESS,
  DRAFT_FAILURE,
} from "../constants";

const draftRequest = () => ({
  type: DRAFT_REQUEST
});

const draftSuccess = data => ({
  type: DRAFT_SUCCESS,
  payload: {
    data: data
  }
});

const draftFailure = error => ({
  type: DRAFT_FAILURE,
  payload: {
    error
  }
});

  export const getDraftData = (token) => {
    console.log('token',token)
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': token
      };

      dispatch(draftRequest());
      
      return axios({
        method: 'get',
        url: END_POINT_GET_DRAFT_DATA,
        headers: headers
    }).then(res => {
      console.log('res',res)
      dispatch(draftSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(draftFailure(err.message));
      });
    };
  };    

