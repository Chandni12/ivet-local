import axios from 'axios';
import { END_POINT_GET_BREED_GENDER_OF_ANIMAL,END_POINT_POST_QUESTION,  END_POINT_GET_DATA_TO_POST_QUESTION, END_POINT_POST_DRAFT_AS_A_QUESTION, END_POINT_GET_DATA_FOR_EDIT_QUESTION, END_POINT_SAVE_DATA_FOR_EDIT_QUESTION } from "../api/endPoints";

import {
  POST_QUESTION_REQUEST,
  POST_QUESTION_SUCCESS,
  POST_QUESTION_FAILURE, 

  GET_ANIMAL_BREED_GENDER_FAILURE,
  GET_ANIMAL_BREED_GENDER_SUCCESS,
  GET_ANIMAL_BREED_GENDER_REQUEST,

  GET_SPECIALITY_FAILURE,
  GET_SPECIALITY_REQUEST,
  GET_SPECIALITY_SUCCESS,

  GET_DATA_TO_POST_QUESTION_REQUEST,
  GET_DATA_TO_POST_QUESTION_FAILURE,
  GET_DATA_TO_POST_QUESTION_SUCCESS,

  POST_DRAFT_AS_A_QUESTION_REQUEST,
  POST_DRAFT_AS_A_QUESTION_SUCCESS,
  POST_DRAFT_AS_A_QUESTION_FAILURE,

  GET_DATA_TO_EDIT_QUESTION_REQUEST,
  GET_DATA_TO_EDIT_QUESTION_FAILURE,
  GET_DATA_TO_EDIT_QUESTION_SUCCESS,

  SAVE_EDIT_QUESTION_REQUEST,
  SAVE_EDIT_QUESTION_FAILURE,
  SAVE_EDIT_QUESTION_SUCCESS,


} from "../constants";

// Get complete data to post question 
const getDataToPostQuestionRequest = () => ({
  type: GET_DATA_TO_POST_QUESTION_REQUEST
});

const getDataToPostQuestionSuccess = data => ({
  type: GET_DATA_TO_POST_QUESTION_SUCCESS,
  payload: {
    data: data
  }
});
const getDataToPostQuestionFailure = error => ({
  type: GET_DATA_TO_POST_QUESTION_FAILURE,
  payload: {
    error
  }
});

export const getDataToPostQuestion = (token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };
    
    dispatch(getDataToPostQuestionRequest());

    return axios({
      method: 'post',
      url: END_POINT_GET_DATA_TO_POST_QUESTION,
      headers: headers,
    })
      .then(res => {
       console.log(" Question data ---> ", res);
        dispatch(getDataToPostQuestionSuccess(res.data));
        return res.data;
      })
      .catch(err => {
     
        dispatch(getDataToPostQuestionFailure(err.message));
      });
  };
};

// Post Questions 
const postQuestionRequest = () => ({
  type: POST_QUESTION_REQUEST
});

const postQuestionSuccess = data => ({
  type: POST_QUESTION_SUCCESS,
  payload: {
    data: data
  }
});
const postQuestionFailure = error => ({
  type: POST_QUESTION_FAILURE,
  payload: {
    error
  }
});

export const postQuestion = (data, auth_token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': auth_token
    };

    dispatch(postQuestionRequest());
   

    return axios({
      method: 'post',
      url: END_POINT_POST_QUESTION,
      data: data,
      headers: headers, 
    })
      .then(res => {
        
        dispatch(postQuestionSuccess(res.data));
        return res.data;
      })
      .catch(err => {
        
        dispatch(postQuestionFailure(err.message));
      });
  };
};

// Post Draft as a Questions 

const postDraftAsQuestionRequest = () => ({
  type: POST_DRAFT_AS_A_QUESTION_REQUEST
});

const postDraftAsQuestionSuccess = data => ({
  type: POST_DRAFT_AS_A_QUESTION_SUCCESS,
  payload: {
    data: data
  }
});
const postDraftAsQuestionFailure = error => ({
  type: POST_DRAFT_AS_A_QUESTION_FAILURE,
  payload: {
    error
  }
});

export const postDraftAsQuestion = (data, auth_token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': auth_token
    };

    dispatch(postDraftAsQuestionRequest());
   

    return axios({
      method: 'post',
      url: END_POINT_POST_DRAFT_AS_A_QUESTION,
      data: data,
      headers: headers, 
    })
      .then(res => {
        
        dispatch(postDraftAsQuestionSuccess(res.data));
        return res.data;
      })
      .catch(err => {
       
        dispatch(postDraftAsQuestionFailure(err.message));
      });
  };
};

// Edit scenarios 

const getDataToEditQuestionRequest = () => ({
  type: GET_DATA_TO_EDIT_QUESTION_REQUEST
});

const getDataToEditQuestionSuccess = data => ({
  type: GET_DATA_TO_EDIT_QUESTION_SUCCESS,
  payload: {
    data: data
  }
});
const getDataToEditQuestionFailure = error => ({
  type: GET_DATA_TO_EDIT_QUESTION_FAILURE,
  payload: {
    error
  }
});

export const getDataToEditQuestion = (param,token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': param.auth_token
    };
    
    dispatch(getDataToEditQuestionRequest());

    

    let url = END_POINT_GET_DATA_FOR_EDIT_QUESTION+param.slug

    return axios
    .get(url, {headers:headers})
    .then(res => {
      console.log("Edit Question data ---> ", res);
      dispatch(getDataToEditQuestionSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(getDataToEditQuestionFailure(err.message));
    });

    
  };
};


// Save edited question 

const saveEditedQuestionRequest = () => ({
  type: SAVE_EDIT_QUESTION_REQUEST
});

const saveEditedQuestionSuccess = data => ({
  type: SAVE_EDIT_QUESTION_SUCCESS,
  payload: {
    data: data
  }
});
const saveEditedQuestionFailure = error => ({
  type: SAVE_EDIT_QUESTION_FAILURE,
  payload: {
    error
  }
});

export const saveEditedQuestion = (data, auth_token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': auth_token
    };
    dispatch(saveEditedQuestionRequest());
   
    return axios({
      method: 'post',
      url: END_POINT_SAVE_DATA_FOR_EDIT_QUESTION,
      data: data,
      headers: headers, 
    })
      .then(res => {
       
        dispatch(saveEditedQuestionSuccess(res.data));
        return res.data;
      })
      .catch(err => {
       
        dispatch(saveEditedQuestionFailure(err.message));
      });
  };
};

const getAnimalBreedGenderRequest = () => ({
  type: GET_DATA_TO_POST_QUESTION_REQUEST
});

const getAnimalBreedGenderSuccess = data => ({
  type: GET_DATA_TO_POST_QUESTION_SUCCESS,
  payload: {
    data: data
  }
});
const getAnimalBreedGenderFailure = error => ({
  type: GET_DATA_TO_POST_QUESTION_FAILURE,
  payload: {
    error
  }
});

export const getAnimalBreedGender = (token, animalId) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };
    
    dispatch(getAnimalBreedGenderRequest());
    
    let urlApi = END_POINT_GET_BREED_GENDER_OF_ANIMAL+animalId;
   
    return axios.get(urlApi, {headers:headers}).then(res => {
     
       dispatch(getAnimalBreedGenderSuccess(res.data));
       return res.data;
     })
     .catch(err => {
       dispatch(getAnimalBreedGenderFailure(err.message));
     });
     
  };
};
