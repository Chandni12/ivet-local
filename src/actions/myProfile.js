import axios from 'axios';
import { END_POINT_GET_MY_PROFILE, END_POINT_GET_NICK_NAME, END_POINT_UPDATE_MY_PROFILE, END_POINT_ADD_EDUCATION_PROFILE,END_POINT_UPDATE_EDUCATION_PROFILE,END_POINT_ADD_WORK_EXPERIENCE_PROFILE,END_POINT_UPDATE_WORK_PROFILE, END_POINT_SET_NICK_NAME } from "../api/endPoints";

import {
  MY_PROFILE_REQUEST,
  MY_PROFILE_SUCCESS,
  MY_PROFILE_FAILURE,

  MY_PROFILE_UPDATE_REQUEST,
  MY_PROFILE_UPDATE_SUCCESS,
  MY_PROFILE_UPDATE_FAILURE,

  ADD_EDUCATION_PROFILE_REQUEST,
  ADD_EDUCATION_PROFILE_SUCCESS,
  ADD_EDUCATION_PROFILE_FAILURE,

  UPDATE_EDUCATION_PROFILE_REQUEST,
  UPDATE_EDUCATION_PROFILE_SUCCESS,
  UPDATE_EDUCATION_PROFILE_FAILURE,

  ADD_WORK_EXPERIENCE_PROFILE_REQUEST,
  ADD_WORK_EXPERIENCE_PROFILE_SUCCESS,
  ADD_WORK_EXPERIENCE_PROFILE_FAILURE,

  UPDATE_WORK_EXPERIENCE_PROFILE_REQUEST,
  UPDATE_WORK_EXPERIENCE_PROFILE_SUCCESS,
  UPDATE_WORK_EXPERIENCE_PROFILE_FAILURE,

  SET_NICK_NAME_REQUEST,
  SET_NICK_NAME_SUCCESS,
  SET_NICK_NAME_FAILURE,

  GET_NICK_NAME_REQUEST,
  GET_NICK_NAME_SUCCESS,
  GET_NICK_NAME_FAILURE,

} from "../constants";

const myProfileRequest = () => ({
  type: MY_PROFILE_REQUEST
});

const myProfileSuccess = data => ({
  type: MY_PROFILE_SUCCESS,
  payload: {
    data: data
  }
});

const myProfileFailure = error => ({
  type: MY_PROFILE_FAILURE,
  payload: {
    error
  }
});

const setNickNameRequest = () => ({
  type: SET_NICK_NAME_REQUEST
});

const setNickNameSuccess = data => ({
  type: SET_NICK_NAME_SUCCESS,
  payload: {
    data: data
  }
});

const setNickNameFailure = error => ({
  type: SET_NICK_NAME_FAILURE,
  payload: {
    error
  }
});

const getNickNameRequest = () => ({
  type: GET_NICK_NAME_REQUEST,
});

const getNickNameSuccess = data => ({
  type: GET_NICK_NAME_SUCCESS,
  payload: {
    data: data
  }
});

const getNickNameFailure = error => ({
  type: GET_NICK_NAME_FAILURE,
  payload: {
    error
  }
});

export const setNickName = (token, data) => {
  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(setNickNameRequest());
    return axios({
      method: 'post',
      url: END_POINT_SET_NICK_NAME + data,
      headers: headers
    }).then(res => {
      dispatch(setNickNameSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        dispatch(setNickNameFailure(err.message));
      });
  };
};

export const getNickName = (token, name) => {
  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(getNickNameRequest());
    return axios({
      method: 'post',
      url: END_POINT_GET_NICK_NAME + name,
      headers: headers
    }).then(res => {
      dispatch(getNickNameSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        dispatch(getNickNameFailure(err.message));
      });
  };
};

export const getMyProfileData = (token) => {

  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(myProfileRequest());
    return axios({
      method: 'post',
      url: END_POINT_GET_MY_PROFILE,
      headers: headers
    }).then(res => {
      console.log(" my profile --", res.data);
      dispatch(myProfileSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        dispatch(myProfileFailure(err.message));
      });
  };
};

const myProfileUpdateRequest = () => ({
  type: MY_PROFILE_UPDATE_REQUEST
});

const myProfileUpdateSuccess = data => ({
  type: MY_PROFILE_UPDATE_SUCCESS,
  payload: {
    data: data
  }
});

const myProfileUpdateFailure = error => ({
  type: MY_PROFILE_UPDATE_FAILURE,
  payload: {
    error
  }
});

export const myProfileUpdateData = (token, param) => {

  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(myProfileUpdateRequest());

    return axios({
      method: 'post',
      url: END_POINT_UPDATE_MY_PROFILE,
      data:param,
      headers: headers
    }).then(res => {
      console.log(" my profile --", res.data);
      dispatch(myProfileUpdateSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        dispatch(myProfileUpdateFailure(err.message));
      });
  };
};

// Work experience
const addWorkExperienceProfileRequest = () => ({
  type: ADD_WORK_EXPERIENCE_PROFILE_REQUEST
});

const addWorkExperienceProfileSuccess = data => ({
  type: ADD_WORK_EXPERIENCE_PROFILE_SUCCESS,
  payload: {
    data: data
  }
});

const addWorkExperienceProfileFailure = error => ({
  type: ADD_WORK_EXPERIENCE_PROFILE_FAILURE,
  payload: {
    error
  }
});

export const addWorkExperienceProfileData = (token, param) => {

  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(addWorkExperienceProfileRequest());

    return axios({
      method: 'post',
      url: END_POINT_ADD_WORK_EXPERIENCE_PROFILE,
      data:param,
      headers: headers
    }).then(res => {
      console.log(" my profile --", res.data);
      dispatch(addWorkExperienceProfileSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        console.log(" channdni e --", err.response);
        dispatch(addWorkExperienceProfileFailure(err.message));
      });
  };
};

const updateWorkExperienceProfileRequest = () => ({
  type: UPDATE_WORK_EXPERIENCE_PROFILE_REQUEST
});

const updateWorkExperienceProfileSuccess = data => ({
  type: UPDATE_WORK_EXPERIENCE_PROFILE_SUCCESS,
  payload: {
    data: data
  }
});

const updateWorkExperienceProfileFailure = error => ({
  type: UPDATE_WORK_EXPERIENCE_PROFILE_FAILURE,
  payload: {
    error
  }
});

export const updateWorkExperienceProfileData = (token, param) => {

  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };
 
    dispatch(updateWorkExperienceProfileRequest());

    return axios({
      method: 'post',
      url: END_POINT_UPDATE_WORK_PROFILE,
      data:param,
      headers: headers
    }).then(res => {
      console.log(" my profile --", res.data);
      dispatch(updateWorkExperienceProfileSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        console.log(" error is 000", err.res); 
        dispatch(updateWorkExperienceProfileFailure(err.message));
      });
  };
};

// Add Educational Detail 

const addEducationProfileRequest = () => ({
  type: ADD_EDUCATION_PROFILE_REQUEST
});

const addEducationProfileSuccess = data => ({
  type: ADD_EDUCATION_PROFILE_SUCCESS,
  payload: {
    data: data
  }
});

const addEducationProfileFailure = error => ({
  type: ADD_EDUCATION_PROFILE_FAILURE,
  payload: {
    error
  }
});

export const addEducationProfileData = (token, param) => {

  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(addEducationProfileRequest());

    return axios({
      method: 'post',
      url: END_POINT_ADD_EDUCATION_PROFILE,
      data:param,
      headers: headers
    }).then(res => {
      console.log(" my profile --", res.data);
      dispatch(addEducationProfileSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        dispatch(addEducationProfileFailure(err.message));
      });
  };
};

// Update education detail 

const updateEducationProfileRequest = () => ({
  type: UPDATE_WORK_EXPERIENCE_PROFILE_REQUEST
});

const updateEducationProfileSuccess = data => ({
  type: UPDATE_WORK_EXPERIENCE_PROFILE_SUCCESS,
  payload: {
    data: data
  }
});

const updateEducationProfileFailure = error => ({
  type: UPDATE_WORK_EXPERIENCE_PROFILE_FAILURE,
  payload: {
    error
  }
});

export const updateEducationProfileData = (token, param) => {

  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(updateEducationProfileRequest());

    return axios({
      method: 'post',
      url: END_POINT_UPDATE_EDUCATION_PROFILE,
      data:param,
      headers: headers
    }).then(res => {
      console.log(" my profile --", res.data);
      dispatch(updateEducationProfileSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        dispatch(updateEducationProfileFailure(err.message));
      });
  };
};