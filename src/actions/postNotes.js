import axios from 'axios';
import { END_POINT_SAVE_NOTE_AFTER_PREVIEW,END_POINT_POST_NOTES, END_POINT_GET_DATA_TO_POST_NOTES, END_POINT_GET_DATA_FOR_EDIT_VET_NOTE, END_POINT_SAVE_EDITTED_VET_NOTE } from "../api/endPoints";

import {
  POST_NOTES_REQUEST,
  POST_NOTES_SUCCESS,
  POST_NOTES_FAILURE, 
  
  GET_DATA_TO_POST_NOTES_REQUEST,
  GET_DATA_TO_POST_NOTES_FAILURE,
  GET_DATA_TO_POST_NOTES_SUCCESS,

  POST_DRAFT_AS_A_NOTES_REQUEST,
  POST_DRAFT_AS_A_NOTES_SUCCESS,
  POST_DRAFT_AS_A_NOTES_FAILURE,

  GET_DATA_TO_EDIT_POST_NOTES_REQUEST,
  GET_DATA_TO_EDIT_POST_NOTES_SUCCESS,
  GET_DATA_TO_EDIT_POST_NOTES_FAILURE,

  SAVE_VET_NOTE_EDIT_REQUEST,
  SAVE_VET_NOTE_EDIT_FAILURE,
  SAVE_VET_NOTE_EDIT_SUCCESS,

  SAVE_NOTE_AFTER_PREIVEW_REQUEST,
  SAVE_NOTE_AFTER_PREIVEW_FAILURE,
  SAVE_NOTE_AFTER_PREIVEW_SUCCESS,
  

} from "../constants";

// Get complete data to post question 
const getDataToPostNotesRequest = () => ({
  type: GET_DATA_TO_POST_NOTES_REQUEST
});

const getDataToPostNotesSuccess = data => ({
  type: GET_DATA_TO_POST_NOTES_SUCCESS,
  payload: {
    data: data
  }
});
const getDataToPostNotesFailure = error => ({
  type: GET_DATA_TO_POST_NOTES_FAILURE,
  payload: {
    error
  }
});

export const getDataToPostNotes = (token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };
    
    dispatch(getDataToPostNotesRequest());
    return axios
    .get(END_POINT_GET_DATA_TO_POST_NOTES, {headers:headers})
    .then(res => {
      console.log(" Vet note  data ---> ", res);
      dispatch(getDataToPostNotesSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(getDataToPostNotesFailure(err.message));
    });
  };
};

// Post Notess 
const postNotesRequest = () => ({
  type: POST_NOTES_REQUEST
});

const postNotesSuccess = data => ({
  type: POST_NOTES_SUCCESS,
  payload: {
    data: data
  }
});
const postNotesFailure = error => ({
  type: POST_NOTES_FAILURE,
  payload: {
    error
  }
});

export const postNotesApiCall = (data, auth_token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': auth_token
    };

    dispatch(postNotesRequest());

    return axios({
      method: 'post',
      url: END_POINT_POST_NOTES,
      data: data,
      headers: headers, 
    })
      .then(res => {
        console.log("postNotesApiCall res.data ---", res.data);
        dispatch(postNotesSuccess(res.data));
        return res.data;
      })
      .catch(err => {
        
        dispatch(postNotesFailure(err.message));
      });
  };
};

// SAVE NOTES AFTER PREVIEW

const saveNoteAfterPreviewRequest = () => ({
  type: SAVE_NOTE_AFTER_PREIVEW_REQUEST
});

const saveNoteAfterPreviewSuccess = data => ({
  type: SAVE_NOTE_AFTER_PREIVEW_SUCCESS,
  payload: {
    data: data
  }
});
const saveNoteAfterPreviewFailure = error => ({
  type: SAVE_NOTE_AFTER_PREIVEW_FAILURE,
  payload: {
    error
  }
});

export const saveNoteAfterPreviewApiCall = (data, auth_token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': auth_token
    };

    dispatch(saveNoteAfterPreviewRequest());

    return axios({
      method: 'post',
      url: END_POINT_SAVE_NOTE_AFTER_PREVIEW,
      data: data,
      headers: headers, 
    })
      .then(res => {
        console.log(" res.data ---", res.data);
        dispatch(saveNoteAfterPreviewSuccess(res.data));
        return res.data;
      })
      .catch(err => {
        
        dispatch(saveNoteAfterPreviewFailure(err.message));
      });
  };
};


// EDIT SCENARIOS

// Get complete data to post question 
const getDataToEditVetNotesRequest = () => ({
  type: GET_DATA_TO_POST_NOTES_REQUEST
});

const getDataToEditVetNotesSuccess = data => ({
  type: GET_DATA_TO_POST_NOTES_SUCCESS,
  payload: {
    data: data
  }
});
const getDataToEditVetNotesFailure = error => ({
  type: GET_DATA_TO_POST_NOTES_FAILURE,
  payload: {
    error
  }
});

export const getDataToEditVetNotes = (param,token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': param.auth_token
    };
    
    dispatch(getDataToEditVetNotesRequest());

   
    let url = END_POINT_GET_DATA_FOR_EDIT_VET_NOTE+param.slug;
      return axios
        .get(url,
         {headers: headers})
    
        .then(res => {
          console.log(" Edit vet note res.data ---- ",res.data); 
          dispatch(getDataToEditVetNotesSuccess(res.data));
          return res.data;
        })
        .catch(err => {
          
          dispatch(getDataToEditVetNotesFailure(err.message));
        });
  };
};

const saveEditedVetNoteRequest = () => ({
  type: SAVE_VET_NOTE_EDIT_REQUEST
});

const saveEditedVetNoteSuccess = data => ({
  type: SAVE_VET_NOTE_EDIT_SUCCESS,
  payload: {
    data: data
  }
});
const saveEditedVetNoteFailure = error => ({
  type: SAVE_VET_NOTE_EDIT_FAILURE,
  payload: {
    error
  }
});

export const saveEditedVetNoteApiCall = (data, auth_token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': auth_token
    };

    dispatch(saveEditedVetNoteRequest());

    return axios({
      method: 'post',
      url: END_POINT_SAVE_EDITTED_VET_NOTE,
      data: data,
      headers: headers, 
    })
      .then(res => {
        console.log("saveEditedVetNoteApiCall res.data ---", res.data);
        dispatch(saveEditedVetNoteSuccess(res.data));
        return res.data;
      })
      .catch(err => {
       
        dispatch(saveEditedVetNoteFailure(err.message));
      });
  };
};