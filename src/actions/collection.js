import axios from 'axios';
import { END_POINT_GET_COLLECTION_DATA } from "../api/endPoints";

import {
  GET_COLLECTION_DATA_REQUEST,
  GET_COLLECTION_DATA_SUCCESS,
  GET_COLLECTION_DATA_FAILURE,

} from "../constants";

const getCollectionDataRequest = () => ({
  type: GET_COLLECTION_DATA_REQUEST
});

const getCollectionDataSuccess = data => ({
  type: GET_COLLECTION_DATA_SUCCESS,
  payload: {
    data: data
  }
});

const getCollectionDataFailure = error => ({
  type: GET_COLLECTION_DATA_FAILURE,
  payload: {
    error
  }
});

export const getCollectionData = (token) => {

  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    dispatch(getCollectionDataRequest());

    return axios({
      method: 'post',
      url: END_POINT_GET_COLLECTION_DATA,
      headers: headers
    }).then(res => {
      console.log(" res 000", res);
      dispatch(getCollectionDataSuccess(res.data));
      return res.data;
    })
      .catch(err => {
        console.log(" error 000", err.response);
        dispatch(getCollectionDataFailure(err.message));
      });
  };
};

