import axios from 'axios';
import {END_POINT_GET_QUESTION_LIST} from '../api/endPoints'

import {
  QUESTION_REQUEST,
  QUESTION_SUCCESS,
  QUESTION_FAILURE
} from "../constants";

const questionRequest = () => ({
  type: QUESTION_REQUEST
});

const questionSuccess = data => ({
  type: QUESTION_SUCCESS,
  payload: {
    data: data
  }
});

const questionFailure = error => ({
  type: QUESTION_FAILURE,
  payload: {
    error
  }
});

export const getQuestionList = (param, token) => {

 

  return dispatch => {

   const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

    
   dispatch(questionRequest());

    return axios
      .get(END_POINT_GET_QUESTION_LIST,{headers:headers})
      .then(res => {
    
        dispatch(questionSuccess(res.data));
        return res.data;
      })
      .catch(err => {
        dispatch(questionFailure(err.message));
        
      });
  };
};