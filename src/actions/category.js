import axios from 'axios';
import { END_POINT_GET_CATEGORY_SUBCATEGORY, END_POINT_SAVE_SELECTED_CATEGORY } from "../api/endPoints";

import {
  CATEGORY_REQUEST,
  CATEGORY_SUCCESS,
  CATEGORY_FAILURE, 
  ADD_CATEGORY_REQUEST,
  ADD_CATEGORY_SUCCESS,
  ADD_CATEGORY_FAILURE

} from "../constants";

const categoryRequest = () => ({
  type: CATEGORY_REQUEST
});

const categorySuccess = data => ({
  type: CATEGORY_SUCCESS,
  payload: {
    data: data
  }
});

const categoryFailure = error => ({
  type: CATEGORY_FAILURE,
  payload: {
    error
  }
});

const addCategoryRequest = () => ({
  type: ADD_CATEGORY_REQUEST
});

const addCategorySuccess = data => ({
  type: ADD_CATEGORY_SUCCESS,
  payload: {
    data: data
  }
});

const addCategoryFailure = error => ({
  type: ADD_CATEGORY_FAILURE,
  payload: {
    error
  }
});

export const getCategoryData = (token) => {

  return dispatch => {

   const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };

   dispatch(categoryRequest());

    return axios
      .get(END_POINT_GET_CATEGORY_SUBCATEGORY, {headers:headers})
      .then(res => {
        
        dispatch(categorySuccess(res.data));
        return res.data;
      })
      .catch(err => {
        dispatch(categoryFailure(err.message));
      });
  };
};

export const saveCategoryData = (token, data) =>{

  return dispatch => {

    const headers = {
       "Content-Type": "application/json",
       'X-Access-Token': token
     };
 
    dispatch(addCategoryRequest());
 
     return axios
       .post(END_POINT_SAVE_SELECTED_CATEGORY, data ,{headers:headers})
       .then(res => {
         
         dispatch(addCategorySuccess(res.data));
         return res.data;
       })
       .catch(err => {
         dispatch(addCategoryFailure(err.message));
       });
   };
};