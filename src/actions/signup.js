import axios from 'axios';
import { END_POINT_SIGNUP } from "../api/endPoints";

import {
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAILURE
} from "../constants";

const signupRequest = () => ({
  type: SIGNUP_REQUEST
});

const signupSuccess = data => ({
  type: SIGNUP_SUCCESS,
  payload: {
    data: data
  }
});

const signupFailure = error => ({
  type: SIGNUP_FAILURE,
  payload: {
    error
  }
});

export const signup = (data) => {
  return dispatch => {
    dispatch(signupRequest());
   
    return axios({
      method: 'post',
      url: END_POINT_SIGNUP,
      data: data,
    })
      .then(res => {
       
        dispatch(signupSuccess(res.data));
        return res.data;
      })
      .catch(err => {
      
        dispatch(signupFailure(err.message));
      });
  };
};