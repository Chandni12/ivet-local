import axios from 'axios';
import {END_POINT_GET_DASHBOARD_SEARCH_DATA ,END_POINT_GET_DASHBOARD_DATA, END_POINT_GET_DASHBOARD_Q_AND_A, END_POINT_POST_GET_CLINICAL_NOTES, END_POINT_POST_GET_TIME_LINE, END_POINT_POST_GET_TAG_FEED, END_POINT_POST_GET_SELECTED_TAG_DATA, END_POINT_POST_FOLLOW_UNFOLLOW } from "../api/endPoints";

import {
  DASHBOARD_REQUEST,
  DASHBOARD_SUCCESS,
  DASHBOARD_FAILURE, 

  DASHBOARD_Q_AND_A_REQUEST,
  DASHBOARD_Q_AND_A_SUCCESS,
  DASHBOARD_Q_AND_A_FAILURE, 

  DASHBOARD_TIMELINE_FAILURE,
  DASHBOARD_TIMELINE_SUCCESS,
  DASHBOARD_TIMELINE_REQUEST,

  DASHBOARD_CLINICAL_NOTES_REQUEST,
  DASHBOARD_CLINICAL_NOTES_SUCCESS,
  DASHBOARD_CLINICAL_NOTES_FAILURE,

  DASHBOARD_TAG_FEED_REQUEST,
  DASHBOARD_TAG_FEED_SUCCESS,
  DASHBOARD_TAG_FEED_FAILURE,

  DASHBOARD_SELECTED_TAG_REQUEST,
  DASHBOARD_SELECTED_TAG_SUCCESS,
  DASHBOARD_SELECTED_TAG_FAILURE,

  DASHBOARD_FOLLOW_UNFOLLOW_REQUEST,
  DASHBOARD_FOLLOW_UNFOLLOW_SUCCESS,
  DASHBOARD_FOLLOW_UNFOLLOW_FAILURE,

  SEARCH_DASHBOARD_REQUEST,
  SEARCH_DASHBOARD_SUCCESS,
  SEARCH_DASHBOARD_FAILURE,

} from "../constants";

//// ****** Complete Dashboard Data *******///

const dashboardRequest = () => ({
  type: DASHBOARD_REQUEST,
});

const dashboardSuccess = data => ({
  type: DASHBOARD_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardFailure = error => ({
  type: DASHBOARD_FAILURE,
  payload: {
    error
  }
});

export const getDashboardData = (data, token) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };
   //let url = END_POINT_GET_DASHBOARD_DATA+data.paginate
  let url = END_POINT_GET_DASHBOARD_DATA+1
    dispatch(dashboardRequest());
    
    return axios({
      method: 'post',
      url: url,
      data:data,
      headers: headers
    }) 
    .then(res => {
        dispatch(dashboardSuccess(res));
        return res;
      })
      .catch(err => {
        dispatch(dashboardFailure(err.message));
      });
  };
};

///*** Q and A  *////
const dashboardQandArequest = () => ({
  type: DASHBOARD_Q_AND_A_REQUEST,
});

const dashboardQandAsuccess = data => ({
  type: DASHBOARD_Q_AND_A_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardQandAfailure = error => ({
  type: DASHBOARD_Q_AND_A_FAILURE,
  payload: {
    error
  }
});

export const getDashboardQandA = (data,parameters) => {


  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters
    };
    
    dispatch(dashboardQandArequest());

    return axios({
      method: 'post',
      url: END_POINT_GET_DASHBOARD_Q_AND_A,
      data:data,
      headers: headers
  }) 
  .then(response => {
   
    dispatch(dashboardQandAsuccess(response));
    return response;
  })
  .catch(error => {
   
    dispatch(dashboardQandAfailure(error.response));
    return error.response;
  });
  };
};

///*** Clinical Notes ****///
const dashboardClinicalNotesRequest = () => ({
  type: DASHBOARD_CLINICAL_NOTES_REQUEST,
});


const dashboardClinicalNotesSuccess = data => ({
  type: DASHBOARD_CLINICAL_NOTES_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardClinicalNotesfailure = error => ({
  type: DASHBOARD_CLINICAL_NOTES_FAILURE,
  payload: {
    error
  }
});

export const getDashboardClinicalNotes = (data,parameters) => {
  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token':parameters
    };
    
    dispatch(dashboardClinicalNotesRequest());

    return axios({
      method: 'post',
      url: END_POINT_POST_GET_CLINICAL_NOTES,
      data:data,
      headers: headers
  }) 
  .then(response => {
 
    dispatch(dashboardClinicalNotesSuccess(response));
    return response;
  })
  .catch(error => {

    dispatch(dashboardClinicalNotesfailure(error.response));
    return error.response;
  });
  };
};

///*** Tag Feed  ****//////

const dashboardTagFeedRequest = () => ({
  type: DASHBOARD_TAG_FEED_REQUEST,
});

const dashboardTagFeedSuccess = data => ({
  type: DASHBOARD_TAG_FEED_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardTagFeedFailure = error => ({
  type: DASHBOARD_TAG_FEED_FAILURE,
  payload: {
    error
  }
});

export const getDashboardTagFeed = (data,parameters) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters
    };
    //let url = END_POINT_GET_DASHBOARD_VET_NOTE_DETAIL+parameters.slug
    

    dispatch(dashboardTagFeedRequest());

    return axios({
      method: 'post',
      url: END_POINT_POST_GET_TAG_FEED,
      data:data,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
  .then(response => {
   
    dispatch(dashboardTagFeedSuccess(response));
    return response;
  })
  .catch(error => {
    
    dispatch(dashboardTagFeedFailure(error.response));
    return error.response;
  });
  };
};

///*** Time Line  ****//////

const dashboardTimeLineRequest = () => ({
  type: DASHBOARD_TIMELINE_REQUEST,
});

const dashboardTimeLineSuccess = data => ({
  type: DASHBOARD_TIMELINE_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardTimeLineFailure = error => ({
  type: DASHBOARD_TIMELINE_FAILURE,
  payload: {
    error
  }
});

export const getDashboardTimeLineData = (data,parameters) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters
    };
    //let url = END_POINT_GET_DASHBOARD_VET_NOTE_DETAIL+parameters.slug


    dispatch(dashboardTimeLineRequest());

    return axios({
      method: 'post',
      url: END_POINT_POST_GET_TIME_LINE,
      data:data,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
  .then(response => {
    
    dispatch(dashboardTimeLineSuccess(response));
    return response;
  })
  .catch(error => {
    
    dispatch(dashboardTimeLineFailure(error.response));
    return error.response;
  });
  };
};

///*** Selected Tag Data   *////
const dashboardSelectedTagRequest = () => ({
  type: DASHBOARD_SELECTED_TAG_REQUEST,
});

const dashboardSelectedTagSuccess = data => ({
  type: DASHBOARD_SELECTED_TAG_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardSelectedTagFailure = error => ({
  type: DASHBOARD_SELECTED_TAG_FAILURE,
  payload: {
    error
  }
});

export const getDashboardSelectedTag = (data,parameters) => {
  

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters
    };

    dispatch(dashboardSelectedTagRequest());
    return axios({
      method: 'post',
      url: END_POINT_POST_GET_SELECTED_TAG_DATA,
      data:data,
      headers: headers
  }) 
  .then(response => {
  
    dispatch(dashboardSelectedTagSuccess(response));
    return response;
  })
  .catch(error => {
    
    dispatch(dashboardSelectedTagFailure(error.response));
    return error.response;
  });
  };
};

//*************** Follow / unfollow apis *************//

const dashboardFollowUnfollowRequest = () => ({
  type: DASHBOARD_FOLLOW_UNFOLLOW_REQUEST,
});

const dashboardFollowUnfollowSuccess = data => ({
  type: DASHBOARD_FOLLOW_UNFOLLOW_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardFollowUnfollowFailure = error => ({
  type: DASHBOARD_FOLLOW_UNFOLLOW_FAILURE,
  payload: {
    error
  }
});

export const callApiToFollowUnfollowPost  = (data,parameters) => {


  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters
    };
let apiUrl = END_POINT_POST_FOLLOW_UNFOLLOW+'/'+data
    dispatch(dashboardFollowUnfollowRequest());
    return axios({
      method: 'post',
      url: apiUrl,
      headers: headers
  }) 
  .then(response => {
   // console.log(" response ------", response.data);
    dispatch(dashboardFollowUnfollowSuccess(response.data));
    return response;
  })
  .catch(error => {
    
    dispatch(dashboardFollowUnfollowFailure(error.response));
    return error.response;
  });
  };
};


//***********CALL SEARCH API  */

const dashboardSearchDataRequest = () => ({
  type: SEARCH_DASHBOARD_REQUEST,
});

const dashboardSearchDataSuccess = data => ({
  type: SEARCH_DASHBOARD_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardSearchDataFailure = error => ({
  type: SEARCH_DASHBOARD_FAILURE,
  payload: {
    error
  }
});

export const getDashboardSearchData  = (data, token, isSearchButtonClick) => {

  let parameter = {'search_text':data};

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': token
    };
    let url
    dispatch(dashboardSearchDataRequest());
    if (isSearchButtonClick) {
      url = END_POINT_GET_DASHBOARD_SEARCH_DATA;
    } else {
      url = END_POINT_GET_DASHBOARD_SEARCH_DATA+'/'+1;
    }
   
    return axios({
      method: 'post',
      url: END_POINT_GET_DASHBOARD_SEARCH_DATA,
      data:parameter,
      headers: headers
  }) 
  .then(response => {
    dispatch(dashboardSearchDataSuccess(response.data));
    return response;
  })
  .catch(error => {
    
    dispatch(dashboardSearchDataFailure(error.response));
    return error.response;
  });
  };
};
