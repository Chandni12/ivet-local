import axios from 'axios';
import { END_POINT_GET_NOTIFICATION_DATA, END_POINT_GET_NOTIFICATION_DELETE } from "../api/endPoints";

import {
  NOTIFICATION_REQUEST,
  NOTIFICATION_SUCCESS,
  NOTIFICATION_FAILURE,
  NOTIFICATION_DELETE_REQUEST,
  NOTIFICATION_DELETE_SUCCESS,
  NOTIFICATION_DELETE_FAILURE,

} from "../constants";

const notificationRequest = () => ({
  type: NOTIFICATION_REQUEST
});

const notificationSuccess = data => ({
  type: NOTIFICATION_SUCCESS,
  payload: {
    data: data
  }
});

const notificationFailure = error => ({
  type: NOTIFICATION_FAILURE,
  payload: {
    error
  }
});

const deleteNotifRequest = () => ({
  type: NOTIFICATION_DELETE_REQUEST
});

const deleteNotifSuccess = data => ({
  type: NOTIFICATION_DELETE_SUCCESS,
  payload: {
    data: data
  }
});

const deleteNotifFailure = error => ({
  type: NOTIFICATION_DELETE_FAILURE,
  payload: {
    error
  }
});

  export const getNotificationData = (token) => {
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': token
      };

      dispatch(notificationRequest());
      
      return axios({
        method: 'get',
        url: END_POINT_GET_NOTIFICATION_DATA,
        headers: headers
    }).then(res => {
      dispatch(notificationSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(notificationFailure(err.message));
      });
    };
  };    

  export const deleteNotification = (data) => {
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': data.auth_token
      };
      dispatch(deleteNotifRequest());
    let apiUrl = END_POINT_GET_NOTIFICATION_DELETE+data.id
      return axios({
        method: 'post',
        url: apiUrl,
        headers: headers
    }).then(res => {
      console.log('response notification',res)
      dispatch(deleteNotifSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(deleteNotifFailure(err.message));
      });
    };
  }; 
