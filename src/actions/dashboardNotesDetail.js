import axios from 'axios';
import { END_POINT_ADD_COMMENTS_VET_NOTE, END_POINT_GET_NOTE_HISTORY, END_POINT_REPORT_NOTE_INAPPROPRIATE, END_POINT_ADD_NOTE_TO_COLLECTION,   END_POINT_VOTE_UP_DOWN_NOTES, END_POINT_LIKE_NOTE_COMMENT } from "../api/endPoints";
import {

  MAKE_NOTES_INAPPROPRIATE_REQUEST,
  MAKE_NOTES_INAPPROPRIATE_SUCCESS,
  MAKE_NOTES_INAPPROPRIATE_FAILURE, 

  GET_NOTE_HISTORY_FAILURE,
  GET_NOTE_HISTORY_REQUEST,
  GET_NOTE_HISTORY_SUCCESS,

  VOTE_UP_DOWN_NOTES_REQUEST, 
  VOTE_UP_DOWN_NOTES_SUCCESS,
  VOTE_UP_DOWN_NOTES_FAILURE,

  LIKE_NOTES_COMMENT_REQUEST, 
  LIKE_NOTES_COMMENT_SUCCESS,
  LIKE_NOTES_COMMENT_FAILURE,

  ADD_NOTE_TO_COLLECTION_REQUEST, 
  ADD_NOTE_TO_COLLECTION_SUCCESS,
  ADD_NOTE_TO_COLLECTION_FAILURE,

  ADD_COMMENTS_REQUEST,
  ADD_COMMENTS_SUCCESS,
  ADD_COMMENTS_FAILURE,
  
} from "../constants";

const getNotesHistoryRequest = () => ({
    type: GET_NOTE_HISTORY_REQUEST,
  });
  
  const getNotesHistorySuccess = data => ({
    type: GET_NOTE_HISTORY_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const getNotesHistoryFailure = error => ({
    type: GET_NOTE_HISTORY_FAILURE,
    payload: {
      error
    }
  });

  export const getNotesHistory = (parameters, postData) => {

    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
   //  let data = {'id':parameters.id };
     let urlApi = END_POINT_GET_NOTE_HISTORY+parameters.id
  
      dispatch(getNotesHistoryRequest());
  
       return axios({
        method: 'post',
        url: urlApi,
        headers: headers,
        // validateStatus: status => status >= 200 && status < 500, // default
    }) 
        .then(response => {
          dispatch(getNotesHistorySuccess(response));
          return response;
        })
        .catch(err => {
          dispatch(getNotesHistoryFailure(err.response));
        });
    };
  };

   
  // VOTE UP/ DOWN Notes

const voteUpDownNotesRequest = () => ({
    type: VOTE_UP_DOWN_NOTES_REQUEST,
  });
  
  const voteUpDownNotesSuccess = data => ({
    type: VOTE_UP_DOWN_NOTES_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const voteUpDownNotesFailure = error => ({
    type: VOTE_UP_DOWN_NOTES_FAILURE,
    payload: {
      error
    }
  });
  
  export const voteUpDownNotesInDetail = (parameters, postData) => {
  
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
     let urlApi = END_POINT_VOTE_UP_DOWN_NOTES+parameters.id+'/'+postData;
  
      dispatch(voteUpDownNotesRequest());
  
       return axios({
        method: 'post',
        url: urlApi,
        headers: headers,
    }) 
        .then(response => {
          dispatch(voteUpDownNotesSuccess(response));
          return response;
        })
        .catch(err => {
          dispatch(voteUpDownNotesFailure(err.response));
        });
    };
  };

  // Inappropriate Notes 

  // Make Question inappropriate 

const makeNotesInappropriateRequest = () => ({
    type: MAKE_NOTES_INAPPROPRIATE_REQUEST,
  });
  
  const makeNotesInappropriateSuccess = data => ({
    type: MAKE_NOTES_INAPPROPRIATE_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const makeNotesInappropriateFailure = error => ({
    type: MAKE_NOTES_INAPPROPRIATE_FAILURE,
    payload: {
      error
    }
  });
  
  export const makeNotesInappropriateInDetail = (parameters, postData) => {
  
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
   //  let data = {'id':parameters.id };
     let urlApi = END_POINT_REPORT_NOTE_INAPPROPRIATE+parameters.id
  
      dispatch(makeNotesInappropriateRequest());
  
       return axios({
        method: 'post',
        url: urlApi,
        data:postData,
        headers: headers,
        // validateStatus: status => status >= 200 && status < 500, // default
    }) 
        .then(response => {
          dispatch(makeNotesInappropriateSuccess(response));
          return response;
        })
        .catch(err => {
          dispatch(makeNotesInappropriateFailure(err.response));
        });
    };
  };

  const likeNotesCommentRequest = () => ({
    type: LIKE_NOTES_COMMENT_REQUEST,
  });
  
  const likeNotesCommentSuccess = data => ({
    type: LIKE_NOTES_COMMENT_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const likeNotesCommentFailure = error => ({
    type: LIKE_NOTES_COMMENT_FAILURE,
    payload: {
      error
    }
  });
  
  export const likeNotesComment = (parameters) => {
  
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
     let urlApi = END_POINT_LIKE_NOTE_COMMENT+parameters.id
  
      dispatch(likeNotesCommentRequest());
  
       return axios({
        method: 'post',
        url: urlApi,
        headers: headers,
    }) 
        .then(response => {
          dispatch(likeNotesCommentSuccess(response));
          return response;
        })
        .catch(err => {
           
          dispatch(likeNotesCommentFailure(err.response));
        });
    };
  };
  
  const addNoteToCollectionRequest = () => ({
    type: ADD_NOTE_TO_COLLECTION_REQUEST,
  });
  
  const addNoteToCollectionSuccess = data => ({
    type: ADD_NOTE_TO_COLLECTION_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const addNoteToCollectionFailure = error => ({
    type: ADD_NOTE_TO_COLLECTION_FAILURE,
    payload: {
      error
    }
  });
  
  export const addNoteToCollection = (parameters) => {
  
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
     let urlApi = END_POINT_ADD_NOTE_TO_COLLECTION+parameters.id
  
      dispatch(addNoteToCollectionRequest());
  
       return axios({
        method: 'post',
        url: urlApi,
        headers: headers,
    }) 
        .then(response => {
          dispatch(addNoteToCollectionSuccess(response));
          return response;
        })
        .catch(err => {
           
          dispatch(addNoteToCollectionFailure(err.response));
        });
    };
  };

  const addCommentsRequest = () => ({
    type: ADD_COMMENTS_REQUEST,
  });
  
  const addCommentsSuccess = data => ({
    type: ADD_COMMENTS_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const addCommentsFailure = error => ({
    type: ADD_COMMENTS_FAILURE,
    payload: {
      error
    }
  });
  
  export const addCommentsInPost = (parameters) => {
  
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
     let data = {'comment':parameters.comment, 'post_id':parameters.post_id, 'user_id':parameters.user_id };
  
      dispatch(addCommentsRequest());
  
       return axios({
        method: 'post',
        url: END_POINT_ADD_COMMENTS_VET_NOTE,
        data:data,
        headers: headers,
        // validateStatus: status => status >= 200 && status < 500, // default
    }) 
        .then(response => {
         
          dispatch(addCommentsSuccess(response));
          return response;
        })
        .catch(err => {
          dispatch(addCommentsFailure(err.response));
        });
    };
  };
  