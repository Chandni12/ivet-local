import axios from 'axios';
import {   END_POINT_EDIT_ANSWER, END_POINT_GET_ANSWER } from "../api/endPoints";
import {

  GET_ANSWER_REQUEST, 
  GET_ANSWER_SUCCESS,
  GET_ANSWER_FAILURE,

  EDIT_ANSWER_REQUEST, 
  EDIT_ANSWER_SUCCESS,
  EDIT_ANSWER_FAILURE,

} from "../constants";

const getAnswerRequest = () => ({
    type: GET_ANSWER_REQUEST,
  });
  
  const getAnswerSuccess = data => ({
    type: GET_ANSWER_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const getAnswerFailure = error => ({
    type: GET_ANSWER_FAILURE,
    payload: {
      error
    }
  });
  
  export const getAnswerForEditing = (parameters) => {
    
    return dispatch => {
  
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
      let url = END_POINT_GET_ANSWER+parameters.id
     
       
      dispatch(getAnswerRequest());
  
      return axios({
        method: 'post',
        url: url,
        headers: headers,
        // validateStatus: status => status >= 200 && status < 500, // default
    }) 
    .then(response => {
      dispatch(getAnswerSuccess(response));
      return response;
    })
    .catch(error => {
      
      dispatch(getAnswerFailure(error.response));
      return error.response;
    });
    };
  };

  const editAnswerRequest = () => ({
    type: EDIT_ANSWER_REQUEST,
  });
  
  const editAnswerSuccess = data => ({
    type: EDIT_ANSWER_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const editAnswerFailure = error => ({
    type: EDIT_ANSWER_FAILURE,
    payload: {
      error
    }
  });
  
  export const saveEditedAnswer = (parameters, postData) => {
    return dispatch => {
  
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
      let url = END_POINT_EDIT_ANSWER+parameters.id
     
     

      dispatch(editAnswerRequest());
  
      return axios({
        method: 'post',
        url: url,
        headers: headers,
        data: postData
        // validateStatus: status => status >= 200 && status < 500, // default
    }) 
    .then(response => {

      dispatch(editAnswerSuccess(response));
      return response;
    })
    .catch(error => {
      console.log(" error--", error); 
      dispatch(editAnswerFailure(error.response));
      return error.response;
    });
    };
  };