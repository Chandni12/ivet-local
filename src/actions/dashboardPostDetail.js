import axios from 'axios';
import { END_POINT_GET_TAGS_BASIS_OF_CATEGORY ,END_POINT_GET_SPECIALITY_BASIS_OF_CATEGORY ,END_POINT_ADD_COMMENTS, END_POINT_GET_DASHBOARD_QUESTION_DETAIL, END_POINT_GET_DASHBOARD_VET_NOTE_DETAIL, END_POINT_POST_ANSWER, END_POINT_REPORT_AS_INAPROPRIATE_QUESTION, END_POINT_REPORT_AS_INAPROPRIATE_ANSWER,END_POINT_MAKE_QUESTION_FAVORITE, END_POINT_MAKE_NOTES_FAVORITE, END_POINT_MARK_CORRECT_ANSWER, END_POINT_VOTE_UP_DOWN_QUESTION, END_POINT_VOTE_UP_DOWN_ANSWER } from "../api/endPoints";
import {

  ADD_COMMENTS_REQUEST,
  ADD_COMMENTS_SUCCESS,
  ADD_COMMENTS_FAILURE, 

  DASHBOARD_QUESTION_DETAIL_REQUEST, 
  DASHBOARD_QUESTION_DETAIL_SUCCESS,
  DASHBOARD_QUESTION_DETAIL_FAILURE,

  DASHBOARD_VET_NOTE_DETAIL_FAILURE,
  DASHBOARD_VET_NOTE_DETAIL_REQUEST,
  DASHBOARD_VET_NOTE_DETAIL_SUCCESS,

  DASHBOARD_QUESTION_EDIT_REQUEST, 
  DASHBOARD_QUESTION_EDIT_SUCCESS,
  DASHBOARD_QUESTION_EDIT_FAILURE,

  POST_ANSWER_REQUEST, 
  POST_ANSWER_SUCCESS,
  POST_ANSWER_FAILURE,

  MAKE_ANSWER_INAPPROPRIATE_REQUEST, 
  MAKE_ANSWER_INAPPROPRIATE_SUCCESS,
  MAKE_ANSWER_INAPPROPRIATE_FAILURE,

  MAKE_QUESTION_INAPPROPRIATE_REQUEST, 
  MAKE_QUESTION_INAPPROPRIATE_SUCCESS,
  MAKE_QUESTION_INAPPROPRIATE_FAILURE,

  VOTE_UP_DOWN_QUESTION_REQUEST, 
  VOTE_UP_DOWN_QUESTION_SUCCESS,
  VOTE_UP_DOWN_QUESTION_FAILURE,

  VOTE_UP_DOWN_ANSWER_REQUEST, 
  VOTE_UP_DOWN_ANSWER_SUCCESS,
  VOTE_UP_DOWN_ANSWER_FAILURE,

  MAKE_QUESTION_FAVORITE_REQUEST, 
  MAKE_QUESTION_FAVORITE_SUCCESS,
  MAKE_QUESTION_FAVORITE_FAILURE,

  GET_TAGS_BASIS_OF_CATEGORY_REQUEST, 
  GET_TAGS_BASIS_OF_CATEGORY_SUCCESS,
  GET_TAGS_BASIS_OF_CATEGORY_FAILURE,

  GET_SPECIALITY_BASIS_OF_CATEGORY_REQUEST, 
  GET_SPECIALITY_BASIS_OF_CATEGORY_SUCCESS,
  GET_SPECIALITY_BASIS_OF_CATEGORY_FAILURE,

  MARK_CORRECT_ANSWER_REQUEST, 
  MARK_CORRECT_ANSWER_SUCCESS,
  MARK_CORRECT_ANSWER_FAILURE,

} from "../constants";

const makeAnswerInappropriateRequest = () => ({
  type: MAKE_ANSWER_INAPPROPRIATE_REQUEST,
});

const makeAnswerInappropriateSuccess = data => ({
  type: MAKE_ANSWER_INAPPROPRIATE_SUCCESS,
  payload: {
    data: data
  }
});

const makeAnswerInappropriateFailure = error => ({
  type: MAKE_ANSWER_INAPPROPRIATE_FAILURE,
  payload: {
    error
  }
});

export const makeAnswerInappropriateInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
 //  let data = {'id':parameters.id };
   let urlApi = END_POINT_REPORT_AS_INAPROPRIATE_ANSWER+parameters.id

    dispatch(makeAnswerInappropriateRequest());

     return axios({
      method: 'post',
      url: urlApi,
      data:postData,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
      .then(response => {
        dispatch(makeAnswerInappropriateSuccess(response));
        return response;
      })
      .catch(err => {
        dispatch(makeAnswerInappropriateFailure(err.response));
      });
  };
};

const dashboardQuestionDetailRequest = () => ({
  type: DASHBOARD_QUESTION_DETAIL_REQUEST,
});

const dashboardQuestionDetailSuccess = data => ({
  type: DASHBOARD_QUESTION_DETAIL_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardQuestionDetailFailure = error => ({
  type: DASHBOARD_QUESTION_DETAIL_FAILURE,
  payload: {
    error
  }
});

export const getDashboardQuestionDetail = (parameters) => {

 
  return dispatch => {

    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
    let url = END_POINT_GET_DASHBOARD_QUESTION_DETAIL+parameters.slug
   
    dispatch(dashboardQuestionDetailRequest());

    return axios({
      method: 'post',
      url: url,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
  .then(response => {
    console.log(" Question response ------", response); 
    dispatch(dashboardQuestionDetailSuccess(response));
    return response;
  })
  .catch(error => {
    console.log(" error--", error); 
    dispatch(dashboardQuestionDetailFailure(error.response));
    return error.response;
  });
  };
};


const dashboardVetNotesDetailRequest = () => ({
  type: DASHBOARD_VET_NOTE_DETAIL_REQUEST,
});

const dashboardVetNoteDetailSuccess = data => ({
  type: DASHBOARD_VET_NOTE_DETAIL_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardVetNoteDetailFailure = error => ({
  type: DASHBOARD_VET_NOTE_DETAIL_FAILURE,
  payload: {
    error
  }
});

export const getDashboardVetNoteDetail = (parameters) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
    let url = END_POINT_GET_DASHBOARD_VET_NOTE_DETAIL+parameters.slug
  

    dispatch(dashboardVetNotesDetailRequest());

    return axios({
      method: 'post',
      url: url,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
  .then(response => {
   console.log(" vet note res ---", response); 
    dispatch(dashboardVetNoteDetailSuccess(response));
    return response;
  })
  .catch(error => {
   
    dispatch(dashboardVetNoteDetailFailure(error.response));
    return error.response;
  });
  };
};


// Edit Question 

const dashboardEditQuestionRequest = () => ({
  type: DASHBOARD_QUESTION_EDIT_REQUEST,
});

const dashboardEditQuestionSuccess = data => ({
  type: DASHBOARD_QUESTION_EDIT_SUCCESS,
  payload: {
    data: data
  }
});

const dashboardEditQuestionFailure = error => ({
  type: DASHBOARD_QUESTION_EDIT_FAILURE,
  payload: {
    error
  }
});

export const getDashboardEditQuestion = (parameters) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
    let url = END_POINT_GET_DASHBOARD_VET_NOTE_DETAIL+parameters.slug
    
    dispatch(dashboardEditQuestionRequest());

    return axios({
      method: 'post',
      url: url,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
  .then(response => {
  
    dispatch(dashboardEditQuestionSuccess(response));
    return response;
  })
  .catch(error => {
   
    dispatch(dashboardEditQuestionFailure(error.response));
    return error.response;
  });
  };
};

// POST ANSWER 
const postAnswerRequest = () => ({
  type: POST_ANSWER_REQUEST,
});

const postAnswerSuccess = data => ({
  type: POST_ANSWER_SUCCESS,
  payload: {
    data: data
  }
});

const postAnswerFailure = error => ({
  type: POST_ANSWER_FAILURE,
  payload: {
    error
  }
});

export const postAnswerInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
 //  let data = {'id':parameters.id };
   let urlApi = END_POINT_POST_ANSWER+parameters.id

    dispatch(postAnswerRequest());

     return axios({
      method: 'post',
      url: urlApi,
      data:postData,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
      .then(response => {
       
        dispatch(postAnswerSuccess(response));
        return response;
      })
      .catch(err => {
        dispatch(postAnswerFailure(err.response));
      });
  };
};

const addCommentsRequest = () => ({
  type: ADD_COMMENTS_REQUEST,
});

const addCommentsSuccess = data => ({
  type: ADD_COMMENTS_SUCCESS,
  payload: {
    data: data
  }
});

const addCommentsFailure = error => ({
  type: ADD_COMMENTS_FAILURE,
  payload: {
    error
  }
});

export const addCommentsInPost = (parameters) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
   let data = {'comment':parameters.comment, 'type':parameters.type, 'id':parameters.id };

    dispatch(addCommentsRequest());

     return axios({
      method: 'post',
      url: END_POINT_ADD_COMMENTS,//END_POINT_ADD_COMMENTS_VET_NOTE,
      data:data,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
      .then(response => {
       
        dispatch(addCommentsSuccess(response));
        return response;
      })
      .catch(err => {
        dispatch(addCommentsFailure(err.response));
      });
  };
};



// Make Question inappropriate 

const makeQuestionInappropriateRequest = () => ({
  type: MAKE_QUESTION_INAPPROPRIATE_REQUEST,
});

const makeQuestionInappropriateSuccess = data => ({
  type: MAKE_QUESTION_INAPPROPRIATE_SUCCESS,
  payload: {
    data: data
  }
});

const makeQuestionInappropriateFailure = error => ({
  type: MAKE_QUESTION_INAPPROPRIATE_FAILURE,
  payload: {
    error
  }
});

export const makeQuestionInappropriateInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
 //  let data = {'id':parameters.id };
   let urlApi = END_POINT_REPORT_AS_INAPROPRIATE_QUESTION+parameters.id

    dispatch(makeQuestionInappropriateRequest());

     return axios({
      method: 'post',
      url: urlApi,
      data:postData,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
      .then(response => {
        dispatch(makeQuestionInappropriateSuccess(response));
        return response;
      })
      .catch(err => {
        dispatch(makeQuestionInappropriateFailure(err));
      });
  };
};

// Make question favorite 

const makeQuestionFavoriteRequest = () => ({
  type: MAKE_QUESTION_FAVORITE_REQUEST,
});

const makeQuestionFavoriteSuccess = data => ({
  type: MAKE_QUESTION_FAVORITE_SUCCESS,
  payload: {
    data: data
  }
});

const makeQuestionFavoriteFailure = error => ({
  type: MAKE_QUESTION_FAVORITE_FAILURE,
  payload: {
    error
  }
});

export const makeQuestionFavoriteInDetail = (parameters) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
   let urlApi = END_POINT_MAKE_QUESTION_FAVORITE+parameters.id

    dispatch(makeQuestionFavoriteRequest());

     return axios({
      method: 'post',
      url: urlApi,
      headers: headers,
  }) 
      .then(response => {
        dispatch(makeQuestionFavoriteSuccess(response));
        return response;
      })
      .catch(err => {
         
        dispatch(makeQuestionFavoriteFailure(err.response));
      });
  };
};

// Mark Correct Answer 

const markAnswerCorrectRequest = () => ({
  type: MARK_CORRECT_ANSWER_REQUEST,
});

const markAnswerCorrectSuccess = data => ({
  type: MARK_CORRECT_ANSWER_SUCCESS,
  payload: {
    data: data
  }
});

const markAnswerCorrectFailure = error => ({
  type: MARK_CORRECT_ANSWER_FAILURE,
  payload: {
    error
  }
});
export const markAnswerCorrectInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
   let urlApi = END_POINT_MARK_CORRECT_ANSWER+postData.id

    dispatch(markAnswerCorrectRequest());

     return axios({
      method: 'post',
      url: urlApi,
      headers: headers,
      // validateStatus: status => status >= 200 && status < 500, // default
  }) 
      .then(response => {
        dispatch(markAnswerCorrectSuccess(response));
        return response;
      })
      .catch(err => {
        dispatch(markAnswerCorrectFailure(err.response));
      });
  };
};

// VOTE UP/ DOWN QUESTION

const voteUpDownQuestionRequest = () => ({
  type: VOTE_UP_DOWN_QUESTION_REQUEST,
});

const voteUpDownQuestionSuccess = data => ({
  type: VOTE_UP_DOWN_QUESTION_SUCCESS,
  payload: {
    data: data
  }
});

const voteUpDownQuestionFailure = error => ({
  type: VOTE_UP_DOWN_QUESTION_FAILURE,
  payload: {
    error
  }
});

export const voteUpDownQuestionInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };

   let urlApi = END_POINT_VOTE_UP_DOWN_QUESTION+postData+'/'+"question/"+parameters.id

    dispatch(voteUpDownQuestionRequest());

     return axios({
      method: 'post',
      url: urlApi,
      data:postData,
      headers: headers,
  }) 
      .then(response => {
        dispatch(voteUpDownQuestionSuccess(response));
        return response;
      })
      .catch(err => {
        dispatch(voteUpDownQuestionFailure(err.response));
      });
  };
};

// Vote up down Answer 



const voteUpDownAnswerRequest = () => ({
  type: VOTE_UP_DOWN_ANSWER_REQUEST,
});

const voteUpDownAnswerSuccess = data => ({
  type: VOTE_UP_DOWN_ANSWER_SUCCESS,
  payload: {
    data: data
  }
});

const voteUpDownAnswerFailure = error => ({
  type: VOTE_UP_DOWN_ANSWER_FAILURE,
  payload: {
    error
  }
});

export const voteUpDownAnswerInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters.auth_token
    };
    let urlApi = END_POINT_VOTE_UP_DOWN_ANSWER+postData+"/answer/"+parameters.id
    console.log(' url api -----', urlApi); 

    dispatch(voteUpDownAnswerRequest());

     return axios({
      method: 'post',
      url: urlApi,
      data:postData,
      headers: headers,
  }) 
      .then(response => {
        dispatch(voteUpDownAnswerSuccess(response));
        return response;
      })
      .catch(err => {
        dispatch(voteUpDownAnswerFailure(err.response));
      });
  };
};

// GET SPECIALITY ON THE BASIS OF CATEGORY

const getSpecialityBasisOfCategoryRequest = () => ({
  type:GET_SPECIALITY_BASIS_OF_CATEGORY_REQUEST,
});

const getSpecialityBasisOfCategorySuccess = data => ({
  type:GET_SPECIALITY_BASIS_OF_CATEGORY_SUCCESS,
  payload: {
    data: data
  }
});

const getSpecialityBasisOfCategoryFailure = error => ({
  type:GET_SPECIALITY_BASIS_OF_CATEGORY_FAILURE,
  payload: {
    error
  }
});

export const getSpecialityBasisOfCategoryInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters
    };
 //  let data = {'id':parameters.id };
   let urlApi = END_POINT_GET_SPECIALITY_BASIS_OF_CATEGORY+postData
  
    dispatch(getSpecialityBasisOfCategoryRequest());

  return axios.get(urlApi, {headers:headers}).then(response => {

    dispatch(getSpecialityBasisOfCategorySuccess(response.data));
    return response;
  })
  .catch(err => {
    dispatch(getSpecialityBasisOfCategoryFailure(err.response));
  });
  };
};





// GET TAGS ON THE BASIS OF CATEGORY 
const getTagsBasisOfCategoryRequest = () => ({
  type:GET_TAGS_BASIS_OF_CATEGORY_REQUEST,
});

const getTagsBasisOfCategorySuccess = data => ({
  type:GET_TAGS_BASIS_OF_CATEGORY_SUCCESS,
  payload: {
    data: data
  }
});

const getTagsBasisOfCategoryFailure = error => ({
  type:GET_TAGS_BASIS_OF_CATEGORY_FAILURE,
  payload: {
    error
  }
});

export const getTagsBasisOfCategoryInDetail = (parameters, postData) => {

  return dispatch => {
    const headers = {
      "Content-Type": "application/json",
      'X-Access-Token': parameters
    };
 //  let data = {'id':parameters.id };
   let urlApi = END_POINT_GET_TAGS_BASIS_OF_CATEGORY+postData

    dispatch(getTagsBasisOfCategoryRequest());

  return axios.get(urlApi, {headers:headers}).then(response => {
    dispatch(getTagsBasisOfCategorySuccess(response));
    return response;
  })
  .catch(err => {
    dispatch(getTagsBasisOfCategoryFailure(err.response));
  });
  };
};
