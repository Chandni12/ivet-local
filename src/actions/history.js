import axios from 'axios';
import {   END_POINT_GET_QUESTION_EDIT_HISTORY, END_POINT_GET_ANSWER_EDIT_HISTORY, END_POINT_GET_NOTE_HISTORY} from "../api/endPoints";
import {

  GET_QUESTION_EDITED_HISTORY_REQUEST, 
  GET_QUESTION_EDITED_HISTORY_SUCCESS,
  GET_QUESTION_EDITED_HISTORY_FAILURE,

  GET_ANSWER_EDITED_HISTORY_REQUEST, 
  GET_ANSWER_EDITED_HISTORY_SUCCESS,
  GET_ANSWER_EDITED_HISTORY_FAILURE,

  GET_NOTE_HISTORY_REQUEST, 
  GET_NOTE_HISTORY_SUCCESS,
  GET_NOTE_HISTORY_FAILURE,

} from "../constants";

const getHistoryRequest = () => ({
    type: GET_QUESTION_EDITED_HISTORY_REQUEST,
  });
  
  const getHistorySuccess = data => ({
    type: GET_QUESTION_EDITED_HISTORY_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const getHistoryFailure = error => ({
    type: GET_QUESTION_EDITED_HISTORY_FAILURE,
    payload: {
      error
    }
  });
  
  export const getHistoryToCheck = (parameters) => {
    console.log( 'history parameters -----', parameters);

    return dispatch => {
  
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
      let url = END_POINT_GET_QUESTION_EDIT_HISTORY+parameters.id
       
      dispatch(getHistoryRequest());
  
      return axios({
        method: 'post',
        url: url,
        headers: headers,
    }) 
    .then(response => {
       
      dispatch(getHistorySuccess(response));
      return response;
    })
    .catch(error => {
      console.log(" history error", error)
      dispatch(getHistoryFailure(error.response));
      return error.response;
    });
    };
  };

  // GET ANSWER 

  const getAnswerHistoryRequest = () => ({
    type: GET_ANSWER_EDITED_HISTORY_REQUEST,
  });
  
  const getAnswerHistorySuccess = data => ({
    type: GET_ANSWER_EDITED_HISTORY_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const getAnswerHistoryFailure = error => ({
    type: GET_ANSWER_EDITED_HISTORY_FAILURE,
    payload: {
      error
    }
  });
  
  export const getAnswerHistoryToCheck = (parameters) => {
    console.log( 'history parameters -----', parameters);

    return dispatch => {
  
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
      let url = END_POINT_GET_ANSWER_EDIT_HISTORY+parameters.id
     
       
      dispatch(getAnswerHistoryRequest());
  
      return axios({
        method: 'post',
        url: url,
        headers: headers,
    }) 
    .then(response => {
       
      dispatch(getAnswerHistorySuccess(response));
      return response;
    })
    .catch(error => {
      console.log(" history error", error)
      dispatch(getAnswerHistoryFailure(error.response));
      return error.response;
    });
    };
  };

  // Get Vet Note History 
   

  const getVetNoteHistoryRequest = () => ({
    type: GET_NOTE_HISTORY_REQUEST,
  });
  
  const getVetNoteHistorySuccess = data => ({
    type: GET_NOTE_HISTORY_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const getVetNoteHistoryFailure = error => ({
    type: GET_NOTE_HISTORY_FAILURE,
    payload: {
      error
    }
  });
  
  export const getVetNoteHistoryToCheck = (parameters) => {
    console.log( 'history parameters -----', parameters);

    return dispatch => {
  
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': parameters.auth_token
      };
      let url = END_POINT_GET_NOTE_HISTORY+parameters.id
     console.log( 'history url is -----', url)
       
      dispatch(getVetNoteHistoryRequest());
  
      return axios({
        method: 'post',
        url: url,
        headers: headers,
    }) 
    .then(response => {
       
      dispatch(getVetNoteHistorySuccess(response));
      return response;
    })
    .catch(error => {
      console.log(" history error", error)
      dispatch(getVetNoteHistoryFailure(error.response));
      return error.response;
    });
    };
  };
