import axios from 'axios';
import { END_POINT_GET_EDIT_REQUEST_DATA, END_POINT_GET_EDIT_OTHER_REQUEST_DATA} from "../api/endPoints";

import {
  EDIT_REQUEST,
  EDIT_REQUEST_SUCCESS,
  EDIT_REQUEST_FAILURE,
  EDIT_OTHER_REQUEST,
  EDIT_OTHER_REQUEST_SUCCESS,
  EDIT_OTHER_REQUEST_FAILURE
} from "../constants";

const editRequestRequest = () => ({
  type: EDIT_REQUEST
});

const editRequestSuccess = data => ({
  type: EDIT_REQUEST_SUCCESS,
  payload: {
    data: data
  }
});

const editRequestFailure = error => ({
  type: EDIT_REQUEST_FAILURE,
  payload: {
    error
  }
});

const editOtherRequestRequest = () => ({
    type: EDIT_OTHER_REQUEST
  });
  
  const editOtherRequestSuccess = data => ({
    type: EDIT_OTHER_REQUEST_SUCCESS,
    payload: {
      data: data
    }
  });
  
  const editOtherRequestFailure = error => ({
    type: EDIT_OTHER_REQUEST_FAILURE,
    payload: {
      error
    }
  });

  export const getEditRequestData = (data) => {
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': data.auth_token
      };
      dispatch(editRequestRequest());
      
      return axios({
        method: 'get',
        url: END_POINT_GET_EDIT_REQUEST_DATA,
        headers: headers
    }).then(res => {
      console.log('response',res)
      dispatch(editRequestSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(editRequestFailure(err.message));
      });
    };
  };

  export const getEditOtherRequestData = (data) => {
    return dispatch => {
      const headers = {
        "Content-Type": "application/json",
        'X-Access-Token': data.auth_token
      };
      dispatch(editOtherRequestSuccess());
      
      return axios({
        method: 'get',
        url: END_POINT_GET_EDIT_OTHER_REQUEST_DATA,
        headers: headers
    }).then(res => {
      dispatch(editRequestSuccess(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(editOtherRequestFailure(err.message));
      });
    };
  };