import {
    DASHBOARD_REQUEST,
    DASHBOARD_SUCCESS,
    DASHBOARD_FAILURE
  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const dashboard = (state = initialState, action) => {
    switch (action.type) {
      case DASHBOARD_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case DASHBOARD_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case DASHBOARD_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });
      default:
        return state;
    }
  };
  
  export default dashboard;