import {
    CATEGORY_REQUEST,
    CATEGORY_SUCCESS,
    CATEGORY_FAILURE, 
    ADD_CATEGORY_REQUEST,
    ADD_CATEGORY_SUCCESS,
    ADD_CATEGORY_FAILURE

  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const category = (state = initialState, action) => {
    switch (action.type) {
      case CATEGORY_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case CATEGORY_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case CATEGORY_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });

        case ADD_CATEGORY_REQUEST:
          return Object.assign({}, state, {
            isRequesting: true,
            data: {},
            error: {}
          });
        case ADD_CATEGORY_SUCCESS:
          return Object.assign({}, state, {
            data: action.payload.data,
            isRequesting: false,
            error: {}
          });
        case ADD_CATEGORY_FAILURE:
          return Object.assign({}, state, {
            error: action.payload.error,
            isRequesting: false
          });

      default:
        return state;
    }
  };
  
  export default category;