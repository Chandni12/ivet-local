import {
  NOTIFICATION_REQUEST,
  NOTIFICATION_SUCCESS,
  NOTIFICATION_FAILURE,
  NOTIFICATION_DELETE_REQUEST,
  NOTIFICATION_DELETE_SUCCESS,
  NOTIFICATION_DELETE_FAILURE,
} from "../constants";

const initialState = {
  error: {},
  data: {},
  isRequesting: false
}
const follow = (state = initialState, action) => {
  switch (action.type) {
    case NOTIFICATION_REQUEST:
      return Object.assign({}, state, {
        isRequesting: true,
        data: {},
        error: {}
      });
    case NOTIFICATION_SUCCESS:
      return Object.assign({}, state, {
        data: action.payload.data,
        isRequesting: false,
        error: {}
      });
    case NOTIFICATION_FAILURE:
      return Object.assign({}, state, {
        error: action.payload.error,
        isRequesting: false
      });
    default:
      return state;
  }
};

export default follow;