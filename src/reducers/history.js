import {
    
    GET_EDITED_HISTORY_REQUEST, 
    GET_EDITED_HISTORY_SUCCESS,
    GET_EDITED_HISTORY_FAILURE,

    } from "../constants";
    
    const initialState = {
        error: {},
        data: {},
        isRequesting: false
    }
    const history = (state = initialState, action) => {
        
      switch (action.type) {
        case GET_EDITED_HISTORY_REQUEST:
          return Object.assign({}, state, {
            isRequesting: true,
            data: {},
            error: {}
          });
        case GET_EDITED_HISTORY_SUCCESS:
          return Object.assign({}, state, {
            data: action.payload.data,
            isRequesting: false,
            error: {}
          });
        case GET_EDITED_HISTORY_FAILURE:
          return Object.assign({}, state, {
            error: action.payload.error,
            isRequesting: false
          });

        default:
          return state;
      }
    };
    
    export default history;