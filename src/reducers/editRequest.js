import {
  EDIT_REQUEST,
  EDIT_REQUEST_SUCCESS,
  EDIT_REQUEST_FAILURE,
  EDIT_OTHER_REQUEST,
  EDIT_OTHER_REQUEST_SUCCESS,
  EDIT_OTHER_REQUEST_FAILURE
  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const follow = (state = initialState, action) => {
    switch (action.type) {
      case EDIT_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case EDIT_REQUEST_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case EDIT_REQUEST_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });
        case EDIT_OTHER_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case EDIT_OTHER_REQUEST_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case EDIT_OTHER_REQUEST_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });
      default:
        return state;
    }
  };
  
  export default follow;