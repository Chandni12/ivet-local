import {
    QUESTION_REQUEST,
    QUESTION_SUCCESS,
    QUESTION_FAILURE, 

  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const questions = (state = initialState, action) => {
    switch (action.type) {
      case QUESTION_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case QUESTION_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case QUESTION_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });
      default:
        return state;
    }
  };
  
  export default questions;