import {
    FOLLOW_REQUEST,
    FOLLOW_SUCCESS,
    FOLLOW_FAILURE
  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const follow = (state = initialState, action) => {
    switch (action.type) {
      case FOLLOW_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case FOLLOW_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case FOLLOW_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });
      default:
        return state;
    }
  };
  
  export default follow;