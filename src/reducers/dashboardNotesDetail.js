import {
    ADD_COMMENTS_REQUEST,
    ADD_COMMENTS_SUCCESS,
    ADD_COMMENTS_FAILURE,
   
    DASHBOARD_QUESTION_DETAIL_REQUEST, 
    DASHBOARD_QUESTION_DETAIL_SUCCESS,
    DASHBOARD_QUESTION_DETAIL_FAILURE,
  
    DASHBOARD_VET_NOTE_DETAIL_FAILURE,
    DASHBOARD_VET_NOTE_DETAIL_REQUEST,
    DASHBOARD_VET_NOTE_DETAIL_SUCCESS,
  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const dashboardNotesDetail = (state = initialState, action) => {
    switch (action.type) {
      case ADD_COMMENTS_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case ADD_COMMENTS_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case ADD_COMMENTS_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });

      default:
        return state;
    }
  };
  
  export default dashboardNotesDetail;