import {
    DRAFT_REQUEST,
    DRAFT_SUCCESS,
    DRAFT_FAILURE,
  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const draft = (state = initialState, action) => {
    switch (action.type) {
      case DRAFT_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case DRAFT_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case DRAFT_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });
      default:
        return state;
    }
  };
  
  export default draft;