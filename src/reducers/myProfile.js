import {
  MY_PROFILE_REQUEST,
  MY_PROFILE_SUCCESS,
  MY_PROFILE_FAILURE,

  SET_NICK_NAME_REQUEST,
  SET_NICK_NAME_SUCCESS,
  SET_NICK_NAME_FAILURE,

  GET_NICK_NAME_REQUEST,
  GET_NICK_NAME_SUCCESS,
  GET_NICK_NAME_FAILURE,

} from "../constants";

const initialState = {
  error: {},
  data: {},
  isRequesting: false
}
const myProfile = (state = initialState, action) => {
  switch (action.type) {
    case MY_PROFILE_REQUEST:
      return Object.assign({}, state, {
        isRequesting: true,
        data: {},
        error: {}
      });
    case MY_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        data: action.payload.data,
        isRequesting: false,
        error: {}
      });
    case MY_PROFILE_FAILURE:
      return Object.assign({}, state, {
        error: action.payload.error,
        isRequesting: false
      });
    case GET_NICK_NAME_REQUEST:
      return Object.assign({}, state, {
        data: {},
        isRequesting: false,
        error: {}
      });
    case GET_NICK_NAME_SUCCESS:
      return Object.assign({}, state, {
        error: action.payload.error,
        isRequesting: false
      });
    case GET_NICK_NAME_FAILURE:
      return Object.assign({}, state, {
        isRequesting: true,
        data: {},
        error: {}
      });
    case SET_NICK_NAME_REQUEST:
      return Object.assign({}, state, {
        data: {},
        isRequesting: false,
        error: {}
      });
    case SET_NICK_NAME_SUCCESS:
      return Object.assign({}, state, {
        error: action.payload.error,
        isRequesting: false
      });
    case SET_NICK_NAME_FAILURE:
      return Object.assign({}, state, {
        isRequesting: true,
        data: {},
        error: {}
      });
    default:
      return state;
  }
};

export default myProfile;