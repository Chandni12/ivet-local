import {
    
  GET_ANSWER_REQUEST, 
  GET_ANSWER_SUCCESS,
  GET_ANSWER_FAILURE,

  EDIT_ANSWER_REQUEST, 
  EDIT_ANSWER_SUCCESS,
  EDIT_ANSWER_FAILURE,

  } from "../constants";
  
  const initialState = {
      error: {},
      data: {},
      isRequesting: false
  }
  const editAnswer = (state = initialState, action) => {
      
    switch (action.type) {
      case GET_ANSWER_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case GET_ANSWER_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case GET_ANSWER_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });

        case EDIT_ANSWER_REQUEST:
        return Object.assign({}, state, {
          isRequesting: true,
          data: {},
          error: {}
        });
      case EDIT_ANSWER_SUCCESS:
        return Object.assign({}, state, {
          data: action.payload.data,
          isRequesting: false,
          error: {}
        });
      case EDIT_ANSWER_FAILURE:
        return Object.assign({}, state, {
          error: action.payload.error,
          isRequesting: false
        });


      default:
        return state;
    }
  };
  
  export default editAnswer;