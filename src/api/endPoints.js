//export const BASE_URL = "http://54.250.243.234/api/v1/";

export const BASE_URL = "http://13.231.4.209/api/v1/";
export const BASE_URL_DOMAIN = "http://13.231.4.209/"; // This is using for image 

//export const BASE_URL = "http://64ac2d2b.ngrok.io/vetforum/www/public/api/v1/";

export const END_POINT_FORGOT_PASSWORD = BASE_URL + "password/email";
export const END_POINT_LOGIN = BASE_URL + "login";
export const END_POINT_SIGNUP = BASE_URL + "register";
export const END_POINT_GET_DASHBOARD_DATA = BASE_URL + "dashboard?page=";
export const END_POINT_GET_DASHBOARD_QUESTION_DETAIL = BASE_URL + "question/details/";
export const END_POINT_GET_DASHBOARD_VET_NOTE_DETAIL = BASE_URL + "vet-note/details/";
export const END_POINT_GET_DASHBOARD_Q_AND_A = BASE_URL + "questions";
export const END_POINT_GET_DASHBOARD_SEARCH_DATA = BASE_URL + "search";
export const END_POINT_GET_FOLLOWERS_DATA = BASE_URL + "my-followers";
export const END_POINT_GET_NOTIFICATION_DATA = BASE_URL + "notifications";
export const END_POINT_GET_NOTIFICATION_DELETE = BASE_URL + "notifications/delete/";
export const END_POINT_GET_DRAFT_DATA = BASE_URL + "posts-drafts";

export const END_POINT_ADD_COMMENTS = BASE_URL + "comment/store";
export const END_POINT_ADD_COMMENTS_VET_NOTE = BASE_URL + "vet-note/post_comment";

export const END_POINT_GET_CATEGORY_SUBCATEGORY = BASE_URL + "CategoryWithSubCategories";
export const END_POINT_SAVE_SELECTED_CATEGORY = BASE_URL + "add-category";
export const END_POINT_GET_QUESTION_LIST = BASE_URL + "questions-list";

export const END_POINT_POST_GET_CLINICAL_NOTES = BASE_URL + "vet-note";
export const END_POINT_POST_GET_TIME_LINE = BASE_URL + "timeline";
export const END_POINT_POST_GET_TAG_FEED = BASE_URL + "tagFeed";
export const END_POINT_POST_GET_SELECTED_TAG_DATA = BASE_URL + "tags";
export const END_POINT_POST_FOLLOW_UNFOLLOW = BASE_URL + "followUnfollowUser";

export const END_POINT_GET_EDIT_REQUEST_DATA = BASE_URL + "my-edit-request/edit-request";
export const END_POINT_GET_EDIT_OTHER_REQUEST_DATA = BASE_URL + "my-edit-request/other-edit-request";

// End points needs to be confirm by api developer 

export const END_POINT_GET_HELP_DATA = BASE_URL + "";
export const END_POINT_FAQ = BASE_URL + "faqs";
export const END_POINT_FAQ_DETAIL = BASE_URL + "faqs-detail";
export const END_POINT_GET_COLLECTION_DATA = BASE_URL + "questions/collections";
export const END_POINT_MY_PAGE = BASE_URL + "activities";
export const END_POINT_GROUP_QUESTION = BASE_URL + "group-questions";

export const END_POINT_GET_SPECIALITY = BASE_URL + "";
export const END_POINT_POST_DRAFT_AS_A_QUESTION = BASE_URL + "save-previewed-question";
export const END_POINT_POST_QUESTION = BASE_URL + "question/store";

export const END_POINT_GET_DATA_TO_POST_QUESTION = BASE_URL + "question-ask";

export const END_POINT_GET_SPECIALITY_BASIS_OF_CATEGORY = BASE_URL+ "getSpecialities?cat_id=";

export const END_POINT_GET_TAGS_BASIS_OF_CATEGORY = BASE_URL+"get-tags-by-catid?category_Id=";
export const END_POINT_GET_BREED_GENDER_OF_ANIMAL = BASE_URL+"getBreedGenderByAnimal?animal_id=";

// End point to get post notes. 
export const END_POINT_GET_DATA_TO_POST_NOTES = BASE_URL + "vet-note-post";
export const END_POINT_POST_NOTES = BASE_URL + "vet-note/save-post";
export const END_POINT_SAVE_NOTE_AFTER_PREVIEW = BASE_URL + "vet-note/save-previewed-draft";

export const END_POINT_GET_DATA_FOR_EDIT_VET_NOTE = BASE_URL + "vet-note/edit-for-request/";
export const END_POINT_SAVE_EDITTED_VET_NOTE = BASE_URL + "vet-note/edit-note-post";

export const END_POINT_GET_DATA_FOR_EDIT_QUESTION = BASE_URL + "question/edit-question/";
export const END_POINT_SAVE_DATA_FOR_EDIT_QUESTION = BASE_URL + "question/update";

//export const END_POINT_GET_ = BASE_URL+"";

export const END_POINT_POST_ANSWER = BASE_URL + "answer/store/";
export const END_POINT_EDIT_ANSWER = BASE_URL + "answer/update/";
export const END_POINT_GET_ANSWER = BASE_URL + "answer/";

export const END_POINT_GET_QUESTION_EDIT_HISTORY = BASE_URL + "question/revisionable-list/";
export const END_POINT_GET_ANSWER_EDIT_HISTORY = BASE_URL + "answer/revisionable-list/";

export const END_POINT_REPORT_AS_INAPROPRIATE_QUESTION = BASE_URL + "question/report/";
export const END_POINT_REPORT_AS_INAPROPRIATE_ANSWER = BASE_URL + "answer/report/";

export const END_POINT_MAKE_QUESTION_FAVORITE = BASE_URL + "favorite/question/";
export const END_POINT_MAKE_NOTES_FAVORITE = BASE_URL + "favorite/case-study/";
export const END_POINT_MARK_CORRECT_ANSWER = BASE_URL + "accept/answer/";

export const END_POINT_VOTE_UP_DOWN_QUESTION = BASE_URL + "vote/";
export const END_POINT_VOTE_UP_DOWN_ANSWER = BASE_URL + "vote/";

// **** Api Notes  ****//

export const END_POINT_ADD_NOTE_TO_COLLECTION = BASE_URL + "favorite/case-study/";
export const END_POINT_GET_NOTE_HISTORY = BASE_URL + "vet-note/revisionable-list/";
export const END_POINT_REPORT_NOTE_INAPPROPRIATE = BASE_URL + "vet-note/mark-inappropriate/";
export const END_POINT_VOTE_UP_DOWN_NOTES = BASE_URL + "vet-note/do-vote/";
export const END_POINT_LIKE_NOTE_COMMENT = BASE_URL + "likes/case-study/";

// ******* *******//

export const END_POINT_GET_MY_PROFILE = BASE_URL + "myProfile";
export const END_POINT_UPDATE_MY_PROFILE = BASE_URL + "profileUpdate";
export const END_POINT_SET_NICK_NAME = BASE_URL + "set_nickname/";
export const END_POINT_GET_NICK_NAME = BASE_URL + "nickname_suggest/";

export const END_POINT_ADD_EDUCATION_PROFILE = BASE_URL + "user/education/add";
export const END_POINT_UPDATE_EDUCATION_PROFILE = BASE_URL + "user/education/update";

export const END_POINT_ADD_WORK_EXPERIENCE_PROFILE = BASE_URL + "user/experience/add";
export const END_POINT_UPDATE_WORK_PROFILE = BASE_URL + "user/experience/update";

export const END_POINT_FOLLOWING_TAGS = BASE_URL + "tags/following";
export const END_POINT_FOLLOWING_TAGS_DETAIL_BASED_ON_CATEGORY_NAME = BASE_URL + "tags/following?category=";

export const END_POINT_ADD_REMOVE_TAG_IN_TAG_FEED = BASE_URL+'follow/tag/';

// **** Image prefix urls ****//
export const IMAGE_USER_PROFILE_PREFIX_URL = BASE_URL_DOMAIN+'images/';
export const IMAGE_DASHBOARD_PREFIX_URL = BASE_URL_DOMAIN+"images/public-document-users-";
export const IMAGE_DASHBOARD_NICK_NAME_PREFIX_URL =  BASE_URL_DOMAIN+"images/public-document-nickname_icons-"
export const IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL = BASE_URL_DOMAIN+"images/public-document-nickname-"
export const IMAGE_DASHBOARD_QUESTION_PREFIX_URL = BASE_URL_DOMAIN+"images/public-document-question-";
export const IMAGE_VETNOTE_PREFIX_URL = BASE_URL_DOMAIN+"images/public-document-casestudy-";
export const IMAGE_EDIT_ANSWER_PREFIX_URL = BASE_URL_DOMAIN+"images/public-document-answer-";
export const IMAGE_USER_WHO_POSTED_ANSWER = BASE_URL_DOMAIN+"images/public-document-users-";
export const IMAGE_ANSWER_DETAIL_PREFIX_URL = BASE_URL_DOMAIN+'images/public-document-answer-';
