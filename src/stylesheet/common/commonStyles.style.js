import { scale, verticalScale, moderateScale, ScaledSheet } from 'react-native-size-matters';

export default {
    listText: {
        color: '#0385ff',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        fontSize: 14,
        marginRight: 8,
        marginLeft: 8,
    },
    profilePhotoWithoutCircle: {
        width: 30,
        height: 30,
        borderRadius: 15,
        // borderColor: 'rgb(2, 132, 254)',
        // borderWidth: 1.5,

    },
    styleModalRow: {

        fontSize: 14,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
        //backgroundColor:'white'
    },
    styleModalRowSelected: {

        fontSize: 14,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
        //backgroundColor:'white'
    },
    viewModalRowBackgnd: {
        marginTop:'3%', marginBottom:'3%', marginLeft:'3%', marginRight:'3%' , flexDirection:'row', backgroundColor:'white'
        
    },
    background: {
        flex: 1,
        backgroundColor: 'white',
    },
    buttonViewBlue: {
        width: '80%',
        backgroundColor: 'rgb(66, 130, 191)',
        borderRadius: 5,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonViewRed: {
        width: '80%',
        backgroundColor: 'rgb(214, 22, 22)',
        borderRadius: 5,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        margin: '5%'
    },
    textBlueButton: {
        color: 'white',
        multiLine: true,
        textAlign: 'center',
        padding: 10
    },
    spaceBottom1x: {
        marginBottom: 5,
    },
    spaceBottom2x: {
        marginBottom: 10
    },
    spaceBottom3x: {
        marginBottom: 15
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
}