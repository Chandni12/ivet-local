import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput } from 'react-native';
import CommunityScoreConstants from '../../constants/CommunityScore';
import commonStyles from '../../stylesheet/common/commonStyles.style';
//import styles from './styles/profileStyle.style';
import { checkError } from '../../utils'
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFeather from 'react-native-vector-icons/Feather';

import communityScoreConstants from '../../constants/CommunityScore';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import ProfileConstants from '../../constants/Profile';
import EditIconView from '../../commonComponent/EditIconView';

import moment from 'moment';
import profileConstants from '../../constants/Profile';

export default class Career extends Component {
    constructor(props) {
        super(props);
    }

    onClickEditIcon = (data) => {
        console.log(" eidt ---")
        this.props.onClickAddOrEditWorkExp(false, data);

    }
    renderItem = (item, index) => {
        return (
            <View style={{}}>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, width:'100%', alignSelf:'center' }}>

                    <IconFontAwesome name="black-tie" size={30} />
                    <Text style={styles.textTitle}>{item.item.title}</Text>
                    <TouchableOpacity onPress={() => this.onClickEditIcon(item.item)}>
                        <IconFeather name="edit" style={{ fontSize: 20, color: 'rgb(51,51,51)' }} />
                    </TouchableOpacity>

                </View>
                    <View  >
                        
                        <View style={{ flexDirection: 'row', alignSelf:'center'}}>
                            <Text>{moment().month(item.item.start_month - 1).format('MMMM')} {item.item.start_year}</Text>

                            {item.item.currently_working === 'Y' ? <Text> - {profileConstants.CURRENTLY_WORKING_HERE}</Text> : null}
                        </View>
                        <Text style={{alignSelf:'center'}}>{item.item.description}</Text>
                    </View>

                  <View style={{paddingBottom:10, paddingTop:10}}/>
                <View style={[styles.viewSingleLine ]} />
            </View>
        )
    }
    render() {
        const { user, data, onClickAddOrEditWorkExp, workExperienceArray, educationArray } = this.props;
        return (
            <View style={styles.viewBox}>
                <EditIconView title={ProfileConstants.CAREER} data={data} onClickAddOrEditWorkExp={onClickAddOrEditWorkExp} />
                <FlatList
                    data={workExperienceArray}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />

            </View>
        )
    }


}

const styles = {
    viewSingleLine:{
   width:'95%',
   height:1,
   backgroundColor:'#b6c6d3',
   alignSelf:'center'
    },
    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: '#b6c6d3',
        borderWidth: 1,
        shadowColor: '#b6c6d3',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        // alignItems: 'center',
    },
    textTitle: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textSubTitle: {
        fontSize: 13.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textInLightBlue: {
        fontSize: 13.5,
        color: ' rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',

    },
}
