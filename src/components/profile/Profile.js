import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput, Dimensions, ActivityIndicator } from 'react-native';
import ProfileConstants from '../../constants/Profile';
import commonStyles from '../../stylesheet/common/commonStyles.style'
import { SafeAreaView } from 'react-navigation';
import { checkError } from '../../utils'
import * as Animatable from 'react-native-animatable';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import HeaderThreeOptions from '../../commonComponent/HeaderThreeOptions';
import CommunityScoreConstants from '../../constants/CommunityScore';
import * as Progress from 'react-native-progress';
import ViewPopup from '../../commonComponent/ViewPopup';

import User from './User';
import Career from './Career';
import EditBasicInfo from './EditBasicInfo';
import EditWorkExperience from './EditWorkExperience';
import EditEducationDetail from './EditEducationDetail';
import Education from './Education';
import { ScrollView } from 'react-native-gesture-handler';
import profileConstants1 from '../../constants/Profile';

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            progressValue: 0.5,
            progressWithOnComplete: 0,
            progressCustomized: 0,
            borderColorProgressbar: 'white',
            isPopupShow: false,
        }
        this.arrayPopupData = [ProfileConstants.MY_PAGE, ProfileConstants.COMMUNITY_SCORE, ProfileConstants.FOLLOW_ME]
    }
    showPopup = () => {
        this.setState({ isPopupShow: !this.state.isPopupShow });
    }
    render() {
        const { callApiToUpdateProfile, veterinarian_type_name_display, user, veterinarian_types, data, onClickAddOrEditWorkExp, onClickAddOrEditEducationalDetails, onClickEditIconUserView, basicInfoData, handleChangeWorkExp, handleChangeBasicInfo, errors, fetching, veterinarian_types_name, veterinarian_types_complete_data, workExperience, yearList, monthList, closeWorkExpPopup, educationData, callApiToUpdateWorkExperience, callApiToAddUpdateEducation, workExperienceArray, educationArray, callApiTosetNickName, nick_names, callApiTogetNickName, categories, nickNameType, selectedNickName } = this.props;

        return (
            <SafeAreaView>
                <HeaderThreeOptions showRightButton = {true} title={ProfileConstants.MY_PAGE} onRightButtonPress={() => this.showPopup()} navigation={this.props.navigation} />
                {/* <View style={[{  alignSelf: 'center', height: 60, backgroundColor: 'rgb(61, 160, 248)', alignItems:'center', width:'100%'}]} /> */}
                <ScrollView style={{ backgroundColor: 'white' }}>



                    <User fetching={fetching} basicInfoData={basicInfoData} onClickEditIconUserView={onClickEditIconUserView} user={user} veterinarian_types={veterinarian_types} />

                    {/* <View style={[styles.viewBox]}>
                        <Progress.Bar
                            // backgroundColor= {'#f6cc4c'}
                            //backgroundColor= {'white'}
                            color={'#3ad716'}
                            unfilledColor={'rgb(230,230,230)'}
                            height={verticalScale(25)}
                            borderRadius={4}
                            width={Dimensions.get('screen').width / 1.15}
                            progress={this.state.progressValue}
                            style={{ alignSelf: 'center', marginTop: '5%' }}
                        />
                        <Text style={styles.textTitleProgressBar}>{ProfileConstants.COMPLETE_YOUR_PROFILE_SETTING}</Text>
                        <Text style={styles.textDescProgressBar}>{ProfileConstants.SET_FIELD_OF_INTEREST}</Text>
                    </View> */}

                    {/* <View style={[styles.viewBox]}>
                        <Text style={[styles.textTitleCategory, { marginTop: '5%' }]}>{CommunityScoreConstants.BY_CATEGORY}</Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '90%', marginTop: '5%', marginBottom: '5%' }}>
                            <View>
                                <Text style={styles.textTitleScoreCategory}>2960 cs</Text>
                                <Text style={styles.textScoreDescCategory}>{CommunityScoreConstants.CLINICAL}</Text>
                            </View>
                            <View>
                                <Text style={styles.textTitleScoreCategory}>58 cs</Text>
                                <Text style={styles.textScoreDescCategory}>{CommunityScoreConstants.LIFE}</Text>
                            </View>
                            <View>
                                <Text style={styles.textTitleScoreCategory}>117 cs</Text>
                                <Text style={[styles.textScoreDescCategory]}>{CommunityScoreConstants.HOSPITAL_MANAGEMENT}</Text>
                            </View>
                        </View>
                    </View> */}


                    <Career workExperienceArray={workExperienceArray}
                        educationArray={educationArray} fetching={fetching} user={user} data={data} onClickAddOrEditWorkExp={onClickAddOrEditWorkExp} />
                    <Education workExperienceArray={workExperienceArray}
                        educationArray={educationArray} fetching={fetching} handleChangeWorkExp={handleChangeWorkExp} user={user} data={data} onClickAddOrEditEducationalDetails={onClickAddOrEditEducationalDetails} />

                    {this.state.isPopupShow ?
                        <Animatable.View style={styles.viewPopup}>
                            <ViewPopup arrayData={this.arrayPopupData} showPopup={this.showPopup} />
                        </Animatable.View> :
                        <View />
                    }

                    <View style={{ marginBottom: '30%' }} />
                </ScrollView>

                {data['isEditIconClick'] ? <View style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: Dimensions.get('screen').height, width: "100%", position: 'absolute', justifyContent: 'center', }} >
                    <View style={{ width: '85%', height: '70%', alignSelf: 'center' }}>
                        <EditBasicInfo nick_names={nick_names} fetching={fetching} callApiToUpdateProfile={callApiToUpdateProfile} veterinarian_type_name_display={veterinarian_type_name_display} veterinarian_types_complete_data={veterinarian_types_complete_data}
                            selectedNickName = {selectedNickName} callApiTosetNickName={callApiTosetNickName} callApiTogetNickName={callApiTogetNickName} categories={categories} nickNameType={nickNameType} veterinarian_types_name={veterinarian_types_name} errors={errors} fetching={fetching} handleChangeBasicInfo={handleChangeBasicInfo} basicInfoData={basicInfoData} user={user} onClickEditIconUserView={onClickEditIconUserView} />
                    </View>
                </View> : null}

                {data['isAddWorkExperience'] || data['isEditWorkExperience'] ? <View style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: Dimensions.get('screen').height, width: "100%", position: 'absolute', justifyContent: 'center', }} >
                    <View style={{ width: '85%', height: '70%', alignSelf: 'center' }}>
                        <EditWorkExperience fetching={fetching} closeWorkExpPopup={this.props.closeWorkExpPopup}
                            handleChangeEducation={this.props.handleChangeEducation} yearList={yearList} monthList={monthList} workExperience={workExperience} handleChangeWorkExp={handleChangeWorkExp} data={data} errors={errors} fetching={fetching} handleChangeBasicInfo={handleChangeBasicInfo} user={user} callApiToUpdateWorkExperience={callApiToUpdateWorkExperience} />
                    </View>
                </View> : null}

                {data['isAddEducationDetails'] || data['isEditEducationalDetails'] ? <View style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: Dimensions.get('screen').height, width: "100%", position: 'absolute', justifyContent: 'center', }} >
                    <View style={{ width: '85%', height: '70%', alignSelf: 'center' }}>

                        <EditEducationDetail fetching={fetching} callApiToAddUpdateEducation={this.props.callApiToAddUpdateEducation}
                            closeEducationPopup={this.props.closeEducationPopup} yearList={yearList} monthList={monthList} educationData={educationData} handleChangeWorkExp={handleChangeWorkExp} data={data} errors={errors} fetching={fetching} handleChangeEducation={this.props.handleChangeEducation} user={user} />
                    </View>
                </View> : null}

                {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                </View> : null}

            </SafeAreaView>
        )
    }
}

const styles = {
    viewPopup: {
        position: 'absolute',
        width: '25%',
        height: 70,
        marginTop: '7%',
        right: '5%',
        backgroundColor: 'white',
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        backgroundColr: 'pink'
    },
    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: '#b6c6d3',
        borderWidth: 1,
        shadowColor: '#b6c6d3',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        height: 'auto',
        alignItems: 'center',
    },
    textTitleCategory: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textTitleProgressBar: {
        fontSize: 18,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        margin: '2%',

    },
    textDescProgressBar: {
        fontSize: 15,
        color: ' rgba(97, 97, 97, 255);',
        fontFamily: 'NotoSansCJKjp-Medium',
        margin: '2%',
    },

    textTitleScoreCategory: {
        fontSize: 22.5,
        color: 'rgba(11, 123, 217, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textScoreDescCategory: {
        fontSize: 13.5,
        color: 'rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
}
