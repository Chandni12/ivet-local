import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput } from 'react-native';
import CommunityScoreConstants from '../../constants/CommunityScore';
import commonStyles from '../../stylesheet/common/commonStyles.style';
//import styles from './styles/profileStyle.style';
import { checkError } from '../../utils'
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFeather from 'react-native-vector-icons/Feather';

import communityScoreConstants from '../../constants/CommunityScore';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ProfileConstants from '../../constants/Profile';
import EditIconView from '../../commonComponent/EditIconView';

import moment from 'moment';
import profileConstants from '../../constants/Profile';

export default class Education extends Component {
    constructor(props) {
        super(props);
    }
    onClickEditIcon = (data) => {
        console.log(" eidt ---")
        this.props.onClickAddOrEditEducationalDetails(false, data);

    }
    renderItem = (item, index) => {
        return (
            <View >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                    <IconFontAwesome5 name="university" size={30} />
                    <Text style={styles.textTitle}>{item.item.institute}</Text>
                    <IconFeather name="edit" style={{ fontSize: 20, color: 'rgb(51,51,51)' }} onPress={() => this.onClickEditIcon(item.item)} />
                </View>

                <View style={{alignSelf:'center', alignItems:'center'}}>
                    <Text style={[styles.textSubTitle, {textAlign:'center'} ]}>{item.item.degree} {item.item.study_field}</Text>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textSubTitle}>{item.item.start_year}-{item.item.end_year}</Text>
                    </View>
                    <Text>{item.item.description}</Text>
                </View>

                <View style={styles.viewSingleLine} />
            </View>
        )
    }
    render() {
        const { user, data, onClickAddOrEditEducationalDetails, workExperienceArray, educationArray } = this.props;
        return (
            <View style={styles.viewBox}>
                <EditIconView title={profileConstants.EDUCATION} onClickAddOrEditEducationalDetails={onClickAddOrEditEducationalDetails} />
                <FlatList
                    data={educationArray}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
}

const styles = {
    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: '#b6c6d3',
        borderWidth: 1,
        shadowColor: '#b6c6d3',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        // alignItems: 'center',
    },
    viewSingleLine: {
        width: '95%',
        height: 1,
        backgroundColor: '#b6c6d3',
        alignSelf: 'center'
    },
    textTitle: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textSubTitle: {
        fontSize: 13.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textInLightBlue: {
        fontSize: 13.5,
        color: ' rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',

    },
}
