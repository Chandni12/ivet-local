import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput } from 'react-native';
import CommunityScoreConstants from '../../constants/CommunityScore';
import commonStyles from '../../stylesheet/common/commonStyles.style'
import { checkError } from '../../utils'
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import communityScoreConstants from '../../constants/CommunityScore';
import IconEntypo from 'react-native-vector-icons/Entypo';
import ProfileConstants from '../../constants/Profile';
import { IMAGE_USER_PROFILE_PREFIX_URL } from '../../api/endPoints';
import profileConstants from '../../constants/Profile';



export default class User extends Component {
    constructor(props) {
        super(props);
       
    }

    render() {
        const { user, veterinarian_types,onClickEditIconUserView } = this.props;
        let userImageUrl = IMAGE_USER_PROFILE_PREFIX_URL + user.profile_image;
        let userImage
        // if (userImageUrl) {
        //     userImage = <Image style={[styles.userIcon, { marginTop: '-12%' }]} source={{ uri: userImageUrl }} />
        // } else
        {
            userImage = <Image style={[styles.userIcon, { marginTop: '-12%' }]} source={require('../../../assets/profilePhoto.png')} />
        }

        return (

            <View style={[styles.viewBox, { marginTop: '9%' }]}>
                <View style={{ flexDirection: 'row', flex:5 }}>
                    <View style={{ flex: 1.5 }} />

                    <View style={{ flex: 7 }}>
                        {userImage}
                    </View>

                    <View style={{ flex: 1.5, marginTop: '2%', marginRight: '2%' }}>
                        <View style={[[styles.viewIconPencil]]}>
                            <IconMaterial name="edit" style={{ fontSize: 20, color: 'rgb(12, 118,214)' }} onPress={()=> onClickEditIconUserView(true)}/>
                        </View>
                    </View>
                </View>

<View style={{flex:5}}>
<Text style={[styles.textTitle, {height:25}]}>{user.nick_name}</Text>
                {/* <Text style={styles.textSubTitle}>{ProfileConstants.SMALL_ANIMAL_CLINICIAN_DOCTOR}</Text> */}

                <View style={[styles.viewSingleLine, { marginTop: '2%', marginBottom: '2%' }]} />

                <Text style={[styles.textInLightBlue, {height:20}]}>{user.last_name} {user.first_name}</Text>
                <Text style={[styles.textInLightBlue, {height:20}]}>{user.email}</Text>

                <View style={{ flexDirection: 'row', alignItems:'center', marginBottom:'5%' }}>
                    <View style={[styles.viewBlueBox, {marginRight:'2%'}]}>
                        <Text style={styles.textInBlueBox}>  {ProfileConstants.IVET_TEACHER_STATUS}  </Text>
                    </View>
                   {user.role? <Text>{user.role.role_title}</Text> : null} 
                </View>

</View>
              

                {/* <View style={{ width: '90%' }}>
                    <View style={styles.viewRow}>
                        <View style={styles.viewBlueBox}>
                            <Text style={styles.textInBlueBox}>  {ProfileConstants.SPECIALTY_AREA}  </Text>
                        </View>
                        <Text style={styles.textValue}>{ProfileConstants.SURGERY}</Text>
                    </View>

                    <View style={styles.viewRow}>
                        <View style={styles.viewBlueBox}>
                            <Text style={styles.textInBlueBox}>  {ProfileConstants.QUALIFICATION}  </Text>
                        </View>
                        <Text style={styles.textValue}> {ProfileConstants.JAHA_JAPAN_ASSOCIATION} </Text>
                    </View>

                    <View style={styles.viewRow}>
                        <View style={styles.viewBlueBox}>
                            <Text style={styles.textInBlueBox}>  {ProfileConstants.ACADEMIC_SOCIETIES}  </Text>
                        </View>
                        <View>
                            <Text style={styles.textValue}>{ProfileConstants.JAPAN_SOCIATY_MEDICAL_CANCER}</Text>
                        </View>
                    </View>
                </View> */}
            </View>

        )
    }
}

const styles = {
    viewRow: {
        flexDirection: 'row',
        //width: '75%',
        // alignSelf: 'center',
        //justifyContent: 'space-between',
        marginTop: '2%',
        marginBottom: '2%',
        // backgroundColor: 'yellow',
        //flexWrap: 'wrap',
    },
    viewIconPencil: {
        width: 30,
        height: 30,
        borderRadius: 15,
        borderColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        borderWidth: 0.5,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 1,
            height: 1
        },
        marginRight: '10%',
        //position:'absolute',
        // marginTop:'5%',

    },
    viewBlueBox: {
        // borderWidth: 2,
        backgroundColor: 'rgb(50, 99, 200)',
        height: verticalScale(25),
        borderRadius: 5,
    },

    viewSingleLine: {
        width: '60%',
        height: 2,
        backgroundColor: '#b6c6d3',
    },

    userIcon: {
        width: 100,
        height: 100,
        borderColor: 'white',
        borderWidth: 5,
        borderRadius: 50,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        alignSelf: 'center',
    },
    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: '#b6c6d3',
        borderWidth: 0.5,
        shadowColor: '#b6c6d3',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        alignItems: 'center',
        // marginLeft:'5%'

    },

    textTitle: {
        fontSize: 22.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textSubTitle: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textInLightBlue: {
        fontSize: 13.5,
        color: ' rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textInBlueBox: {
        padding: 5,
        fontSize: 12,
        color: '#ffffff',
        fontFamily: 'NotoSansCJKjp-Medium',
        //flexWrap: 'wrap',
        // marginTop: '2%',
        // marginBottom: '2%',
        height: 20

        // marginLeft:'2%', 
        // marginRight: '2%',

    },
    textValue: {
        fontSize: 13.5,
        color: 'gray',
        fontFamily: 'NotoSansCJKjp-Medium',
        flexWrap: 'wrap',
        // backgroundColor:'red',
        marginLeft: '2%',
        marginRight: '35%'
    }
}