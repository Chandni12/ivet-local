export const styles = {
    
    viewPopup: {
        position: 'absolute',
        width: '25%',
        height: 70,
        marginTop: '7%',
        right: '5%',
        backgroundColor: 'white',
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        backgroundColr:'pink'
    },
    
    textTitleCategory: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textTitleProgressBar:{
        fontSize: 18,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        margin:'2%', 
        
    },
    textDescProgressBar:{
        fontSize: 15,
        color: ' rgba(97, 97, 97, 255);',
        fontFamily: 'NotoSansCJKjp-Medium',
        margin:'2%', 
    },

    textTitleScoreCategory: {
        fontSize: 22.5,
        color: 'rgba(11, 123, 217, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textScoreDescCategory: {
        fontSize: 13.5,
        color: 'rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    viewRow: {
        flexDirection: 'row',
        width: '80%',
        alignSelf: 'center',
        justifyContent: 'space-between',
        marginTop: '2%',
        marginBottom: '2%',
        // backgroundColor:'red',
        flexWrap: 'wrap',
    },
    viewIconPencil: {
        width: 30,
        height: 30,
        borderRadius: 15,
        borderColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        borderWidth: 0.5,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 1,
            height: 1
        },
        marginRight: '10%'
    },
    viewBlueBox: {
        // borderWidth: 2,
        backgroundColor: 'rgb(50, 99, 200)',
        height: 25,
    },
    textTitleCliniclNotes: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center',
    },
    viewSingleLine: {
        width: '60%',
        height: 2,
        backgroundColor: '#b6c6d3',
        alignSelf: 'center'
    },
    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: '#b6c6d3',
        borderWidth: 1,
        shadowColor: '#b6c6d3',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        // alignItems: 'center',
    },

    textTitle: {
        fontSize: 22.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textSubTitle: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textInLightBlue: {
        fontSize: 13.5,
        color: ' rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',

    },

    textPosition: {
        fontSize: 18.5,
        color: 'rgba(72, 72, 72, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    textPositionDesc: {
        fontSize: 14.5,
        color: 'rgba(72, 72, 72, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        marginBottom: '2%',
    },

    textPositionDesc2: {
        fontSize: 13.5,
        color: ' rgba(209, 209, 209, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        marginBottom: '2%',
    },
    textPositionDesc3: {
        fontSize: 13.5,
        color: 'rgba(72, 72, 72, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        marginBottom: '2%',
        // marginLeft: '7%'
    },
    viewBottomDesc3: {
        flexDirection: 'row',
        marginLeft: '5%'
    }
}
