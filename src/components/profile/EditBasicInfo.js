import React, { Component } from 'react';
import { Alert, View, TouchableOpacity, Image, Text, FlatList,ActivityIndicator, TextInput, ScrollView, Switch } from 'react-native';
import SimplePicker from 'react-native-simple-picker';
import { ScaledSheet, verticalScale, moderateScale, Button } from 'react-native-size-matters';
import * as Animatable from 'react-native-animatable';

// import commonStyles from '../../stylesheet/common/commonStyles.style';
// //import styles from './styles/profileStyle.style';
// import { checkError } from '../../../utils'
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import profileConstants from '../../constants/Profile';
import signupConstants from '../../constants/Signup';
import DatePicker from 'react-native-datepicker'
import moment from 'moment';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { SafeAreaView } from 'react-navigation';

export default class EditBasicInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            veterinarian_type: '',
            category: '',
            nickName: '',
            pickerSelected:''
        }
    }
    componentDidMount() {

    }
    handleVetenarianType = (option) => {

        this.setState({ veterinarian_type: option });
        let value = '';

        this.props.veterinarian_types_complete_data.map((data) => {

            if (String( data.veterinarian_type_name) === String(option)) {
                value = data.id;
            }
        })
        this.props.handleChangeBasicInfo('veterinarian_type', value);
    }

    handleCategoryClick = (option) => {
        this.setState({
            selectedCategory: option,
            isPickerSelected: true
        });
    }

    handleNickNameType = (option) => {
        this.props.callApiTogetNickName(option)
        this.setState({
            nickNameType: option,
            nickNameTypePickerSelected: true
        });
    }

    handleNickName = (name) => {
        this.props.callApiTosetNickName(name)
        this.setState({ pickerSelected: '', nickName: name, nickNameTypePickerSelected: false, isPickerSelected: false, nickNameType: '', selectedCategory: '' })
        Alert.alert(
            'Set Nick Name',
            'Do you want to set ' + name + ' as your nick name',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }

    openPicker = () => {
        this.setState({ pickerSelected: 'pickerCategory' })
    }

    returnTextfieldWithTitle(title, name, value) {
        return (
            <View style={{ marginLeft: '5%', marginTop: '5%', flex: 0.6 }}>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textTitle}>{title}</Text>
                    {this.props.errors[name] ?
                        <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" />
                        : null}
                </View>

                <TextInput
                    style={{}}
                    placeholder={title}
                    placeholderTextColor={'rgb(153, 153, 153)'}
                    style={[styles.rowText]}
                    value={value}
                    name={name}
                    onChangeText={val => this.props.handleChangeBasicInfo(name, val)}
                    autoCapitalize='none'
                />
            </View>
        )
    }

    render() {
console.log('this.state.nickNameTypePickerSelected',this.state.nickNameTypePickerSelected)
        const { callApiToUpdateProfile, veterinarian_type_name_display, veterinarian_types_name, veterinarian_types_complete_data, user, data, workExperience, fetching, onClickEditIconUserView, basicInfoData, handleChangeBasicInfo, errors, categories, nickNameType, callApiTogetNickName, nick_names, callApiTosetNickName } = this.props;
        return (
            <SafeAreaView>
                <View style={{ backgroundColor: 'white' }}>
                    <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>

                        <IconFontAwesome name="user-circle-o" size={30} style={{ marginLeft: '3%', flex: 1.5 }} />

                        <Text style={[styles.textTitle, { flex: 7, alignSelf: 'center', textAlign: 'center' }]}>{profileConstants.BASIC_INFO} </Text>

                        <TouchableOpacity style={{ marginRight: '1%', flex: 1.2 }} onPress={() => onClickEditIconUserView(false)} >
                            <Image source={require('../../../assets/cross1x.png')} style={{ width: 13, height: 13 }} />
                        </TouchableOpacity>

                    </View>
                    <View style={styles.singleLine} />
                </View>
                <KeyboardAwareScrollView style={{ backgroundColor: 'white' }}>

                    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                        {this.returnTextfieldWithTitle(signupConstants.LAST_NAME, 'last_name', basicInfoData['last_name'])}

                        {this.returnTextfieldWithTitle(signupConstants.FIRST_NAME, 'first_name', basicInfoData['first_name'])}
                    </View>

                    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                        {this.returnTextfieldWithTitle(signupConstants.LAST_KANA_NAME, 'last_kana_name', basicInfoData['last_kana_name'])}

                        {this.returnTextfieldWithTitle(signupConstants.FIRST_KANA_NAME, 'first_kana_name', basicInfoData['first_kana_name'])}
                    </View>

                    <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'space-between' }}>
                        {this.returnTextfieldWithTitle(profileConstants.NICK_NAME, 'nick_name', this.state.nickName)}
                        <Animatable.View>
                            <View style={[styles.viewSectionOpen, { marginTop: '10%', flex: 0.4 }]}>
                                <TouchableOpacity style={{ height: 50 }} onPress={() => { this.openPicker() }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <IconAntDesign name={"caretright"} style={{ marginLeft: '5%', color: 'rgba(51,51,51,255)', marginRight: '1%', marginTop: '2%' }} />
                                        <Text style={styles.basicInfoTitle} >{profileConstants.CHOOSE_NICK_NAME}</Text>
                                    </View>
                                </TouchableOpacity>

                                {this.state.pickerSelected === 'pickerCategory' &&
                                    <View style={{ marginBottom: '5%' }}>
                                        <View style={styles.viewInside}>
                                            <Text style={[styles.basicInfoValue]}>{profileConstants.SELECT_TYPE}</Text>

                                            <View style={[{ flex: 0.7 }]}>
                                                <TouchableOpacity onPress={() => { this.refs.pickerCategory.show() }} style={[styles.buttonHospital]}>
                                                    <Text style={[styles.rowTextPicker, { flex: 7 }]}>{this.state.selectedCategory}</Text>
                                                    <IconAntDesign name={"caretdown"} color={'gray'} size={17} style={{ flex: 1 }} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>}

                                {this.state.isPickerSelected ?
                                    <View style={{ marginBottom: '5%' }}>
                                        <View style={styles.viewInside}>
                                            <Text style={[styles.basicInfoValue]}>{profileConstants.SELECT_CATEGORY_TYPE}</Text>
                                            <View style={[{ flex: 0.7 }]}>
                                                <TouchableOpacity onPress={() => {
                                                    { this.refs.pickerCategoryType.show() }
                                                }} style={[styles.buttonHospital]}>
                                                    <Text style={[styles.rowTextPicker, { flex: 7 }]}>{this.state.nickNameType}</Text>
                                                    <IconAntDesign color={'gray'} size={17} name={"caretdown"} style={{ flex: 1 }} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View> : null}

                                {this.state.nickNameTypePickerSelected ?
                                    <View style={{ marginBottom: '5%' }}><View style={styles.viewInside}>
                                        <Text style={[styles.basicInfoValue]}>{profileConstants.NICK_NAME}</Text>
                                        <View style={[{ flex: 0.7 }]}>
                                            <TouchableOpacity onPress={() => {
                                                { this.refs.pickerNickName.show() }
                                            }} style={[styles.buttonHospital]}>
                                                <Text style={[styles.rowTextPicker, { flex: 7 }]}>{this.props.selectedNickName}</Text>
                                                <IconAntDesign color={'gray'} size={17} name={"caretdown"} style={{ flex: 1 }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    </View> : null}

                                <SimplePicker
                                    ref={'pickerCategory'}
                                    cancelText={signupConstants.CANCEL}
                                    confirmText={signupConstants.CONFIRM}
                                    options={categories}
                                    onSubmit={(option) => {
                                        this.handleCategoryClick(option);
                                    }}
                                />
                                {this.state.isPickerSelected && <SimplePicker
                                    ref={'pickerCategoryType'}
                                    cancelText={signupConstants.CANCEL}
                                    confirmText={signupConstants.CONFIRM}
                                    options={nickNameType}
                                    onSubmit={(option) => {
                                        this.handleNickNameType(option)
                                    }}
                                />}
                                {nick_names ? <SimplePicker
                                    ref={'pickerNickName'}
                                    cancelText={signupConstants.CANCEL}
                                    confirmText={signupConstants.CONFIRM}
                                    options={nick_names}
                                    onSubmit={(option) => {
                                        this.handleNickName(option)
                                    }}
                                /> : null
                                }
                            </View>
                        </Animatable.View>
                    </View>


                    <View style={{ marginLeft: '5%', marginTop: '5%' }}>

                        <View style={{ flexDirection: 'row', marginBottom: '5%' }}>
                            <Text style={{}}>{profileConstants.VETENARIAN_TYPE}</Text>

                            {errors['veterinarian_type'] ? <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" /> : null}

                        </View>
                        <View style={styles.viewInside}>
                            <TouchableOpacity style={[styles.buttonHospital]} onPress={() => this.refs.pickerUserType.show()}>
                                <Text style={[styles.rowTextPicker, { flex: 9 }]}>{this.state.veterinarian_type != '' ? this.state.veterinarian_type : veterinarian_type_name_display}</Text>
                                <IconAntDesign name="caretdown" color={'gray'} size={17} style={{ flex: 1 }} />
                            </TouchableOpacity>

                        </View>
                    </View>


                    <View style={{ marginLeft: '5%', marginTop: '5%' }}>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.textTitle}>{profileConstants.EMAIL_ADDRESS}</Text>
                            {this.props.errors['email'] ? <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" /> : null}
                        </View>
                        <TextInput
                            placeholder={profileConstants.EMAIL_ADDRESS}
                            placeholderTextColor={'rgb(153, 153, 153)'}
                            style={[styles.rowText, { width: '83%' }]}
                            value={basicInfoData['email']}
                            name={'email'}
                            keyboardType='email-address'
                            onChangeText={val => handleChangeBasicInfo('email', val)}
                        />
                    </View>

                    <View style={{ marginLeft: '5%', marginTop: '5%' }}>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.textTitle}>{profileConstants.PHONE_NUMBER}</Text>
                            {this.props.errors['phone_number'] ? <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" /> : null}
                        </View>

                        <TextInput
                            placeholder={profileConstants.PHONE_NUMBER}
                            placeholderTextColor={'rgb(153, 153, 153)'}
                            style={[styles.rowText, { width: '83%' }]}
                            value={basicInfoData['phone_number']}
                            name={'phone_number'}
                            keyboardType='phone-pad'
                            onChangeText={val => handleChangeBasicInfo('phone_number', val)}
                        />
                    </View>

                    <View style={{ marginLeft: '5%', marginTop: '5%' }}>

                        <View style={{ flexDirection: 'row', marginBottom: '5%' }}>
                            <Text style={{}}>{profileConstants.BIRTH_DAY}</Text>
                            {errors['dob'] ? <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" /> : null}

                        </View>
                        <View style={styles.viewInside}>

                            <DatePicker
                                style={{ width: 250 }}
                                date={basicInfoData['dob']}
                                mode="date"
                                placeholder={profileConstants.BIRTH_DAY}
                                format="YYYY-MM-DD"
                                minDate="2000-05-01"
                                // maxDate="2016-06-01"
                                confirmBtnText={profileConstants.CONFIRM}
                                cancelBtnText={profileConstants.CANCEL}
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        // marginLeft: 36,
                                        borderColor: 'gray',
                                        borderWidth: 1,
                                        borderRadius: 5,

                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => { this.props.handleChangeBasicInfo('dob', date) }}
                            />

                            {/* <TouchableOpacity style={[styles.buttonHospital]} onPress={() => }>
                                <Text style={[styles.rowTextPicker, { flex: 9 }]}>{basicInfoData['dob']}</Text>
                                <IconAntDesign name="caretdown" color={'gray'} size={17} style={{ flex: 1 }} />
                            </TouchableOpacity> */}

                        </View>
                    </View>

                    <View style={{ marginBottom: '5%', marginTop: '10%' }}>
                        {fetching ?
                            <TouchableOpacity style={[styles.button, { justifyContent: 'center' }]} >
                                <View style={[styles.buttonViewBlue, { justifyContent: 'center' }]}>
                                    <Text style={styles.buttonText}>{profileConstants.SAVE}</Text>
                                </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={[styles.button, {}]} onPress={callApiToUpdateProfile}>
                                <View style={[styles.buttonViewBlue, {}]}>
                                    <Text style={styles.buttonText}>{profileConstants.SAVE}</Text>
                                </View>
                            </TouchableOpacity>}
                    </View>
                    <View style={{ marginBottom: '5%', marginTop: '5%' }} />
                </KeyboardAwareScrollView>
                <SimplePicker
                    ref={'pickerUserType'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={veterinarian_types_name}
                    onSubmit={(option) => {
                        this.handleVetenarianType(option);
                    }}
                />

            </SafeAreaView>

        )
    }
}

const styles = {
    viewInside: {

        justifyContent: 'space-between',

        marginRight: '4%',
        flexDirection: 'row',
        // alignItems: 'center'
    },
    emailText: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',
        marginLeft: '1.3%',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    viewMiddle: {
        height: 45,
        // justifyContent: 'center',
        backgroundColor: 'pink'

    },
    textCurrentlyWorking: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '2%',
        height: 40
    },
    buttonHospital: {
        borderColor: 'gray',
        borderWidth: 1,
        height: 45,
        alignItems: 'center',
        width: '83%',
        flexDirection: 'row',
        borderRadius: 5
    },
    buttonPicker: {
        borderColor: 'gray',
        borderWidth: 1,
        height: 45,
        alignItems: 'center',
        // width: '100%',
        flexDirection: 'row',
        borderRadius: 5
    },
    viewInside: {

        justifyContent: 'space-between',

        marginRight: '4%',
        flexDirection: 'row',
        // alignItems: 'center'
    },
    viewInsidePicker: {

        justifyContent: 'space-between',
        // marginLeft: '6%',
        //  marginRight: '4%',
        flexDirection: 'row',
        // alignItems: 'center'
    },
    rowTextPicker: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '2%',
        paddingRight: '2%',
        // marginLeft: '1.3%',
        //  borderColor: 'gray',
        // borderWidth: 1,
        // height: 45,
        // marginTop: '5%',
        // width: '80%',
        // padding: '2%',
        borderRadius: 5
    },
    rowText: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',

        fontFamily: 'NotoSansCJKjp-Medium',
        borderColor: 'gray',
        borderWidth: 1,
        height: 45,
        marginTop: '5%',
        width: '80%',
        padding: '2%',
        borderRadius: 5
    },
    buttonViewBlue: {
        width: '40%',
        backgroundColor: 'rgb(3, 132, 255)',
        borderRadius: 5,
        height: 35,
        alignItems: 'center',
        // autoContent: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    descriptionBox: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        borderColor: 'gray',
        borderWidth: 1,
        height: 145,
        marginTop: '5%',
        width: '80%',
        padding: '2%',
        borderRadius: 5,
        textAlign: '',
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'gray'
    },

    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: '#b6c6d3',
        borderWidth: 1,
        shadowColor: '#b6c6d3',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        // alignItems: 'center',
    },
    textTitle: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        // alignSelf: 'center'
    },
    textSubTitle: {
        fontSize: 13.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textInLightBlue: {
        fontSize: 13.5,
        color: ' rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',

    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        //padding: 10,
        textAlign: 'auto',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    viewSectionOpen: {
        backgroundColor: '#f7f8fa',
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
    },
    basicInfoTitle: {
        fontSize: 16,
        color: 'rgba(51,51,51,255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoValue: {
        fontSize: 12,
        color: 'rgba(51,51,51,255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        flex: 0.3,
        paddingLeft: '5%',

    }
}
