import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput, ScrollView, Switch, ActivityIndicator } from 'react-native';
import SimplePicker from 'react-native-simple-picker';
import { ScaledSheet, verticalScale, moderateScale, Button } from 'react-native-size-matters';
import DatePicker from 'react-native-datepicker'

// import CommunityScoreConstants from '../../constants/CommunityScore';
// import commonStyles from '../../stylesheet/common/commonStyles.style';
// //import styles from './styles/profileStyle.style';
// import { checkError } from '../../../utils'
// import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
// import IconFeather from 'react-native-vector-icons/Feather';
// import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

// import communityScoreConstants from '../../constants/CommunityScore';

import profileConstants from '../../constants/Profile';
import signupConstants from '../../constants/Signup';

// import EditIconView from '../../commonComponent/EditIconView';

import { SafeAreaView } from 'react-navigation';

export default class EditWorkExperience extends Component {
    constructor(props) {
        super(props);
 this.state={
     isSwitchOn: false
 }
    }

    returnPickerTextFieldWithTitle(title, name, value) {

        return (
            <View style={{ marginTop: '5%', flex: 0.6 }}>

                <View style={{ flexDirection: 'row', marginBottom: '5%' }}>
                    <Text style={{ marginBottom: '5%' }}>{title}</Text>

                    {this.props.errors[name] ? <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" /> : null}

                </View>
                <View style={[styles.viewInside]}>
                    <TouchableOpacity style={[styles.buttonPicker, { flex: 8, marginRight: '4%' }]} onPress={() => {
                        name === 'start_year' ? this.refs.pickerStartYear.show() : name === 'end_year' ? this.refs.pickerEndYear.show() : name === 'start_month' ? this.refs.pickerStartMonth.show() : this.refs.pickerEndMonth.show()
                    }}>
                        <Text style={[styles.rowTextPicker, { flex: 8 }]}>{value}</Text>
                        <IconAntDesign name="caretdown" color={'gray'} size={17} style={{ flex: 2 }} />
                    </TouchableOpacity>

                </View>
            </View>
        )
    }

    returnTextfieldWithTitle(title, name, value) {
        return (
            <View style={{ marginLeft: '5%', marginTop: '5%' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textTitle}>{title}</Text>
                    {this.props.errors[name] ?
                        <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" />
                        : null}
                </View>
                <TextInput
                    style={{}}
                    placeholder={title}
                    placeholderTextColor={'rgb(153, 153, 153)'}
                    style={[styles.rowText]}
                    value={value}
                    name={name}
                    onChangeText={val => this.props.handleChangeWorkExp(name, val)}
                    autoCapitalize='none'
                />
            </View>
        )
    }

    handleUserType = (name, value) => {

        this.props.handleChangeWorkExp(name, value);
    }

    toggleSwitch = (value)=>{
        let dataSend = '';
        if (value) {
            dataSend =  'Y';
         } else {
            dataSend =  'N';
         }
        this.props.handleChangeWorkExp('public', dataSend);
    }

    render() {
        const { user, data, workExperience, fetching, errors, handleChangeWorkExp, yearList, monthList, closeWorkExpPopup } = this.props;

        var switchValue = false;
        if (workExperience['public'] === 'Y') {
            switchValue = true
        } else {
            switchValue = false
        }

        return (
            <SafeAreaView>
                <View style={{ backgroundColor: 'white' }}>
                    <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>

                        <IconFontAwesome name="black-tie" size={30} style={{ marginLeft: '3%', flex: 1.5 }} />

                        <Text style={[styles.textTitle, { flex: 7, alignSelf: 'center', textAlign: 'center' }]}>{profileConstants.WORK_EXPERIENCE} </Text>

                        <TouchableOpacity style={{ marginRight: '1%', flex: 1.2 }} onPress={closeWorkExpPopup} >
                            <Image source={require('../../../assets/cross1x.png')} style={{ width: 13, height: 13 }} />
                        </TouchableOpacity>

                    </View>
                    <View style={styles.singleLine} />
                </View>
                <KeyboardAwareScrollView style={{ backgroundColor: 'white' }}>

                    {this.returnTextfieldWithTitle(profileConstants.POSITION_TITLE, 'title', workExperience['title'])}

                    {this.returnTextfieldWithTitle(profileConstants.THE_NAME_OF_HOSPITAL, 'clinic_name', workExperience['clinic_name'])}


                    {/* <View style={{ marginLeft: '5%', marginTop: '5%' }}>

                        <View style={{ flexDirection: 'row', marginBottom: '5%' }}>
                            <Text style={{}}>{profileConstants.HOSPITAL_ADDRESS}</Text>
                            {errors['hospitalAddress'] ? <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" /> : null}
                        </View>

                        <View style={styles.viewInside}>
                            <TouchableOpacity style={[styles.buttonHospital]} onPress={() => this.refs.pickerUserType.show()}>
                                <Text style={[styles.rowTextPicker, { flex: 9 }]}>{profileConstants.HOSPITAL_ADDRESS}</Text>
                                <IconAntDesign name="caretdown" color={'gray'} size={17} style={{ flex: 1 }} />
                            </TouchableOpacity>

                        </View>
                    </View> */}

                    <View style={{ flexDirection: 'row', height: 45, marginLeft: '5%', width: '80%', marginTop: '5%' }}>
                        {workExperience['currently_working'] === 'Y' ?
                            <IconMaterial name="check-box" color='gray' size={20} style={{ marginRight: '2%', height: 40, paddingTop: '5%' }} onPress={()=> handleChangeWorkExp('currently_working', 'N')}/> :
                            <IconMaterial name="check-box-outline-blank" color='gray' size={20} style={{ marginRight: '2%', height: 40, paddingTop: '5%' }} onPress={()=> handleChangeWorkExp('currently_working', 'Y')}/>}

                        <Text style={[styles.textCurrentlyWorking, { paddingTop: '5%' }]}>{profileConstants.CURRENTLY_WORKING_AT_HOSPITAL}</Text>
                    </View>

                    <View style={{ marginLeft: '5%', marginRight: '5%', flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>

                        {this.returnPickerTextFieldWithTitle(profileConstants.START_YEAR, 'start_year', workExperience['start_year'])}

                        {this.returnPickerTextFieldWithTitle(profileConstants.END_YEAR, 'end_year', workExperience['end_year'])}
                    </View>

                    <View style={{ marginLeft: '5%', marginRight: '5%', flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>

                        {this.returnPickerTextFieldWithTitle(profileConstants.START_MONTH, 'start_month', workExperience['start_month'])}

                        {this.returnPickerTextFieldWithTitle(profileConstants.END_MONTH, 'end_month', workExperience['end_month'])}

                    </View>


                    <View style={{ marginBottom: '5%', marginTop: '5%', marginLeft: '5%' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{}}>{profileConstants.JOB_DESCRIPTION}</Text>
                            {errors['jobTitle'] ? <IconMaterial style={[{ fontSize: 25, color: '#d61515', marginLeft: '2%', marginTop: '-1%' }]} name="error-outline" /> : null}
                        </View>

                        <TextInput
                            placeholder={profileConstants.JOB_DESCRIPTION}
                            placeholderTextColor={'rgb(153, 153, 153)'}
                            style={[styles.descriptionBox]}
                            value={workExperience['description']}
                            name={'description'}
                            multiline={true}
                            onChangeText={val => handleChangeWorkExp('description', val)}
                            autoCapitalize='none'
                        />
                        <Text style={{ marginTop: '5%', marginBottom: '5%' }}>{profileConstants.PUBLISH_INFO_WITH_IVET_TEACHER}</Text>

                        <Switch value={switchValue} onValueChange={this.toggleSwitch}/>

                    </View>
                    <View style={{ marginBottom: '5%', marginTop: '5%' }}>
                        {fetching ?
                            <TouchableOpacity style={[styles.button, { justifyContent: 'center' }]} >
                                <View style={[styles.buttonViewBlue, { justifyContent: 'center' }]}>
                                <Text style={styles.buttonText}>{profileConstants.SAVE}</Text>
                                </View>
                            </TouchableOpacity>

                            :
                            <TouchableOpacity style={[styles.button, {}]} onPress={this.props.callApiToUpdateWorkExperience}>
                                <View style={[styles.buttonViewBlue, {}]}>
                                    <Text style={styles.buttonText}>{profileConstants.SAVE}</Text>
                                </View>
                            </TouchableOpacity>}
                    </View>
                    <View style={{ marginBottom: '5%', marginTop: '5%' }} />
                </KeyboardAwareScrollView>
                <SimplePicker
                    ref={'pickerStartYear'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={yearList}
                    onSubmit={(option) => {
                        this.handleUserType('start_year', option);
                    }}
                />
                <SimplePicker
                    ref={'pickerEndYear'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={yearList}
                    onSubmit={(option) => {
                        this.handleUserType('end_year', option);
                    }}
                />
                <SimplePicker
                    ref={'pickerStartMonth'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={monthList}
                    onSubmit={(option) => {
                        this.handleUserType('start_month', option);
                    }}
                />
                <SimplePicker
                    ref={'pickerEndMonth'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={monthList}
                    onSubmit={(option) => {
                        this.handleUserType('end_month', option);
                    }}
                />
            </SafeAreaView>

        )
    }
    returnPickerTextFieldWithTitle1(title, errors) {
        return (
            <View style={{ marginLeft: '5%', marginTop: '5%' }}>
                <Text style={{ marginBottom: '5%' }}>{title}</Text>
                <View style={styles.viewMiddle}>
                    <TouchableOpacity style={{}} onPress={() => this.refs.pickerUserType.show()}>

                        <View style={styles.viewInside}>

                            <Text style={[styles.rowTextPicker]}>{title}</Text>

                            <IconMaterial style={[{ marginTop: moderateScale(2, 0.1), fontSize: 25, color: '#d61515' }]} name="error-outline" />

                            {/* {this.state.userType === '' ? <Text style={[styles.rowText]}>{signupConstants.SELECT_AN_ITEM}</Text>
                                : <Text style={[styles.rowText]}>{this.state.userType}</Text>
                            }
                            {errors[fieldName] ? <IconMaterial style={[{ marginTop: moderateScale(2, 0.1), fontSize: 25, color: '#d61515' }]} name="error-outline" />
                                : null
                            } */}
                        </View>
                        {/* {
                            errors[fieldName] ? <Animatable.Text animation="fadeIn" style={{ marginLeft: '2%', marginBottom: '2%', color: '#d61515' }} > {errors[fieldName]}</Animatable.Text> : null
                        } */}
                    </TouchableOpacity>
                </View>

            </View>
        )
    }

}

const styles = {
    viewMiddle: {
        height: 45,
        // justifyContent: 'center',
        backgroundColor: 'pink'

    },
    textCurrentlyWorking: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '2%',
        height: 40
    },
    buttonHospital: {
        borderColor: 'gray',
        borderWidth: 1,
        height: 45,
        alignItems: 'center',
        width: '83%',
        flexDirection: 'row',
        borderRadius: 5
    },
    buttonPicker: {
        borderColor: 'gray',
        borderWidth: 1,
        height: 45,
        alignItems: 'center',
        // width: '100%',
        flexDirection: 'row',
        borderRadius: 5
    },
    viewInside: {

        justifyContent: 'space-between',

        marginRight: '4%',
        flexDirection: 'row',
        // alignItems: 'center'
    },
    viewInsidePicker: {

        justifyContent: 'space-between',
        // marginLeft: '6%',
        //  marginRight: '4%',
        flexDirection: 'row',
        // alignItems: 'center'
    },
    rowTextPicker: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '2%',
        paddingRight: '2%',
        // marginLeft: '1.3%',
        //  borderColor: 'gray',
        // borderWidth: 1,
        // height: 45,
        // marginTop: '5%',
        // width: '80%',
        // padding: '2%',
        borderRadius: 5
    },
    rowText: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',

        fontFamily: 'NotoSansCJKjp-Medium',
        borderColor: 'gray',
        borderWidth: 1,
        height: 45,
        marginTop: '5%',
        width: '80%',
        padding: '2%',
        borderRadius: 5
    },
    buttonViewBlue: {
        width: '40%',
        backgroundColor: 'rgb(3, 132, 255)',
        borderRadius: 5,
        height: 35,
        alignItems: 'center',
        // autoContent: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    descriptionBox: {
        fontSize: 14.5,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        borderColor: 'gray',
        borderWidth: 1,
        height: 145,
        marginTop: '5%',
        width: '80%',
        padding: '2%',
        borderRadius: 5,
        textAlign: '',
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'gray'
    },

    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: '#b6c6d3',
        borderWidth: 1,
        shadowColor: '#b6c6d3',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        // alignItems: 'center',
    },
    textTitle: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        // alignSelf: 'center'
    },
    textSubTitle: {
        fontSize: 13.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textInLightBlue: {
        fontSize: 13.5,
        color: ' rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',

    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        //padding: 10,
        textAlign: 'auto',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
}
