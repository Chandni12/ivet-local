import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, Dimensions, TextInput, SafeAreaView, Modal } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import IconEvilIcons from 'react-native-vector-icons/EvilIcons';

import othersConstants from '../constants/Others';
import * as Animatable from 'react-native-animatable';
import BottomDrawer from 'rn-bottom-drawer';
import { ScrollView } from 'react-native-gesture-handler';
const TAB_BAR_HEIGHT = 70;
const SCREEN_HEIGHT = Dimensions.get('window').height;



export default class Others extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: true,
            selectedIndex: '',
            startUp: 0
        }
    }

    fadeInUpView = () => this.refs.tab1.fadeInUpBig(250).then(endState => endState.finished ? this.setState({ startUp: 1 }) : 'Slide down cancelled');

    fadeInDownView = () => this.refs.tab1.fadeOutDownBig(920).then(endState => endState.finished ? 'fade in up' : 'Slide down cancelled');

    componentDidMount() {
        this.fadeInUpView()

    }

    closeView = (viewName, selectedViewIndex) => {

        this.setState({ selectedIndex: selectedViewIndex });

        //this.fadeInDownView();

        if (viewName === 'postMenu') {
            this.props.showHidePopupView('postMenu');
            return;
        }
        if (viewName !== '') {

            console.log(" view name ---", viewName);
            if (viewName === othersConstants.COLLECTION) {

                dataToPass = { 'viewName': othersConstants.COLLECTION, auth_token: this.props.navigation.state.params.auth_token }
                this.props.navigation.navigate('ViewWithDropDownRow', { auth_token: this.props.navigation.state.params.auth_token });
                return;
            } else
                this.props.navigation.navigate(viewName, { auth_token: this.props.navigation.state.params.auth_token });
        }

    }
    // moveToPostMenu = () =>{
    //     console.log('moveToPostMenu');

    //    // this.props.navigation.navigate('Dashboard');
    //     this.props.showHidePopupView('postMenu')
    // }

    renderContent = () => {
        return (
            <Animatable.View ref={'tab1'} style={[styles.viewAnimatable]}>
                {/* <View style={{ width: '100%', height: '100%' }}>
                    <View style={styles.viewInside}> */}

                <View style={styles.viewSingleLineSmall} />

                <Text style={styles.textTitle}>{othersConstants.OTHER_MENU}</Text>

                <View style={styles.viewSingleLine} />

                <View style={styles.viewRow}>
                    <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('notification', 0)}>
                        <IconSimpleLineIcons name="bell" style={this.state.selectedIndex === 0 ? styles.iconSelected : styles.iconNormal} />
                        <Text style={this.state.selectedIndex === 0 ? styles.textSelected : styles.textNormal}>{othersConstants.INFORMATION}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.viewRow}>
                    <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView(othersConstants.COLLECTION, 1)}>
                        <IconSimpleLineIcons name="star" style={this.state.selectedIndex === 1 ? styles.iconSelected : styles.iconNormal} />
                        <Text style={this.state.selectedIndex === 1 ? styles.textSelected : styles.textNormal}>{othersConstants.COLLECTION}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.viewRow}>
                    <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('Tags', 2)}>
                        <IconEvilIcons name="user" style={this.state.selectedIndex === 2 ? styles.iconSelectedUser : styles.iconNormalUser} />
                        <Text style={this.state.selectedIndex === 2 ? styles.textSelected : styles.textNormal}>{othersConstants.FOLLOWING_TAGS}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.viewRow}>
                    <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('postMenu', 3)}>
                        <IconFontAwesome name="edit" style={this.state.selectedIndex === 3 ? styles.iconSelected : styles.iconNormal} />
                        <Text style={this.state.selectedIndex === 3 ? styles.textSelected : styles.textNormal}>{othersConstants.POSTING_MENU}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.viewRow}>
                    <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('', 4)}>
                        <IconSimpleLineIcons name="question" style={this.state.selectedIndex === 4 ? styles.iconSelected : styles.iconNormal} />
                        <Text style={this.state.selectedIndex === 4 ? styles.textSelected : styles.textNormal}>{othersConstants.HELP}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.viewRow}>
                    <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('profile', 5)}>
                        <IconSimpleLineIcons name="settings" style={this.state.selectedIndex === 5 ? styles.iconSelected : styles.iconNormal} />
                        <Text style={this.state.selectedIndex === 5 ? styles.textSelected : styles.textNormal}>{othersConstants.SETTINGS}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.viewSingleLine} />

                <Text style={styles.textTermsCondition}>{othersConstants.TERMS_OF_USE_AND_PRIVACY}</Text>

                {/* </View>
                </View> */}
            </Animatable.View>

        )
    }

    onClosedDrawer = () => {
        console.log(" onClosedDrawer");

        setTimeout(() => {
            this.props.showHidePopupView('');
        }, 300);

    }

    render1() {
        return (
            <View style={{ backgroundColor: 'pink', height: 400, marginTop: 400 }}>
                <Animatable.View ref={'tab1'} style={[styles.viewAnimatable]}>
                    {/* <View style={{ width: '100%', height: '100%' }}>
                    <View style={styles.viewInside}> */}

                    <View style={styles.viewSingleLineSmall} />

                    <Text style={styles.textTitle}>{othersConstants.OTHER_MENU}</Text>

                    <View style={styles.viewSingleLine} />

                    <View style={styles.viewRow}>
                        <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('notification', 0)}>
                            <IconSimpleLineIcons name="bell" style={this.state.selectedIndex === 0 ? styles.iconSelected : styles.iconNormal} />
                            <Text style={this.state.selectedIndex === 0 ? styles.textSelected : styles.textNormal}>{othersConstants.INFORMATION}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.viewRow}>
                        <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView(othersConstants.COLLECTION, 1)}>
                            <IconSimpleLineIcons name="star" style={this.state.selectedIndex === 1 ? styles.iconSelected : styles.iconNormal} />
                            <Text style={this.state.selectedIndex === 1 ? styles.textSelected : styles.textNormal}>{othersConstants.COLLECTION}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.viewRow}>
                        <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('Tags', 2)}>
                            <IconSimpleLineIcons name="user" style={this.state.selectedIndex === 2 ? styles.iconSelected : styles.iconNormal} />
                            <Text style={this.state.selectedIndex === 2 ? styles.textSelected : styles.textNormal}>{othersConstants.FOLLOWING_TAGS}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.viewRow}>
                        <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('postMenu', 3)}>
                            <IconFontAwesome name="edit" style={this.state.selectedIndex === 3 ? styles.iconSelected : styles.iconNormal} />
                            <Text style={this.state.selectedIndex === 3 ? styles.textSelected : styles.textNormal}>{othersConstants.POSTING_MENU}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.viewRow}>
                        <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('', 4)}>
                            <IconSimpleLineIcons name="question" style={this.state.selectedIndex === 4 ? styles.iconSelected : styles.iconNormal} />
                            <Text style={this.state.selectedIndex === 4 ? styles.textSelected : styles.textNormal}>{othersConstants.HELP}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.viewRow}>
                        <TouchableOpacity style={styles.buttonRow} onPress={() => this.closeView('profile', 5)}>
                            <IconSimpleLineIcons name="settings" style={this.state.selectedIndex === 5 ? styles.iconSelected : styles.iconNormal} />
                            <Text style={this.state.selectedIndex === 5 ? styles.textSelected : styles.textNormal}>{othersConstants.SETTINGS}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.viewSingleLine} />

                    <Text style={styles.textTermsCondition}>{othersConstants.TERMS_OF_USE_AND_PRIVACY}</Text>

                    {/* </View>
                </View> */}
                </Animatable.View>
            </View>
        )
    }

    render() {
        console.log(" start up ---", this.state.startUp);
        return (
            <BottomDrawer
                containerHeight={SCREEN_HEIGHT - (SCREEN_HEIGHT / 2 - 30)} //{SCREEN_HEIGHT-450} // Need to give device specific
                offset={TAB_BAR_HEIGHT}
                startUp={this.state.startUp}
                downDisplay={44}
                roundedEdges={true}
                shadow={false}
                backgroundColor='white'
                onCollapsed={() => this.onClosedDrawer()}
            >
                {this.renderContent()}
            </BottomDrawer>
        )
    }

}

const styles = {
    iconNormal: {
        // marginTop: '1%',
        marginRight: '3%',
        // width: 12,
        // height: 19,
        color: 'rgba(88, 88, 88, 255)',
        fontSize: 27
    },
    iconSelected: {
        marginRight: '3%',
        color: 'rgb(2,132, 255)',
        fontSize: 25,
    },
    iconNormalUser: {
        // marginTop: '1%',
        right: 5,
        // width: 12,
        // height: 19,
        color: 'rgba(88, 88, 88, 255)',
        fontSize: 38
    },
    iconSelectedUser: {
        right: 5,
        color: 'rgb(2,132, 255)',
        fontSize: 38,
    },

    textNormal: {
        fontSize: 14.5,
        color: 'rgba(88, 88, 88, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },

    textSelected: {
        // marginRight: '7%',
        fontSize: 14.5,
        color: 'rgb(2,132, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },

    textTitleComingSoon: {
        fontSize: 19,
        color: 'rgb(2, 132, 254)',
        fontFamily: 'NotoSansCJKjp-Regular',
        alignSelf: 'center',
        marginTop: '23%',
        marginBottom: '3%'
    },
    viewAnimatable: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        width: '95%',
        alignSelf: 'center',
        backgroundColor: 'white',
        bottom: 0,
    },
    viewInside: {
        // height: '60%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        width: '95%',
        alignSelf: 'center',
        backgroundColor: 'white',
        bottom: 0,
        // position: 'absolute'
    },
    viewSingleLine: {
        alignSelf: 'center',
        backgroundColor: '#e5e5e5',
        height: 0.8,
        width: '100%'

    },
    viewSingleLineSmall: {
        alignSelf: 'center',
        backgroundColor: '#d3d3d3',
        height: 3,
        width: '8%',
        marginTop: '1%',
        borderRadius:10
       
    },


    viewRow: {
        // flexDirection: 'row',
        marginLeft: '5%',
        justifyContent: 'center',
        //marginRight: '2%',
        paddingTop: '2%',
        paddingBottom: '2%',
    },
    buttonRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textTitle: {
        fontSize: 19,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        alignSelf: 'center',
        marginTop: '3.6%',
        marginBottom: '3%'
    },

    textTermsCondition: {
        fontSize: 14,
        color: 'rgba(73, 73, 73, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        alignSelf: 'center',
        marginTop: '5%',
        marginBottom: '3%',
    }

}