import React, { Component } from 'react';
import { View, FlatList, Text, CheckBox, Image, TouchableOpacity, Button, ActivityIndicator } from 'react-native';
import TagsConstants from '../constants/Tags'
import dashboardConstants from '../constants/Dashboard';
import ProfileConstants from '../constants/Profile';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import commonStyles from '../stylesheet/common/commonStyles.style'
import TableRow from '../commonComponent/TableRow';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
import HeaderThreeOptions from '../commonComponent/HeaderThreeOptions';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { TextInput, ScrollView } from 'react-native-gesture-handler';

// import 'react-native-gesture-handler';


export default class Tags extends Component {
    constructor(props) {
        super(props);

    }

    renderItem = (item) => {
        let dict = item.item;
        let starView = <View />

        if (this.props.arraySelectedTags.indexOf(dict.id) === -1) {
            starView = <IconFontAwesome name={"star"} style={styles.starNotSelected} onPress={() => this.props.onClickStar(dict.id)} />
        } else {
            starView = <IconFontAwesome name={"star"} style={styles.starSelected} onPress={() => this.props.onClickStar(dict.id)} />
        }

        return (
            <View style={{ marginTop: '2%', width: '90%', marginBottom: '2%', marginLeft: '5%' }}>
                <View style={[styles.viewOuter, { flexDirection: 'row', justifyContent: 'space-between', padding: 15, alignItems: 'center' }]}>
                    <View style={[styles.viewInnerLightBlue,]}>
                        <Text style={styles.textInBlue}>{dict.title}</Text>
                    </View>

                    {/* Temporary */}
                    {/* {starView} */}

                    <IconFontAwesome name={"star"} style={dict.pivot ? styles.starSelected : styles.starNotSelected} onPress={() => this.props.onClickStar(dict.id)} />

                </View>
            </View>
        )
    }

    render() {
        const { onChangeSegmentedControl, onChangeSearchText, onClickFindOtherTag, searchText, selectedIndex, arrayTags, errors, fetching, data , onClickManageFollowingTag} = this.props;
        return (
            <SafeAreaView>
                <HeaderThreeOptions showRightButton={false} title={TagsConstants.FEED_TAG} onRightButtonPress={() => this.showPopup()} navigation={this.props.navigation} />
                <ScrollView >

               {data['isFindOtherTagClick']? <TouchableOpacity style={{ marginRight: '5%', marginBottom: '5%' }} onPress={onClickFindOtherTag}>
                    <Text style={styles.textFindTags}>{TagsConstants.FIND_OTHER_TAGS}</Text>
                </TouchableOpacity>: null}

               {data['isManageFollowingTags']? <TouchableOpacity style={{ marginRight: '5%', marginBottom: '5%' }} onPress={onClickManageFollowingTag}>
                    <Text style={styles.textFindTags}>{TagsConstants.MANAGE_FOLLOWING_TAGS}</Text>
        </TouchableOpacity> : null}

                <SegmentedControlTab
                    values={[dashboardConstants.CLINICAL, dashboardConstants.LIFE, dashboardConstants.HOTEL_MANAGEMENT]}
                    // selectedIndex={data['selectedSegmentIndex']}
                    selectedIndex={selectedIndex}
                    onTabPress={onChangeSegmentedControl}
                    //tabTextStyle={styles.textDesc}
                    tabsContainerStyle={{ marginLeft: '5%', marginRight: '5%' }}
                />
                <Text style={styles.textDescription}>{TagsConstants.TAGS_DESCRIPTION}</Text>
                <TextInput
                    placeholder={TagsConstants.SEARCH_TAGS}
                    placeholderTextColor={'rgb(153, 153, 153)'}
                    style={[styles.rowText]}
                    value={searchText}
                    name={'searchText'}
                    onChangeText={val => onChangeSearchText(name, val)}
                    autoCapitalize='none'
                />


                <View style={{ marginTop: '5%' }}>
                    <FlatList
                        data={arrayTags}
                        renderItem={this.renderItem}
                        scrollEnabled= {false}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                <View style={{marginBottom:'10%'}} />
                </ScrollView>

                {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                </View> : null}

            </SafeAreaView>
        )

    }
}

const styles = {
    rowText: {
        fontSize: 13.5,
        color: 'rgb(153, 153, 153)',

        fontFamily: 'NotoSansCJKjp-Medium',
        borderColor: 'lightgray',
        borderWidth: 1,
        height:40,
        marginLeft: '5%',
        marginRight: '5%',
        // alignSelf:'flex-end',
        width: '60%',
        padding:'2%',
        borderRadius: 5
    },
    textFindTags: {
        marginTop: '5%',
        fontSize: 14.5,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'flex-end',
        height: 20,
    },
    textDescription: {

        margin: '5%',
        fontSize: 12.5,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
        alignSelf: 'flex-end',
        textAlign: 'justify',
    },
    viewOuter: {
        backgroundColor: 'rgb(239, 243, 247)',
        borderRadius: 5
    },
    viewInnerLightBlue: {
        backgroundColor: 'rgb(216, 231, 249)',
        borderRadius: 20
    },
    textInBlue: {
        fontSize: 12.5,
        color: 'rgb(2, 132, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        padding: 10
    },
    starSelected: {
        color: '#ecbc02',
        fontSize: 25,
    },
    starNotSelected: {
        color: '#bcc8d4',
        fontSize: 25,
    }
}