import React, { Component } from 'react';
import { View, TouchableOpacity, Image, ImageBackground, Platform, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-navigation';
const backgroundBlack = require('../assets/background_black.png')
const iconInQuestion = require('../assets/in_question.png');
var {height, width} = Dimensions.get('window');

export default class InQuestion extends Component {
    render() {
        return (
            
                <View style={{ flex: 1 }}>
                    <ImageBackground source={backgroundBlack} style={{width:'100%', height:'100%', justifyContent:'center'}}>
                        <View style={{ alignSelf: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity >
                            <Image source={iconInQuestion} style={{width:width, height:100}}/>
                        </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
        )
    }
}