import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput, SafeAreaView, Modal } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import postMenuConstants from '../constants/PostMenu';
import othersConstants from '../constants/Others';
import * as Animatable from 'react-native-animatable';


export default class PostMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
            // This is for showing view 
            constPostQuestion: false,
            constPostNotes: false,
            constTopicsFollwing: false,
            constListOfMyPost: false,
            constListOfDraft: false,

            // This is for showing data of category/Post type   on the Dashboard 
            selectedIndexPost: '', 
            selectedIndexCategory:''

        }
        this.arrayPost = [{ 'title': postMenuConstants.POST_A_QUESTION, 'viewName': 'PostQuestionOrNotes', 'isQuestion': true }, { 'title': postMenuConstants.POST_CLINICAL_NOTES, 'viewName': 'PostQuestionOrNotes', 'isQuestion': false }, { 'title': postMenuConstants.TOPICS_YOU_ARE_FOLLOWING, 'viewName': 'follow', 'isQuestion': '' }, { 'title': postMenuConstants.LIST_OF_MY_POST, 'viewName': 'Dashboard', 'isQuestion': '' }, { 'title': postMenuConstants.LIST_OF_DRAFTS, 'viewName': '', 'isQuestion': '' }],

        this.arrayCategory = [{ 'type': postMenuConstants.POST_TYPE, 'title': postMenuConstants.Q_A_FORUM }, { 'type': postMenuConstants.POST_TYPE, 'title': postMenuConstants.CLINICAL_NOTES }, { 'type': postMenuConstants.POST_TYPE, 'title': postMenuConstants.TIME_LINE }, { 'type': postMenuConstants.POST_TYPE, 'title': postMenuConstants.TAG_FEED }, { 'type': postMenuConstants.CATEGORY, 'title': postMenuConstants.CLINICAL }, { 'type': postMenuConstants.CATEGORY, 'title': postMenuConstants.LIFE }, { 'type': postMenuConstants.CATEGORY, 'title': postMenuConstants.HOTEL_MANAGEMENT }]
    }


    fadeInUpView = () => this.refs.tab1.fadeInUpBig(920).then(endState => endState.finished ? 'fade in up' : 'Slide down cancelled');

    fadeInDownView = () => this.refs.tab1.fadeOutDownBig(920).then(endState => endState.finished ? 'fade in up' : 'Slide down cancelled');

    componentDidMount() {
        this.fadeInUpView()
    }


    returnText = (text1, text2) => {

        return (
            <View style={styles.viewRow}>
                <TouchableOpacity style={styles.buttonRow} onPress={() => this.props.postMenuOptionClick(text2, false)}>
                    <Text style={styles.textRowBottom}>{text1}: {text2}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    closeView = (viewName) => {
       
        this.fadeInDownView();
        setTimeout(() => {
            this.setState({
                modalVisible: false
            })
        }, 400);
        this.props.onClickPostMenu();
        if (viewName !== '') {
            this.props.navigation.navigate(viewName)
        }
    }

    onClickBack = () => {
        this.props.onClickBackInPostMenu();
    }

    onClickPost = (item) => {
        this.setState({ selectedIndexPost: item.index })
        let param = { 'isQuestion': item.item.isQuestion };
        this.props.postMenuOptionClick( item.item.viewName, item.item.title,param)

    }


    onClickCategory = (item) =>{
        this.setState({ selectedIndexCategory: item.index })
       
        if (item.index === 4 || item.index === 5 || item.index === 6) {
            this.props.postMenuOptionClick('Tags',true, item.item.title)
        }else{
            this.props.postMenuOptionClick(item.item.title, '', false)
        }

     
    }

    renderItemForPost = (item, index) => {
       
        return (
            <View style={styles.viewRow}>
                <TouchableOpacity style={[styles.buttonRow]} onPress={() => this.onClickPost(item)} >
                    <Image source={require('../../assets/menu-icons.png')} style={styles.iconRow} />

                    {item.index === this.state.selectedIndexPost ? <Text style={{ color: 'rgb(2, 132, 254)' }}>{item.item.title.trim()}</Text> :
                        <Text style={styles.textSubTitle}>{item.item.title.trim()}</Text>}
                </TouchableOpacity>
            </View>
        )
    }
    renderItemForCategory = (item, index) => {
        return (
            <View style={styles.viewRow}>
                <TouchableOpacity style={styles.buttonRow} onPress={() => this.onClickCategory(item)}>
                {item.index === this.state.selectedIndexCategory ?  
                <Text style={[styles.textRowBottom, {color: 'rgb(2, 132, 254)'}]}>{item.item.type}: {item.item.title}</Text> :
                
                <Text style={styles.textRowBottom}><Text style={{color:'#d3d3d3'}}>{item.item.type}:</Text>  {item.item.title}</Text>}
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const { postMenuOptionClick, selectedCategoryOption } = this.props;
        return (
            <Animatable.View ref={'tab1'} >

                <View style={{ width: '100%', height: '100%' }}>

                    <View style={[styles.viewInside]}>

                        <View style={styles.viewSingleLineSmall} />

                        <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{ flex: 1 }}>
                                <Image source={require('../../assets/back.png')} style={styles.backIcon} />
                            </View>

                            <View style={{ flex: 8 }}>
                                <TouchableOpacity onPress={() => this.closeView('')}>
                                    <Text style={styles.textTitle}>{postMenuConstants.POST_MENU}</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flex: 1 }} />
                        </View>

                        <View style={[styles.viewSingleLine, {marginBottom:'2%'} ]} />

                        <FlatList
                            data={this.arrayPost}
                            renderItem={this.renderItemForPost}
                            scrollEnabled={false}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        
                        <View style={[styles.viewSingleLine, {marginBottom:'2%'}]} />

                        <FlatList
                            data={this.arrayCategory}
                            renderItem={this.renderItemForCategory}
                            scrollEnabled={false}
                            keyExtractor={(item, index) => index.toString()}
                        />

                        <View style={{ marginBottom: '2%' }} />

                    </View>
                </View>


            </Animatable.View>
        )
    }
}

const styles = {
    container: {
        height: '100%',
    },
    testColor: {
        color: 'blue'
    },
    testColorblack: {
        color: 'rgba(88, 88, 88, 255)'
    },
    backIcon: {
        width: 22,
        height: 22,
        tintColor: 'black',
        marginLeft: 15
    },

    viewInside: {
        height: '65%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        width: '100%',
        alignSelf: 'center',
        backgroundColor: 'white',
        bottom: 50,
        position: 'absolute'
    },
    viewSingleLine: {
        alignSelf: 'center',
        backgroundColor: '#e5e5e5',
        height: 0.5,
        width: '100%'
    },
    viewSingleLineSmall: {
        alignSelf: 'center',
        backgroundColor: '#d3d3d3',
        height: 3,
        width: '8%',
        marginTop: '1%',
        borderRadius:10
    },
    iconRow: {

        // marginTop: '1%',
        marginRight: '7%',
        width: 13,
        height: 15,
    },
    viewRow: {
        flexDirection: 'row',
        marginLeft: '8%',
        alignItems: 'center',
        marginRight: '2%',
        paddingTop: '1%',
        paddingBottom: '1%',

    },
    buttonRow: {
        flexDirection: 'row',
    },
    textSubTitle:{
        fontSize: 14.5,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
       
    },
    textTitle: {
        fontSize: 17,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        alignSelf: 'center',
        marginTop: '4%',
        marginBottom: '3%',

    },
    textRowBottom: {
        fontSize: 14.5,
        color: 'rgba(88, 88, 88, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        paddingLeft: '5%',
        // marginTop: '2%',
        // marginBottom: '2%',
        // paddingTop:'1%',
        // paddingBottom:'1%'
    },
    textTermsCondition: {
        fontSize: 14,
        color: 'rgba(73, 73, 73, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
        alignSelf: 'center',
        marginTop: '3%',
        marginBottom: '3%',
    }

}