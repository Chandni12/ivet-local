import React, { Component } from 'react';
import { NavigationActions, StackActions } from 'react-navigation';
import { ScrollView, Text, View, Image, TouchableOpacity, Alert, AsyncStorage, Platform } from 'react-native';
import IconAntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import IconEntypo from "react-native-vector-icons/Entypo"; 
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import DrawerComponentConstants from '../constants/DrawerComponent';
import * as wordConstants from '../constants/WordConstants';
import Feather from "react-native-vector-icons/Feather";

export default class DrawerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            auth_token: undefined,
        }
    }
    
    componentDidMount = () => {
        this._getAuthToken();
        // this.onClickLogout();
        // this.props.navigation.closeDrawer();
    }
    onClickLogout() {
        Alert.alert(
            DrawerComponentConstants.ARE_YOU_SURE_TO_LOGOUT,
            '',
            [
                { text: DrawerComponentConstants.CANCEL, onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: DrawerComponentConstants.OK, onPress: this.onPressOk.bind(this) },
            ],
            { cancelable: false }
        )
    }

    async _getAuthToken() {
        const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);
        if (auth_token == null) {
        } else {
          this.setState({
            auth_token: auth_token
          });
        }
      }

    async onPressOk() {
        this.props.navigation.closeDrawer();
        try {
            await AsyncStorage.removeItem(wordConstants.CONST_AUTH_TOKEN);
            await AsyncStorage.removeItem(wordConstants.CONST_USER_DATA);
            this.props.navigation.navigate('Welcome');
        } catch (error) {

        }
    }

    render() {
        return (
            <View style={{ backgroundColor: 'transparant', width: '50%', height: '90%' }}>

                <View style={{marginTop:40}}/>
                <TouchableOpacity style={{ marginTop: 30 }} onPress={()=> this.props.navigation.closeDrawer()}>
                    <View style={{ flexDirection: 'row' }}>
                        <IconEntypo name="home" style={styles.icon} />
                        <Text style={styles.textTitle}>{DrawerComponentConstants.DASHBOARD} </Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: 30 }} onPress={() =>
                    this.props.navigation.navigate('follow', {auth_token: this.state.auth_token})}>
                    <View style={{ flexDirection: 'row' }}>
                        <FontAwesome name="users" style={styles.icon} />
                        <Text style={styles.textTitle}>{DrawerComponentConstants.FOLLOW}</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: 30 }} onPress={() =>
                    this.props.navigation.navigate('DraftContainer', {auth_token: this.state.auth_token})}>
                    <View style={{ flexDirection: 'row' }}>
                        <MaterialIcons name="drafts" style={styles.icon} />
                        <Text style={styles.textTitle}>{DrawerComponentConstants.DRAFT}</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: 30 }} onPress={() =>
                    this.props.navigation.navigate('EditRequest', {auth_token: this.state.auth_token})}>
                    <View style={{ flexDirection: 'row' }}>
                        <Feather name="edit" style={styles.icon} />
                        <Text style={styles.textTitle}>{DrawerComponentConstants.EDIT_REQUEST_LIST}</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: 30 }} onPress={this.onClickLogout.bind(this)}>

                    <View style={{ flexDirection: 'row' }}>
                        <IconAntDesign name="logout" style={styles.icon} />
                        <Text style={styles.textTitle}>{DrawerComponentConstants.LOGOUT}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )

    }
}

const styles = {
    icon: {
        color: "rgba(51, 51, 51, 255)",
        marginLeft: 10,
        marginRight: 10,
        fontSize: 20
    },
    textTitle: {
        // backgroundColor: 'rgb(68,153,218)',
        fontFamily: 'NotoSansCJKjp-Medium',
        color: 'rgba(51, 51, 51, 255)',
        fontSize: 14,
        paddingTop: 2.2,
    },
}