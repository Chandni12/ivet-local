import React, { Component } from 'react';
import { View, FlatList, Text, CheckBox, Image, TouchableOpacity, Button, ActivityIndicator } from 'react-native';
import FollowConstants from '../constants/Follow'
import commonStyles from '../stylesheet/common/commonStyles.style'
import { SafeAreaView } from 'react-navigation';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { verticalScale } from 'react-native-size-matters';
import IconFeather from 'react-native-vector-icons/Feather';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const profilePhoto = require('../../assets/profilePhoto.png');
const iconBack = require('../../assets/back.png');

export default class FollowSelection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            isShowCheckMark:false,
        };
    }
    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };

    renderItem = (item) => {
        // const baseUrl = "";
        // const userImage = baseUrl + item.item.profile_image;
        return (
            <View>
            <View style={{ flexDirection: 'row', padding: '4%', alignItems: 'center' }}>
               <Image source={profilePhoto} style={styles.profilePhotoWithoutCircle} />
                <View style={{marginLeft:'2%', marginRight:'2%'}}>
                    <Text style={styles.textDesc1}>{item.item.nick_name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <IconAntDesign name="eye" style={{ color: '#0a2814', fontSize: 16 }} />
                    <Text style={[styles.textDesc, {marginLeft:'2%', marginTop:'2%'}]}>{item.item.views + "\t"}</Text>
                    <FontAwesome5 name="award" style={{ color: "#d99818",fontSize: 12 }} />
                    <Text style={[styles.textDesc, {marginLeft:'2%', marginTop:'2%'}]}>{item.item.views + "CS"}</Text>
                    </View>
                </View>
                {this.state.isShowCheckMark? <IconEntypo name="check" style={{color:'rgb(93, 175, 249)', fontSize: 20, marginRight:'4%'}} />: null }
                 </View>
            <View style={styles.singleLine} />
        </View>
        )
    }
    render() {
        const {fetching } = this.props;

        return (
            <SafeAreaView style={{ flex: 10 }}>
                <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' , backgroundColor: 'rgb(61, 160, 248)'}]}>
                    <TouchableOpacity style={{ marginLeft: '3%', flex: 1 }} onPress={() => this.props.navigation.goBack()} >
                        <Image source={iconBack} style={{ width: 16, height: 16 , tintColor:'white'}} />
                    </TouchableOpacity>
                    <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center', color:'white' }]}>{FollowConstants.MY_PAGE}</Text>
                    <View style={{ flex: 1 }}>
                    </View>
                </View>
                <View  style={styles.viewComplete}>
                
                    {/* <View style={{margin:'5%', height:verticalScale(100)}}>  */}
                    <SegmentedControlTab
                        values={[FollowConstants.FOLLOW_ME, FollowConstants.I_FOLLOW,]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                        //tabTextStyle={styles.textDesc}
                        tabsContainerStyle={{marginLeft:'5%',marginRight:'5%', marginTop:'3%' ,flex:0.1}}
                    />
                    {/* </View> */}
                    {/* <Text style={[styles.textDesc, { marginLeft: '5%', marginRight: '5%', textAlign: 'center', marginTop: '4.5%' }]}>{FollowConstants.FOLLOW_ME}</Text>
                    <Text style={[styles.textDesc, { marginLeft: '5%', marginRight: '5%', textAlign: 'center', marginTop: '4.5%' }]}>{FollowConstants.I_FOLLOW}</Text> */}

                    <View style={[styles.singleLine, {marginTop:'3%'}]} />
                    <FlatList
                        style={{ flex: 5 }}
                        data={this.state.selectedIndex === 0 ? this.props.followerDetails : this.props.followingUsersDetails}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    {/* <View style={{ alignItems: 'center', alignSelf: 'center', height: '7%', marginTop:'2%' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('tabNavigator')}>
                            <Text style={styles.textLoadMoreButton}>{FollowConstants.LOAD_MORE}</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>
                {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                </View> : null}
            </SafeAreaView>
        )
    }
}

const styles = {
    textTitle: {
        fontSize: 15,
       
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDesc1: {
        fontSize: 13,
        color: '#0a2814',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDesc: {
        fontSize: 12,
        color: '#898989',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDescLightBlue:{
        color: '#bed4e6'
    },
    textLoadMoreButton: {
        fontSize: 13,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    singleLine: {
        width: '90%',
        height: 1,
        backgroundColor: 'lightgray',
        alignSelf:'center'
    },
    profilePhotoWithoutCircle: {
        width: 40,
        height: 40,
        borderRadius: 20,
        // borderColor: 'rgb(2, 132, 254)',
        // borderWidth: 1.5,
    },
    viewComplete:{
         flex: 10, 
         backgroundColor:'white', 
         margin:'3%' ,
         shadowColor: 'gray',
         shadowOpacity: 0.6,
         shadowRadius: 2,
         shadowOffset: {
            height: 5,
            width: 5
          },
          elevation: 1,
          borderColor:'lightgray',
          borderWidth:1
    }
}