import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, AsyncStorage,StyleSheet, Image, ScrollView, Linking } from 'react-native';
import welcomeConstants from '../constants/Welcome'
import commonStyles from '../stylesheet/common/commonStyles.style';
import { SafeAreaView } from 'react-navigation';
import { scale, verticalScale, moderateScale, ScaledSheet } from 'react-native-size-matters';
import firebase, { Notification, NotificationOpen } from "react-native-firebase";
import * as wordConstants from '../constants/WordConstants';
import DeviceInfo from 'react-native-device-info';


console.disableYellowBox = true;
export default class WelcomeIos extends Component {
    constructor(props) {
        super(props);

        this.state = {
            auth_token: '',
        }
    }

    openURLInBrowser = (url) => {
        Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
    };

    componentWillMount = ()=>{
        this.createNotificationListenersIos();
        let uniqueId = DeviceInfo.getUniqueId();
        console.log(" uniqued id =====",uniqueId ); 
       
      this._getAuthToken();
    }
    // async componentDidMount() {
    //     this.createNotificationListenersIos();
    //     let uniqueId = DeviceInfo.getUniqueId();
    //     console.log(" uniqued id =====",uniqueId ); 
       
    //   this._getAuthToken();
      

    // }

    async _getAuthToken() {
        const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);
        console.log("************", auth_token);
        if (auth_token == null) {
        } else {
          this.setState({
            auth_token: auth_token
          });
          //  return auth_token
           // Need to work for checking category exist or not
          this.props.navigation.navigate('MainContainer', { auth_token: this.state.auth_token, arrayCategory: [] });
        }
      }
    async createNotificationListenersIos() {

        console.log("createNotificationListenersIOs ");

        /* Triggered when a particular notification has been received in foreground */
        const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
            .setDescription('My apps test channel');

        // Create the channel
        firebase.notifications().android.createChannel(channel);
        this.notificationListener = firebase.notifications().onNotification((notification) => {

            console.log(" notification is === 81 ", notification);

            notification
                .android.setChannelId('test-channel')
                .android.setSmallIcon('ic_launcher');
            firebase.notifications()
                .displayNotification(notification);

            this.checkForData(notification);

        });

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows: */

        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            console.log(" notification is === 99 =======", notificationOpen.notification);
            this.checkForData(notificationOpen.notification);
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
       */
        /*
        firebase.notifications().getInitialNotification()
              .then((notificationOpen: NotificationOpen) => {
                if (notificationOpen) {
        
                    console.log(" 45 ===== ", notificationOpen);
                  // App was opened by a notification
                  // Get the action triggered by the notification being opened
                  const action = notificationOpen.action;
                  // Get information about the notification that was opened
                  const notification: Notification = notificationOpen.notification;  
        
                  if (notification) {
        
                    // console.log("+================= notificationOpen ", notification, "===========");
                     // App was opened by a notification
                     // Get the action triggered by the notification being opened
                    // const action = notificationOpen.action;
                     // Get information about the notification that was opened
                    
                     console.log("^^^^^^^^^^^^^^^^^^",notification._notification);
                    this.checkForData(notification._notification)
                 }
                }
              });  
              */
    }

    checkForData(notification) {
        if (notification._data) {

            if (notification._data.type) {
                if (notification._data.type.toUpperCase() !== wordConstants.CONST_ADMIN) {
                    return;
                }
            }

            console.log("69 ^^^^^^^^^^^^^^^^^^", notification._data.status);

            let notificationStatus = notification._data.status;
            console.log(" notification status ====", notificationStatus.toUpperCase());

            if (notificationStatus.toUpperCase() === wordConstants.CONST_APPROVE) {

                this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_CONFIRM, userData: {} });
            } else {
                this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_FAILED, userData: {} });
            }
        } else {

        }
    }




    render() {
        return (
            // <SafeAreaView style={{flex:1}}>
            <View style={{ flex: 1 }}>
                {/* <ScrollView> */}
                <ImageBackground resizeMode={'stretch'} source={require('../../assets/welcome_background.png')} style={{ width: '100%', height: '100%' }}>
                    
                    <Image source={require('../../assets/welcome_title.png')} style={{alignSelf: 'center', 
                        marginTop:"39%", 
                        width:175, height: 40}} />
                        {/* <Text style={[styles.textTitleBlue, {marginTop:'40%'}]}>
                    {welcomeConstants.TITLE}
                    <Text style={styles.textTitleBlack}>{welcomeConstants.TITLE_BLACK}</Text>
                    </Text> */}
                    
                    <Text style={[styles.textSubTitle]}>{welcomeConstants.DESCRIPTION}</Text>

                    <Text style={[styles.textSubTitle2, { marginTop: '12%' }]}>{welcomeConstants.WELCOME_TEXT}</Text>
                    <Text style={[styles.textSubTitle2, { marginTop: '1%' }]}>{welcomeConstants.REGISTRATION_TITLE}</Text>

                    <View style={[commonStyles.buttonViewRed, { marginTop: '6%', alignSelf:'center' }]}>
                        <TouchableOpacity style={{width:'100%', height:'100%'}} onPress={() => this.props.navigation.navigate('Signup')}>
                            <Text style={styles.buttonText}>
                                {welcomeConstants.REGISTRATION_BUTTON_TITLE}
                                <Text style={styles.buttonText_small}>  {welcomeConstants.REGISTRATION_BUTTON_TITLE_2}</Text>
                            </Text>
                        </TouchableOpacity >
                    </View>

                    <View style={[commonStyles.buttonViewBlue, { marginTop: '5%',alignSelf:'center' }]}>
                        <TouchableOpacity style={{width:'100%', height:'100%'}} onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={styles.buttonText}>{welcomeConstants.LOGIN_BUTTON_TITLE}</Text>
                        </TouchableOpacity>
                    </View>

                    {/* <TouchableOpacity style={commonStyles.buttonViewBlue} onPress={() => onSubmit()}>
                        <View>
                            <Text style={styles.textBlueButton}>{loginConstants.LOGIN_BUTTON_TITLE}</Text>
                        </View>
                    </TouchableOpacity> */}

                    <View style={{ bottom: '2%', position: 'absolute', marginLeft: '26%' }}>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.textBottom1Gray, { flexWrap: 'wrap' }]}>
                                {welcomeConstants.BY_CONTINUEING_USAGE_RULES}
                            </Text>
                            <TouchableOpacity onPress={() => this.openURLInBrowser(welcomeConstants.TERMS_AND_CONDITION_URL)}>
                                <Text style={styles.textBottom1Blue}>{welcomeConstants.IT_IS_CONSIDERED}</Text>

                            </TouchableOpacity>

                            <Text style={[styles.textBottom1Gray]}>{welcomeConstants.IT_IS_CONSIDERED_2}</Text>
                        </View>


                        <View style={{ flexDirection: 'row' }}>

                            <TouchableOpacity onPress={() => this.openURLInBrowser(welcomeConstants.PRIVACY_POLICY_URL)}>
                                <Text style={[styles.textBottom1Blue, { flexWrap: 'wrap', marginTop: '1%' }]}>
                                    {welcomeConstants.THAT_YOU_HAVE_AGREED_PRIVACY_POLICY}
                                </Text>
                            </TouchableOpacity>

                            <Text style={styles.textBottom1Gray}>{welcomeConstants.PRIVACY_POLICY_2}</Text>

                        </View>

                        {/* <TouchableOpacity onPress={()=> this.openURLInBrowser()}>
                               <Text style={[styles.textBottom1Blue , {flexWrap:'wrap', marginTop:'1%'}]}>
                                {welcomeConstants.THAT_YOU_HAVE_AGREED_PRIVACY_POLICY}
                                <Text style={styles.textBottom1Gray}>{welcomeConstants.PRIVACY_POLICY_2}</Text>
                                </Text>
                               </TouchableOpacity> */}


                        <Text style={[styles.textBottom1Gray, { right: '3%', alignSelf: 'flex-end', marginTop: '8%' }]}>{welcomeConstants.COPYRIGHT_TEXT}</Text>

                    </View>

                </ImageBackground>
                {/* </ScrollView> */}
            </View>
            //  </SafeAreaView>
        )
    }
}

const styles = ScaledSheet.create({
    background: {
        flex: 1,
    },
    imageTop: {
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        width: '100%',
        height: verticalScale(120)
    },
    textTitleBlue: {
        fontSize: "34@ms",
        color: 'rgb(2, 132, 254)',
        alignSelf: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textTitleBlack: {
        fontSize: "34@ms",
        color: 'black',
        alignSelf: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textSubTitle: {
        alignSelf: 'center',
        fontSize: "15@ms",
        color: 'rgb(153, 153, 153)',
        textAlign: 'center',
        marginTop: '5%',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textSubTitle2: {
        alignSelf: 'center',
        fontSize: "12@ms",
        color: 'rgb(153, 153, 153)',
        textAlign: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textSubTitle3: {
        alignSelf: 'center',
        fontSize: "14@ms",
        color: 'gray',
        textAlign: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    buttonViewRed: {
        width: '80%',
        backgroundColor: 'rgb(214, 22, 22)',
        borderRadius: 2,
        height: verticalScale(45),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonViewBlue: {
        width: '80%',
        backgroundColor: 'rgb(66, 130, 191)',
        borderRadius: 2,
        height: verticalScale(45),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    // button: {
    //     margin: '5%'
    // },
    imageBottom: {
        marginLeft: 0,
        marginBottom: 0,
    },
    buttonText: {
        color: 'white',
        // multiLine: true,
        textAlign: 'center',
        padding: 10,
        fontSize: "14@ms",
        fontFamily: 'NotoSansCJKjp-Medium',
        height:30
    },
    buttonText_small: {
        color: 'white',
        // multiLine: true,
        textAlign: 'center',
        fontSize: "10@ms",
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textBottom1Gray: {
        color: 'gray',
        fontSize: "10@ms",
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textBottom1Blue: {
        color: 'rgb(66, 130, 191)',
        fontSize: "10@ms",
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textBottomCopyRight: {
        fontFamily: 'NotoSansCJKjp-Medium'
    }
})