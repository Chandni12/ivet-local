import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, FlatList, TextInput, TouchableWithoutFeedback, Keyboard, Dimensions } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale, scale } from 'react-native-size-matters';
import IconAntDesign from 'react-native-vector-icons/AntDesign'
import ImagePicker from 'react-native-image-crop-picker';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import { IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL , IMAGE_DASHBOARD_QUESTION_PREFIX_URL} from '../../api/endPoints'

import CastConstants from '../../constants/Cast'

const screenWidth = Math.round(Dimensions.get('window').width);

export default class PostPhotoSection extends Component {
    constructor(props) {
        super(props);

    }
    openImagePicker = (indexOfArray) => {

        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            console.log("After selection ---",image);
            this.props.handleFileChange('imagePhoto', image.path, indexOfArray);
        });
    }
    deleteImage = (index) => {
        console.log("deleteImage $$$$$$$ ===", index);
        this.props.onClickDeleteImage(index);
    }

    renderItem = (item, index) => {
        const { handleChange, data } = this.props;
       
        let image_Http_URL = { uri: this.props.arrayOfImages[item.index].document };

        let imageNumber = CastConstants.IMAGE + ' ' + (item.index + 1)
        return (
            <View style={{}}>
                <View style={{ flexDirection: 'row', marginTop: '2%', marginBottom: '2%', height: 35 }}>
                    <Text style={styles.textSubTitle}>{imageNumber}</Text>
                    <TouchableOpacity style={{ marginLeft: '10%', fontSize: 14 }} onPress={() => this.deleteImage(item.index)}>
                        <Text style={[styles.titleText, { color: 'rgba(255, 170, 170, 255)' }]}>{CastConstants.RED_TEXT_PHOTO_SECTION}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: '5%', }}>
                    <View style={[styles.buttonPhoto, { flex: 0.3, marginRight: '10%' }]}>
                        <TouchableOpacity onPress={() => this.openImagePicker(item.index)} >
                            {!this.props.arrayOfImages[item.index].document || this.props.arrayOfImages[item.index].document === '' ? <IconEntypo name="image" style={{ fontSize: 82, color: 'gray' }} /> : <Image source={image_Http_URL} style={styles.imageProperty} />}
                        </TouchableOpacity>
                    </View>

                    {/* Text field with keyboard handling  */}
                    <View style={{ flex: 0.7, right: '5%' }}>
                        <View>
                        <View style={[styles.inputTitleStyle, { marginBottom: '3%', justifyContent: 'center', }]}>
                            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{ backgroundColor: 'red' }}>
                                <TextInput
                                    placeholder={CastConstants.PLEASE_ENTER_IMAGE_TITLE}
                                    onChangeText={value => handleChange('image_title', value, item.index)}
                                    value={item.item.image_title}
                                    name={"image_title"}
                                    style={{ paddingTop: '4%', height: 40 }}
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: '1%' }}>
                                <View style={{ flex: 0.96 }} />
                                <Text style={[styles.charLimit, { alignSelf: 'flex-end', }]}>{item.item.charLimitImgTitle} {CastConstants.CHARACTER_LIMIT}</Text>
                            </View>
                        </View>
                        <View>
                        <View style={styles.inputTextAreaStyle}>
                            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} >
                                <TextInput
                                    placeholder={CastConstants.PLEASE_PROVIDE_IMAGE_DESCRIPTION}
                                    onChangeText={value => handleChange('image_description', value, item.index)}
                                    value={item.item.image_description}
                                    name={"image_description"}
                                    multiline={true}
                                    numberOfLines={5}
                                // style={styles.inputTextAreaStyle}
                                />
                            </TouchableWithoutFeedback>
                        </View>
                         <View style={{ flexDirection: 'row', marginTop: '1%' }}>
                                <View style={{ flex: 0.96 }} />
                                <Text style={[styles.charLimit, { alignSelf: 'flex-end', }]}>{item.item.charLimitImgDesc} {CastConstants.CHARACTER_LIMIT}</Text>
                            </View>
                       </View>
                    </View>
                </View>
            </View>
        )

    }
    render() {
        const { arrayOfImages, onClickAddImage } = this.props;

        return (
            <View style={{}}>
                <View style={styles.singleLine} />
                <View style={{ paddingLeft: '5%', paddingRight: '5%', }}>
                    <Text style={[styles.titleText, { paddingTop: '5%' }]}>{CastConstants.DO_YOU_HAVE_IMAGES}</Text>

                    <FlatList
                        style={{}}
                        data={arrayOfImages}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        scrollEnabled={false}
                    />

                    <View style={{ height: 50 }}>
                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={onClickAddImage} >
                            <View style={{ flexDirection: 'row' }}>
                                {arrayOfImages.length === 0 ? <IconAntDesign name="camera" style={{ fontSize: 22, marginRight: '5%', color: 'gray', paddingTop: '5%' }} /> : <IconMaterial name="add" style={{ fontSize: 22, marginRight: '5%', color: 'gray' }} />}
                                {arrayOfImages.length === 0 ? <Text style={[styles.addText, { paddingTop: '5%' }]}>{CastConstants.ADD}</Text>
                                    : <Text style={[styles.addText]}>{CastConstants.ADD}</Text>}
                            </View>

                        </TouchableOpacity>
                    </View>
                </View>
                <View style={[styles.singleLine, { marginTop: '5%' }]} />
            </View>
        )
    }
}

const styles = {
    buttonPhoto: {
        // width: screenWidth / 2 - 30,
        //height: screenWidth / 2 - 100,
        // borderColor: 'lightgray',
        // borderWidth: 1,
        // borderRadius: 5,


    },
    charLimit: {
        fontSize: 12,
        color: '#9b9b9b',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    imageProperty: {
        // maxHeight: '100%',
        // maxWidth: '100%',
        width: 100,
        height: 100,
        borderRadius: 10,
        marginLeft: 2
    },
    titleText: {
        fontSize: 16,
        color: 'rgba(51,51,51, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    textSubTitle: {
        fontSize: 16,
        color: 'rgba(129, 128, 129, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    valueText: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    plusText: {
        fontSize: 26,
        color: 'gray',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    addText: {
        fontSize: 16,
        color: 'rgba(129, 128, 129, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },

    singleLine: {
        width: '100%',
        height: 5,
        backgroundColor: 'lightgray'
    },
    inputTitleStyle: {
        height: 40,
        // backgroundColor: 'white',
        //marginTop: '3%',
        borderColor: 'lightgray',
        borderWidth: 1,
        borderRadius: 5,
        // shadowColor: 'rgba(0,0,0,0.7)',
        // shadowOffset: {
        //     width: 2,
        //     height: 4
        // },
        // shadowOpacity: 0.5,
        // shadowRadius: 1,
        // borderRadius: 8,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 13,
        color: '#262626',
        //paddingTop: 21,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    inputTextAreaStyle: {
        // marginLeft: '3%',
        // marginRight: '3%',
        height: verticalScale(60),
        backgroundColor: 'white',
        //marginTop: '3%',
        borderColor: 'lightgray',
        borderWidth: 1,
        borderRadius: 5,
        // shadowColor: 'rgba(0,0,0,0.7)',
        // shadowOffset: {
        //     width: 2,
        //     height: 4
        // },
        // shadowOpacity: 0.5,
        // shadowRadius: 1,
        // borderRadius: 8,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 13,
        color: '#262626',
        //paddingTop: 21,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,

    },


}