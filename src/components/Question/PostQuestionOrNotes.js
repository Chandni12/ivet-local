import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, FlatList, TextInput, TouchableWithoutFeedback, Keyboard, ActivityIndicator } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale, scale } from 'react-native-size-matters';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-navigation';
import HeaderBlue from '../../commonComponent/HeaderBlue'
import SegmentedControlTab from "react-native-segmented-control-tab";
import SimplePicker from 'react-native-simple-picker';

import CastConstants from '../../constants/Cast'
import { ScrollView } from 'react-native-gesture-handler';
import InputTextWithCharLimit from '../../commonComponent/InputTextWithCharLimit';
import PostPhotoSection from './PostPhotoSection';
import PostCategorySection from './PostCategorySection';
import PostBasicInfo from './PostBasicInfo';
import PostNotes from './PostNotes';
import TabbarTitleConstant from '../../constants/TabbarTitle';

import PostMenu from '../PostMenu';
import Others from '../Others';

export default class PostQuestionOrNotes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedGender: '',
            isSaveDraftClicked: false,
            isPreviewClicked: false,
            isShowPostMenu: false,
            isModalVisible: true,
            isShowOthersPopup: false,
            isShowTopProfileSection: true,
            isPopupShow: false,
        }
    }
    componentDidMount = () => {

        this.props.navigation.addListener(
            'willFocus',
            () => {
                console.log(" in post ", this.props.navigation.state.routeName);
                if (this.props.navigation.state.routeName === TabbarTitleConstant.OTHERS && !this.state.isShowPostMenu) {
                    this.setState({
                        isShowOthersPopup: true,
                        isModalVisible: true
                    })
                }
            });
    }

    showHidePopupView = (viewName) => {

        // if (viewName === 'postMenu') {
        //     this.onClickPostMenu(true);
        //     this.setState({
        //         isShowOthersPopup: false,
        //     })
        // }
        // this.setState({
        //     isShowOthersPopup: !this.state.isShowOthersPopup,
        //     isModalVisible: !this.state.isModalVisible
        // })
    }


    render() {
        const { showTagSearchData, arraySearchTags, onClickCategoryToLoadData,getBasicDetailToPostNotes,getBasicDetailToPostQuestion,removeAllSpeciality, callApiToGetSpecialityBasisOfCategory, arraySelectedTagsOfNotes, arraySelectedSpecialitiesNotes, arraySelectedSpecialities, onClickAddImageOrTextOfNotes, arrayImageTextOfNotes, tagObject, titleArrayTag, categoryObject, titleArrayCategory, titleArraySpecialityMaster, specialityObject, data, titleArrayAnimalSpecies, handleFileChange, errors, fetching, onSubmit, handleChange, navigation, arrayAnimalSpecies, arrayBreed, titleArrayBreed, arrayGender, titleArrayGender, arraySimilarQuestions, arrayOfImages, onClickAddImage, onClickDeleteImage, postQuestion, onClickSaveDraft, onClickPreview, is_previewClicked, onSelectedTagsChange, handleMultiSelect, arraySelectedTags, onClickDeleteImageOrTextOfNotes, handleChangeNotes, handleFileChangeNotes, arraySummaryNotes, onClickAddSummaryNotes, onClickDeleteSummaryNotes, onClickPostNotesAsDraft, onClickPostNotesAsPreview, onClickPostNotes, isPreviewClickedNotes, onChangeSegmentedControl, onClickDeleteMultiSelect, onClickToOpenAnimalSection } = this.props;


        return (
            <SafeAreaView>
                <HeaderBlue
                    title={CastConstants.CAST_TITLE}
                    navigation={this.props.navigation}
                    onClickBackInPostMenu={this.props.onClickBackInPostMenu}
                    viewNameComeFrom={this.props.navigation.state.params.viewNameComeFrom}
                />

                <KeyboardAwareScrollView>
                    <ScrollView style={{ marginBottom: '16%' }}>
                        <View >
                            <View style={{ marginTop: '5%' }} />
                            <SegmentedControlTab
                                values={[CastConstants.POSTING_QUESTION, CastConstants.SET_NOTES]}
                                selectedIndex={data['selectedSegmentIndex']}
                                onTabPress={onChangeSegmentedControl}
                                //tabTextStyle={styles.textDesc}
                                activeTabStyle ={{marginLeft:-0.5}}
                                tabsContainerStyle={{ marginLeft: '5%', marginRight: '5%' }}
                            />
                            {data['selectedSegmentIndex'] === 0 ?
                                <View style={{}}>

                                   <View style={{ flex: 2, marginBottom: '5%' }}>
                                        <PostCategorySection showTagSearchData={showTagSearchData}  arraySearchTags={arraySearchTags} onClickCategoryToLoadData ={onClickCategoryToLoadData} getBasicDetailToPost= {getBasicDetailToPostQuestion} removeAllSpeciality={removeAllSpeciality} callApiToGetSpecialityBasisOfCategory={callApiToGetSpecialityBasisOfCategory} arraySelectedSpecialities={arraySelectedSpecialities} data={data} categoryObject={categoryObject} titleArraySpecialityMaster={titleArraySpecialityMaster} specialityObject={specialityObject} titleArrayCategory={titleArrayCategory} tagObject={tagObject} titleArrayTag={titleArrayTag} handleChange={handleChange} errors={errors} onSelectedTagsChange={onSelectedTagsChange} handleMultiSelect={handleMultiSelect} arraySelectedTags={arraySelectedTags} isQuestion={true} onClickDeleteMultiSelect={onClickDeleteMultiSelect} />
                                    </View>

                                    <View style={{ flex: 1 }}>
                                        <InputTextWithCharLimit value={data['title']} placeholder={CastConstants.ENTER_YOUR_QUESTION} charLimit={data['charLimitTitle']} handleChange={handleChange} name={'title'} titleLabel={CastConstants.ENTER_YOUR_QUESTION} arraySimilarQuestion={arraySimilarQuestions} errors={errors} />
                                    </View>

                                    <View style={{ flex: 1 }}>
                                        <PostBasicInfo
                                        getBasicDetailToPostQuestion={getBasicDetailToPostQuestion}
                                            onClickToOpenAnimalSection={onClickToOpenAnimalSection}
                                            data={data}
                                            titleArrayAnimalSpecies={titleArrayAnimalSpecies}
                                            arrayAnimalSpecies={arrayAnimalSpecies}
                                            arrayBreed={arrayBreed}
                                            titleArrayBreed={titleArrayBreed}
                                            arrayGender={arrayGender}
                                            titleArrayGender={titleArrayGender}
                                            handleChange={handleChange} />
                                    </View>

                                    <View style={{ flex: 2 }}>
                                        <InputTextWithCharLimit value={data['detail']} placeholder={CastConstants.PLEASE_FILL_DETAILS_CORRECTLY} charLimit={data['charLimitDetail']} name={'detail'} handleChange={handleChange} titleLabel={CastConstants.PLEASE_ENTER_DETAILS_OF_QUESTION} arraySimilarQuestion={[]} errors={errors} />
                                    </View>

                                    <View style={{ flex: 2 }}>
                                        <PostPhotoSection data={data} arrayOfImages={arrayOfImages} onClickAddImage={onClickAddImage} handleFileChange={handleFileChange} handleChange={handleChange} onClickDeleteImage={onClickDeleteImage} />
                                    </View>

                                    {data['isEditQuestion'] ? <View style={{ marginBottom: '5%' }}>
                                        <InputTextWithCharLimit value={data['editReasonCommentQuestion']} placeholder={CastConstants.WRITE_DOWN_MEMO} charLimit={data['charLimitReason']} name={'editReasonCommentQuestion'} handleChange={handleChange} titleLabel={CastConstants.PLEASE_INCLUDE_NOTE} arraySimilarQuestion={[]} errors={errors} />
                                    </View> : null}

                                    {data['isEditQuestion'] ? null : <View style={[styles.buttonSaveDraft, { height: 50 }]}>

                                        <TouchableOpacity style={{ justifyContent: 'center', width: '100%', height: '100%', }} onPress={onClickSaveDraft}>

                                            {this.state.isSaveDraftClicked ? <Text style={[styles.completeText, { alignSelf: 'center', height: 50 }]}>{CastConstants.SAVE_DRAFT}</Text>
                                                :
                                                <Text style={[styles.completeText, { alignSelf: 'center', height: 50 }]}>{CastConstants.DISCARD_DRAFT}</Text>
                                            }
                                        </TouchableOpacity>
                                    </View>}

                                    {data['isEditQuestion'] ?
                                        <View style={[styles.buttonRoundCorner, { justifyContent: 'center', }]}>
                                            <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => {
                                                postQuestion()
                                            }}>
                                                <Text style={[styles.textButton]}>{CastConstants.POST_A_QUESTION}</Text>
                                            </TouchableOpacity>
                                        </View> :
                                        <View style={[styles.buttonRoundCorner, { justifyContent: 'center', }]}>
                                            <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => {
                                                { is_previewClicked ? postQuestion() : onClickPreview() }
                                            }}>
                                                {is_previewClicked ? <Text style={[styles.textButton]}>{CastConstants.POST_A_QUESTION}</Text>
                                                    : <Text style={[styles.textButton]}>{CastConstants.PREVIEW_CONFIRMATION}</Text>}
                                            </TouchableOpacity>
                                        </View>}
                                </View>
                                :
                                <PostNotes
                                showTagSearchData={showTagSearchData}
                                arraySearchTags={arraySearchTags}
                                onClickCategoryToLoadData={onClickCategoryToLoadData}
                                getBasicDetailToPostNotes={getBasicDetailToPostNotes}
                                    removeAllSpeciality={removeAllSpeciality}
                                    arraySelectedSpecialitiesNotes={arraySelectedSpecialitiesNotes}
                                    callApiToGetSpecialityBasisOfCategory={callApiToGetSpecialityBasisOfCategory}
                                    onClickDeleteMultiSelect={onClickDeleteMultiSelect}
                                    onClickAddImageOrTextOfNotes={onClickAddImageOrTextOfNotes}
                                    onClickDeleteImageOrTextOfNotes={onClickDeleteImageOrTextOfNotes}
                                    arrayImageTextOfNotes={arrayImageTextOfNotes} tagObject={tagObject}
                                    titleArrayTag={titleArrayTag} categoryObject={categoryObject} titleArrayCategory={titleArrayCategory} titleArraySpecialityMaster={titleArraySpecialityMaster} specialityObject={specialityObject} data={data} titleArrayAnimalSpecies={titleArrayAnimalSpecies} handleFileChange={handleFileChange} errors={errors} fetching={fetching} onSubmit={onSubmit} handleChange={handleChange} navigation={navigation} arrayAnimalSpecies={arrayAnimalSpecies} arrayBreed={arrayBreed} titleArrayBreed={titleArrayBreed} arrayGender={arrayGender} titleArrayGender={titleArrayGender} arraySimilarQuestions={arraySimilarQuestions} arrayOfImages={arrayOfImages} onClickAddImage={onClickAddImage} onClickDeleteImage={onClickDeleteImage} postQuestion={postQuestion} onClickSaveDraft={onClickSaveDraft} onClickPreview={onClickPreview} is_previewClicked={is_previewClicked} onSelectedTagsChange={onSelectedTagsChange} handleMultiSelect={handleMultiSelect} arraySelectedTags={arraySelectedTagsOfNotes}
                                    handleFileChangeNotes={handleFileChangeNotes}
                                    handleChangeNotes={handleChangeNotes}
                                    onClickDeleteSummaryNotes={onClickDeleteSummaryNotes}
                                    onClickAddSummaryNotes={onClickAddSummaryNotes}
                                    arraySummaryNotes={arraySummaryNotes}
                                    onClickPostNotes={onClickPostNotes}
                                    onClickPostNotesAsDraft={onClickPostNotesAsDraft}
                                    onClickPostNotesAsPreview={onClickPostNotesAsPreview}
                                    isPreviewClickedNotes={isPreviewClickedNotes}
                                />
                            }
                        </View>



                    </ScrollView>

                </KeyboardAwareScrollView>
                {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                </View> : null}

                {this.state.isShowOthersPopup ?

                    <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: '200%', width: "100%", position: 'absolute' }} onPress={() => this.showHidePopupView(false)}>
                        <Others showHidePopupView={this.showHidePopupView} navigation={this.props.navigation} />
                    </TouchableOpacity>

                    : <View />
                }
            </SafeAreaView>

        )
    }
}

const styles = {
    buttonRoundCorner: {
        backgroundColor: '#0084ff',
        borderRadius: 10,
        alignSelf: 'center',
        // justifyContent: 'center',
        height: verticalScale(50),
        // marginBottom:'5%'
    },
    buttonSaveDraft: {
        // borderRadius: 10,
        alignSelf: 'center',
        height: verticalScale(50),
    },
    textButton: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '3%',
        paddingRight: '3%'
    },
    charLimit: {
        fontSize: 12,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoValue: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    completeText: {
        fontSize: 14,
        color: 'gray',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    caretDownIcon: {

        marginRight: '2%'
    },
    textInputAge: {
        backgroundColor: 'white',
        paddingLeft: '2%',
        borderColor: 'lightgray',
        borderWidth: 1
    },
    inputTextStyle: {
        marginLeft: '5%',
        marginRight: '5%',
        //marginBottom: '5%',
        // marginTop:'5%',
        height: 40,
        backgroundColor: 'white',
        borderColor: 'lightgray',
        borderWidth: 1,
        justifyContent: 'center',

        // paddingTop: 21,
        // paddingBottom: 20,
        // paddingLeft: 10,
        // paddingRight: 10,
    },
    inputTextAreaStyle: {
        // marginLeft: '5%',
        // marginRight: '5%',
        height: 100,
        backgroundColor: 'white',
        marginTop: '3%',
        borderColor: 'gray',
        borderWidth: 1,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingTop: 21,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    viewSection: {
        flexDirection: 'row',
        margin: '5%',
        height: verticalScale(40),
        backgroundColor: '#f7f8fa',
        borderColor: 'gray',
        borderWidth: 1,
        shadowColor: 'rgba(0,0,0,0.7)',
        shadowOffset: {
            width: 2,
            height: 4
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        alignItems: 'center',
    },
    viewSectionOpen: {

        margin: '5%',
        backgroundColor: '#f7f8fa',
        borderColor: 'gray',
        borderWidth: 1,
        shadowColor: 'rgba(0,0,0,0.7)',
        shadowOffset: {
            width: 2,
            height: 4
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        height: '50%',
        marginBottom: '5%',
        justifyContent: 'space-around'
    },

}