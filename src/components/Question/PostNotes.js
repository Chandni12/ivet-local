import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, FlatList, TextInput, TouchableWithoutFeedback, Keyboard, TextComponent } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale, scale } from 'react-native-size-matters';

import IconEntypo from 'react-native-vector-icons/Entypo';
import CastConstants from '../../constants/Cast'
import { ScrollView } from 'react-native-gesture-handler';
import InputTextWithCharLimit from '../../commonComponent/InputTextWithCharLimit';
import PostPhotoSection from './PostPhotoSection';
import PostCategorySection from './PostCategorySection';
import PostBasicInfo from './PostBasicInfo';
import ImagePicker from 'react-native-image-crop-picker';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';

export default class PostNotes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            selectedGender: '',
        }
    }

    //********* Add/ Delete Sentence and Images  */
    openImagePicker = (indexOfArray) => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            console.log(image);
            this.props.handleFileChangeNotes('imagePhoto', image.path, indexOfArray);
        });
    }
    addImageOrText = (isText) => {
        this.props.onClickAddImageOrTextOfNotes(isText)
    }
    deleteImageOrText = (index) => {
       
        this.props.onClickDeleteImageOrTextOfNotes(index);
    }

    renderItem = (item, index) => {
       
        const { handleChangeNotes, handleFileChangeNotes } = this.props;

        if (!this.props.arrayImageTextOfNotes[item.index].isText) {

            let image_Http_URL = { uri: this.props.arrayImageTextOfNotes[item.index].document };

            let imageNumber = CastConstants.IMAGE + ' ' + (item.index + 1)
            return (
                <View style={{ paddingLeft: '6%', paddingRight: '4%' }}>
                    <View style={{ flexDirection: 'row', marginTop: '2%', marginBottom: '2%', height: 35 }}>
                        <Text style={styles.textSubTitle}>{imageNumber}</Text>
                        <TouchableOpacity style={{ marginLeft: '10%', fontSize: 14 }} onPress={() => this.deleteImageOrText(item.index)}>
                            <Text style={[styles.titleText, { color: 'rgba(255, 170, 170, 255)' }]}>{CastConstants.RED_TEXT_PHOTO_SECTION}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: '5%', }}>
                        <View style={[styles.buttonPhoto, { flex: 0.3, marginRight: '10%' }]}>
                            <TouchableOpacity onPress={() => this.openImagePicker(item.index)} >
                                {!this.props.arrayImageTextOfNotes[item.index].document || this.props.arrayImageTextOfNotes[item.index].document === '' ? <IconEntypo name="image" style={{ fontSize: 82, color: 'gray' }} /> : <Image source={image_Http_URL} style={styles.imageProperty} />}
                            </TouchableOpacity>
                        </View>

                        {/* Text field with keyboard handling  */}
                        <View style={{ flex: 0.7, right: '5%' }}>
                            <View style={[styles.inputTitleStyle, { marginBottom: '3%', justifyContent: 'center', }]}>
                                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{ backgroundColor: 'red' }}>
                                    <TextInput
                                        placeholder={CastConstants.PLEASE_ENTER_IMAGE_TITLE}
                                        onChangeText={value => handleChangeNotes('image_title', value, item.index)}
                                        value={item.item.image_title}
                                        name={"image_title"}
                                        style={{ paddingTop: '4%', height: 40 }}
                                    />
                                </TouchableWithoutFeedback>
                            </View>

                            <View style={styles.inputTextAreaStyle} >
                                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} >
                                    <TextInput
                                        placeholder={CastConstants.PLEASE_PROVIDE_IMAGE_DESCRIPTION}
                                        onChangeText={value => handleChangeNotes('image_description', value, item.index)}
                                        value={item.item.image_description}
                                        name={"image_description"}
                                        multiline={true}
                                        numberOfLines={5}
                                    // style={styles.inputTextAreaStyle}
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                    </View>
                </View>)
        } else {
            let number = CastConstants.TEXT + ' ' + (item.index + 1);
            return (
                <View style={{}}>
                    <View style={{ flexDirection: 'row', marginTop: '2%', height: 35, paddingLeft: '6%' }}>
                        <Text style={styles.textSubTitle}>{number}</Text>
                       <TouchableOpacity style={{ marginLeft: '5%', fontSize: 14 }} onPress={() => this.deleteImageOrText(item.index)}>
                            <Text style={[styles.titleText, { color: 'rgba(255, 170, 170, 255)' }]}>{CastConstants.RED_DELETE_TEXT}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.viewTextBlock} >
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} >
                            <TextInput
                                // placeholder={CastConstants.PLEASE_PROVIDE_IMAGE_DESCRIPTION}
                                onChangeText={value => handleChangeNotes('sentence', value, item.index)}
                                value={item.item.sentence}
                                name={"sentence"}
                                multiline={true}
                                numberOfLines={5}
                            // style={styles.inputTextAreaStyle}
                            />
                        </TouchableWithoutFeedback>
                    </View>
                </View>)
        }
    }

    //******************************************************   */

    //********* Add/ Delete Summary   */
    addSummary = () => {
        this.props.onClickAddSummaryNotes()
    }
    deleteSummary = (index) => {
        console.log("deleteImage $$$$$$$ ===", index);
        this.props.onClickDeleteSummaryNotes(index);
    }
    renderItemSummary = (item, index) => {

       
        const { handleChangeNotes, handleFileChangeNotes } = this.props;

        let strSentence = item.item;
        {
            let number = CastConstants.SUMMARY + ' ' + (item.index + 1);
            return (
                <View style={{ flexDirection: 'row', marginLeft: '7%', alignSelf: 'center', marginRight: '6%' }}>
                    {/* <View style={{ flexDirection: 'row', marginTop: '2%', height: 35, paddingLeft: '6%' }}>
                        <Text style={styles.textSubTitle}>{number}</Text>
                        <TouchableOpacity style={{ marginLeft: '5%', fontSize: 14 }} onPress={() => this.deleteSummary(item.index)}>
                            <Text style={[styles.titleText, { color: 'rgba(255, 170, 170, 255)' }]}>{CastConstants.RED_DELETE}</Text>
                        </TouchableOpacity>
                    </View> */}
                    <View style={[styles.viewSummaryBlock, { width: '90%' }]} >
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} >
                            <TextInput
                                // placeholder={CastConstants.PLEASE_PROVIDE_IMAGE_DESCRIPTION}
                                onChangeText={value => handleChangeNotes('summary', value, item.index)}
                                value={strSentence}
                                name={"summary"}
                                // multiline={true}
                                // numberOfLines={5}
                                scrollEnabled={false}
                            // style={styles.inputTextAreaStyle}
                            />
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={{ marginLeft: '5%', justifyContent: 'center', }}>
                        {item.index === 0 ? <IconEntypo name="circle-with-plus" style={{ fontSize: 22 }} onPress={() => this.addSummary()} /> : null}
                        {item.index > 0 ? <IconEntypo name="circle-with-minus" style={{ fontSize: 22 }} onPress={() => this.deleteSummary(item.index)} /> : null}
                    </View>
                </View>)
        }
    }

    render() {

        const { showTagSearchData,arraySearchTags , onClickCategoryToLoadData,getBasicDetailToPostNotes,removeAllSpeciality, callApiToGetSpecialityBasisOfCategory,arraySelectedSpecialitiesNotes,tagObject, titleArrayTag, categoryObject, titleArrayCategory, titleArraySpecialityMaster, specialityObject, data,  handleFileChange, errors, fetching, onSubmit, handleChange, navigation, arrayAnimalSpecies, arrayBreed, titleArrayBreed, arrayGender, titleArrayGender, arraySimilarQuestions, arrayOfImages, onClickAddImage, onClickDeleteImage, postQuestion, onClickSaveDraft, onClickPreview, is_previewClicked, onSelectedTagsChange, handleMultiSelect, arraySelectedTags, onClickAddImageOrTextOfNotes, arrayImageTextOfNotes, onClickDeleteImageOrTextOfNotes, handleChangeNotes, handleFileChangeNotes, arraySummaryNotes, onClickPostNotesAsDraft, onClickPostNotesAsPreview, onClickPostNotes, isPreviewClickedNotes, onClickDeleteMultiSelect } = this.props;
        return (
            <View>
                  <View style={{ flex: 2, marginBottom: '5%' }}>
                    <PostCategorySection showTagSearchData={showTagSearchData}  arraySearchTags={arraySearchTags} onClickCategoryToLoadData={onClickCategoryToLoadData} getBasicDetailToPost={getBasicDetailToPostNotes} removeAllSpeciality ={removeAllSpeciality} callApiToGetSpecialityBasisOfCategory={callApiToGetSpecialityBasisOfCategory} arraySelectedSpecialities= {arraySelectedSpecialitiesNotes} data={data} categoryObject={categoryObject} titleArraySpecialityMaster={titleArraySpecialityMaster} specialityObject={specialityObject} titleArrayCategory={titleArrayCategory} tagObject={tagObject} titleArrayTag={titleArrayTag} handleChange={handleChangeNotes} errors={errors} onSelectedTagsChange={onSelectedTagsChange} onClickDeleteMultiSelect={onClickDeleteMultiSelect} handleMultiSelect={handleMultiSelect} arraySelectedTags={arraySelectedTags} isQuestion={false} />
                </View>
                
                <View style={{ flex: 1 }}>
                    <InputTextWithCharLimit value={data['titleNotes']} placeholder={CastConstants.ENTER_CLINICAL_NOTES_TITLE} charLimit={data['charLimitTitleNotes']} handleChange={handleChangeNotes} name={'titleNotes'} titleLabel={CastConstants.GIVE_YOUR_NOTEBOOK_A_TITLE} arraySimilarQuestion={arraySimilarQuestions} errors={errors} />
                </View>

                <FlatList
                    style={{ paddingBottom: '5%' }}
                    data={arrayImageTextOfNotes}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    scrollEnabled={false}
                />
              
        {/* {errors['textImageContent']?  <IconMaterial style={[{  fontSize: 25, color: '#d61515', alignSelf:'flex-end', marginRight:'5%' }]} name="error-outline" />: null } */}
               
                <View style={styles.viewAddButton}>
                    <IconEntypo name="plus" style={{ fontSize: 25, color: 'rgb(0, 123, 255)' }} />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '80%' }}>

                        <TouchableOpacity onPress={() => this.addImageOrText(true)} >
                            <Text style={styles.textAddImage} >{CastConstants.ADD_TEXT}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.addImageOrText(false)}>
                            <Text style={styles.textAddImage}>{CastConstants.ADD_IMAGE}</Text>
                        </TouchableOpacity>

                    </View>
                </View>
                <View>
                    <View style={{ flexDirection: 'row',  justifyContent:'center', marginTop:'5%' }}>
                        <Text style={styles.textSummaryTitle}> {CastConstants.ENTER_SUMMARY}</Text>

                        {errors['summary'] ? <IconMaterial style={[{ marginTop: '2%', fontSize: 25, color: '#d61515',  }]} name="error-outline" /> : null
                        }
                    </View>
                    <FlatList
                        style={{ paddingBottom: '5%', paddingTop:'5%' }}
                        data={arraySummaryNotes}
                        renderItem={this.renderItemSummary}
                        keyExtractor={(item, index) => index.toString()}
                        scrollEnabled={false}
                    />
                </View>


                <View style={[styles.singleLine, { marginTop: '5%' }]} />

              

                {data['isEditVetNote'] ? <View style={{ marginBottom: '5%' }}>

                <InputTextWithCharLimit value={data['editReasonCommentNotes']} placeholder={CastConstants.WRITE_DOWN_MEMO} charLimit={data['charLimitTitleNotes']} handleChange={handleChangeNotes} name={'editReasonCommentNotes'} titleLabel={CastConstants.PLEASE_INCLUDE_NOTE} arraySimilarQuestion={arraySimilarQuestions} errors={errors} />

                                        {/* <InputTextWithCharLimit value={data['editReasonCommentNotes']} placeholder={CastConstants.WRITE_DOWN_MEMO} charLimit={data['charLimitReason']} name={'editReasonCommentNotes'} handleChange={handleChangeNotes} titleLabel={CastConstants.PLEASE_INCLUDE_NOTE} arraySimilarQuestion={[]} errors={errors} /> */}
                                    </View> : null}


              {data['isEditVetNote']? null:  <View style={[styles.buttonSaveDraft, { height: 50, }]}>
                    <TouchableOpacity style={{ justifyContent: 'center', width: '100%', height: '100%' }} onPress={onClickPostNotesAsDraft}>
                        {this.state.isSaveDraftClicked ? <Text style={[styles.completeText, { alignSelf: 'center', height: 50 }]}>{CastConstants.SAVE_DRAFT}</Text>
                            :
                            <Text style={[styles.completeText, { alignSelf: 'center', height: 50 }]}>{CastConstants.DISCARD_DRAFT}</Text>
                        }
                    </TouchableOpacity>
                    </View> }

                    {data['isEditVetNote']?   <View style={[styles.buttonRoundCorner, { justifyContent: 'center', }]}>
                    <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => {
                         onClickPostNotes()  
                    }}>
                       <Text style={[styles.textButton]}>{CastConstants.SAVE_POST}</Text>
                            
                    </TouchableOpacity>
                </View>
                :  <View style={[styles.buttonRoundCorner, { justifyContent: 'center', }]}>
                <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => {
                    { isPreviewClickedNotes ? onClickPostNotes() : onClickPostNotesAsPreview() }
                }}>
                    {isPreviewClickedNotes ? <Text style={[styles.textButton]}>{CastConstants.SAVE_POST}</Text>
                        : <Text style={[styles.textButton]}>{CastConstants.PREVIEW_CONFIRMATION}</Text>}
                </TouchableOpacity>
            </View> }

            </View>
        )
    }
}
const styles = {
    textSummaryTitle: {
        fontSize: 16,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center',
       
    },
    inputTitleStyle: {
        height: 40,
        borderColor: 'lightgray',
        borderWidth: 1,
        borderRadius: 5,

        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 13,
        color: '#262626',

        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    buttonPhoto: {
        // width: screenWidth / 2 - 30,
        //height: screenWidth / 2 - 100,
        // borderColor: 'lightgray',
        // borderWidth: 1,
        // borderRadius: 5,
    },
    imageProperty: {
        // maxHeight: '100%',
        // maxWidth: '100%',
        width: 100,
        height: 100,
        borderRadius: 10,
        marginLeft: 2
    },
    textSubTitle: {
        fontSize: 16,
        color: 'rgba(129, 128, 129, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    singleLine: {
        width: '100%',
        height: 5,
        backgroundColor: 'lightgray'
    },
    viewSummaryBlock: {

        height: 40,
        backgroundColor: 'white',
        marginTop: '2%',
        borderColor: '#aaaaaa',
        borderWidth: 1,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 3,

    },
    viewTextBlock: {
        // marginLeft: '5%',
        // marginRight: '5%',
        alignSelf: 'center',
        height: 100,
        backgroundColor: 'white',
        marginTop: '1%',
        borderColor: '#aaaaaa',
        borderWidth: 1,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 3,
        width: '90%',
    },
    viewAddButton: {
        backgroundColor: 'rgb(229, 242,255)',
        width: '80%',
        height: 100,
        //marginBottom:'2%', 
        alignSelf: 'center',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textAddImage: {
        fontSize: 14,
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    titleText: {
        fontSize: 16,
        color: 'rgba(51,51,51, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    buttonRoundCorner: {
        backgroundColor: '#0084ff',
        borderRadius: 10,
        alignSelf: 'center',
        // justifyContent: 'center',
        height: verticalScale(50),
        // marginBottom:'5%'
    },
    textButton: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '3%',
        paddingRight: '3%'
    },
    textNoteBookTitle: {

        fontSize: 16,
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    charLimit: {
        fontSize: 12,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoValue: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    completeText: {
        fontSize: 14,
        color: 'gray',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    caretDownIcon: {

        marginRight: '2%'
    },
    textInputAge: {
        backgroundColor: 'white',
        paddingLeft: '2%',
        borderColor: 'lightgray',
        borderWidth: 1
    },
    inputTextStyle: {
        marginLeft: '5%',
        marginRight: '5%',
        //marginBottom: '5%',
        // marginTop:'5%',
        height: 40,
        backgroundColor: 'white',
        borderColor: 'lightgray',
        borderWidth: 1,
        justifyContent: 'center',

        // paddingTop: 21,
        // paddingBottom: 20,
        // paddingLeft: 10,
        // paddingRight: 10,
    },
    inputTextAreaStyle: {
        // marginLeft: '5%',
        // marginRight: '5%',
        height: 100,
        backgroundColor: 'white',
        marginTop: '3%',
        borderColor: 'gray',
        borderWidth: 1,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingTop: 21,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,

    },
    viewSection: {
        flexDirection: 'row',
        margin: '5%',
        height: verticalScale(40),
        backgroundColor: '#f7f8fa',
        borderColor: 'gray',
        borderWidth: 1,
        shadowColor: 'rgba(0,0,0,0.7)',
        shadowOffset: {
            width: 2,
            height: 4
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        alignItems: 'center',
    },
    viewSectionOpen: {

        margin: '5%',
        backgroundColor: '#f7f8fa',
        borderColor: 'gray',
        borderWidth: 1,
        shadowColor: 'rgba(0,0,0,0.7)',
        shadowOffset: {
            width: 2,
            height: 4
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        height: '50%',
        marginBottom: '5%',
        justifyContent: 'space-around'
    },

}