import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Dimensions, FlatList, TextInput, ActivityIndicator } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { ScaledSheet, verticalScale } from 'react-native-size-matters'
import IconEvillIcon from "react-native-vector-icons/EvilIcons";
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { isEmpty, isNull } from "lodash";
import commonStyle from '../../stylesheet/common/commonStyles.style'
import DashboardConstants from '../../constants/Dashboard'

export default class QuestionsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isSearchbarDataShow: false,
            isCrossClick: false,
            searchBarBgColor: '', //'rgb(64,66, 67)',
            searchTextColor: 'gray',
            searchIconColor: 'gray',
            textSearch: '',
            query: "",
            visible: true,
            isNotificationShow: false,
        }
        // this.arrayData = [];
     this.arrayData = [{ type: DashboardConstants.TOPICS, title: 'Diabetes' }, { type: DashboardConstants.TOPICS, title: 'Diabetes for cats' }, { type: DashboardConstants.TAGS, title: 'Diabetes type || syndrome' }, { type: DashboardConstants.TAGS, title: 'Diabetes retinopathy' } ]
      
    }

    // _onChange = (name, value) => {
    //     let text = value;
    //     if (text.length > 0) {
    //         this.setState({
    //             isSearchbarDataShow: true,
    //             //searchBarBgColor: 'white',
    //             // searchTextColor: 'black',
    //             // searchIconColor: 'black'
    //         })
    //     } else {
    //         this.setState({
    //             isSearchbarDataShow: false,
    //             // searchBarBgColor: 'rgb(64,66, 67)',
    //             // searchTextColor: 'white',
    //             // searchIconColor: 'white',
    //         })
    //     }
    //     this.setState(
    //         {
    //             [name]: value
    //         },
    //         () => {
    //             const query = value.trim();
    //             if (!isEmpty(query)) {
    //                 this._search(query);
    //             } else {
    //                 // this._resetState();
    //             }
    //         }
    //     );
    // };
    _resetState = () => {
        // return this.props.searchActions.resetState();
    };
    renderSearchRow = (item) => {
        return (
            <View style={{ height: 50, justifyContent: 'center' }}>
                <TouchableOpacity style={{ margin: '2%' }} onPress={() => this.moveToUserProfile(item.item.id)}>
                    <View style={{ flexDirection: "row" }}>
                        <Image style={{ width: 25, height: 25 }} />
                        <Text style={[styles.textModalData, { marginRight: '5%', color: 'black' }]}> {getUsername(item.item)}</Text>

                    </View>
                </TouchableOpacity>
            </View>
        )
    }

  
    renderQuestionItem = (item) => {
        return (
            <TouchableOpacity style={{backgroundColor:'white'}} >
                <Text style={{ margin: '5%' }}> {item.item.post_type};  {item.item.title} </Text>
                <View style={commonStyle.singleLine} />
            </TouchableOpacity>
        )
    }

    renderItem = (item) => {
        return (
            <TouchableOpacity style={{backgroundColor:'white'}} onPress={()=> this.setState({query:''})}>
                <Text style={{ margin: '5%' }}> {item.item.type};  {item.item.title} </Text>
                <View style={commonStyle.singleLine} />
            </TouchableOpacity>
        )
    }
    render() {
        const { query, visible } = this.state;
        const {fetching, arrayQuestionData,handleChange, data, onClickSearchButton } = this.props;
        return (
            <SafeAreaView>
                {/* <KeyboardAwareScrollView> */}
                {/* Top search bar view  */}
                <View style={{ flexDirection: 'row', height: '11%' }}>
                    <IconAntDesign name="left" style={{ marginLeft: '5%', fontSize: 25, marginTop: '5%' }} onPress={()=> this.props.navigation.goBack()} />
                    <View style={[styles.searchBarView, { backgroundColor: this.state.searchBarBgColor }]}>
                        
                        <TextInput
                            placeholderTextColor={this.state.searchTextColor}
                            placeholder= {`${DashboardConstants.SEARCH} ...`}
                            style={[styles.inputSearchStyle, { color: 'black' }]}
                            value={data['search']}
                            onChangeText={text => handleChange('search', text)}
                            onSubmitEditing ={onClickSearchButton}
                            name="search"
                        />
                    </View>
                </View>
                <View style={commonStyle.singleLine} />

                {/* Search view with icon */}
               {data['search'].length>0? <View style={{flexDirection:'row', alignItems:'center'}}>
                        <IconEvillIcon name="search" color={this.state.searchIconColor} style={{  marginLeft: '3%', marginRight: '1%', fontSize: 30 }} /> 
                        <Text style={{margin:'5%'}} >{data['search']}</Text>
                        </View> : null}
                        {data['search'].length>0?<View style={commonStyle.singleLine} /> : null}
                {/*  */}

                <View style={{ alignItems: 'center', width: '100%', height: '90%', backgroundColor: 'rgb(245, 247, 249)' }}>
                 
              {arrayQuestionData.length>0 ?
               <FlatList
               style={{ width: '100%'}}
               data={arrayQuestionData}
               renderItem={this.renderQuestionItem}
               keyExtractor={(item, index) => index.toString()}
      /> :
      <Image source={require('../../../assets/doctorPlaceholder.png')} style={styles.doctorPlaceholder} />}

{fetching ? <View style={{ justifyContent: 'center', position:'absolute', alignSelf:'center',  width:'100%', height:'100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                    </View>: null}

                 
                 

{/* top text field search flat list  */}
               {/* {data['search'].length>0? <FlatList
                        style={{ width: '100%', position:'absolute' }}
                        data={arrayQuestionData}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
               />: null } */}
                </View>


                {/* </KeyboardAwareScrollView> */}


            </SafeAreaView>
        )
    }
}

const styles = {
    searchBarView: {
        flexDirection: 'row',
        margin: '5%',
        height: verticalScale(40),
        borderRadius: 5,
        borderColor: 'gray',
        borderWidth: 0.5,
    },
    inputSearchStyle: {
        height: verticalScale(40),
        //fontFamily: 'Montserrat-Regular',
        fontWeight: '300',
        fontSize: 16,
        width: '70%',
        color: 'black',
        marginLeft: '5%',
    },
    doctorPlaceholder: {
        alignSelf: 'center',
        marginTop: '20%',

    },

}