
import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, FlatList, TextInput, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale, scale } from 'react-native-size-matters';
import signupConstants from '../../constants/Signup';

import CastConstants from '../../constants/Cast'
import * as Animatable from 'react-native-animatable';

import SimplePicker from 'react-native-simple-picker';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

export default class PostBasicInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            selectedGender: '',
            isSectionOpen: false,
            isAnimalSelected: false,
            query: "",

            animalVariety: "",
            animalGender: '',
            animalAgeYear: '',
            animalAgeMonth: '',
            pickerSelected: '',
        }
    }

    fadeInDownView = () => this.refs.animalView.fadeInDown(920).then(endState => endState.finished ? this.setState({ isShowAnimalSpecies: !this.state.isShowAnimalSpecies }) : 'Slide down cancelled');

    showAnimalSpecies = () => {
        // this.setState({ isShowAnimalSpecies: !this.state.isShowAnimalSpecies });
        this.fadeInDownView()
    }

    openVarietyPicker = () => {
        this.setState({ pickerSelected: 'pickerVariety' })
        this.refs.pickerVariety.show();
    }

    render() {
        const { onClickToOpenAnimalSection, data, arrayAnimalSpecies, titleArrayAnimalSpecies, arrayBreed, titleArrayBreed, arrayGender, titleArrayGender, handleChange } = this.props;

        return (
            <Animatable.View ref={'animalView'}>

                <View style={[styles.viewSectionOpen]}>
                    <TouchableOpacity style={{ height: 50 }} onPress={onClickToOpenAnimalSection}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <IconAntDesign name={"caretright"} style={{ marginLeft: '5%', color: 'rgba(51,51,51,255)', marginRight: '1%', marginTop: '2%' }} />
                            <Text style={styles.basicInfoTitle} >{CastConstants.BASIC_INFORMATION}</Text>
                        </View>

                    </TouchableOpacity>

                    {data['isAnimalSectionOpen'] ? <View>
                        <View style={styles.viewHorizontalValues}>
                            <Text style={[styles.basicInfoValue]}>{CastConstants.BASIC_INFO_ANIMAL_SPECIES}</Text>

                            <View style={[{ flex: 0.7 }]}>
                                <View style={styles.inputTextStyle}>
                                    <TouchableOpacity onPress={() => {
                                        this.setState({ pickerSelected: 'pickerAnimalSpecies' })
                                        this.refs.pickerAnimalSpecies.show()
                                    }} style={{ justifyContent: 'center' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            {data['animalSpecies'] === '' || data['animalSpecies'] < 1 ? <Text style={[styles.basicInfoPlaceholder, { flex: 0.95, paddingRight: '2%' }]} > {CastConstants.PLEASE_SELECT_AN_ANIMAL_SPECIES}</Text> : <Text style={[styles.basicInfoValue, { flex: 0.95, paddingRight: '2%' }]} > {data['animalSpecies']}</Text>
                                            }
                                            <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        {data['animalSpecies'] != '' || data['animalSpecies'] > 0 ?
                            <View>
                                <View style={styles.viewHorizontalValues}>
                                    <Text style={[styles.basicInfoValue]}>{CastConstants.BASIC_INFO_VARIETY}</Text>

                                    <View style={[{ flex: 0.7 }]}>
                                        <View style={styles.inputTextStyle}>
                                            <TouchableOpacity onPress={() => this.openVarietyPicker()} style={{ justifyContent: 'center' }}>

                                                <View style={{ flexDirection: 'row' }}>
                                                    {data['animalVarietyShow'] === '' ? <Text style={[styles.basicInfoPlaceholder, { flex: 0.95, paddingRight: '2%' }]} > {CastConstants.PLEASE_SELECT_A_BREED}</Text> : <Text style={[styles.basicInfoValue, { flex: 0.95, paddingRight: '2%' }]} > {data['animalVarietyShow']}</Text>
                                                    }
                                                    <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />
                                                </View>

                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.viewHorizontalValues}>
                                    <Text style={[styles.basicInfoValue]}>{CastConstants.BASIC_INFO_SEX}</Text>
                                    <View style={[{ flex: 0.7 }]}>
                                        <View style={styles.inputTextStyle}>
                                            <TouchableOpacity onPress={() => this.refs.pickerGender.show()} style={{ justifyContent: 'center' }}>
                                                <View style={{ flexDirection: 'row', }}>

                                                    {data['animalGender'] === '' || data['animalGender'] < 1 ? <Text style={[styles.basicInfoPlaceholder, { flex: 0.95, paddingRight: '2%' }]} > {CastConstants.PLEASE_SELECT_A_GENDER}</Text> : <Text style={[styles.basicInfoValue, { flex: 0.95, paddingRight: '2%' }]} > {data['animalGender']}</Text>
                                                    }
                                                    <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />

                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>

                                <View style={[styles.viewHorizontalValues, { marginBottom: '5%' }]}>
                                    <Text style={styles.basicInfoValue}>{CastConstants.BASIC_INFO_AGE}</Text>
                                    <View style={{ flex: 0.7, marginLeft: '6%', marginRight: '4%' }}>
                                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <TextInput
                                                placeholder='年'
                                                keyboardType='numeric'
                                                value={data['animalAgeYear']}
                                                onChangeText={(value) => handleChange('animalAgeYear', value, -1)}
                                                style={[styles.textInputAge, { height: verticalScale(30), width: scale(60), flex: 0.45 }]} />
                                            <TextInput
                                                placeholder='月'
                                                keyboardType='numeric'
                                                value={data['animalAgeMonth']}
                                                onChangeText={(value) => handleChange('animalAgeMonth', value, -1)}
                                                style={[styles.textInputAge, { height: verticalScale(30), width: scale(100), flex: 0.45 }]} />
                                        </View>
                                    </View>
                                </View>
                            </View> : <View style={styles.viewHorizontalValues} />}
                    </View> : null}
                </View>
                <SimplePicker
                    ref={'pickerAnimalSpecies'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={titleArrayAnimalSpecies}
                    onSubmit={(option) => {
                        handleChange('animalSpecies', option);
                    }}
                />
                {this.state.pickerSelected === 'pickerVariety' ? <SimplePicker
                    ref={'pickerVariety'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={titleArrayBreed}
                    onSubmit={(option) => {

                        // We need to pass index not name of the object in api.
                        let indexOfOption = titleArrayBreed.indexOf(option)
                        let objectWithCompleteData = arrayBreed[indexOfOption];

                        console.log(" animal --variety ", objectWithCompleteData.id, "animalVarietyShow---", option);

                        handleChange('animalVarietyShow', option ? option : '');
                        handleChange('animalVariety', objectWithCompleteData.id ? objectWithCompleteData.id : '');
                    }}
                /> : <SimplePicker
                        ref={'pickerVariety'}
                        cancelText={signupConstants.CANCEL}
                        confirmText={signupConstants.CONFIRM}
                        options={titleArrayBreed}

                    />}
                <SimplePicker
                    ref={'pickerGender'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={titleArrayGender}
                    onSubmit={(option) => {

                        handleChange('animalGender', option);
                    }}
                />
            </Animatable.View>)
    }
}


const styles = {
    viewMiddle: {
        height: verticalScale(60), justifyContent: 'center'
    },
    viewInside: {
        justifyContent: 'space-between',
        marginLeft: '6%',
        marginRight: '4%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    viewHorizontalValues: {
        flexDirection: 'row',
        marginLeft: '5%',
        marginTop: '1%',
        marginBottom: '2%',
        //backgroundColor:'pink', 
        marginRight: '2%'
    },

    basicInfoTitle: {
        fontSize: 16,
        color: 'rgba(51,51,51,255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginTop: '3%',

    },
    basicInfoValue: {
        fontSize: 12,
        color: 'rgba(110,110,110,255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        flex: 0.3,
        paddingLeft: '5%',

    },
    basicInfoPlaceholder: {
        fontSize: 12,
        color: 'lightgray',
        fontFamily: 'NotoSansCJKjp-Medium',
        flex: 0.3,
        paddingLeft: '5%'
    },
    caretDownIcon: {
        marginRight: '2%',
        flex: 0.1
    },
    textInputAge: {
        backgroundColor: 'white',
        paddingLeft: '5%',
        borderColor: 'lightgray',
        borderWidth: 1
    },
    inputTextStyle: {
        marginLeft: '5%',
        marginRight: '5%',
        //marginBottom: '5%',
        // marginTop:'5%',
        height: 40,
        backgroundColor: 'white',
        borderColor: 'lightgray',
        borderWidth: 1,
        justifyContent: 'center',

        // paddingTop: 21,
        // paddingBottom: 20,
        // paddingLeft: 10,
        // paddingRight: 10,
    },

    viewSection: {
        flexDirection: 'row',
        margin: '5%',
        height: verticalScale(40),
        backgroundColor: '#f7f8fa',
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        alignItems: 'center',
    },
    viewSectionOpen: {
        margin: '5%',
        backgroundColor: '#f7f8fa',
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        //height: '50%',

        // justifyContent: 'space-around'
    },

}
