import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, FlatList, TextInput, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale, scale } from 'react-native-size-matters';
import IconAntDesign from 'react-native-vector-icons/AntDesign'
import IconEntypo from 'react-native-vector-icons/Entypo'

import SimplePicker from 'react-native-simple-picker';
import signupConstants from '../../constants/Signup';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';

import CastConstants from '../../constants/Cast'


export default class CastCategorySelection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            strSelectedTags: '',
            arrayTagButtons: [],
            arraySpecialityButtons: [],
            tagValue: '',
            specialityNotes: '',
            pickerSelected:'',
          
        }

    }

    componentDidMount(){
       
    }

    removeSpecialityFromArray = (indexOfDeleteItem) => {
        // var array = [...this.state.arraySpecialityButtons]; // make a separate copy of the array
        // var index = indexOfDeleteItem;

        // console.log(" on delete index", index, "array of ===", array);
        // if (index > -1) {
        //     array.splice(index, 1);
        //     this.setState({ arraySpecialityButtons: array });
        // }
        // console.log(" arraySpecialityButtons  ", this.state.arraySpecialityButtons);

        { this.props.isQuestion ? this.props.onClickDeleteMultiSelect('specialty', indexOfDeleteItem) : this.props.onClickDeleteMultiSelect('specialtyNotes', indexOfDeleteItem) };
    }

    removeTagFromArray = (indexOfDeleteItem) => {
        { this.props.isQuestion ? this.props.onClickDeleteMultiSelect('tags', indexOfDeleteItem) : this.props.onClickDeleteMultiSelect('tagNotes', indexOfDeleteItem) };
    }

    // Add user typed text in the array. 

    addNewTagInArray(valueToAdd) {

       // console.log("---- handleMultiSelect --- ", addTypeTagInArray);

        { this.props.isQuestion ? this.props.handleMultiSelect('tags', valueToAdd) : this.props.handleMultiSelect('tagNotes', valueToAdd) };
        this.setState({ tagValue: "" })

    }

    renderSpecialityItem = (item, index) => {
        return (
            <View >
                <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 20, height: 30, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginRight: 5 }}>
                    <IconEntypo name="cross" style={{ paddingLeft: 5, color: 'gray', fontSize: 16 }} onPress={() => this.removeSpecialityFromArray(item.index)} />

                    <Text style={[styles.textOfTag, { paddingRight: 5 }]}>{item.item} </Text>
                </View>
            </View>

        )
    }

    renderTagItem = (item, index) => {
        return (
            <View>
                <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 20, height: 30, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginRight: 5 }}>
                    <Text style={styles.textOfTag}>{item.item} </Text>
                    <IconEntypo name="cross" style={{ color: 'red', fontSize: 16, paddingRight: 5 }} onPress={() => this.removeTagFromArray(item.index)} />
                </View>
            </View>

        )
    }

    renderSearchTagItem =(item, index) =>{
       
            return( <TouchableOpacity onPress ={() => {
               
                this.addNewTagInArray(item.item)
            }}>
                <View style={{ justifyContent: 'center'}}>
                    <Text style={styles.tagInSuggestion}>{item.item} </Text>
                    <View style={{width:'100%', height:'1%', backgroundColor:'lightgray',}}/>
                </View>
            </TouchableOpacity>)

    }
    
    onClickSpecialityCross = () => {
        { this.props.isQuestion ? this.props.removeAllSpeciality('specialty') : this.props.removeAllSpeciality('specialtyNotes') }
    }

    onClickOpenCategory = ()=>{
        if (this.props.titleArrayCategory.length>0) {
            this.refs.pickerCategory.show();
        } else {
            this.props.onClickCategoryToLoadData();
            setTimeout(() => {
                this.setState({
                    pickerSelected:'pickerCategory'
                }),
                this.refs.pickerCategory.show();
            }, 1000);
        }      
    }

    render() {
        const { arraySearchTags,showTagSearchData,onClickCategoryToLoadData, getBasicDetailToPost,removeAllSpeciality, arraySelectedSpecialities, categoryObject, titleArrayCategory, tagObject, titleArrayTag, specialityObject, titleArraySpecialityMaster, titleArrayAnimalSpecies, handleChange, errors, onSelectedTagsChange, handleMultiSelect, data, arraySelectedTags, isQuestion } = this.props;

        return (
            <View>
                <View style={{ margin: '5%' }}>
                    <Text style={styles.basicInfoTitle}>{CastConstants.SET_THE_POST_CATEGORY}</Text>

                    <View style={{ flexDirection: 'row', marginTop: '5%', marginBottom: '5%' }}>
                        <Image source={require('../../../assets/lightBulb.png')} />
                        <View>
                            <Text style={styles.textInLightGray}>{CastConstants.MORE_ANSWER}</Text>
                            <Text style={styles.textInLightGray}>{CastConstants.IT_WILL_BE_EASIER}</Text>
                        </View>
                    </View>

                    <View style={styles.viewSectionOpen}>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.basicInfoValue, { flex: 0.3 }]}>{CastConstants.QUESTION_CATEGORY}</Text>

                            <View style={[{ flex: 0.7 }]}>
                                <View style={styles.inputTextStyle}>
                                    <TouchableOpacity onPress={() => this.onClickOpenCategory()} style={{ justifyContent: 'center' }}>
                                        {isQuestion ?
                                            <View style={{ flexDirection: 'row' }}>
                                                {data['categoryOfQuestion'] === '' ? <Text style={[styles.basicInfoPlaceholder, { flex: 0.95, paddingRight: '2%' }]} > {CastConstants.PLEASE_SELECT_A_CATEGORY_OF_QUESTION}</Text> : <Text style={[styles.basicInfoValue, { flex: 0.95, paddingRight: '2%' }]} > {data['categoryOfQuestion']}</Text>
                                                }
                                                <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />
                                            </View> : <View style={{ flexDirection: 'row' }}>
                                                {data['categoryOfVetNote'] === '' ? <Text style={[styles.basicInfoPlaceholder, { flex: 0.95, paddingRight: '2%' }]} > {CastConstants.PLEASE_SELECT_A_CATEGORY_OF_QUESTION}</Text> : <Text style={[styles.basicInfoValue, { flex: 0.95, paddingRight: '2%' }]} > {data['categoryOfVetNote']}</Text>
                                                }
                                                <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />
                                            </View>}
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                        {errors['category'] || errors['categoryNotes'] ? <IconMaterial style={[{ marginTop: '2%', fontSize: 25, color: '#d61515', position: 'absolute', right: -28 }]} name="error-outline" />
                            : null
                        }

                      
                        <View style={{ flexDirection: 'row', marginTop: '5%', marginBottom: '5%' }}>
                            <Text style={[styles.basicInfoValue, { flex: 0.3 }]}>{CastConstants.SPECIALITY_AREA}</Text>

                            <View style={[{ flex: 0.7 }]}>

                            <View style={[styles.inputTextStyle, {flexDirection:'row'}]}>

                            {arraySelectedSpecialities && arraySelectedSpecialities.length > 0 ? <View style={{paddingLeft:0, marginRight:5,flex:0.92, paddingTop:3.5}}>
                            <FlatList
                            
                                scrollEnabled={true}
                                horizontal={true}
                                data={arraySelectedSpecialities}
                                renderItem={this.renderSpecialityItem}
                                keyExtractor={(item, index) => index.toString()}
                            //extraData={titleArraySpecialityMaster}
                            />
                        </View> : <View />}

                            
                                    <TouchableOpacity onPress={() => {
                                        
                                            this.setState({pickerSelected:'pickerSpecialityArea'}),
                                            isQuestion ? (data['categoryOfQuestion'] === '' ? '' : this.refs.pickerSpecialityArea.show()) :
                                                data['categoryOfVetNote'] === '' ? '' : this.refs.pickerSpecialityArea.show()
                                        
                                    }} style={{ justifyContent: 'center' }}>
                                        <View style={{ flexDirection: 'row',alignItems:'center'}}>

                                {arraySelectedSpecialities && arraySelectedSpecialities.length === 0 ?  <Text style={[styles.basicInfoPlaceholder,{width:'91%'}]} > {CastConstants.PLEASE_SELECT_SUB_CATEGORY}</Text> :null}


                                            <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                       
                        {errors['specialty'] || errors['specialtyNotes'] ? <IconMaterial style={[{ marginTop: '21%', fontSize: 25, color: '#d61515', position: 'absolute', right: -28 }]} name="error-outline" />
                            : null
                        }
                       


                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.basicInfoValue, { flex: 0.3 }]}>{CastConstants.TAG}</Text>

                            <View style={[{ flex: 0.7 }]}>

                            <View style={[styles.tagInputField, { flexDirection:'row', flexWrap:'wrap'}]}>

                            {arraySelectedTags && arraySelectedTags.length > 0 ? <View style={{paddingLeft:5 ,marginRight:9, paddingTop:3.5, alignItems:'flex-start',}}>
                            <FlatList

                                scrollEnabled={true}
                                horizontal={true}
                                data={arraySelectedTags} //{this.state.arrayTagButtons}
                                renderItem={this.renderTagItem}
                                keyExtractor={(item, index) => index.toString()}
                            //extraData={this.state.arrayTagButtons}
                            />
                        </View> : <View style={{}}/>}

                               
                                    <TextInput
                                       style={arraySelectedTags && arraySelectedTags.length > 0 ? styles.textInputInTagWithData: styles.textInputInTagEmpty}
                                        placeholder={CastConstants.ADD_TAG}
                                        onChangeText={(text)=> {
                                            this.setState({tagValue: text}),
                                            showTagSearchData(text)
                                        }}
                                        onSubmitEditing={() => this.addNewTagInArray(this.state.tagValue)}
                                        value={this.state.tagValue}
                                    />

                                    {/* <TouchableOpacity onPress={() => {
                                        this.setState({pickerSelected:'pickerTag'}),
                                            isQuestion ? (data['categoryOfQuestion'] === '' ? '' : this.refs.pickerTag.show()) :

                                                data['categoryOfVetNote'] === '' ? '' : this.refs.pickerTag.show()
                                    
                                    }} style={{ justifyContent: 'center',  right:5, position:'absolute', marginTop:'3%' }}>
                                        <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />
                                    </TouchableOpacity> */}

                                
                            </View>
                          
                            
                            </View>
                        </View>

                    </View>
                </View>
              {this.state.tagValue && this.state.tagValue.length>1 && arraySearchTags&& arraySearchTags.length>0? <View style={styles.tagFlatList}>
                            <FlatList
                            
                            //style={{width:'100%', height:'100%'}}
                           
                            data={arraySearchTags}
                            renderItem={this.renderSearchTagItem}
                            keyExtractor={(item, index) => index.toString()}
                        //extraData={titleArraySpecialityMaster}
                        />
                        <View style={{width:'100%', height:'2%'}}/>
                    </View>: null }
                <View style={styles.singleLine} />

                <SimplePicker
                    ref={'pickerCategory'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={titleArrayCategory}
                    onSubmit={(option) => {

                        { isQuestion ? data['categoryOfQuestion'] = option : data['categoryOfVetNote'] = option };


                        let object = categoryObject;
                        let selectedKey = ''
                        let arrayTemp = Object.keys(object).map(function (key) {
                            if (object[key] === option) {
                                selectedKey = key;
                               
                            }
                            return object[key];
                        });
                        { isQuestion ? handleChange('category', selectedKey, -1) : handleChange('categoryNotes', selectedKey, -1) }
                        ;
                    }}
                />
               {this.state.pickerSelected === 'pickerSpecialityArea' ? <SimplePicker
                    ref={'pickerSpecialityArea'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={titleArraySpecialityMaster}
                    onSubmit={(option) => {
                        { isQuestion ? handleMultiSelect('specialty', option) : handleMultiSelect('specialtyNotes', option) };

                    }}
                /> : <SimplePicker
                 ref={'pickerSpecialityArea'}
                cancelText={signupConstants.CANCEL}
                confirmText={signupConstants.CONFIRM}
                options={titleArraySpecialityMaster}
            />}
              {this.state.pickerSelected === 'pickerTag' ?  <SimplePicker
                    ref={'pickerTag'}
                    cancelText={signupConstants.CANCEL}
                    confirmText={signupConstants.CONFIRM}
                    options={titleArrayTag}
                    onSubmit={(option) => {
                        { isQuestion ? handleMultiSelect('tags', option) : handleMultiSelect('tagNotes', option) };

                    }}
                /> : <SimplePicker
                 ref={'pickerTag'}
                cancelText={signupConstants.CANCEL}
                confirmText={signupConstants.CONFIRM}
                options={titleArrayTag}
               
            />}
            </View>
        )
    }
}

/*

 <View style={styles.inputTextStyle}>
                                    <TouchableOpacity onPress={() => this.refs.pickerTag.show()} style={{ justifyContent: 'center' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            {this.state.tagOfQuestion === '' ? <Text style={[styles.basicInfoPlaceholder, { flex: 0.95, paddingRight: '2%' }]} > {CastConstants.ADD_TAG}</Text> : <Text style={[styles.basicInfoValue, { flex: 0.95, paddingRight: '2%' }]} > {this.state.tagOfQuestion}</Text>
                                            }
                                            <IconAntDesign name={"caretdown"} style={[styles.caretDownIcon]} />
                                        </View>
                                    </TouchableOpacity>
                                </View>

*/

const styles = {
    singleLine: {
        width: '100%',
        height: 5,
        backgroundColor: 'lightgray'
    },
    viewTagFlatlist: {
        borderColor: 'lightgray',
        borderRadius: 5,
        borderWidth: 1
    },
    charLimit: {
        fontSize: 12,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoTitle: {
        fontSize: 16,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoValue: {
        fontSize: 12,
        color: 'rgba(110, 110, 110, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    tagInSuggestion:{
        fontSize: 12,
        color: 'rgba(166, 176, 198, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        padding:10
    },
    textOfTag: {
        flex: 0.9, alignSelf: 'center', textAlign: 'center', marginTop: 5,
        paddingLeft: 5,
        fontSize: 12,
        color: 'rgba(166, 176, 198, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textInLightGray: {
        fontSize: 12,
        color: 'rgba(166, 176, 198, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    caretDownIcon: {
   
        right: '2%'
    },
    textInput: {
        backgroundColor: 'white',
        paddingLeft: '2%',
        borderColor: 'lightgray',
        borderWidth: 1
    },
    basicInfoPlaceholder: {
        fontSize: 12,
        color: 'lightgray',
        fontFamily: 'NotoSansCJKjp-Medium',
       // flex: 0.3,
       // paddingLeft: '5%'
    },
    basicInfoPlaceholder2: {
        fontSize: 9,
        color: 'lightgray',
        fontFamily: 'NotoSansCJKjp-Regular',
        flex: 0.3,
        paddingLeft: '5%'
    },
    viewSectionOpen: {

        margin: '5%',
        //backgroundColor: '#f7f8fa',
        // borderColor: 'gray',
        // borderWidth: 1,
        // shadowColor: 'rgba(0,0,0,0.7)',
        // shadowOffset: {
        //     width: 2,
        //     height: 4
        // },
        // shadowOpacity: 0.5,
        //shadowRadius: 1,
        //  borderRadius: 8,
        //height: '50%',
        marginBottom: '5%',
        justifyContent: 'space-around'
    },
    inputTextStyle: {

        height: verticalScale(40),
        borderColor: 'lightgray',
        borderWidth: 1,
        justifyContent: 'center',

    },
    tagFlatList:{
        width:'90%', 
        minHeight:60, 
        marginLeft:'5%', 
        marginRight:'5%',
        borderColor: 'lightgray',
        borderWidth: 1,
        marginBottom:'5%',
        borderRadius:5
    },
    tagInputField:{
        borderColor: 'lightgray',
        borderWidth: 1,
       // justifyContent: 'center',
        minHeight:40
    },
    textInputInTagEmpty:{
     width:'90%',
     marginTop:'2%',
     padding:5
    },
    textInputInTagWithData:{
        marginTop:'2%',
        width:'40%'
    },
    inputTextStyleLarge: {

        height: verticalScale(60),
        borderColor: 'lightgray',
        borderWidth: 1,
        justifyContent: 'center',
    },
    inputTextAreaStyle: {
        // marginLeft: '5%',
        // marginRight: '5%',
        height: 30,
        backgroundColor: 'white',
        marginTop: '3%',
        borderColor: 'lightgray',
        borderWidth: 1,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingTop: 21,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    multiSelectDownStyle: {
        height: 40,
        backgroundColor: 'white',
        marginRight: '2%',
        shadowColor: 'rgba(0,0,0,0.7)',
        shadowOffset: {
            width: 2,
            height: 4
        },
        borderColor: 'lightgray',
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingLeft: 30
    },
    multiSelectStyle: {
        marginTop: '2%',
        height: 40,
        borderColor: 'lightgray',
        backgroundColor: 'pink',
        marginRight: '2%',
        shadowColor: 'rgba(0,0,0,0.7)',
        shadowOffset: {
            width: 2,
            height: 4
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingLeft: 30
    },
    multiSelectListStyle: {
        marginTop: '5%',
        height: 150,
        backgroundColor: 'yellow',
        shadowColor: 'rgba(0,0,0,0.7)',
        shadowOffset: {
            width: 2,
            height: 4
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        marginBottom: '2%',
    },
    listText: {
        color: '#0385ff',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        fontSize: 14,
        marginRight: 8,
        marginLeft: 8,
    },
}