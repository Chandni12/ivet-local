import React, { Component } from 'react';
import { View, FlatList, Text, CheckBox, Image, TouchableOpacity, Button, ActivityIndicator } from 'react-native';
import NotificationConstants from '../constants/Notification'
import commonStyles from '../stylesheet/common/commonStyles.style'
import { SafeAreaView } from 'react-navigation';
import IconAntDesign from 'react-native-vector-icons/AntDesign'
import HeaderBlue from '../commonComponent/HeaderBlue';
import ViewPopup from '../commonComponent/ViewPopup';
import * as Animatable from 'react-native-animatable';
import * as timeConstants from '../constants/TimeConstants';
import { IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL , IMAGE_DASHBOARD_NICK_NAME_PREFIX_URL} from '../api/endPoints'
const profilePhoto = require('../../assets/profilePhoto.png');

export default class NotificationSelection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNotificationPopupShow: false
        }
        this.arrayData = [NotificationConstants.NOTIFICATION_DESC1, NotificationConstants.NOTIFICATION_DESC1, NotificationConstants.NOTIFICATION_DESC2, NotificationConstants.NOTIFICATION_DESC2, NotificationConstants.NOTIFICATION_DESC3, NotificationConstants.NOTIFICATION_DESC4];

        this.arrayPopupData = [NotificationConstants.ALL_NOTIFICATIONS, NotificationConstants.POST_NOTIFICATION, NotificationConstants.IMPORTANT_NOTICE, NotificationConstants.OTHER_NOTIFICATIONS,
        ];
    }

    fadeInView = () => this.refs.popup.fadeIn(10).then(endState => endState.finished ? 'fade in up' : 'Slide down cancelled');

    fadeOutView = () => this.refs.popup.fadeOut(10).then(endState => endState.finished ? 'fade in up' : 'Slide down cancelled');

    renderItem = (item) => {
        console.log(" ")
        let url = '';
        let imgNickName = item.item.nickname_icon;
        let nickNameUrl = IMAGE_DASHBOARD_NICK_NAME_PREFIX_URL+imgNickName
        console.log(" item , nicknameurl ", nickNameUrl, item); 
        let userImgUrl;

        if (!item.item.nickname_icon ) {
            userImgUrl = profilePhoto;
        } else{
            userImgUrl = { uri: nickNameUrl };
        }
        
        return (
            <View>
                <View style={styles.viewBox}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: '10%', marginLeft: '2%', justifyContent: 'space-evenly', marginRight: '2%' }}>
                        <View style={styles.viewVertical} />
                        <Image source={userImgUrl} style={styles.profilePhotoWithoutCircle} />

                        <Text style={styles.textData}>{item.item.content + "\n" + timeConstants.dateDiffIn_dd_mm_yy(item.item.created_at)}</Text>
                        <TouchableOpacity onPress={() => this.deleteItemById(item.item.id)}>
                            <IconAntDesign name="closecircleo" style={{ color: '#f17e7e', fontSize: 19, marginLeft: '2%' }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    showPopup = () => {

        this.setState({ isNotificationPopupShow: !this.state.isNotificationPopupShow });


        // if (this.state.isNotificationPopupShow) {
        //     this.setState({ isNotificationPopupShow: false });
        //     setTimeout(() => {
        //         this.fadeOutView();
        //     }, 80);
        // } else {
        //     this.setState({ isNotificationPopupShow: true });
        //     setTimeout(() => {
        //         this.fadeInView();
        //     }, 50);
        // }

    }
    deleteItemById =(id) => {
        this.props.deleteItemById(id)
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 10 }}>
                <View style={{ flex: 10 }} >

                    <HeaderBlue title={NotificationConstants.NOTIFICATION} navigation={this.props.navigation} />

                    {/* <View style={styles.viewAllNotifications}>
                        <TouchableOpacity onPress={() => this.showPopup()} style={styles.buttonPopup}>

                            <View style={styles.viewInsidePoupButton}>
                                <Text style={styles.textNotificationTitle}>{NotificationConstants.ALL_NOTIFICATIONS}</Text>

                                <IconAntDesign name="caretdown" style={{ color: 'gray', fontSize: 14, marginLeft: '2%', height: 50 }} />
                            </View>
                        </TouchableOpacity>
                    </View> */}


                    {/* <View style={[styles.singleLine]} /> */}
                    <FlatList
                        style={{ flex: 7 }}
                        data={this.props.notificationData}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        extraData={this.props.notificationData}
                    />
                    {/* <View style={{ alignItems: 'center', alignSelf: 'center', height: '7%', }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('tabNavigator')}>
                            <Text style={styles.textStartButton}>{NotificationConstants.LOAD_MORE}</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>
                {this.state.isNotificationPopupShow ?
                    <Animatable.View ref={'popup'} style={styles.viewPopup}>
                        <ViewPopup arrayData={this.arrayPopupData} showPopup={this.showPopup} />
                    </Animatable.View> :
                    <View />
                }

                 {this.props.fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                </View> : null}
            </SafeAreaView>
        )
    }
}

const styles = {
    textNotificationTitle: {
        height: 50, marginLeft: '5%',
        color: 'rgba(155, 155, 155, 255)',
        fontSize: 13
    },
    viewPopup: {
        position: 'absolute',
        width: '18%',
        height: 60,
        marginTop: '27%',
        marginLeft: '15%',
        backgroundColor: 'white',
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1
        },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        borderRadius: 8,
        backgroundColr:'pink'
    },

    viewInsidePoupButton: {
        flexDirection: 'row',
        height: '100%',
        width: '100%',
        justifyContent: 'center',
    },

    textTitle: {
        fontSize: 15,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDesc: {
        fontSize: 10,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textStartButton: {
        fontSize: 18,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
    viewAllNotifications: {

        // borderColor: 'gray',
        // borderWidth: 1,
        width: '35%',
        marginLeft: '5%',
        marginTop: '5%',
        height: '5%',

        // alignItems: 'center',
        // justifyContent:'center',
        // backgroundColor:'yellow'
    },
    buttonPopup: {

        // width: '100%',
        marginTop: '5%',
        // height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor:'pink'
    },
    viewBox: {
        backgroundColor: 'white',


        margin: '3%',
        borderColor: '#d1dde5',
        borderWidth: 1,
        shadowColor: '#d1dde5',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        // backgroundColor:'red'
    },
    viewVertical: {
        width: '2%',
        backgroundColor: 'rgb(61, 160, 248)',
        height: '70%',
        marginTop: '2%',
        marginLeft:'2%',
        marginRight:'2%'
    },
    profilePhotoWithoutCircle: {
        width: 40,
        height: 40,
        borderRadius: 20,
        // marginTop:'2%'
        // borderColor: 'rgb(2, 132, 254)',
        // borderWidth: 1.5,
    },
    crossButton: {

    },
    textData: {
        fontSize: 13,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: '2%',
        width: '75%',

    }
}