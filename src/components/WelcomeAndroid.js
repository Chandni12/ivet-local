import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Linking, BackHandler, AppState } from 'react-native';
import welcomeConstants from '../constants/Welcome'
import commonStyle from '../stylesheet/common/commonStyles.style';
import { SafeAreaView } from 'react-navigation';
import { scale, verticalScale, moderateScale, ScaledSheet } from 'react-native-size-matters';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import firebase, { Notification, NotificationOpen } from "react-native-firebase";
import * as wordConstants from '../constants/WordConstants';
const iconWelcomeAndroid = require('../../assets/welcome_title_android.png');
const imageBackgroundAsset = require('../../assets/welcome_background.png');

console.disableYellowBox = true;
export default class WelcomeAndroid extends Component {
constructor (props){
    super (props); 
    this.state = {
        appState: AppState.currentState
      }
}

  
    
    openURLInBrowser = (url) => {
        Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
    };

    //****** Push notification  *****//

    async componentDidMount() {

    //     AppState.addEventListener('change', this._handleAppStateChange);

     // Foreground app work well    
         this.foregroundNotificationHandler();
       
    // Background 
        this.backgroundNotificationHandler();
    
    }

async backgroundNotificationHandler (){
    firebase.notifications().getInitialNotification()
      .then((notificationOpen: NotificationOpen) => {
        if (notificationOpen) {

            console.log(" 45 ===== ", notificationOpen);
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action;
          // Get information about the notification that was opened
          const notification: Notification = notificationOpen.notification;  

          if (notification) {

            // console.log("+================= notificationOpen ", notification, "===========");
             // App was opened by a notification
             // Get the action triggered by the notification being opened
            // const action = notificationOpen.action;
             // Get information about the notification that was opened
            
             console.log("^^^^^^^^^^^^^^^^^^",notification._data);
             if (notification._data) {

                 if(notification._data.type){
                     if ( notification._data.type.toUpperCase() !== wordConstants.CONST_ADMIN) {
                       return;
                     }
                   }
                  
                   console.log("69 ^^^^^^^^^^^^^^^^^^",notification._data.status);

                 let notificationStatus = notification._data.status;
                 console.log(" notification status ====", notificationStatus.toUpperCase());
     
                 if(notificationStatus.toUpperCase() === wordConstants.CONST_APPROVE){
     
                     this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_CONFIRM, userData: {} });
                 }else{
                     this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_FAILED, userData: {} });
                 }
             } else {
                 
             }
         }
     
        }
      });

   
}

    async foregroundNotificationHandler(){
        console.log(" createNotificationListeners ");

        this.notificationListener = firebase.notifications().onNotification(notification => {

            console.log(" createNotification notification is ===", notification);
            
         //  notification.android.setAutoCancel(false)

           // this.getInspectionUserLogs(this.state.user);

            const channelId = new firebase.notifications.Android.Channel(
                'Default',
                'Default',
                firebase.notifications.Android.Importance.High
            );
            firebase.notifications().android.createChannel(channelId);

            let notification_to_be_displayed = new firebase.notifications.Notification({
                data: notification._android._notification._data,
                sound: 'default',
                show_in_foreground: true,
                title: notification.title,
                body: notification.body,
            });

            if (Platform.OS == 'android') {
                notification_to_be_displayed.android
                    .setPriority(firebase.notifications.Android.Priority.High)
                    .android.setChannelId('Default')
                    .android.setVibrate(1000);
            }

            firebase.notifications().displayNotification(notification_to_be_displayed); 

            console.log("^^^^^^^^^",notification._data, "Only noti======",notification);
            if (notification._data) {
                if(notification._data.type){
                    if ( notification._data.type.toUpperCase() !== wordConstants.CONST_ADMIN) {
                      return;
                    }
                  }
                 
                let notificationStatus = notification._data.status;
                console.log(" notification status ====", notificationStatus.toUpperCase());

                if(notificationStatus.toUpperCase() === wordConstants.CONST_APPROVE){

                    this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_CONFIRM, userData: {} });
                }else{
                    this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_FAILED, userData: {} });
                }
            } else {
                
            }
        });
    }

    
    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);

    //  this.notificationListener();
      //  BackHandler.removeEventListener('hardwareBackPress', HomeScreen.handleBackButtonClick);
    }

    _handleAppStateChange = (nextAppState) => {
        if ( this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
          console.log('======== App has come to the foreground!');
          this.backgroundNotificationHandler();
        }else{
            // console.log('======== App in bacjgend!');
            // this.backgroundNotificationHandler();
        }
        this.setState({appState: nextAppState});
      };


    static handleBackButtonClick() {
        BackHandler.exitApp()
    }
    /*

//****** Push notification  *****/


    render() {
        return (
            // <SafeAreaView style={{flex:1}}>
            <View style={{ flex: 1 }}>
                {/* <ScrollView> */}
                <ImageBackground resizeMode={'stretch'} source={imageBackgroundAsset} style={{ width: '100%', height: '100%', justifyContent: 'space-around', }}>
                    <View style={{ flexDirection: 'row', alignSelf: 'center', flex: 0.23 }}>
                        <Image source={iconWelcomeAndroid} style={styles.welcomeNameLogo} />
                    </View>

                    <Text style={[styles.textSubTitle, { flex: 0.1, marginLeft: '2%', marginRight: '2%' }]}>{welcomeConstants.DESCRIPTION}</Text>
                    <Text style={[styles.textSubTitle2, { flex: 0.05, marginTop: '-4%' }]}>{welcomeConstants.WELCOME_TEXT}</Text>
                    <Text style={[styles.textSubTitle2, { flex: 0.05, marginTop: '-4%' }]}>{welcomeConstants.REGISTRATION_TITLE}</Text>

                    <View style={[styles.buttonViewRed, { flex: 0.07 }]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')}>
                            <Text style={styles.buttonText}>
                                {welcomeConstants.REGISTRATION_BUTTON_TITLE}
                                <Text style={styles.buttonText_small}>  {welcomeConstants.REGISTRATION_BUTTON_TITLE_2}</Text>
                            </Text>
                        </TouchableOpacity >
                    </View>

                    <View style={[styles.buttonViewBlue, { marginTop: '2%', flex: 0.07 }]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={styles.buttonText}>{welcomeConstants.LOGIN_BUTTON_TITLE}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ bottom: 0, marginLeft: '26%', flex: 0.2 }}>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.textBottom1Gray, { flexWrap: 'wrap' }]}>
                                {welcomeConstants.BY_CONTINUEING_USAGE_RULES}
                            </Text>
                            <TouchableOpacity onPress={() => this.openURLInBrowser(welcomeConstants.TERMS_AND_CONDITION_URL)}>
                                <Text style={styles.textBottom1Blue}>{welcomeConstants.IT_IS_CONSIDERED}</Text>

                            </TouchableOpacity>

                            <Text style={[styles.textBottom1Gray]}>{welcomeConstants.IT_IS_CONSIDERED_2}</Text>
                        </View>


                        <View style={{ flexDirection: 'row' }}>

                            <TouchableOpacity onPress={() => this.openURLInBrowser(welcomeConstants.PRIVACY_POLICY_URL)}>
                                <Text style={[styles.textBottom1Blue, { flexWrap: 'wrap', marginTop: '1%' }]}>
                                    {welcomeConstants.THAT_YOU_HAVE_AGREED_PRIVACY_POLICY}
                                </Text>
                            </TouchableOpacity>

                            <Text style={styles.textBottom1Gray}>{welcomeConstants.PRIVACY_POLICY_2}</Text>

                        </View>

                        {/* <TouchableOpacity onPress={()=> this.openURLInBrowser()}>
                               <Text style={[styles.textBottom1Blue , {flexWrap:'wrap', marginTop:'1%'}]}>
                                {welcomeConstants.THAT_YOU_HAVE_AGREED_PRIVACY_POLICY}
                                <Text style={styles.textBottom1Gray}>{welcomeConstants.PRIVACY_POLICY_2}</Text>
                                </Text>
                               </TouchableOpacity> */}


                        <Text style={[styles.textBottom1Gray, { right: '3%', alignSelf: 'flex-end', marginTop: '13%' }]}>{welcomeConstants.COPYRIGHT_TEXT}</Text>

                    </View>

                </ImageBackground>
                {/* </ScrollView> */}
            </View>
            //  </SafeAreaView>
        )
    }
}

const styles = ScaledSheet.create({
    welcomeNameLogo: {
        // width: wp('52.5%'),
        // height: hp('7%'),
        alignSelf: 'center',
        marginTop: verticalScale(148)
    },
    background: {
        flex: 1,
    },
    imageTop: {
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        width: '100%',
        height: verticalScale(120)
    },
    textTitleBlue: {
        fontSize: "34@ms",
        color: 'rgb(2, 132, 254)',
        alignSelf: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textTitleBlack: {
        fontSize: "34@ms",
        color: 'black',
        alignSelf: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textSubTitle: {
        alignSelf: 'center',
        fontSize: "15@ms",
        color: 'rgb(153, 153, 153)',
        textAlign: 'center',
        marginTop: '5%',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textSubTitle2: {
        alignSelf: 'center',
        fontSize: "12@ms",
        color: 'rgb(153, 153, 153)',
        textAlign: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textSubTitle3: {
        alignSelf: 'center',
        fontSize: "14@ms",
        color: 'gray',
        textAlign: 'center',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    buttonViewRed: {
        width: '80%',
        backgroundColor: 'rgb(214, 22, 22)',
        borderRadius: 2,
        height: verticalScale(45),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    buttonViewBlue: {
        width: '80%',
        backgroundColor: 'rgb(66, 130, 191)',
        borderRadius: 2,
        height: verticalScale(45),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    // button: {
    //     margin: '5%'
    // },
    imageBottom: {
        marginLeft: 0,
        marginBottom: 0,
    },
    buttonText: {
        color: 'white',
        // multiLine: true,
        textAlign: 'center',
        padding: 10,
        fontSize: "14@ms",
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    buttonText_small: {
        color: 'white',
        // multiLine: true,
        textAlign: 'center',
        fontSize: "10@ms",
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textBottom1Gray: {
        color: 'gray',
        fontSize: "10@ms",
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textBottom1Blue: {
        color: 'rgb(66, 130, 191)',
        fontSize: "10@ms",
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    textBottomCopyRight: {
        fontFamily: 'NotoSansCJKjp-Medium'
    }
})