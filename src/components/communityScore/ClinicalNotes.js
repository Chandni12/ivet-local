import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput } from 'react-native';
import CommunityScoreConstants from '../../constants/CommunityScore';
import commonStyles from '../../stylesheet/common/commonStyles.style'
import { checkError } from '../../utils'
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import communityScoreConstants from '../../constants/CommunityScore';
import IconEntypo from 'react-native-vector-icons/Entypo';
import ProfileConstants from '../../constants/Profile';
import EditIconView from '../../commonComponent/EditIconView';


export default class ClinicalNotes extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <View style={styles.viewBox}>

                <EditIconView title = {CommunityScoreConstants.CLINICAL}/>
             
                <View style={{ width: '90%', marginTop: '2.4%' }}>
                    <View style={{ flexDirection: 'row', marginLeft: '5%' }}>
                        <View style={[styles.viewOrangeBox, { marginRight: '5%' }]}>
                            <Text style={styles.textOrange}>{communityScoreConstants.BLOOD_IMMUNITY}</Text>
                        </View>
                        <Text>{communityScoreConstants.CS355}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '80%', marginTop: '5%', marginBottom: '5%' }}>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_1}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_2}</Text>
                        </View>
                    </View>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.BEST_ANSWERE}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.NOTES_0}</Text>
                        </View>
                    </View>

                </View>
                <View style={styles.viewSingleLine} />

                <View style={{ width: '90%', marginTop: '5%' }}>
                    <View style={{ flexDirection: 'row', marginLeft: '5%' }}>
                        <View style={[styles.viewOrangeBox, { marginRight: '5%' }]}>
                            <Text style={styles.textOrange}>{communityScoreConstants.BLOOD_IMMUNITY}</Text>
                        </View>
                        <Text>{communityScoreConstants.CS355}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '80%', marginTop: '2.5%', marginBottom: '2.5%' }}>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_1}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_2}</Text>
                        </View>
                    </View>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.BEST_ANSWERE}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.NOTES_0}</Text>
                        </View>
                    </View>

                </View>
                <View style={styles.viewSingleLine} />

                <View style={{ width: '90%', marginTop: '5%' }}>
                    <View style={{ flexDirection: 'row', marginLeft: '5%' }}>
                        <View style={[styles.viewOrangeBox, { marginRight: '5%' }]}>
                            <Text style={styles.textOrange}>{communityScoreConstants.BLOOD_IMMUNITY}</Text>
                        </View>
                        <Text>{communityScoreConstants.CS355}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '80%', marginTop: '5%', marginBottom: '5%' }}>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_1}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_2}</Text>
                        </View>
                    </View>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.BEST_ANSWERE}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.NOTES_0}</Text>
                        </View>
                    </View>

                </View>
                <View style={styles.viewSingleLine} />

                <Text style={[styles.textTitleLife, { marginTop: '3%', marginBottom: '3%' }]}>{communityScoreConstants.LIFE}</Text>

                <View style={{ width: '90%', marginTop: '5%' }}>
                    <View style={{ flexDirection: 'row', marginLeft: '5%' }}>
                        <View style={[styles.viewOrangeBox, { marginRight: '5%' }]}>
                            <Text style={styles.textOrange}>{communityScoreConstants.BLOOD_IMMUNITY}</Text>
                        </View>
                        <Text>{communityScoreConstants.CS355}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '80%', marginTop: '5%', marginBottom: '5%' }}>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_1}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.FOREST_TO_2}</Text>
                        </View>
                    </View>

                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.BEST_ANSWERE}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <IconEntypo name='dot-single' style={{ fontSize: 20 }} />
                            <Text>{communityScoreConstants.NOTES_0}</Text>
                        </View>
                    </View>

                </View>
                <View style={styles.viewSingleLine} />

                <Text style={[styles.textTitleLife, { marginTop: '3%', marginBottom: '3%' }]}>{communityScoreConstants.HOSPITAL_MANAGEMENT}</Text>

                <Text style={{ marginBottom: '10%', marginTop: '10%' }}>Updating ...</Text>

            </View>
        )
    }
}

const styles = {

    helpIcon: {

        width: 20,
        height: 20,
        // alignSelf: 'flex-end',
        // marginTop: '2%',
        // marginRight: '2%',
        // marginBottom: '-3%'
    },
    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: 'gray',
        borderWidth: 1,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        alignItems: 'center',
    },
    textTitleCliniclNotes: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center',
    },
    textTitleLife: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center',
    },
    viewIconPencil: {
        width: 30,
        height: 30,
        borderRadius: 15,
        borderColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        borderWidth: 0.5,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 1,
            height: 1
        },
    },
    viewOrangeBox: {
        borderWidth: 2,
        backgroundColor: '#ffedd2',
        borderColor: '#eb9f2c',
        height: verticalScale(25),
    },
    textOrange: {
        fontSize: 12,
        color: '#eb9f2c',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center',
    },
   
    textLightBlue: {
        fontSize: 13.5,
        color: 'rgba(190, 212, 230, 255);',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    viewSingleLine: {
        width: '80%',
        backgroundColor: 'lightgray',
        height: 1,
    }
}