import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput, ScrollView } from 'react-native';
import CommunityScoreConstants from '../../constants/CommunityScore';
import commonStyles from '../../stylesheet/common/commonStyles.style'
import { SafeAreaView } from 'react-navigation';
import * as Animatable from 'react-native-animatable';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import HeaderBlue from '../../commonComponent/HeaderBlue';
import ClinicalNotes from './ClinicalNotes';
import Description from './Description';


export default class CommunityScore extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView>
                    <HeaderBlue title={CommunityScoreConstants.MY_PAGE} navigation={this.props.navigation}/>
                <ScrollView>
                
                    {/* Community Score section */}
                    <View style={styles.viewBox}>
                        <Image source={require('../../../assets/help_icon.png')} style={styles.helpIcon} />
                        <Text style={styles.textTitle}>{CommunityScoreConstants.COMMUNITY_SCORE}</Text>
                        <Text style={styles.textSubheading}>Community Score (CS)</Text>
                        <Text style={[styles.textScore]}>354 cs</Text>
                        <Text style={styles.textPercentage}>{CommunityScoreConstants.TOP}-00%</Text>
                    </View>

                    {/* Category  */}
                    <View style={styles.viewBox}>
                        <Text style={[styles.textTitleCategory, { marginTop: '5%' }]}>{CommunityScoreConstants.BY_CATEGORY}</Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '90%', marginTop: '5%', marginBottom: '5%' }}>
                            <View>
                                <Text style={styles.textTitleScoreCategory}>2960 cs</Text>
                                <Text style={styles.textScoreDescCategory}>{CommunityScoreConstants.CLINICAL}</Text>
                            </View>
                            <View>
                                <Text style={styles.textTitleScoreCategory}>58 cs</Text>
                                <Text style={styles.textScoreDescCategory}>{CommunityScoreConstants.LIFE}</Text>
                            </View>
                            <View>
                                <Text style={styles.textTitleScoreCategory}>117 cs</Text>
                                <Text style={[styles.textScoreDescCategory]}>{CommunityScoreConstants.HOSPITAL_MANAGEMENT}</Text>
                            </View>
                        </View>
                    </View>

                    {/* Clinical Notes  */}
                    <ClinicalNotes />

                    {/* Description */}
                    <Description />
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = {
    helpIcon: {
        width: 20,
        height: 20,
        alignSelf: 'flex-end',
        marginTop: '2%',
        marginRight: '2%',
        marginBottom: '-3%'
    },
    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: 'gray',
        borderWidth: 1,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        height: 'auto',
        alignItems: 'center',
    },
    textTitle: {
        fontSize: 22,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textSubheading: {
        fontSize: 13.5,
        color: 'rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textScore: {
        fontSize: 34,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    textPercentage: {
        fontSize: 13.5,
        color: 'rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginBottom: '2%',
    },
    textTitleCategory: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },

    textTitleScoreCategory: {
        fontSize: 22.5,
        color: 'rgba(11, 123, 217, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textScoreDescCategory: {
        fontSize: 13.5,
        color: 'rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    basicInfoValue: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
}
