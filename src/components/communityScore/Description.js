import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput } from 'react-native';
import commonStyles from '../../stylesheet/common/commonStyles.style'
import { checkError } from '../../utils'
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import communityScoreConstants from '../../constants/CommunityScore';
import IconEntypo from 'react-native-vector-icons/Entypo';


export default class Description extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.viewBox}>

                <View style={{ width: '80%', alignself: 'center', marginBottom: '5%' }}>
                    <Text style={styles.textTitle}>CS:NL7</Text>
                    <Text style={styles.textSubTitle}>{communityScoreConstants.WHAT_IS_COMMUNITY_SCORE}</Text>
                    <Text style={styles.textDesc}>{communityScoreConstants.COMMUNITY_SCORE_DESC1}</Text>
                    <Text style={[styles.textDesc]}>{communityScoreConstants.COMMUNITY_SCORE_DESC2}</Text>
                </View>

                <Text style={styles.titleList}>{communityScoreConstants.LIST_OF_COMMUNITY_SCORE}</Text>

                <View style={[styles.viewSingleLine, { marginTop: '2%' }]} />
                <View style={styles.viewInsideList}>
                    <Text style={styles.textSubTitle}>{communityScoreConstants.AN_EVENT}</Text>
                    <Text style={styles.textSubTitle}>CS</Text>
                </View>
                <View style={[styles.viewSingleLine, { marginBottom: '2%' }]} />

                <View style={styles.viewInsideList}>
                    <Text style={styles.textList}>{communityScoreConstants.VOTE_FOR_ANSWERS2}</Text>
                    <Text style={styles.textList}>10</Text>
                </View>

                <View style={styles.viewInsideList}>
                    <Text style={styles.textList}>{communityScoreConstants.VOTE_FOR_NOTES}</Text>
                    <Text style={styles.textList}>7</Text>
                </View>

                <View style={styles.viewInsideList}>
                    <Text style={styles.textList}>{communityScoreConstants.VOTE_FOR_NOTES}</Text>
                    <Text style={styles.textList}>7</Text>
                </View>

                <View style={styles.viewInsideList}>
                    <Text style={styles.textList}>{communityScoreConstants.VOTE_FOR_QUESTION2}</Text>
                    <Text style={styles.textList}>5</Text>
                </View>

                <View style={styles.viewInsideList}>
                    <Text style={styles.textList}>{communityScoreConstants.ANSWERE_SELECTED_AS_BEST_ANSWERE}</Text>
                    <Text style={styles.textList}>5</Text>
                </View>

                <View style={styles.viewInsideList}>
                    <Text style={styles.textList}>{communityScoreConstants.SELECTED_BEST_ANSWERE}</Text>
                    <Text style={styles.textList}>3</Text>
                </View>

                <View style={styles.viewInsideList}>
                    <Text style={styles.textList}>{communityScoreConstants.POST}</Text>
                    <Text style={styles.textList}>1</Text>
                </View>
                <View style={{ marginBottom: '10%' }} />

            </View>
        )
    }
}

const styles = {
    viewSingleLine: {
        width: '90%',
        height: 1,
        backgroundColor: 'lightgray',

    },
    viewInsideList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '80%',
        alignself: 'center',
    },
    titleList: {
        alignSelf: 'center',
        marginBottom: '2%',
        marginTop: '2%',
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textList: {
        fontSize: 13.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDesc: {
        textAlign: 'center',
        fontSize: 12,
        color: 'rgba(46, 44, 44, 0.8)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginTop: '2%'
    },
    helpIcon: {
        width: 20,
        height: 20,
        alignSelf: 'flex-end',
        marginTop: '2%',
        marginRight: '2%',
        marginBottom: '-3%'
    },

    viewBox: {
        backgroundColor: 'white',
        width: '95%',
        alignSelf: 'center',
        marginTop: '2.5%',
        marginBottom: '2.5%',
        borderColor: 'gray',
        borderWidth: 1,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 2,
            height: 1
        },
        alignItems: 'center',
    },
    textTitle: {
        alignSelf: 'center',
        marginBottom: '5%',
        marginTop: '5%',
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textSubTitle: {
        alignSelf: 'center',
        paddingTop: '2%',
        paddingBottom: '2%',
        fontSize: 13.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },

    textSubheading: {
        fontSize: 13.5,
        color: 'rgba(190, 212, 230, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
}