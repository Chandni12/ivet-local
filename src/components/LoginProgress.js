import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Platform } from 'react-native';
import loginProgressConstants from '../constants/LoginProgressConstants'
import commonStyles from '../stylesheet/common/commonStyles.style'
import { SafeAreaView } from 'react-navigation';
import { checkError } from '../utils';
import { verticalScale, moderateScale, scale } from 'react-native-size-matters';
import * as wordConstants from '../constants/WordConstants';
const iconWelcomeTitle = require('../../assets/welcome_title.png');
const iconBearWithMen = require('../../assets/bear_with_men.png');
const iconVas = require('../../assets/vas.png');
const iconCross = require('../../assets/cross1x.png');

export default class LoginProgress extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     status: wordConstants.CONST_FAILED, // checking/confirmed/failed
        //     userData: ''
        // }
    }

    render() {
        const { status, userData, moveToCategory } = this.props;
       
        let textStatusMsg = <Text />
        let statusMessage = '';
        if (status === wordConstants.CONST_FAILED) {
            statusMessage = loginProgressConstants.FAILED;
            textStatusMsg = <Text style={[styles.textSubTitle, { color: '#d61515' }]}> {statusMessage} </Text>
        } else if (status === wordConstants.CONST_CONFIRM) {
            statusMessage = loginProgressConstants.CONFIRMED
            textStatusMsg = <Text style={[styles.textSubTitle, { color: 'green' }]}> {statusMessage} </Text>
        } else {
            statusMessage = loginProgressConstants.CHECKING
            textStatusMsg = <Text style={[styles.textSubTitle]}> {statusMessage} </Text>
        }
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    {/* <ScrollView> */}
                    <View style={{ flex: 0.85 }}>
                      {status === wordConstants.CONST_CHECKING? <View style={{ marginTop: '5%' }} /> :  <TouchableOpacity style={{ marginLeft: '5%', marginTop: '5%' }} onPress={() => this.props.navigation.goBack()} >
                            <Image source={iconCross} style={{ width: scale(15), height: verticalScale(15) }} />
        </TouchableOpacity> }

        <Image source={iconWelcomeTitle} style={{alignSelf: 'center', 
                        marginTop:"10%", marginBottom:'10%',
                        width:175, height: 40}} />
                        {/* <Image source={require('../../assets/welcome_title.png')} style={Platform.OS === 'ios' ? styles.imageTitleIos : styles.imageTitleAndroid} /> */}

                        <Text style={[styles.textSubTitle]}>{loginProgressConstants.REGISTRATION_FOR_IVET}</Text>
                        <Text style={[styles.textSubTitle2, { marginTop: '1%' }]}>{loginProgressConstants.CLICK_THE_BUTTON_BELOW}</Text>
                        <Text style={[styles.textSubTitle, { marginBottom: '5%', marginTop: '1%' }]}>{loginProgressConstants.I_CAN_CONFIRMED}</Text>

                        <Text style={Platform.OS === 'ios' ? styles.textStatusCenterIos : styles.textStatusCenterAndroid}>{loginProgressConstants.STATUS}:
                       {textStatusMsg}
                        </Text>

                        {status === wordConstants.CONST_FAILED && <View style={{ marginTop: '8%' }}>
                            <Text style={[styles.textSubTitle]}>{loginProgressConstants.FAILED_TO_VERIFY}</Text>
                            <Text style={styles.textSubTitle2}>{loginProgressConstants.NOT_APPOROPRIATE_INFORMATION}</Text>

                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                <Text style={styles.textSubTitle}>{loginProgressConstants.IT_CAN_BE_CONSIDERED}></Text>

                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup', { userData: userData })}>
                                    <Text style={styles.textSubTitle_green}>{loginProgressConstants.CLICK_HERE_REAPPLICATION}</Text>
                                </TouchableOpacity>
                            </View>

                        </View>}

                        {status === wordConstants.CONST_CHECKING && <TouchableOpacity disabled={true} style={styles.button} >
                            <View style={styles.buttonViewBlueDisabled}>
                                <Text style={styles.buttonText}>{loginProgressConstants.USE_START}</Text>
                            </View>
                        </TouchableOpacity>
                        }

                        {status === wordConstants.CONST_CONFIRM && <TouchableOpacity style={styles.button} onPress={moveToCategory}>
                            <View style={styles.buttonViewBlue}>
                                <Text style={styles.buttonText}>{loginProgressConstants.NEXT}</Text>
                            </View>
                        </TouchableOpacity>
                        }
                        {/* </ScrollView> */}
                        <View style={{ flexDirection: 'row', width: '100%', marginTop: '21%' }}>
                            <View style={{ flex: 5.5 }} />
                            <Image source={iconBearWithMen} style={Platform.OS === 'ios' ? styles.imageBearWithMenIos : styles.imageBearWithMenAndroid} />
                        </View>
                    </View>

                    <View style={{ flex: 0.22 }}>
                        <Image source={iconVas} style={Platform.OS === 'ios' ? styles.vasImageIos : styles.vasImageAndroid} />
                    </View>

                </View>
            </SafeAreaView>
        )
    }
}

const styles = {
    background: {
        flex: 1,
    },
    vasImageAndroid: {
        marginLeft: '5%', width: scale(78), height: verticalScale(125)
    },
    vasImageIos: {
        marginLeft: '5%', width: 78, height: 125
    },
    textStatusCenterIos: {
        marginTop: '6%', alignSelf: 'center',

        fontSize: 10,
        color: 'rgb(147, 147, 147)',
        textAlign: 'auto',
        // marginLeft: '3%',
        // marginRight: '3%',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textStatusCenterAndroid: {
        marginTop: '1%', alignSelf: 'center',
        fontSize: 10,
        color: 'rgb(147, 147, 147)',
        textAlign: 'auto',
        // marginLeft: '3%',
        // marginRight: '3%',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    imageTitleAndroid: {
        marginTop: '5%', marginBottom: '5%',
        width: scale(184), height: verticalScale(42),
        alignSelf: 'center'
    },
    imageTitleIos: {
        marginTop: '10%', marginBottom: '10%',
        width: scale(178), height: verticalScale(43),
        alignSelf: 'center'
    },
    imageBearWithMenIos: {
        right: '12%', flex: 4.5, width:520 , height: 170, bottom: '8%'
    },
    imageBearWithMenAndroid: {
        right: '12%', flex: 4.5, width: scale(570), height: verticalScale(160), bottom: '8%'
    },
    textTitleBlue: {
        fontSize: 28,
        color: 'rgb(2, 132, 254)',
        alignSelf: 'center',
    },
    textTitleBlack: {
        fontSize: 28,
        color: 'black',
        alignSelf: 'center',
    },
    imageTop: {
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        // width: 100,
        // height: 50,
        alignSelf: 'center'
    },
    textTitle: {
        fontSize: 25,
        color: 'rgb(3, 130, 253)',
        alignSelf: 'center',
    },
    textSubTitle: {
        alignSelf: 'center',
        fontSize: 10,
        color: 'rgb(147, 147, 147)',
        textAlign: 'auto',
        // marginLeft: '3%',
        // marginRight: '3%',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textStatus: {
        fontSize: 10,
        color: 'rgb(147, 147, 147)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textSubTitle_green: {
        alignSelf: 'center',
        fontSize: 10,
        color: 'rgb(147, 147, 147)',
        textAlign: 'auto',
        marginLeft: '3%',
        marginRight: '3%',
        fontFamily: 'NotoSansCJKjp-Medium',
        color: 'rgb(66, 130, 191)',
    },
    textSubTitle2: {
        alignSelf: 'center',
        fontSize: 11,
        color: 'rgb(147, 147, 147)',
        textAlign: 'auto',
        marginLeft: '3%',
        marginRight: '3%',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textRedStatus: {
        fontSize: 14,
        color: '',
        textAlign: 'center',

    },
    textBlueStatus: {

    },
    textGreenStatus: {

    },
    buttonViewRed: {
        width: '95%',
        backgroundColor: 'rgb(214, 22, 22)',
        borderRadius: 20,
        height: 50,
        alignItems: 'center',
        autoContent: 'center'
    },
    buttonViewBlueDisabled: {
        width: '40%',

        backgroundColor: 'rgb(203, 229, 254)',
        borderRadius: 5,
        height: 35,
        alignItems: 'center',
        autoContent: 'center',
        alignSelf: 'center'
    },
    buttonViewBlue: {
        width: '40%',
        backgroundColor: 'rgb(3, 132, 255)',
        borderRadius: 5,
        height: 35,
        alignItems: 'center',
        autoContent: 'center',
        alignSelf: 'center'
    },
    button: {
        marginTop: '2%'
    },
    imageBottom: {
        marginLeft: 0,
        marginBottom: 0,
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        padding: 10,
        textAlign: 'auto'
    },
    textBottom1Gray: {
        color: 'rgb(147, 147, 147)',
        fontSize: 13,
        textAlign: 'auto'
    },
    textBottom1Blue: {
        color: 'rgb(66, 130, 191)',
        fontSize: 13,
        textAlign: 'auto'
    },
    textBottom2: {

    }
}   