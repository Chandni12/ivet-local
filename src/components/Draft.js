import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput, Dimensions, ActivityIndicator } from 'react-native';
import commonStyles from '../stylesheet/common/commonStyles.style';
import CastConstants from '../constants/Cast';
import dashboardConstants from '../constants/Dashboard';
import * as timeConstants from '../constants/TimeConstants';
import { SafeAreaView } from 'react-navigation';

import HTML from 'react-native-render-html';
const Back = require('../../assets/back.png');
const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class Draft extends Component {
    constructor(props) {
        super(props);
    }

    renderItemTag = (place, index) => {
        return (
            <View key={place.index} style={{ flexDirection: 'row' }} >
                {place.item ?
                    <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 20, height: 30, justifyContent: 'center', alignItems: 'center', marginLeft: 2.5, marginRight: 2.5, padding: 2 }}>

                        <Text style={commonStyles.listText}>
                            {place.item.title}
                        </Text>
                    </View> : null}
            </View>
        )
    };

    renderListItem = (item, index) => {

        let categoryValue = ''
        let viewRow = <View />;
        if (item.item.category_id === 1) {
            categoryValue = dashboardConstants.QUESTION + " : " + dashboardConstants.CLINICAL;
        } else if (item.item.category_id === 2) {
            categoryValue = dashboardConstants.QUESTION + " : " + dashboardConstants.LIFE;
        } else {
            categoryValue = dashboardConstants.QUESTION + " : " + dashboardConstants.HOTEL_MANAGEMENT;
        }
        {
            viewRow = <View>

                <View>
                    <View style={{ flexDirection: 'column', marginTop:10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.titleSuggestedNotes, { width: '80%' }]}>{item.item.title}</Text>
                            <Text style={[styles.textData, { width: '20%' }]}>{timeConstants.dateDiffIn_dd_mm_yy(item.item.created_at)}</Text>
                        </View>
                        <View style={{ margin: 10 }}>
                            <HTML html={item.item.summary} />
                        </View>
                    </View>
                </View>

                <View style={{ width: '100%', marginLeft: '1%', marginTop: "2%" }}>
                    <FlatList
                        scrollEnabled={true}
                        horizontal={true}
                        data={item.item.tags}
                        renderItem={this.renderItemTag}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

                <View style={{ flexDirection: 'row', marginTop: '5%', justifyContent: 'flex-start',marginBottom: '5%', }}>
                    <View style={[styles.categoryViewInOval]}>
                        <Text style={{ padding: '2%' }} >{categoryValue}</Text>
                    </View>
                </View>

                <View style={styles.singleLine} />
            </View>
        }
        return (
            <SafeAreaView>
                {viewRow}
            </SafeAreaView>
        )
    }

    render() {
        const { data, arrayData, fetching } = this.props;

        return (
            <SafeAreaView>
                <View>
                    <View>
                        <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                            <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => this.props.navigation.goBack()} >
                                <Image source={Back} style={{ width: 13, height: 13 }} />
                            </TouchableOpacity>
                            <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center' }]}>{data['viewTitle']}</Text>
                            <View style={{ flex: 1.5 }}>
                            </View>
                        </View>
                        <View style={styles.singleLine} />
                    </View>
                    <View>
                        <View style={{ marginTop: '5%' }} />
                        <Text style={{ marginLeft: '5%', marginBottom: '5%' }}>{CastConstants.THERE_ARE} {data['totalDraft']} {CastConstants.DRAFT}</Text>
                        <View style={styles.singleLine} />
                    </View>
                    <FlatList
                         style={{height:'85%'}}
                        data={arrayData}
                        renderItem={this.renderListItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    {/* <View style={{width:'100%', height:20}}/> */}

                    {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: SCREEN_HEIGHT }}>
                        <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                    </View> : null}
                </View>
            </SafeAreaView>)
    }
}

export const styles = {
    textTitle: {
        fontSize: 16,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        // alignSelf: 'center',
        // textAlign: 'center',
    },
    viewRow: {
        margin: '2%'
    },
    categoryViewInOval: {
        marginLeft: 10,
        backgroundColor: 'rgb(223,230, 237)', borderRadius: 5, height: 30
    },
    titleSuggestedNotes: {
        marginLeft: 10,
        color: '#0e0e0e',
        fontFamily: 'NotoSansCJKjp-Medium'
    },
    singleLine: {
        width: '100%',
        marginTop: 10,
        height: 1,
        backgroundColor: 'lightgray'
    },
    listText: {
        color: '#0385ff',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        fontSize: 14,
        marginRight: 8,
        marginLeft: 8,
    },
    textData: {
        fontSize: 13,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: '2%',
        width: '75%',

    }
}