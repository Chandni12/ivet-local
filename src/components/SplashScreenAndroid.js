import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, FlatList, TextInput, Linking, Platform, ActivityIndicator } from 'react-native';
const iconWelcomeAndroid = require('../../assets/welcome_title_android.png');

export default class SplashScreenAndroid extends Component {

    componentDidMount = () => {
        setTimeout(() => {
            this.props.navigation.navigate('Welcome');
        }, 240);
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <Image source={iconWelcomeAndroid} />
            </View>
        )
    }
}