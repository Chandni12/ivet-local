import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, FlatList, TextInput, Linking, Platform, ActivityIndicator,Dimensions } from 'react-native';
import { ScaledSheet, verticalScale, moderateScale, Button } from 'react-native-size-matters';
import { SafeAreaView } from 'react-navigation';


const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class Signup extends Component {

    render(){
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View>
                    <Text style={{alignSelf:"flex-end",padding:10,fontWeight:'bold'}}>Find Other Tags</Text>
                </View>

                <View>
                    <Text style={{fontSize:20,paddingLeft:10}}>
                        Feed Tags
                    </Text>

                    <Text style={{padding:10,color:'#333'}}>
                    When you add a feed tag, any questions or notes related to that 
                    tag will be posted to the tag feed when there are posts related 
                    to the tag displayed here . New tags can be added on the tag page .
                    </Text>
                </View>

                <View style={{padding:10,flexDirection:'row'}}>

                    <View style={{flex:1}}>
                    <Text style={{alignSelf:'center',textAlign:'center'}}>Clinical</Text>
                    </View>

                    <Text style={{flex:1,alignSelf:'center',textAlign:'center'}}>Clinical</Text>

                    <Text style={{flex:1,alignSelf:'center',textAlign:'center'}}>Clinical</Text>

                </View>

            </SafeAreaView>
        )
    }
}