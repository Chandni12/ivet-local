import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Dimensions, FlatList, Platform, Modal, ActivityIndicator, Alert } from 'react-native';
import commonStyles from '../../stylesheet/common/commonStyles.style'
import { SafeAreaView } from 'react-navigation';
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
//import ProgressBarAnimated from 'react-native-progress-bar-animated';
import * as Progress from 'react-native-progress';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather';
import DashboardConstant from '../../constants/Dashboard';
import { ReadMore, RegularText } from 'react-native-read-more-text';
import TableRowDashboard from '../../commonComponent/TableRowDashboard';
import PostMenu from '../PostMenu';
import Others from '../Others';
import TabbarTitleConstant from '../../constants/TabbarTitle';
import * as Animatable from 'react-native-animatable';
import ViewPopup from '../../commonComponent/ViewPopup';
import {IMAGE_DASHBOARD_NICK_NAME_PREFIX_URL} from '../../api/endPoints'

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const welcomeTitle = require('../../../assets/welcome_title.png');
const bellHeader = require('../../../assets/bellHeader.png'); 
const profilePhoto = require('../../../assets/profilePhoto.png');

export default class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            progressValue: 0.5,
            progressWithOnComplete: 0,
            progressCustomized: 0,
            isWordCorrect: false,
            borderColorProgressbar: 'white',
            isShowPostMenu: false,
            isModalVisible: true,
            isShowOthersPopup: false,
            isShowTopProfileSection: true,
            isPopupShow: false,
            
        }
        this.arrayData = [
            {
                status: DashboardConstant.Q_AND_A, subcategoryTitle: DashboardConstant.SUBCATEGORY_TITLE,
                description: DashboardConstant.CLINICAL_DESCRIPTION1, description2: DashboardConstant.CLINICAL_DESCRIPTION2,
                answerDescription1: DashboardConstant.ANSWER_DESCRIPTION1, answerDescription2: DashboardConstant.ANSWER_DESCRIPTION2
            }, {
                status: DashboardConstant.Q_AND_A, subcategoryTitle: DashboardConstant.SUBCATEGORY_TITLE,
                description: DashboardConstant.CLINICAL_DESCRIPTION1, description2: DashboardConstant.CLINICAL_DESCRIPTION2,
                answerDescription1: DashboardConstant.ANSWER_DESCRIPTION1,
                answerDescription2: DashboardConstant.ANSWER_DESCRIPTION2, descriptionImage: '../../../assets/xray.png'
            }];
        // this.arrayPopupData = [DashboardConstant.POSTED_DATE, DashboardConstant.SELECTED_TAG, DashboardConstant.TREND_POST]
        this.arrayPopupData = [DashboardConstant.CALL_FOR_ANSWERS, DashboardConstant.FEATURED_TOPICS, DashboardConstant.THIS_MONTH, DashboardConstant.THIS_WEEK]

    }

    openDrawer() {
        this.props.navigation.openDrawer();
    }

    _renderTruncatedFooter = (handlePress) => {
        return (
            <RegularText style={{ color: Colors.tintColor, marginTop: 5 }} onPress={handlePress}>
                Read more
          </RegularText>
        );
    }

    _renderRevealedFooter = (handlePress) => {
        return (
            <RegularText style={{ color: Colors.tintColor, marginTop: 5 }} onPress={handlePress}>
                Show less
          </RegularText>
        );
    }
    _handleTextReady = () => {
        // ...
    }

    onClickPostMenu = (isOutSideViewClick) => {
        if (isOutSideViewClick) {
            this.setState({
                isShowPostMenu: !this.state.isShowPostMenu,
                isModalVisible: !this.state.isModalVisible
            })
        } else {
            setTimeout(() => {
                this.setState({
                    isShowPostMenu: !this.state.isShowPostMenu,
                    isModalVisible: !this.state.isModalVisible
                })
            }, 250);
        }
    }

    showHidePopupView = (viewName) => {

        if (viewName === 'postMenu') {
            this.onClickPostMenu(true);
            this.setState({
                isShowOthersPopup: false,
            })
        }
        this.setState({
            isShowOthersPopup: !this.state.isShowOthersPopup,
            isModalVisible: !this.state.isModalVisible
        })
    }


    postMenuOptionClick = (option, isViewName, param) => {
        if (option === 'Dashboard') {
            this.props.postMenuOptionClick(option);
            this.onClickPostMenu(false);
            return;
        }

        if (option === 'Tags') {
            this.props.navigation.navigate(option, { auth_token: this.props.auth_token, categoryName: param });
            return;
        }

        if (isViewName) {

            console.log(" option -----", option, 'isView name --', isViewName, 'param --', param);
            this.props.navigation.navigate(option, { auth_token: this.props.auth_token, param: param, viewNameComeFrom: 'Dashboard' });
            return;
        }

        // This will call api with arguments provided.
        this.props.postMenuOptionClick(option);
        this.onClickPostMenu(false);
    }


    renderItem = (item, index) => {

let viewProgressBar = <View />
        if (this.state.isShowTopProfileSection) {
           viewProgressBar = <TouchableOpacity style={{ height: verticalScale(180), marginTop: '3%' }} onPress={() => this.props.navigation.navigate('profile', { 'auth_token': this.props.auth_token })}>

            <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }} >

                <Progress.Bar
                    // backgroundColor= {'#f6cc4c'}
                    //backgroundColor= {'white'}
                    color={'#3ad716'}
                    unfilledColor={'rgb(230,230,230)'}
                    height={verticalScale(20)}
                    borderRadius={4}
                    width={Dimensions.get('screen').width / 1.3}
                    progress={this.state.progressValue}

                />
                <View style={{ marginLeft: '5%' }}>
                    <TouchableOpacity onPress={() => this.setState({ isShowTopProfileSection: false })}>
                        <IconAntDesign name="close" style={{ fontSize: 25, }} />
                    </TouchableOpacity>
                </View>
            </View>

            <Text style={[Platform.OS === 'android' ? styles.textCompleteTitleAndroid : styles.textCompleteTitleiOS]}>{DashboardConstant.BOLD_TEXT_COMPLETE_PROFILE}</Text>

            <Text style={[Platform.OS === 'ios' ? styles.textDescriptionIos : styles.textDescriptionAndroid]}>{DashboardConstant.SET_FIELDS_DESCRIPTION}</Text>

            <View style={[styles.lighBlueButton, { alignItems: 'center', justifyContent: 'center', marginBottom: '1%' }]}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('profile', { 'auth_token': this.props.auth_token })}>
                    <Text style={styles.textStart}>{DashboardConstant.START}</Text>
                </TouchableOpacity>
            </View>
            <View style={[styles.singleLine, { height: 6 }]} />
        </TouchableOpacity> 
        
        } else {
            viewProgressBar = <View />
        }

        if (item.index === 0) {
          
          return(
              viewProgressBar
          )
               
        } else if (item.index === 1) {

            
            let stringTemp = IMAGE_DASHBOARD_NICK_NAME_PREFIX_URL+this.props.userNickNameIcon;
            let completeUrl = {uri: stringTemp};
            
          return(  <View style={{}}>
            {/* <View style={{}}> */}
            <View style={this.state.isShowTopProfileSection ? styles.viewNickName1 : styles.viewNickName2}>
                <Image source={completeUrl} style={[styles.profilePhoto, { borderColor: '#e5e5e5' }]} />
                <Text style={[styles.textNickName, { marginLeft: '2%', marginTop: '2%' }]}>{ this.props.userNickName? this.props.userNickName : DashboardConstant.NICK_NAME }</Text>

            </View>

            <Text style={[styles.textGrayTitle, { marginTop: '5%' }]}>{DashboardConstant.ENTER_QUESTION_ANSWERS}</Text>
            <View style={[styles.singleLine, { height: 6}]} />
            {/* </View> */}

        </View>)
        } else if(item.index === 2){
           return( <IconFeather name="filter" style={{ fontSize: 25, right: '6%', marginTop: '2%', alignSelf: 'flex-end' }} onPress={() => this.showPopup()} />)
        }else {
            return (
                <TableRowDashboard item={item} index={item.index} onClickPostMenu={() => this.onClickPostMenu()} navigation={this.props.navigation} auth_token={this.props.auth_token} handleFollowUnfollow={this.props.handleFollowUnfollow} />
            )
        }
       
    }
    showPopup = () => {
        this.setState({ isPopupShow: !this.state.isPopupShow });
    }

    onClickLogout() {
        Alert.alert(
            'Are you sure to logout',
            '',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: this.onPressOk.bind(this) },
            ],
            { cancelable: false }
        )
    }

    async onPressOk() {
        try {
            await AsyncStorage.removeItem(wordConstants.CONST_AUTH_TOKEN);
            await AsyncStorage.removeItem(wordConstants.CONST_USER_DATA);
            this.props.navigation.navigate(Platform.OS === 'ios' ? "WelcomeIos" : "WelcomeAndroid");
        } catch (error) {
        }
    }

    BottomView = () => {
        return (
            <View style={{ alignItems: 'flex-start', height: screenHeight / 8 }}>
                {(this.props.fetching) ?
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)'
                        style={{ alignSelf: 'center', width: screenWidth, height: screenHeight / 15 }} /> : null
                }
            </View>
        )
    }

    render(){

        const {userNickNameIcon,loadMoreData ,onClickAddTo, dashboard, fetching, myProfileData, auth_token, postMenuOptionClick, selectedCategoryOption, onClickSearchIcon, onClickFilterOption, data } = this.props;
       

        let stringTemp = IMAGE_DASHBOARD_NICK_NAME_PREFIX_URL+userNickNameIcon;
       let completeUrl = {uri: stringTemp};
       

        return (
            <SafeAreaView >
                <View style={{ flexDirection: 'row', width: '90%', height: 33, alignSelf: 'center', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                <View style={{ flexDirection: 'row', width: '50%' }}>
                        <TouchableOpacity onPress={() => this.openDrawer()}>
                            <Image source={completeUrl} style={styles.profilePhoto} /> 
                            {/* Need to change [profile url ] */}
                        </TouchableOpacity>

                        <Image source={welcomeTitle} style={[styles.titleLogo, { marginTop: 7 }]} />
                    </View>

                    <View style={{ flexDirection: 'row', marginBottom: -7 }}>

                        <TouchableOpacity onPress={onClickSearchIcon} style={{ alignItems: 'center', paddingRight: 8 }}>
                            <IconAntDesign name="search1" style={{ fontSize: 18, paddingTop: 5 }} />
                            <Text style={styles.textSearch}>{DashboardConstant.SEARCH}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={onClickAddTo} style={{ alignItems: 'center' }} >
                            {/* <IconAntDesign name="pluscircleo" style={{paddingTop:5 ,fontSize: 18, color:'rgb(0,132, 255)' }}/> */}
                            <Image source={bellHeader} style={{ width: 20, height: 20, marginTop: 5 }} />

                            {data['notificationCount'] > 0 ? <View style={styles.viewNotificationCircle}>
                                <Text style={styles.textNotification}>{data['notificationCount']}</Text>
                            </View> : null}

                            <Text style={styles.textAddTo}>{DashboardConstant.ADD_TO}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={[styles.viewSingleLineTop]} />

                <FlatList
                    style={{ marginTop: '3%' }}
                    scrollEnabled={true}
                    pagingEnabled={true}
                    data={dashboard}
                    renderItem={this.renderItem}
                    onEndReached={loadMoreData}
                    ListFooterComponent = {this.BottomView}
                    keyExtractor={(item, index) => index.toString()}
                />

            {/* {fetching ? <View style={{ position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center', width: 100, height: 100, marginTop: screenHeight / 2 - 100 }} />
                </View> : <View />} */}

                {this.state.isShowPostMenu ?
                    
                    <TouchableOpacity style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: '110%', width: "100%", position: 'absolute' }} onPress={() => this.onClickPostMenu(true)}>

                        <PostMenu
                            onClickPostMenu={() => this.onClickPostMenu(false)}
                            navigation={this.props.navigation}
                            postMenuOptionClick={this.postMenuOptionClick} />
                    </TouchableOpacity>
                    : <View />
                }

                {this.state.isShowOthersPopup ?

                    <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: '200%', width: "100%", position: 'absolute' }} onPress={() => this.showHidePopupView(false)}>
                        <Others
                            showHidePopupView={this.showHidePopupView}
                            navigation={this.props.navigation}
                            auth_token={this.props.auth_token} />
                    </TouchableOpacity>
                    : <View />
                }
                {this.state.isPopupShow ?
                    <Animatable.View style={styles.viewPopup}>
                        <ViewPopup arrayData={this.arrayPopupData} showPopup={this.showPopup} onClickFilterOption={onClickFilterOption} />
                    </Animatable.View> :
                    <View />}

                </SafeAreaView>)
    }

    render1() {

        const {loadMoreData ,onClickAddTo, dashboard, fetching, myProfileData, auth_token, postMenuOptionClick, selectedCategoryOption, onClickSearchIcon, onClickFilterOption, data } = this.props;

       
        return (
            <SafeAreaView >
                <View style={{ flexDirection: 'row', width: '90%', height: 33, alignSelf: 'center', justifyContent: 'space-between', alignItems: 'flex-end' }}>

                    <View style={{ flexDirection: 'row', width: '50%' }}>
                        <TouchableOpacity onPress={() => this.openDrawer()}>
                            <Image source={profilePhoto} style={styles.profilePhoto} />
                        </TouchableOpacity>

                        <Image source={require('../../../assets/welcome_title.png')} style={[styles.titleLogo, { marginTop: 7 }]} />
                    </View>

                    <View style={{ flexDirection: 'row', marginBottom: -7 }}>

                        <TouchableOpacity onPress={onClickSearchIcon} style={{ alignItems: 'center', paddingRight: 8 }}>
                            <IconAntDesign name="search1" style={{ fontSize: 18, paddingTop: 5 }} />
                            <Text style={styles.textSearch}>{DashboardConstant.SEARCH}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={onClickAddTo} style={{ alignItems: 'center' }} >
                            {/* <IconAntDesign name="pluscircleo" style={{paddingTop:5 ,fontSize: 18, color:'rgb(0,132, 255)' }}/> */}
                            <Image source={require('../../../assets/bellHeader.png')} style={{ width: 20, height: 20, marginTop: 5 }} />

                            {data['notificationCount'] > 0 ? <View style={styles.viewNotificationCircle}>
                                <Text style={styles.textNotification}>{data['notificationCount']}</Text>
                            </View> : null}

                            <Text style={styles.textAddTo}>{DashboardConstant.ADD_TO}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={[styles.viewSingleLineTop]} />

                <ScrollView style={{}} onScroll={() => this.setState({ isPopupShow: false })}>

                    <View style={{ flex: 1 }}>

                        {this.state.isShowTopProfileSection ?
                            <TouchableOpacity style={{ height: verticalScale(180), marginTop: '3%' }} onPress={() => this.props.navigation.navigate('profile', { 'auth_token': this.props.auth_token })}>

                                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }} >

                                    <Progress.Bar
                                        // backgroundColor= {'#f6cc4c'}
                                        //backgroundColor= {'white'}
                                        color={'#3ad716'}
                                        unfilledColor={'rgb(230,230,230)'}
                                        height={verticalScale(20)}
                                        borderRadius={4}
                                        width={Dimensions.get('screen').width / 1.3}
                                        progress={this.state.progressValue}

                                    />
                                    <View style={{ marginLeft: '5%' }}>
                                        <TouchableOpacity onPress={() => this.setState({ isShowTopProfileSection: false })}>
                                            <IconAntDesign name="close" style={{ fontSize: 25, }} />
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <Text style={[Platform.OS === 'android' ? styles.textCompleteTitleAndroid : styles.textCompleteTitleiOS]}>{DashboardConstant.BOLD_TEXT_COMPLETE_PROFILE}</Text>

                                <Text style={[Platform.OS === 'ios' ? styles.textDescriptionIos : styles.textDescriptionAndroid]}>{DashboardConstant.SET_FIELDS_DESCRIPTION}</Text>

                                <View style={[styles.lighBlueButton, { alignItems: 'center', justifyContent: 'center', marginBottom: '1%' }]}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('profile', { 'auth_token': this.props.auth_token })}>
                                        <Text style={styles.textStart}>{DashboardConstant.START}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.singleLine, { height: 6 }]} />
                            </TouchableOpacity> : null}

                        <View style={{}}>
                            {/* <View style={{}}> */}
                            <View style={this.state.isShowTopProfileSection ? styles.viewNickName1 : styles.viewNickName2}>
                                <Image source={require('../../../assets/profilePhoto.png')} style={[styles.profilePhoto, { borderColor: '#e5e5e5' }]} />
                                <Text style={[styles.textNickName, { marginLeft: '2%', marginTop: '2%' }]}>{DashboardConstant.NICK_NAME}</Text>
                            </View>

                            <Text style={[styles.textGrayTitle, { marginTop: '5%' }]}>{DashboardConstant.ENTER_QUESTION_ANSWERS}</Text>
                            <View style={[styles.singleLine, { height: 6}]} />
                            {/* </View> */}

                        </View>

                        <IconFeather name="filter" style={{ fontSize: 25, right: '6%', marginTop: '2%', alignSelf: 'flex-end' }} onPress={() => this.showPopup()} />

                        <FlatList
                            style={{ marginTop: '3%' }}
                            scrollEnabled={true}
                            pagingEnabled={true}
                            data={dashboard}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />

                    </View>
                    <View style={{ marginBottom: '5%' }} />
                </ScrollView>

               

                {fetching ? <View style={{ position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center', width: 100, height: 100, marginTop: screenHeight / 2 - 100 }} />
                </View> : <View />}

                {this.state.isShowPostMenu ?
                    
                    <TouchableOpacity style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: '110%', width: "100%", position: 'absolute' }} onPress={() => this.onClickPostMenu(true)}>

                        <PostMenu
                            onClickPostMenu={() => this.onClickPostMenu(false)}
                            navigation={this.props.navigation}
                            postMenuOptionClick={this.postMenuOptionClick} />

                    </TouchableOpacity>
                    // </Modal>
                    : <View />
                }

                {this.state.isShowOthersPopup ?

                    <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: '200%', width: "100%", position: 'absolute' }} onPress={() => this.showHidePopupView(false)}>

                        <Others
                            showHidePopupView={this.showHidePopupView}
                            navigation={this.props.navigation}
                            auth_token={this.props.auth_token} />

                    </TouchableOpacity>

                    : <View />
                }
                {this.state.isPopupShow ?
                    <Animatable.View style={styles.viewPopup}>
                        <ViewPopup arrayData={this.arrayPopupData} showPopup={this.showPopup} onClickFilterOption={onClickFilterOption} />
                    </Animatable.View> :
                    <View />}


            </SafeAreaView>
        )
    }
}

const { width } = Dimensions.get('screen').width;
const { height } = Dimensions.get('screen').height;


const styles = {
    viewNotificationCircle: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: 'red',
        borderColor: 'red',
        borderWidth: 1.5,
        position: 'absolute',
        justifyContent: 'center',
        right: -1
    },
    textNotification: {
        alignSelf: 'center',
        fontSize: 10,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Regular',
        paddingTop: 1
    },
    viewSidemenu: {
        position: 'absolute',
        width: screenWidth / 2,
        height: screenHeight,
        marginTop: 20,

        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1.5
        },
        shadowOpacity: 0.7,
        shadowRadius: 1,
        borderRadius: 8,
        backgroundColor: 'pink'
    },
    navItemStyle: {
        alignItems: 'center',
        padding: 10,
        paddingLeft: 10,
        fontSize: 16,
        flexDirection: 'row'
    },
    viewPopup: {
        position: 'absolute',
        width: 120,
        height: 126,
        marginTop: 375,
        right: 36,
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1.5
        },
        shadowOpacity: 0.7,
        shadowRadius: 1,
        borderRadius: 8,
        backgroundColor: 'white'

    },
    viewNickName1: {
        flexDirection: 'row', marginLeft: '5%', marginRight: '5%', alignItems: 'center', marginTop: '5%'
    },
    viewNickName2: {
        flexDirection: 'row', marginLeft: '5%', marginRight: '5%', alignItems: 'center', marginTop: '2%'
    },
    profilePhoto: {
        width: 28.5,
        height: 28.5,
        borderRadius: 15,
        borderColor: 'rgb(2, 132, 254)',
        borderWidth: 1.5,
    },
    profilePhotoWithoutCircle: {
        width: 30,
        height: 30,
        borderRadius: 15,
        // borderColor: 'rgb(2, 132, 254)',
        // borderWidth: 1.5,
    },
    textNickName: {
        fontSize: 13,
        color: 'rgba(147, 147, 147, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    titleLogo: {
        width: 90,
        height: 20,
        marginLeft: '5%',
    },
    searchIcon: {
        width: 20,
        height: 20,
    },
    plusWithCircle: {
        width: 20,
        height: 20,
    },
    textSearch: {
        fontSize: 9,
        color: 'rgba(51, 51, 51, 255)',
        fontFamily: 'NotoSansCJKjp-Bold',
        paddingTop: '1%',
    },
    textAddTo: {
        color: 'rgba(1, 133, 255, 255)',
        fontSize: 9,
        fontFamily: 'NotoSansCJKjp-Bold',
        paddingTop: '1%',
    },
    viewSingleLineTop: {
        marginTop: '2%',
        width: '100%',
        height: 1.4,
        backgroundColor: '#e5e5e5'
    },
    singleLine: {

        width: '100%',
        backgroundColor: '#e5e5e5',
        marginTop: '2.5%'
    },
    lighBlueButton: {
        borderRadius: 15,
        alignSelf: 'center',
        backgroundColor: 'rgb(229, 243, 255)',
        height: verticalScale(35),
        width: '90%'
    },
    textStatus: {
        // backgroundColor: 'rgb(68,153,218)',
        fontFamily: 'NotoSansCJKjp-Medium',
        color: 'white',
        fontSize: 9,
    },
    viewStatus: {
        backgroundColor: 'rgb(68,153,218)',
        borderRadius: 2,
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: '5%',
        marginTop: '2%',
        height: 20,
        width: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textGrayTitle: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'rgb(147, 147, 147)',
        marginRight: '5%',
        marginLeft: '5%'
    },
    textGraySubTitle: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'rgb(147, 147, 147)',
        marginLeft: '2%'
    },
    textTitleBlackIos: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'black',
        marginLeft: '5%',
        marginBottom: '5%'

    },
    textTitleBlackAndroid: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'black',
        marginLeft: '5%',
        marginBottom: '5%'
    },
    textDescriptionAndroid: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'rgba(97, 97, 97, 255)',
        marginLeft: '5%',
        marginRight: '5%',
        marginBottom: '2%'
    },
    textDescriptionIos: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'rgba(97, 97, 97, 255)',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '1%',
        marginBottom: '1%',
        marginBottom: '2%'
    },

    readMoreLightBlue: {
        color: 'rgb(229, 243, 255)',
    },
    textCompleteTitleAndroid: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 14,
        marginLeft: '5%',
        color: 'rgba(51, 51, 51, 255)'

        // alignSelf: 'center',
    },
    textCompleteTitleiOS: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 14,
        marginLeft: '5%',
        marginTop: '2%',
        marginBottom: '2%',
        color: 'rgba(51, 51, 51, 255)',
        // alignSelf: 'center',
    },
    textStart: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 15,
        color: 'rgba(0, 132, 255, 255)'
    },
    textReadMore: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 11,
        color: 'rgb(2, 132, 254)'
    },


}