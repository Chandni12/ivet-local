import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Keyboard, TouchableWithoutFeedback, TextInput, ScrollView, Dimensions, FlatList } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import postAnswerConstants from '../../constants/PostAnswer';
import { scale, verticalScale } from 'react-native-size-matters';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import * as wordConstants from '../../constants/WordConstants'; 
import * as timeConstants from '../../constants/TimeConstants'; 


export default class CommentList extends Component {
    constructor(props) {
        super(props);

    }

    getDaysInLastFullMonth(day) {
        var d = new Date(day);
        var lastDayOfMonth = new Date(d.getFullYear(), d.getMonth() + 1, 0);
        return lastDayOfMonth.getDate();
    }

    renderItem = (item) => {
      
        let viewItem;

       
            viewItem = <View style={{ backgroundColor: 'rgb(246,246,246)' }}>
                <View style={{ flexDirection: 'row' , marginTop:'1%'}}>
                    <Text style={{ marginLeft: '2%', marginRight: '2%' }}> <Text style={styles.commentText}>{item.item.comment}</Text>
                    </Text>

                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginRight: '2%', marginBottom:'1%' }}>
                    {/* <Text style={{ marginLeft: '6%' }}>{postAnswerConstants.COMMENT_TEXT2}</Text> */}
                    <Text style={[styles.userName ,{ borderRadius: 5, backgroundColor: 'rgb(229, 243, 255)', marginLeft: '4%', marginRight: '2%', textAlign: 'center', marginTop:'1%', marginBottom:'1%' }]}>{item.item.user.nick_name? item.item.user.nick_name:'user name'}</Text>
                    <View style={{ flexDirection: 'row' }}>

                <Text style={[styles.userName,{marginTop:'2%'}]}> {timeConstants.dateDiffInDays_Months_Years(item.item.updated_at? item.item.updated_at: new Date())}</Text> 

                        {/* <TouchableOpacity style={{ marginRight: '5%' }}>
                            <Text>  {postAnswerConstants.EDIT}</Text>
                        </TouchableOpacity>
                        <Text> {postAnswerConstants.NICE}</Text> */}
                    </View>

                </View>
                <View style={styles.singleLine} />
            </View>
        //}

        return (
            viewItem
        )
    }
    render() {

        return (
            <View>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.2 }} />
                    <FlatList

                        data={this.props.data}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

            </View>
        )
    }
}

styles = {
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
    commentText: {
        fontSize: 10,
        color: 'rgb(74,74,74)',
        fontFamily: 'NotoSansCJKjp-Regular',
        marginLeft: 5,
        marginRight: 5,

    },
    userName: {
        fontSize: 12,
        color: 'rgb(134,134,134)',
        fontFamily: 'NotoSansCJKjp-Regular',
      
    },
   
    bottomText: {
        fontSize: 13,
        color: 'rgb(18, 75, 136)',
        fontFamily: 'NotoSansCJKjp-Medium',

    }

}