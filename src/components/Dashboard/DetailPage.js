import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Keyboard, TouchableWithoutFeedback, TextInput, ScrollView, Dimensions, FlatList, ActivityIndicator, Alert } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import postAnswerConstants from '../../constants/PostAnswer';
import CastConstants from '../../constants/Cast';
import dashboardConstants from '../../constants/Dashboard';
import noteDetailConstants from '../../constants/NoteDetail';
import commonStyles from '../../stylesheet/common/commonStyles.style';

import * as wordConstants from '../../constants/WordConstants';
import * as timeConstants from '../../constants/TimeConstants';

import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather';
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFontawesome from 'react-native-vector-icons/FontAwesome';
import IconEntypo from 'react-native-vector-icons/Entypo';

import { scale, verticalScale } from 'react-native-size-matters';
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker';
import CommentList from './CommentList';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Animatable from 'react-native-animatable';
import ViewPopup from '../../commonComponent/ViewPopup';
import ViewAnswerInstruction from '../../commonComponent/ViewAnswerInstruction'
import ModalDropdown from 'react-native-modal-dropdown';
import { IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL, IMAGE_DASHBOARD_QUESTION_PREFIX_URL, IMAGE_ANSWER_DETAIL_PREFIX_URL } from '../../api/endPoints'
import PostPhotoSection from '../Question/PostPhotoSection';
import InputTextWithCharLimit from '../../commonComponent/InputTextWithCharLimit';
import HTML from 'react-native-render-html';


const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);


export default class DetailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isPopupShow: false,
            isFirstButtonClick: false,
            imageFirst: false,
            imageSecond: false,
            showComment: false,
            comment: '',
            voteCount: 0,
            indexClicked: '',

        }

        this.arrayPopupQuestion = [CastConstants.VIEW_EDIT_HISTORY, CastConstants.REPORT_INAPPROPRIATE, CastConstants.EDIT_QUESTION];

        this.arrayPopupAnswer = [CastConstants.VIEW_EDIT_HISTORY, CastConstants.REPORT_INAPPROPRIATE, CastConstants.EDIT_QUESTION];

    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    onClickAddImage = () => {
        let dictTemp = { 'image_title': '', 'image_description': '', 'document': '' };
        if (this.state.arrayOfImages.length === maxImageUpload) {
            alert(wordConstants.CONST_MAX_PHOTO_UPLOAD_WARNING);
            return;
        }
        this.setState({ arrayOfImages: [...this.state.arrayOfImages, dictTemp] });
    }

    onClickDeleteImage = (index) => {
        var array = [...this.state.arrayOfImages]; // make a separate copy of the array
        if (index > -1) {
            array.splice(index, 1);
            this.setState({ arrayOfImages: array });
        }
    }

    addPost(itemData) {
        console.log(this.props.postDetail.comments)
        this.setState({ indexClicked: '' });
        this.props.postComment(itemData);
    }

    onClickCheck = (data) => {
        console.log('Yes Pressed')
        Alert.alert(
            CastConstants.DETERMINE_THE_BEST_ANSWER,
            CastConstants.DETERMINE_THE_BEST_ANSWER,
            [
                {
                    text: CastConstants.YES,
                    onPress: () => {
                        console.log('Yes Pressed')
                        this.props.onClickAnswerCorrect(data);

                    },

                },
                {
                    text: CastConstants.NO,
                    onPress: () => console.log('OK Pressed'),
                    style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    }


    getDaysInLastFullMonth(day) {
        var d = new Date(day);
        var lastDayOfMonth = new Date(d.getFullYear(), d.getMonth() + 1, 0);
        return lastDayOfMonth.getDate();
    }
    showPopup = () => {
        this.setState({ isPopupShow: !this.state.isPopupShow });
    }
    renderModalRow = (item, index) => {
        let viewRow = <View />

        if (item.includes(noteDetailConstants.HISTORY) || item === CastConstants.VIEW_EDIT_HISTORY) {

            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconMaterialCommunity name="history" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>
        } else if (item.includes(noteDetailConstants.INAPPROPRIATE) || item === CastConstants.REPORT_INAPPROPRIATE) {

            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="flag" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>

        } else if (item.includes(noteDetailConstants.EDIT) || item === CastConstants.EDIT_POST || item === CastConstants.EDIT_QUESTION) {
            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="edit" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>

        } else if (item.includes(noteDetailConstants.SHARE) || item === CastConstants.SHARE) {
            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="share" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>
        }
        return (
            viewRow
        )
    }

    renderItemTag = (place, index) => {

        return (
            <View key={place.index} style={{ flexDirection: 'row' }} >
                {place.item.title ?
                    <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 20, height: 30, justifyContent: 'center', alignItems: 'center', marginLeft: 2.5, marginRight: 2.5, padding: 2 }}>

                        <Text style={styles.listText}>
                            {place.item.title}
                        </Text>
                    </View> : null}
            </View>
        )
    };

    renderItemQuestionPhoto = (item, index) => {

        let imageName = item.item.document
        let completeUrl = IMAGE_DASHBOARD_QUESTION_PREFIX_URL + item.item.documentable_id + '-thumb_' + imageName;
        let imgUrl = { uri: completeUrl };
        return (
            <View key={item.index} style={{padding:2}} >
                <Image source={imgUrl} style={styles.buttonPhoto} />
        <View style={styles.titleOnPhoto}>
        <Text style={styles.textTitleOnPhoto}>{CastConstants.PHOTO}{item.item.image_number}</Text>
                                </View>
            </View>
        )
    };

    renderItemAnswerPhoto = (item, index) => {

        let imageName = item.item.document
        let completeUrl = IMAGE_ANSWER_DETAIL_PREFIX_URL + item.item.documentable_id +'-medium_'+imageName;
        let imgUrl = { uri: completeUrl };
        console.log(" imgurl aanser ===", imgUrl); 
        return (
            <View key={item.index} style={{padding:2}} >
                <Image source={imgUrl} style={styles.buttonPhoto} />
        <View style={styles.titleOnPhoto}>
        <Text style={styles.textTitleOnPhoto}>{CastConstants.PHOTO}{item.item.image_number}</Text>
                                </View>
            </View>
        )
    };

    renderItemComment = (item, index) => {
        return (
            <View style={styles.viewRow}>
                <Text style={{}}>{item.item}</Text>
            </View>
        )
    }

    renderItem = (item, index) => {

        const { onClickMakeQuestionFavorite, moveToEditAnswerView, postDetail, handleChange, data, postComment, clickToGiveAnswer, arrayOfImages, handleFileChange, onClickAddImage, onClickDeleteImage, errors, postAnswer, arrayCompleteData, voteUpDownQuestion, voteUpDownAnswer } = this.props;

        let imgPrefixUrl = 'https://reactnativecode.com/wp-content/uploads/2017';
        let imgName = '/05/react_thumb_install.png';

        let urlImg2 = { uri: imgPrefixUrl + imgName };
        let viewRow = <View />;
        let checkmarkView = <View />
        let textAnswerCount = <Text />
        let questionOrAnswerImage = <Image />

        if (data['isAnyAnswerAccepted']) {
            if (item.item.is_accepted === 'Y') {
                checkmarkView = <IconEntypo name="check" style={styles.checkClicked} />
            } else {
                checkmarkView = <View />
            }
            questionOrAnswerImage = <Image source={require('../../../assets/answered.png')} style={{ width: 100, height: 30, marginLeft: '2%' }} />

        }
        else {
            checkmarkView = <IconEntypo name="check" style={styles.checkNormal} onPress={() => this.onClickCheck(item.item)} />
            questionOrAnswerImage = <Image source={require('../../../assets/in_question.png')} style={{ width: 100, height: 30, marginLeft: '2%' }} />
        }

        if (item.index === 0) {

            let categoryValue = ''
            if (item.item.category_id === 1) {
                categoryValue = dashboardConstants.CLINICAL;
            } else if (item.item.category_id === 2) {
                categoryValue = dashboardConstants.LIFE;
            } else {
                categoryValue = dashboardConstants.HOTEL_MANAGEMENT;
            }
            let specialitiesValue = '';
            if (item.item.specialities.length > 0) {
                for (let index = 0; index < item.item.specialities.length; index++) {
                    let dict = item.item.specialities[index];

                    specialitiesValue = specialitiesValue + dict.title + ', ';
                }
            }
            specialitiesValue = specialitiesValue.replace(/,\s*$/, "");

            viewRow = <View>
                <View style={{ flexDirection: 'row', marginTop: '2%' }}>

                    {questionOrAnswerImage}
                    <View style={{ paddingLeft: '2%' }}>
                        <Text style={styles.userName}>{item.item.user.nick_name} ({item.item.cs_score})</Text>
                        <Text style={styles.userName}>{CastConstants.UPDATED_ON} {timeConstants.showTimeWithMonthYearAndDate(item.item.updated_at ? item.item.updated_at : new Date())}</Text>
                    </View>

                    <View style={{ alignSelf: 'flex-end', right: 5, position: 'absolute' }}>
                        <ModalDropdown
                            dropdownStyle={{ width: 100, height: 140 }}
                            dropdownTextStyle={{ color: 'rgb(51,51,51)' }}
                            dropdownTextHighlightStyle={{ color: 'rgb(60, 132,255)' }}
                            options={this.arrayPopupQuestion}
                            renderRow={(item, index) => this.renderModalRow(item, index)}
                            onSelect={(index, value) => this.props.moveToQuestionPopupViews(item.item, index, value)}>
                            <IconFeather name='more-horizontal' size={25} />
                        </ModalDropdown>
                    </View>
                </View>
                <View style={styles.singleLine} />

                <Text style={styles.Question}>{item.item.title}</Text>

                {item.item.animal_detail ? <View style={{ flexDirection: 'row', marginBottom: '2%', marginLeft: '2%', marginRight: '2%', flexWrap: 'wrap', marginTop: '2%' }}>
                    <View style={styles.grayBox}>
                        <Text style={styles.textGrayBox}>{dashboardConstants.ANIMAL}</Text>
                    </View>
                    <Text style={styles.textGrayBox}>{item.item.animal_detail.animal_type ? item.item.animal_detail.animal_type.title : ''}({item.item.animal_detail.animal_breed ? item.item.animal_detail.animal_breed.title : ''})</Text>

                    <View style={styles.grayBox}>
                        <Text style={styles.textGrayBox}>{dashboardConstants.GENDER}</Text>
                    </View>
                    <Text style={styles.textGrayBox}>{item.item.animal_detail.gender}</Text>

                    <View style={styles.grayBox}>
                        <Text style={styles.textGrayBox}>{dashboardConstants.AGE}</Text>
                    </View>
                    <Text style={styles.textGrayBox}>{item.item.animal_detail.age_year}{wordConstants.CONST_YEARS} {item.item.animal_detail.age_month}{wordConstants.CONST_MONTHS}</Text>
                </View> : <View />}

                {item.item.animal_detail ? <View style={[styles.singleLine, { marginBottom: '2%' }]} /> : <View />}

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: .1, alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => voteUpDownQuestion('up')}>
                            <IconAntDesign name="upcircle" style={item.item.total_votes > 0 ? styles.upArrowClicked : styles.upArrowNormal} />
                        </TouchableOpacity>

                        <Text style={{ padding: 5, color: 'rgb(102,102,102)', fontSize: 16 }}>{item.item.total_votes ? item.item.total_votes : 0}</Text>

                        <TouchableOpacity onPress={() => voteUpDownQuestion('down')}>
                            <IconAntDesign name="downcircle" style={item.item.total_votes < 0 ? styles.upArrowClicked : styles.upArrowNormal} />
                        </TouchableOpacity>

                        <View style={{ marginTop: 10 }}>
                            <TouchableOpacity onPress={() => onClickMakeQuestionFavorite()}>
                                <IconAntDesign name="star" style={data['isStarClicked'] ? styles.starClicked : styles.starNormal} />

                            </TouchableOpacity>
                            <Text style={[styles.textCount, { padding: 5 }]}>{data['isStarClicked'] ? 1 : 0}</Text>
                        </View>

                        <IconAntDesign name="eye" style={styles.starNormal} />

                        <Text style={[styles.textCount, { padding: 5 }]}>{item.item.views}</Text>

                    </View>
                    <View style={{ flex: .9 }}>
                        <View>
                            <HTML html={item.item.description} />
                        </View>

                        {/* <Text style={styles.Answer}>{item.item.description}</Text> */}

                        <TouchableOpacity>
                            <Text style={styles.textCategory}>{categoryValue} > {specialitiesValue}</Text>
                        </TouchableOpacity>


                        <View style={{  marginTop: "2%",marginRight:'5%'  }}>
                            <FlatList
                                //style={{ width: '90%' }}
                                scrollEnabled={true}
                                horizontal={true}
                                data={item.item.documents}
                                renderItem={this.renderItemQuestionPhoto}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>

                        <Text style={[styles.textCategory, { marginTop: 5 }]}>{CastConstants.TAG}:</Text>

                        <View style={{ width: '100%', marginLeft: '5%', marginTop: "2%" }}>
                            <FlatList
                                style={{ width: '93%' }}
                                scrollEnabled={true}
                                horizontal={true}
                                data={item.item.tags}
                                renderItem={this.renderItemTag}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>

                        {/* When https url will be ready this section will show image. */}
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: '1%' }}>

                            {/* <TouchableOpacity style={styles.buttonPhoto} onPress={() => this.openImagePicker(this.setState({ isFirstButtonClick: true }))}> */}
                            {/* <View style={styles.buttonPhoto}>

                                <Image source={require('../../../assets/dog1.png')} style={styles.buttonPhoto} />

                                <View style={styles.titleOnPhoto}>
                                    <Text style={styles.textTitleOnPhoto}>写真1</Text>
                                </View>
                            </View> */}

                            {/* <TouchableOpacity style={styles.buttonPhoto} onPress={() => this.openImagePicker(this.setState({ isFirstButtonClick: false }))}> */}
                            {/* <View style={styles.buttonPhoto} >
                                <Image source={require('../../../assets/dog2.png')} style={styles.buttonPhoto} />
                                {/* <Image source={urlImg2} style={styles.buttonPhoto} /> */}

                            {/* <View style={styles.titleOnPhoto}>
                                    <Text style={styles.textTitleOnPhoto}>写真2</Text>
                                </View>

                            </View>  */}
                        </View>
                    </View>

                </View>

                <View style={{ backgroundColor: 'rgb(246,246,246)', marginTop: "2%", marginLeft: '25%', marginRight: "2%", borderRadius: 10 }}>


                    <CommentList data={item.item.comments} />

                    <View style={{ backgroundColor: 'rgb(246,246,246)', paddingLeft: '2%', height: 'auto', borderRadius: 10 }}>
                        <View style={{ justifyContent: 'center', height: verticalScale(35) }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: '2%', marginRight: '2%', alignItems: 'center', }}>

                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', }}>
                                    {/*<Text style={[styles.bottomText, { marginRight: '2%' }]}>{postAnswerConstants.SHOW}</Text>*/}
                                    {/*<IconAntDesign name="caretup" style={{ color: 'rgb(23, 71, 119)', marginLeft: '2%' }} />*/}
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => {
                                    if (this.state.indexClicked === item.index) {
                                        this.setState({
                                            indexClicked: ''
                                        })
                                    } else {
                                        this.setState({
                                            indexClicked: item.index
                                        })
                                    }
                                }} style={{ flexDirection: 'row', alignItems: 'center', }}>
                                    <IconAntDesign name="plus" style={{ color: 'rgb(23, 71, 119)' }} />
                                    <Text style={[styles.bottomText, { marginRight: '2%' }]}>{postAnswerConstants.ADD_SUPPLEMENT_COMMENT}</Text>
                                </TouchableOpacity>

                                {/*<View style={styles.singleLine} />*/}
                            </View>
                        </View>

                        {this.state.indexClicked === item.index ? <View>
                            <TextInput style={styles.input}
                                placeholder="comment"
                                autoCapitalize="none"
                                value={data.commentText}
                                multiline={true}
                                onChangeText={(value) => { handleChange('commentText', value) }}
                            />
                            <View style={{ width: "100%", padding: 10 }} >
                                <TouchableOpacity
                                    onPress={() => this.addPost(item.item)}
                                    style={{ width: 100, alignSelf: 'flex-end', justifyContent: "center" }}>

                                    <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 5, padding: 5 }}>
                                        <Text style={{ color: 'black', alignSelf: "center" }} >
                                            Post
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View> : null}
                    </View>
                </View>
                {arrayCompleteData.length > 1 ? <Text style={[styles.textAnswersCount, { marginLeft: '5%', marginTop: '5%' }]}>{arrayCompleteData.length - 1} {CastConstants.ANSWERS}</Text> : <View />}
                <View style={[styles.singleLine, { marginTop: '2%', marginBottom: '2%' }]} />

            </View>
        }
        else {
            let imgNickName = item.item.nickname_icon;
            let nickNameUrl = IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL + imgNickName

            let userImgUrl;
            if (!item.item.nickname_icon) {
                userImgUrl = require('../../../assets/profilePhoto.png')
            } else {
                userImgUrl = { uri: nickNameUrl };
            }

            viewRow = <View>
                <View style={{ flexDirection: 'row' }}>

                    {item.item.updated_at ?
                        <Text style={styles.textGrayTitle}>{timeConstants.dateDiffInDays_Months_Years(item.item.updated_at ? item.item.updated_at : new Date())}</Text> : null}
                    <View style={{ alignSelf: 'flex-end', right: 5, position: 'absolute' }}>
                        <ModalDropdown
                            dropdownStyle={{ width: 100, height: 100 }}
                            dropdownTextStyle={{ color: 'rgb(51,51,51)' }}
                            dropdownTextHighlightStyle={{ color: 'rgb(60, 132,255)' }}

                            options={this.arrayPopupAnswer}
                            renderRow={(item, index) => this.renderModalRow(item, index)}

                            onSelect={(index, value) => this.props.moveToAnswerPopupViews(item.item, index, value)}>

                            <IconFeather name='more-horizontal' size={25} />
                        </ModalDropdown>
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: .1, alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => voteUpDownAnswer(item.item, 'up')}>
                            <IconAntDesign name="upcircle" style={item.item.votes_count > 0 ? styles.upArrowClicked : styles.upArrowNormal} />
                        </TouchableOpacity>

                        <Text style={{ padding: 5, color: 'rgb(102,102,102)', fontSize: 16 }}>{item.item.votes_count ? item.item.votes_count : 0}</Text>

                        <TouchableOpacity onPress={() => voteUpDownAnswer(item.item, 'down')}>
                            <IconAntDesign name="downcircle" style={item.item.votes_count < 0 ? styles.upArrowClicked : styles.upArrowNormal} />
                        </TouchableOpacity>

                        <View style={{ marginTop: 10 }}>
                            {checkmarkView}
                        </View>



                    </View>
                    <View style={{ flex: .9 }}>

                        <Text style={styles.Answer}>{item.item.answer}</Text>
                        <View style={{  marginTop: "2%",marginRight:'5%'  }}>
                            <FlatList
                                //style={{ width: '90%' }}
                                scrollEnabled={true}
                                horizontal={true}
                                data={item.item.documents}
                                renderItem={this.renderItemAnswerPhoto}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>

                    </View>

                </View>

                <View style={{ backgroundColor: 'rgb(246,246,246)', marginTop: "2%", marginRight: "2%", borderRadius: 10, flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center', }}>
                    <Image source={userImgUrl} style={[commonStyles.profilePhotoWithoutCircle, { marginLeft: '2%' }]} />
                    <View style={{ padding: '2%' }}>

                        <View>
                            <Text style={{ paddingBottom: '1%' }}>{item.item.user.nick_name} </Text>
                            <View style={{ flexDirection: 'row', padding: '2%' }}>
                                <IconAntDesign name="eye" size={14} style={{ paddingRight: 2 }} />

                                {item.item.user.allPostsViews > 1000 ? <Text>1K+</Text> : item.item.user.allPostsViews === 1000 ? <Text>1K</Text> : <Text>{item.item.user.allPostsViews}</Text>}

                                <IconFontawesome name="database" size={14} color={'green'} style={{ paddingLeft: 10, paddingRight: 2 }} />
                                <Text>{item.item.user.cs_score}</Text>

                            </View>
                        </View>
                    </View>

                </View>

                <View style={{ backgroundColor: 'rgb(246,246,246)', marginTop: "2%", marginLeft: '25%', marginRight: "2%", borderRadius: 10 }}>

                    <CommentList data={item.item.comments} />

                    <View style={{ backgroundColor: 'rgb(246,246,246)', paddingLeft: '2%', height: 'auto', borderRadius: 10 }}>
                        <View style={{ justifyContent: 'center', height: verticalScale(35) }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: '2%', marginRight: '2%', alignItems: 'center', }}>

                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', }}>
                                    {/*<Text style={[styles.bottomText, { marginRight: '2%' }]}>{postAnswerConstants.SHOW}</Text>*/}
                                    {/*<IconAntDesign name="caretup" style={{ color: 'rgb(23, 71, 119)', marginLeft: '2%' }} />*/}
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => {
                                    if (this.state.indexClicked === item.index) {
                                        this.setState({
                                            indexClicked: ''
                                        })
                                    } else {
                                        this.setState({
                                            indexClicked: item.index
                                        })
                                    }
                                }} style={{ flexDirection: 'row', alignItems: 'center', }}>
                                    <IconAntDesign name="plus" style={{ color: 'rgb(23, 71, 119)' }} />
                                    <Text style={[styles.bottomText, { marginRight: '2%' }]}>{postAnswerConstants.ADD_SUPPLEMENT_COMMENT}</Text>
                                </TouchableOpacity>

                                {/*<View style={styles.singleLine} />*/}
                            </View>


                        </View>
                        {this.state.indexClicked === item.index ? <View>
                            <TextInput style={styles.input}
                                placeholder="comment"
                                autoCapitalize="none"
                                value={data.commentText}
                                multiline="true"
                                onChangeText={(value) => { handleChange('commentText', value) }}
                            />
                            <View style={{ width: "100%", padding: 10 }} >
                                <TouchableOpacity
                                    onPress={() => this.addPost(item.item)}
                                    style={{ width: 100, alignSelf: 'flex-end', justifyContent: "center" }}>

                                    <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 5, padding: 5 }}>
                                        <Text style={{ color: 'black', alignSelf: "center" }} >
                                            Post
                </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View> : null}

                    </View>

                </View>

                <View style={[styles.singleLine, { marginTop: '2%', marginBottom: '2%' }]} />
            </View>
        }
        return (
            <SafeAreaView >
                {viewRow}
            </SafeAreaView>
        )
    }

    render() {
        const { showSuggestionView, postDetail, handleChange, fetching, data, postComment, clickToGiveAnswer, arrayOfImages, handleFileChange, onClickAddImage, onClickDeleteImage, errors, postAnswer, arrayCompleteData, hideInAppropriatePopup, callApiToMakeQuestionInAppropriate, callApiToMakeAnswerInAppropriate, checkQuestionOrAnswerInAppropriate } = this.props;

        return (
            <SafeAreaView >
                <View>
                    <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                        <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => this.props.navigation.goBack()} >
                            <Image source={require('../../../assets/cross1x.png')} style={{ width: 13, height: 13 }} />
                        </TouchableOpacity>
                        <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center' }]}>{postAnswerConstants.DETAIL_TITLE}</Text>
                        <View style={{ flex: 1.5 }}>
                            {/* <TouchableOpacity onPress={this.showActionSheet}>
                                     <Text style={{ color: 'rgb(2, 132, 254)', fontSize: 17 }}>{postAnswerConstants.SEND}</Text>
                                     </TouchableOpacity> */}
                        </View>
                    </View>
                    <View style={styles.singleLine} />
                </View>
                <KeyboardAwareScrollView>
                    <View>
                        <FlatList
                            data={arrayCompleteData}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    {!data['isClickForAnswer'] && !fetching ?
                        <View style={{ marginTop: '10%', marginBottom: '10%' }}>
                            <TouchableOpacity style={styles.viewAddButton} onPress={() => clickToGiveAnswer()}>
                                <Text style={styles.textAddImage}>{postAnswerConstants.VETENARIANS_HELP_EACH_OTHER}</Text>
                                <Text style={styles.textAddImage}>{postAnswerConstants.ADD_TEXT}</Text>
                            </TouchableOpacity>
                        </View> : null}
                    {data['isClickForAnswer'] && !fetching ? <View style={{ marginBottom: '10%' }}>
                        <View style={{ flex: 2 }}>
                            <InputTextWithCharLimit value={data['answerText']} placeholder={postAnswerConstants.PLEASE_ENTER_YOUR_ANSWER} charLimit={data['charLimitAnswer']} name={'answerText'} handleChange={handleChange} titleLabel={postAnswerConstants.PLEASE_ENTER_YOUR_ANSWER} arraySimilarQuestion={[]} errors={errors} />
                        </View>

                        {data['isShowSuggestionView'] ? <ViewAnswerInstruction showSuggestionView={() => showSuggestionView()} /> : null}

                        <View style={{ flex: 3, marginBottom: '5%' }}>
                            <PostPhotoSection arrayOfImages={arrayOfImages} onClickAddImage={onClickAddImage} handleFileChange={handleFileChange} handleChange={handleChange} onClickDeleteImage={onClickDeleteImage} data={data} />
                        </View>
                        <View style={[styles.buttonRoundCorner, { flex: 2 }]}>
                            <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => { postAnswer() }}>
                                <Text style={[styles.textButton]}>{postAnswerConstants.POST_ANSWER}</Text>
                            </TouchableOpacity>
                        </View>
                    </View> : null}

                    <View style={{ marginBottom: '5%' }} />

                </KeyboardAwareScrollView>

                {fetching ? <View style={{ position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center', marginTop: 250 }} />
                </View> : null}

                {data['isClickToMakeInAppropriate'] ?

                    <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: '110%', width: "100%", position: 'absolute' }}>

                        <View style={{ height: 290, position: 'absolute', width: screenWidth - 20, marginTop: 120, alignSelf: 'center', borderRadius: 10, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[styles.titleInappropriate, { marginLeft: 25, marginTop: 10 }]}>{postAnswerConstants.REPORT_INAPPROPRIATE_POST}</Text>
                                <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => hideInAppropriatePopup()} >
                                    <IconAntDesign name='close' size={25} color={'gray'} style={{ alignSelf: 'flex-end', marginRight: 5 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.singleLine} />

                            <View >
                                <InputTextWithCharLimit value={data['reasonInAppropriate']} placeholder={postAnswerConstants.PLEASE_DESCRIBE_WHAT_IS_INAPPROPRIATE} charLimit={data['charLimitAnswer']} name={'reasonInAppropriate'} handleChange={handleChange} titleLabel={''} arraySimilarQuestion={[]} errors={errors} />
                            </View>


                            <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginRight: 5, marginTop: 5, marginBottom: 5 }}>

                                <TouchableOpacity style={[styles.buttonRoundCorner, { marginRight: 10, backgroundColor: 'rgb(51,51,51)' }]} onPress={() => hideInAppropriatePopup()}>
                                    <Text style={styles.buttonTextInappropriate}>{postAnswerConstants.CANCEL_INAPPROPRIATE}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.buttonRoundCorner, { backgroundColor: 'rgb(0, 123, 255)' }]} onPress={() => checkQuestionOrAnswerInAppropriate()}>
                                    <Text style={styles.buttonTextInappropriate}>{postAnswerConstants.REPORT_INAPPROPRIATE}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableOpacity>

                    : <View />
                }
            </SafeAreaView>
        )
    }
}

const styles = {
    grayBox: {
        backgroundColor: 'rgb(243, 243, 243)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textAnimalInfo: {
        fontSize: 12,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
        padding: 5
    },
    textGrayBox: {
        fontSize: 12,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
        padding: 5
    },
    userName: {
        fontSize: 12,
        color: 'rgb(134,134,134)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    titleInappropriate: {
        fontSize: 18,
        color: 'rgb(134,134,134)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    buttonTextInappropriate: {
        padding: 8,
        fontSize: 16,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    viewSuggestionMessage: {
        backgroundColor: 'rgb(255,248,220)',
        borderColor: 'gray',
        borderRadius: 5,
    },
    viewRow: {
        margin: '2%'
    },
    textButton: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '3%',
        paddingRight: '3%'
    },
    buttonRoundCorner: {
        backgroundColor: 'rgb(3, 125, 58)',
        borderRadius: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        height: verticalScale(40),
        // marginBottom:'5%'
    },
    viewPopup: {
        backgroundColor: 'rgb(3, 125, 58)',
        borderRadius: 10,
        // alignSelf: 'center',
        // justifyContent: 'center',
        // height: verticalScale(40),
    },
    viewAddButton: {
        backgroundColor: 'rgb(229, 242,255)',
        width: '80%',
        height: 100,
        //marginBottom:'2%', 
        alignSelf: 'center',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewPopup: {
        position: 'absolute',
        width: 120,
        height: 126,
        marginTop: 375,
        right: 36,
        borderColor: '#e3e8f1',
        borderWidth: 1,
        shadowColor: '#e3e8f1',
        shadowOffset: {
            width: 2,
            height: 1.5
        },
        shadowOpacity: 0.7,
        shadowRadius: 1,
        borderRadius: 8,
        backgroundColor: 'white'

    },
    textGrayTitle: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 14,
        color: ' rgba(183, 183, 183, 255)',
        marginRight: '5%',
        marginLeft: '5%'
    },
    textAddImage: {
        fontSize: 14,
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    titleText: {
        fontSize: 16,
        color: 'rgba(51,51,51, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textAnswersCount: {
        fontSize: 16,
        color: 'rgba(51,51,51, 255)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    listText: {
        color: '#0385ff',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        fontSize: 14,
        marginRight: 8,
        marginLeft: 8,
    },
    upArrowClicked: {
        color: 'rgb(60, 132, 255)',
        // marginTop: 30,
        fontSize: 18
    },
    upArrowNormal: {
        // marginTop: 30, 
        color: 'rgb(204, 204, 204)',
        fontSize: 18
    },
    starNormal:
    {
        color: 'rgb(204, 204, 204)',
        fontSize: 18
    },
    starClicked: {
        color: 'rgb(221, 176, 0)',
        fontSize: 18
    },
    checkNormal:
    {
        color: 'rgb(204, 204, 204)',
        fontSize: 18
    },
    checkClicked: {
        color: 'green',
        fontSize: 18
    },
    textCount: {
        color: 'rgb(102,102,102)', fontSize: 16
    },
    row: {
        height: verticalScale(58),
        flexDirection: 'row',
    },

    textTitle: {
        fontSize: 20,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },

    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
    singleLineDouble: {
        width: '100%',
        height: 5,
        backgroundColor: 'lightgray'
    },

    input: {
        margin: 10,
        height: 60,
        borderColor: "#d3d3d3",
        borderWidth: 2,
        borderRadius: 5,
        paddingLeft: 10
    },
    inputTextAreaStyle: {
        // marginLeft: '3%',
        marginRight: '3%',
        height: 100,
        backgroundColor: 'white',
        marginTop: '3%',
        // shadowColor: 'rgba(0,0,0,0.7)',
        // shadowOffset: {
        //     width: 2,
        //     height: 4
        // },
        // shadowOpacity: 0.5,
        // shadowRadius: 1,
        // borderRadius: 8,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingTop: 21,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    Question: {
        fontSize: 17,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    commentTitle: {
        fontSize: 13,
        color: 'rgb(58,58,58)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    Answer: {
        fontSize: 13,
        color: 'rgb(116,116,116)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 10
    },
    textCategory: {
        fontSize: 13,
        marginLeft: 10,
        color: 'rgb(116,116,116)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    buttonPhoto: {
        width: screenWidth / 2 - 30,
        height: screenWidth / 2 - 30,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,

    },
    titleOnPhoto: {
       // height: '12%',
        //width: '40%',
        backgroundColor: 'rgb(219, 254, 224)',
        position: 'absolute',
        borderColor: 'green',
        borderWidth: 2,
        margin: '5%',
        justifyContent: 'center',
        alignItems:'center'

    },
    textTitleOnPhoto: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    commentText: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: 5,
        marginRight: 5,

    },
    bottomText: {
        fontSize: 12,
        color: 'rgb(23, 71, 119)',
        fontFamily: 'NotoSansCJKjp-Medium',

    }

}