
import { Dimensions } from 'react-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);


export const styles = {
    buttonPhoto: {
        width: screenWidth / 2 - 30,
        height: screenWidth / 2 - 30,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,

    },
    titleOnPhoto: {
        // height: '12%',
        //width: '40%',
        backgroundColor: 'rgb(219, 254, 224)',
        position: 'absolute',
        borderColor: 'green',
        borderWidth: 1,
         margin: '2%',
        justifyContent: 'center',
        alignItems: 'center'

    },
    textBlock: {
        color: '#212529',
        fontFamily: 'NotoSansCJKjp-Regular',
        fontSize: 14
    },
    textImageDetail: {
        color: '#6e7e98',
        fontFamily: 'NotoSansCJKjp-Regular',
        fontSize: 14
    },
    textTitleOnPhoto: {
        fontSize: 13,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    modalPopupView: {
        alignSelf: 'flex-end', right: 5, position: 'absolute', paddingBottom: 5
    },
    titleInappropriate: {
        fontSize: 18,
        color: 'rgb(134,134,134)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    buttonTextInappropriate: {
        padding: 8,
        fontSize: 16,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    textButtonTitle: {
        fontSize: 18,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingTop: 5,
        alignSelf: 'center'
    },
    buttonClinicalNotes: {
        justifyContent: 'center',
        borderRadius: 5,
        backgroundColor: 'rgb(66, 77,117)',
    },
    textAuthorName: {
        fontSize: 14,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    iconHeader: {
        paddingRight: 3,
        color: 'rgb(134, 144, 166)'
    },

    iconTextHeader: {
        fontSize: 13,
        color: 'rgb(134, 144, 166)'
    },
    iconHeaderSelected: {
        paddingRight: 3,
        color: 'rgb(236, 188, 2)'
    },

    iconTextHeaderSelected: {
        fontSize: 14,
        color: 'rgb(236, 188, 2)'
    },
    textDescription: {
        fontSize: 14,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    textDescriptionIncreaseSize: {
        fontSize: 17,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    viewCircleComment: {
        backgroundColor: '#0e0e0e',
        height: 20,
        width: 20,
        borderRadius: 10,
        alignItems: 'center',
        // justifyContent: 'center',
        position: 'absolute'
    },
    textComment: {
        fontSize: 12,
        paddingTop: 3,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    textAboutVoting: {
        fontSize: 13,
        color: 'rgb(2,132,255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    viewVote: {
        backgroundColor: 'rgb(229, 242,255)',
        width: '90%',
        // height: 100,
        //marginBottom:'2%', 
        alignSelf: 'center',
        borderRadius: 10,
    },
    iconVoteDeselected: {
        color: '#d3d3d3',
        fontSize: 40,
        padding: 5
    },
    iconVoteSelected: {
        color: 'rgb(2, 132, 255)',
        fontSize: 40,
    },
    textTitle: {
        fontSize: 16,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        // alignSelf: 'center',
        // textAlign: 'center',
    },

    textVoteValue: {
        fontSize: 20,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingTop: 5,
        paddingLeft: 5,
        paddingRight: 5
    },
    textVoteTitle: {
        fontSize: 20,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textTime: {
        fontSize: 12,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    viewRow: {
        margin: '2%'
    },
    commentText: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: 5,
        marginRight: 5,

    },
    waitingForVote: {
        fontSize: 13,
        color: 'red',
        fontFamily: 'NotoSansCJKjp-Regular',
    },

    titleArticle: {
        fontSize: 13,
        color: 'rgb(116,116,116)',
        fontFamily: 'NotoSansCJKjp-Bold',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 10
    },
    categoryViewInOval: {
        backgroundColor: 'rgb(223,230, 237)', borderRadius: 5, height: 30, justifyContent: 'center', alignItems: 'center'
    },
    titleSuggestedNotes: {
        color: '#0e0e0e',
        fontFamily: 'NotoSansCJKjp-Medium',
        margin: 10
    },
    Answer: {
        fontSize: 13,
        color: 'rgb(116,116,116)',
        fontFamily: 'NotoSansCJKjp-Regular',
        margin: 10
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
    singleCuttedLine: {
        width: '96%',
        height: 1,
        backgroundColor: '#d3d3d3',
    },
    listText: {
        color: '#0385ff',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        fontSize: 14,
        marginRight: 8,
        marginLeft: 8,
    },
    profilePhotoWithoutCircle: {
        width: 30,
        height: 30,
        borderRadius: 15,
    },
    viewCommentInput: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: '90%',
        borderRadius: 20,
        height: 40,
        backgroundColor: 'rgb(229, 243, 255)'
    },
    viewCommentData: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: '90%',
        borderRadius: 20,
        height: 40,
        backgroundColor: 'rgb(223,230, 237)',

    },
    commentTextRow: {
        paddingLeft: 10,
        fontSize: 13,
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    input: {
        paddingLeft: 10
        // margin: 10,
        // height: 60,
        // borderColor: "#d3d3d3",
        // borderWidth: 2,
        // borderRadius: 5
    },
    thumbSelected: {
        fontSize: 15,
        color: 'rgb(2, 135, 255)',

    },
    thumbNotSelected: {
        fontSize: 15,
        color: 'gray',
    },
    textLike: {
        fontSize: 15,
        fontFamily: 'NotoSansCJKjp-Regular',
        marginLeft: 5,
        color: 'rgb(51,51,51)'
    },
    userName: {
        color: '#1b659e',
        fontSize: 15,
        fontFamily: 'NotoSansCJKjp-Regular',
        marginLeft: '2%'
    }
}