export const styles = {
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },

    commentText: {
        fontSize: 10,
        color: 'rgb(74,74,74)',
        fontFamily: 'NotoSansCJKjp-Regular',
        marginLeft: 5,
        marginRight: 5,

    },
    userName: {
        fontSize: 12,
        color: 'rgb(134,134,134)',
        fontFamily: 'NotoSansCJKjp-Regular',

    },

    bottomText: {
        fontSize: 13,
        color: 'rgb(18, 75, 136)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    viewContainAccordion: {

        width: '90%',
        margin: '5%',
        alignSelf: 'center',
        //  borderColor: 'gray',
        // borderWidth: 1,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowColor: 'gray',
        shadowOpacity: 0.5,
        shadowRadius: 10,
        shadowOffset: {
            width: 2,
            height: 2
        }
    },
    textSubrowTitle: {
        color: '#333',
        fontSize: 12,
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textSubrow: {

        fontSize: 12,
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    viewRowSideHeading: {
        flexDirection: 'row',
        paddingLeft: '5%'
    },
    viewOuter: {

        // borderColor: 'gray',
        // borderWidth: 1,
        // borderRadius: 5,
        // backgroundColor: 'white',

    },
    viewContainHeader: {
        backgroundColor: 'rgb(247,247,247)',

        justifyContent: 'center',
        alignItem: 'center'
    },
    viewHeader: {
        margin: 10,
        backgroundColor: 'rgb(204,204,204)',

        borderRadius: 5,

        alignItem: 'center',
        justifyContent: "center",
    },
    textHeader: {
        color: 'white',
        alignSelf: 'center',
        padding: 10,
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    content: {
        //backgroundColor:'pink'
    },
    textContent: {
        alignSelf: 'center',
        fontFamily: 'NotoSansCJKjp-Regular',

    },
    innerView: {
        padding: '5%',
    },
    backToPreviousView: {
        backgroundColor: 'rgb(2, 132, 255)',
        alignItem: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        marginRight: '5%',
        marginTop: '5%',
        borderRadius: 5
    },
    viewSingleLineData: {
        flexDirection: 'row', padding: '5%'
    }
}