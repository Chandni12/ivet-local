import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Keyboard, TouchableWithoutFeedback, TextInput, ScrollView, Dimensions, FlatList } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import historyConstants from '../../constants/History';
import { styles } from './styles/historyStyle';
import { scale, verticalScale } from 'react-native-size-matters';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import KeyboardAwareScrollView from 'react-native-keyboard-aware-scroll-view';
import * as wordConstants from '../../constants/WordConstants';
import Accordion from 'react-native-collapsible/Accordion';



export default class ViewHistory extends Component {
    state = {
        activeSections: [],
    };

    renderItem = (item, index) => {
        console.log(" item is ---", item);

        let sideHeading = '';
        let viewHeading = <View />

        let viewRow = <View style={styles.innerView}>

            {item.item.old_value ? <View style={{ flexDirection: 'row' }}>
                <Text style={styles.textSubrowTitle} >{historyConstants.BEFORE_CHANGE} </Text>
                <Text style={styles.textSubrow}>{item.item.old_value} </Text>
            </View> : null}

            {item.item.new_value ? <View style={{ flexDirection: 'row' }}>
                <Text style={styles.textSubrowTitle} >{historyConstants.AFTER_CHANGE} </Text>
                <Text style={styles.textSubrow}>{item.item.new_value}</Text>
            </View> : null}

        </View>

        switch (item.item.key) {

            case "new_document":
                sideHeading = historyConstants.IMAGE_TO_BE_CHANGED;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{sideHeading} </Text>
                    {item.item.new_value ? <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textSubrowTitle} >{historyConstants.PHOTO_NUMBER} </Text>
                        <Text style={styles.textSubrow}>{item.item.new_value} </Text>
                    </View> : null}
                </View>
                
                break;
                case "remarks": // Will change according to image title or description
                    sideHeading = historyConstants.REMARKS;
                    viewHeading = <View style={styles.viewRowSideHeading}>
                        <Text style={styles.textSubrowTitle} >{sideHeading} </Text>
                        {item.item.new_value ? <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.textSubrow}>{item.item.new_value} </Text>
                        </View> : null}
                    </View>
                    
                    break;

            case 'created_at':
                sideHeading = historyConstants.CREATED_AT;
                viewRow = <View style={styles.viewSingleLineData}>
                    <Text style={styles.textSubrowTitle}>{historyConstants.ADDITIONAL_CONTENT} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                    <Text style={styles.textSubrow}>{item.item.new_value}</Text>
                </View>
                break;
            case 'description':
                sideHeading = historyConstants.DESCRIPTION;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.CHANGE_THE_SUBJECT} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;

            case 'speciality_id':
                sideHeading = historyConstants.SPECIALITY;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.CHANGE_THE_SUBJECT} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;

            case "tag_id":
                sideHeading = '';
                viewHeading = <View>
                    <Text style={styles.textSubrow}>{item.item.old_value} </Text>
                </View>
                break;

            case "title":
                sideHeading = historyConstants.TITLE;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.CHANGE_THE_SUBJECT} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;

            case "animal_type_id":
                sideHeading = historyConstants.ANIMAL_TYPE;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.TARGET} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;

            case "animal_breed_type_id":
                sideHeading = historyConstants.ANIMAL_BREED_TYPE;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.CHANGE_TARGET} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;

            case "gender":
                sideHeading = historyConstants.GENDER;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.TARGET} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;
            case "age_year":
                sideHeading = historyConstants.AGE_YEAR;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.CHANGE_THE_SUBJECT} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;
            case "age_month":
                sideHeading = historyConstants.AGE_MONTH;
                viewHeading = <View style={styles.viewRowSideHeading}>
                    <Text style={styles.textSubrowTitle} >{historyConstants.CHANGE_THE_SUBJECT} </Text>
                    <Text style={styles.textSubrow}>{sideHeading} </Text>
                </View>
                break;



            case 'question_comment':
                sideHeading = historyConstants.COMMENT;
                viewRow = <View style={styles.viewSingleLineData}>
                    <Text style={styles.textSubrowTitle}>{sideHeading}: </Text>
                    <Text style={styles.textSubrow}>{item.item.new_value}</Text>
                </View>
                break;

            default:

                break;
        }



        return (
            <View>
                {viewHeading}
                {viewRow}
            </View>

        )
    }

    _renderSectionTitle = section => {
        return (
            <View style={styles.content}>

                <Text style={styles.textContent}>{section.content}</Text>
            </View>
        );
    };

    _renderHeader = section => {

        let username = '';
        
        if (this.props.navigation.state.params.data) {
            username = this.props.data.username;
        } else {
            if (section.content.length > 0) {
                username = section.content[0].nick_name;
            } else {
                username = this.props.data.username;
            }
        }
       
        return (
            <View style={styles.viewContainHeader}>
                <View style={styles.viewHeader}>
                    <Text style={styles.textHeader}>{section.title} - {historyConstants.EDITED_BY} {username}</Text>
                </View>
                <View style={styles.singleLine} />
            </View>

        );
    };

    _renderContent = section => {
        return (
            <View style={styles.content}>
                <FlatList
                    data={section.content}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />

                {/* <Text>{section.content}</Text> */}
            </View>
        );
    };

    _updateSections = activeSections => {
        this.setState({ activeSections });
    };

    render() {
        const { arrayHistoryData } = this.props;
        return (
            <SafeAreaView style={{ height: '100%' }}>

                <View>
                    <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>

                        <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => this.props.navigation.goBack()} >
                            <Image source={require('../../../assets/cross1x.png')} style={{ width: 13, height: 13 }} />
                        </TouchableOpacity>

                        <Text>{historyConstants.HISTORY} </Text>
                        <View style={{ flex: 1.5 }} />

                    </View>
                    <View style={styles.singleLine} />
                </View>
                <ScrollView>
                    <View style={styles.viewOuter}>
                        <View style={styles.backToPreviousView}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Text style={styles.textHeader}>{historyConstants.BACK_TO_PREVIOUS_PAGE}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewContainAccordion}>
                            <Accordion
                                underlayColor={'transparent'}
                                sections={arrayHistoryData}
                                activeSections={this.state.activeSections}
                                // renderSectionTitle={this._renderSectionTitle}
                                renderHeader={this._renderHeader}
                                renderContent={this._renderContent}
                                onChange={this._updateSections}
                            />
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView >
        );
    }

}