import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput, ActivityIndicator, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { checkError } from '../../utils'
import * as Animatable from 'react-native-animatable';
import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ModalDropdown from 'react-native-modal-dropdown';
import commonStyles from '../../stylesheet/common/commonStyles.style';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather';
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFontawesome from 'react-native-vector-icons/FontAwesome';

import noteDetailConstants from '../../constants/NoteDetail';
import dashboardConstants from '../../constants/Dashboard';
import CastConstants from '../../constants/Cast';
import * as timeConstants from '../../constants/TimeConstants';

import postAnswerConstants from '../../constants/PostAnswer';
import InputTextWithCharLimit from '../../commonComponent/InputTextWithCharLimit';
import { IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL, IMAGE_VETNOTE_PREFIX_URL } from '../../api/endPoints'
import { styles } from './styles/noteDetailStyle';
import commonStylesStyle from '../../stylesheet/common/commonStyles.style'
import noteDetailJapanese from '../../constants/NoteDetail';
import HTML from 'react-native-render-html';


const screenWidth = Math.round(Dimensions.get('window').width);

export default class NoteDetailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFontSizeClick: false,
        }
        this.arrayPopup = [CastConstants.SHARE, CastConstants.VIEW_EDIT_HISTORY, CastConstants.REPORT_INAPPROPRIATE];
        this.arrayPopupNotes = [CastConstants.VIEW_EDIT_HISTORY, CastConstants.REPORT_INAPPROPRIATE, CastConstants.EDIT_POST];

    }

    addComment() {
        <View>
            <View style={{ backgroundColor: 'rgb(246,246,246)', marginTop: "2%", marginLeft: '25%', marginRight: "2%", borderRadius: 10 }}>

                <CommentList data={item.item.comments} />

                <View style={{ backgroundColor: 'rgb(246,246,246)', paddingLeft: '2%', height: 'auto', borderRadius: 10 }}>
                    <View style={{ justifyContent: 'center', height: verticalScale(35) }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: '2%', marginRight: '2%', alignItems: 'center', }}>

                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', }}>
                                {/*<Text style={[styles.bottomText, { marginRight: '2%' }]}>{postAnswerConstants.SHOW}</Text>*/}
                                {/*<IconAntDesign name="caretup" style={{ color: 'rgb(23, 71, 119)', marginLeft: '2%' }} />*/}
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {
                                if (this.state.indexClicked === item.index) {
                                    this.setState({
                                        indexClicked: ''
                                    })
                                } else {
                                    this.setState({
                                        indexClicked: item.index
                                    })
                                }
                            }} style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <IconAntDesign name="plus" style={{ color: 'rgb(23, 71, 119)' }} />
                                <Text style={[styles.bottomText, { marginRight: '2%' }]}>{postAnswerConstants.ADD_SUPPLEMENT_COMMENT}</Text>
                            </TouchableOpacity>

                            {/*<View style={styles.singleLine} />*/}
                        </View>


                    </View>
                    {this.state.indexClicked === item.index ? <View>
                        <TextInput style={styles.input}
                            placeholder="comment"
                            autoCapitalize="none"
                            value={data.commentText}
                            onChangeText={(value) => { handleChange('commentText', value) }}
                        />
                        <View style={{ width: "100%", padding: 10 }} >
                            <TouchableOpacity
                                onPress={() => this.addPost(item.item)}
                                style={{ width: 100, alignSelf: 'flex-end', justifyContent: "center" }}>

                                <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 5, padding: 5 }}>
                                    <Text style={{ color: 'black', alignSelf: "center" }} >
                                        Post
</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View> : null}

                </View>

            </View>

            <View style={[styles.singleLine, { marginTop: '2%', marginBottom: '2%' }]} />
        </View>
    }
    renderModalRow = (item, index) => {
        let viewRow = <View />

        if (item.includes(noteDetailConstants.HISTORY) || item === CastConstants.VIEW_EDIT_HISTORY) {

            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconMaterialCommunity name="history" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>
        } else if (item.includes(noteDetailConstants.INAPPROPRIATE) || item === CastConstants.REPORT_INAPPROPRIATE) {

            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="flag" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>

        } else if (item.includes(noteDetailConstants.EDIT) || item === CastConstants.EDIT_POST) {
            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="edit" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>

        } else if (item.includes(noteDetailConstants.SHARE) || item === CastConstants.SHARE) {
            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="share" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>
        }


        return (
            viewRow
        )
    }
    renderItemTag = (place, index) => {

        return (
            <View key={place.index} style={{ flexDirection: 'row' }} >
                {place.item.title ?
                    <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 20, height: 30, justifyContent: 'center', alignItems: 'center', marginLeft: 2.5, marginRight: 2.5, padding: 2 }}>

                        <Text style={styles.listText}>
                            {place.item.title}
                        </Text>
                    </View> : null}
            </View>
        )
    };
    renderCommentItem = (item, index) => {
        const { handleChange, data, onClickLikeNotesComment, callApiToPostComment, detailData } = this.props;
        let viewComment = <View />

        let imageName = detailData.nickname_icon ? detailData.nickname_icon : '';

        let completeUrl = IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL + imageName;

        let imageNickNameUrl;

        if (!detailData.nickname_icon) {
            imageNickNameUrl = require('../../../assets/profilePhoto.png')
        } else {
            imageNickNameUrl = { uri: completeUrl }
        }

        if (item.index === 0) {
            viewComment = <View style={{ padding: 10 }}>

                <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', marginRight: '5%', marginLeft: '5%' }} >
                    <Image source={imageNickNameUrl} style={[commonStylesStyle.profilePhotoWithoutCircle, { marginRight: '5%' }]} />

                    <View style={styles.viewCommentInput}>
                        <TextInput style={styles.input}
                            placeholder={noteDetailConstants.ENTER_COMMENT}
                            autoCapitalize="none"
                            value={data.commentForPost}
                            onChangeText={(value) => { handleChange('commentForPost', value) }}
                            onSubmitEditing={() => callApiToPostComment()}
                        />
                    </View>
                </View>
                <Text style={styles.userName}>{item.item.user_nick_name}</Text>

            </View>

        } else {
            viewComment = <View style={{ padding: 10 }}>

                <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', marginRight: '5%', marginLeft: '5%' }} >
                    <Image source={imageNickNameUrl} style={[commonStylesStyle.profilePhotoWithoutCircle, { marginRight: '5%' }]} />
                    <View style={styles.viewCommentData}>
                        <Text style={styles.commentTextRow}>{item.item.comment}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.userName}>{item.item.user_nick_name}</Text>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: '5%', marginTop: '2%', width: '75%' }}>
                        <View style={{ flexDirection: 'row', }}>
                            <IconFontawesome name="thumbs-up" style={item.item.likeByCurrentUser > 0 ? styles.thumbSelected : styles.thumbNotSelected} onPress={() => onClickLikeNotesComment(item.item.id)} />
                            <Text style={styles.textLike}>{item.item.total_likes}</Text>
                            {item.item.time_ago ? item.item.time_ago.includes('秒前') ? <Text style={styles.textLike}>{noteDetailConstants.HOW_NICE}</Text> : null : null}
                        </View>
                        <Text style={{}}>{item.item.time_ago}</Text>
                    </View>
                </View>


            </View>
        }

        return (
            viewComment
        )

    }
    renderItemQuestionPhoto = (item, index) => {
      
        if (item.item.text) {
            return (
                <View key={item.index} style={{ padding: 2 }} >
                    <Text style={styles.textBlock}>{item.item.text}</Text>
                    {/* <View style={styles.titleOnPhoto}>
                        
                    </View> */}
                </View>
            )
        } else {

            let imgUrl = IMAGE_VETNOTE_PREFIX_URL + item.item.case_study_post_id + '-thumb_' + item.item.image_name;
            console.log(" item.item in edit vetnote", imgUrl);

            return (
                <View key={item.index} style={{ padding: 2, flexDirection: 'row' }} >
                    <Image source={{ uri: imgUrl }} style={styles.buttonPhoto} />
                    <View style={styles.titleOnPhoto}>
                        <Text style={styles.textTitleOnPhoto}>{CastConstants.PHOTO}{item.item.image_number}</Text>
                    </View>
                    <View style={{ marginLeft: '2%' }}>
                        <Text style={styles.textBlock}>{item.item.image_title}</Text>
                        <Text style={styles.textImageDetail}>{item.item.image_description}</Text>
                    </View>

                </View>
            )
        }



    };


    renderListItem = (item, index) => {

        const { data, arrayRelatedArticles, fetching, errors, callApiToGetData, hideInAppropriatePopup, callApiToMakeNoteInAppropriate, callApiToPostComment, onClickAddNoteToCollection, onClickLikeNotesComment, detailData, voteUpDownNotes, handleChange } = this.props;

        let viewRow = <View />;
        let questionOrAnswerImage = <Image />

        let categoryValue = ''
        if (item.item.category_id === 1) {
            categoryValue = dashboardConstants.CLINICAL;
        } else if (item.item.category_id === 2) {
            categoryValue = dashboardConstants.LIFE;
        } else {
            categoryValue = dashboardConstants.HOTEL_MANAGEMENT;
        }
        {
            let imgNickName = item.item.nickname_icon;
            let nickNameUrl = IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL + imgNickName
            let userImgUrl = { uri: nickNameUrl };


            viewRow = <View>
                <View >

                    <View style={{ right: 5, position: 'absolute' }}>
                        <ModalDropdown
                            dropdownStyle={{ width: 100, height: 100 }}
                            dropdownTextStyle={{ color: 'rgb(51,51,51)' }}
                            dropdownTextHighlightStyle={{ color: 'rgb(60, 132,255)' }}
                            renderRow={(item, index) => this.renderModalRow(item, index)}
                            options={this.arrayPopup}
                            onSelect={(index, value) => this.props.moveToAnswerPopupViews(item.item, index, value)}>

                            <IconFeather name='more-horizontal' size={25} />
                        </ModalDropdown>
                    </View>

                    <View style={{ marginTop: '5%' }}>
                        <Text style={[styles.titleSuggestedNotes, { marginTop: '5%' }]}>{item.item.title}</Text>
                        <Text style={styles.Answer}>{item.item.summary}</Text>
                    </View>

                    {/* {item.item.updated_at ?
                        <Text style={styles.textGrayTitle}>{wordConst.dateDiffInDays_Months_Years(item.item.updated_at)}</Text> : null} */}

                </View>

                <View style={{ width: '100%', marginLeft: '1%', marginTop: "2%" }}>
                    <FlatList
                        scrollEnabled={true}
                        horizontal={true}
                        data={item.item.tags}
                        renderItem={this.renderItemTag}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

                <View style={{ flexDirection: 'row', marginTop: '5%', alignItems: 'center', justifyContent: 'space-evenly', }}>
                    <View style={[styles.categoryViewInOval]}>
                        <Text style={{ padding: '2%' }} >{categoryValue}</Text>
                    </View>
                    <Text style={styles.waitingForVote}>{noteDetailConstants.WAITING_FOR_VOTE}</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <IconAntDesign name="star" style={styles.iconHeader} size={19} />
                        <Text style={styles.iconTextHeader}>{noteDetailConstants.COLLECTION}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <Text> {noteDetailConstants.VIEWS}:</Text>
                        <Text>{item.item.views}</Text>
                    </View>

                </View>



                <View style={{ backgroundColor: 'rgb(246,246,246)', marginTop: "2%", marginRight: "2%", borderRadius: 10, flexDirection: 'row', alignSelf: 'flex-end', marginBottom: '2%' }}>
                    <Image source={userImgUrl} style={commonStylesStyle.profilePhotoWithoutCircle} />
                    <View style={{ padding: '2%' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ paddingBottom: '1%' }}>{item.item.user.nick_name} </Text>
                            <Text>({item.item.cs_score})</Text>

                        </View>
                    </View>

                </View>
                <View style={styles.singleLine} />


            </View>

        }
        return (
            <SafeAreaView >
                {viewRow}
            </SafeAreaView>
        )
    }

    render() {
        const { data, arrayRelatedArticles, arrayComments, fetching, errors, callApiToGetData, hideInAppropriatePopup, callApiToMakeNoteInAppropriate, callApiToPostComment, onClickAddNoteToCollection, onClickLikeNotesComment, detailData, voteUpDownNotes, moveToQuestionPopupViews, handleChange, callApiToMakeNotesInAppropriate, moveToNotesPopupViews, onShare } = this.props;

        return (
            <SafeAreaView >
                <View>
                    <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                        <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => this.props.navigation.goBack()} >
                            <Image source={require('../../../assets/cross1x.png')} style={{ width: 13, height: 13 }} />
                        </TouchableOpacity>
                        <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center' }]}>{postAnswerConstants.DETAIL_TITLE}</Text>
                        <View style={{ flex: 1.5 }}>
                        </View>
                    </View>
                    <View style={styles.singleLine} />
                </View>
                <KeyboardAwareScrollView >
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', padding: 8, alignItems: 'center' }}>
                            <View style={{ flex: 4 }}>
                                <View style={styles.buttonClinicalNotes}>
                                    <Text style={[styles.textButtonTitle]}>{noteDetailConstants.CLINICAL_NOTES}</Text>
                                </View>
                            </View>
                            <View style={{ flex: 5 }} />

                            <View style={{ alignSelf: 'flex-end', right: 5, position: 'absolute', paddingBottom: 5 }}>
                                <ModalDropdown
                                    dropdownStyle={{ width: 140, height: 90 }}
                                    dropdownTextStyle={{ color: 'black' }}
                                    dropdownTextHighlightStyle={{ color: 'blue' }}
                                    options={this.arrayPopupNotes}
                                    renderRow={(item, index) => this.renderModalRow(item, index)}
                                    onSelect={(index, value) => moveToNotesPopupViews(index, value)}>
                                    <IconFeather name='more-horizontal' size={25} />
                                </ModalDropdown>
                            </View>
                        </View>
                        <View style={styles.singleCuttedLine} />
                        <View style={{
                            width: '96%',
                            height: 100,
                            backgroundColor: '#f8fafc',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>

                            <Text style={styles.textTitle}>
                                {detailData.title}
                            </Text>

                        </View>
                        <View style={[styles.singleCuttedLine]} />
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <View style={{ flex: 6, height: 20 }}>

                                <Text style={styles.textAuthorName}>{noteDetailConstants.AUTHOR}: Touncan545</Text>

                            </View>
                            <View style={{ flex: 4 }}>
                                <View style={{ width: 140, height: 20, borderRadius: 3, backgroundColor: 'rgb(233, 255, 251)', alignItems: 'center', flexDirection: 'row', borderRadius: 3, borderColor: 'green', borderWidth: 1 }}>
                                    <IconMaterialCommunity name='history' size={20} color={'green'} />

                                    <Text style={[styles.textTime, { paddingTop: 3 }]}> {timeConstants.showMonthYearAndDate(detailData.updated_at ? detailData.updated_at : new Date())}</Text>
                                </View>
                            </View>

                        </View>

                        <View style={[styles.singleCuttedLine]} />

                        <View style={{ flexDirection: 'row', padding: 8, width: '100%' }}>

                            <TouchableOpacity>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 2 }}>
                                    <IconMaterial name="library-books" style={styles.iconHeader} size={19} />
                                    <Text style={styles.iconTextHeader}>{noteDetailConstants.ARTICLE_SUMMARY}</Text>
                                </View>
                            </TouchableOpacity>


                            <TouchableOpacity onPress={() => onClickAddNoteToCollection()}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 2, paddingLeft: 5 }}>
                                    <IconAntDesign name="star" style={data['isAddedInCollection'] ? styles.iconHeaderSelected : styles.iconHeader} size={19} />

                                    <Text style={data['isAddedInCollection'] ? styles.iconTextHeaderSelected : styles.iconTextHeader}>{noteDetailConstants.COLLECTION}</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => onShare()}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 2, paddingLeft: 5 }}>
                                    <IconFontawesome name="share-square-o" style={styles.iconHeader} size={19} />
                                    <Text style={styles.iconTextHeader}>{noteDetailConstants.SHARE}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>


                        <View style={{ flexDirection: 'row', paddingLeft: 8, width: '100%' }}>
                            <TouchableOpacity>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 2 }}>
                                    <View>

                                        <IconMaterialCommunity name="comment" style={styles.iconHeader} size={19} />
                                        <View style={[styles.viewCircleComment, { marginTop: -12, marginLeft: 6 }]}>
                                            <Text style={styles.textComment}>{arrayComments.length}</Text>
                                        </View>

                                    </View>
                                    <Text style={styles.iconTextHeader}>{noteDetailConstants.COMMENT}</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ isFontSizeClick: !this.state.isFontSizeClick })}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 2, paddingLeft: 5 }}>
                                    <IconFontawesome name="text-height" style={styles.iconHeader} size={19} />
                                    <Text style={styles.iconTextHeader}>{noteDetailConstants.FONT_SIZE}</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                        <View style={{ width: '100%', padding: 15 }}>
                            {/* <Text style={this.state.isFontSizeClick? styles.textDescriptionIncreaseSize: styles.textDescription}>
                                {data['noteTextBlocks']}
                            </Text> */}
                            {/* <HTML html={data['noteTextBlocks']} /> */}
                        </View>

                        <View style={{ marginTop: "2%", marginRight: '5%', marginLeft:'5%' }}>
                            <FlatList
                                //style={{ width: '90%' }}
                                scrollEnabled={false}
                                //horizontal={true}
                                data={detailData.content}
                                renderItem={this.renderItemQuestionPhoto}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>

                        <TouchableOpacity>
                            <Text style={{ marginLeft: 15 }}>{data['category']} > {data['specialty']}</Text>
                        </TouchableOpacity>

                        <View style={{ width: '100%', marginLeft: '5%', height: 50, paddingTop: 10, paddingRight:'5%' }}>
                            <FlatList
                                scrollEnabled={true}
                                horizontal={true}
                                data={detailData.tags}
                                renderItem={this.renderItemTag}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>

                        <View style={[styles.viewVote, { marginTop: 20, marginBottom: 20 }]}>
                            <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginTop: '2%', marginRight: '2%' }}>
                                <IconAntDesign name="infocirlce" size={16} style={{ paddingRight: 5 }} color={'rgb(2, 132, 255)'} />
                                <Text style={styles.textAboutVoting}>{noteDetailConstants.ABOUT_VOTING}</Text>
                            </View>
                            <Text style={[styles.textTitle, { marginTop: '5%', alignSelf: 'center' }]}> {detailData.title} </Text>

                            <Text style={[styles.textTitle, { alignSelf: 'center' }]}>{noteDetailConstants.PLEASE_GIVE_YOUR_VOTE} </Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', justifyContent: 'center', marginTop: '5%', marginBottom: '5%' }}>
                                <IconFontawesome name="arrow-circle-up" style={detailData.upvotes > 0 ? styles.iconVoteSelected : styles.iconVoteDeselected} onPress={() => voteUpDownNotes(1)} />
                                <Text style={styles.textVoteValue}>{detailData.total_votes}</Text>
                                <IconFontawesome name="arrow-circle-down" style={detailData.downvotes > 0 ? styles.iconVoteSelected : styles.iconVoteDeselected} onPress={() => voteUpDownNotes(0)} />
                            </View>
                        </View>

                        <View style={[styles.singleCuttedLine, { marginTop: 10 }]} />

                        <View style={{ width: '100%', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', }}>

                            <Text style={[styles.textTitle, { fontWeight: 'bold', marginLeft: 10, paddingTop: 4 }]}>
                                {noteDetailConstants.COMMENT}
                            </Text>

                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16, paddingRight: 5 }}>
                                    {noteDetailConstants.POST_ORDER}
                                </Text>
                                <Text style={{ fontSize: 16 }}>
                                    {noteDetailConstants.EVALUATION_NUMBER}
                                </Text>
                            </View>
                        </View>

                        <View style={styles.singleCuttedLine} />

                        {/* Comment List  */}
                        <View >
                            <FlatList
                                scrollEnabled={true}
                                data={arrayComments}
                                renderItem={this.renderCommentItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                        <View style={[styles.singleCuttedLine]} />

                        <Text style={{ padding: 10, fontSize: 16 }}>{noteDetailConstants.RELATED_ARTICLE}</Text>

                        <View style={styles.singleCuttedLine} />

                        <View style={{ width: '100%' }}>
                            <FlatList
                                scrollEnabled={true}
                                data={arrayRelatedArticles}
                                renderItem={this.renderListItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    <View style={{ marginBottom: '15%' }} />

                </KeyboardAwareScrollView>

                {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                </View> : null}

                {data['isClickToMakeInAppropriate'] ?

                    <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: '110%', width: "100%", position: 'absolute' }}>

                        <View style={{ height: 290, position: 'absolute', width: screenWidth - 20, marginTop: 120, alignSelf: 'center', borderRadius: 10, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[styles.titleInappropriate, { marginLeft: 25, marginTop: 10 }]}>{postAnswerConstants.REPORT_INAPPROPRIATE_POST}</Text>
                                <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => hideInAppropriatePopup()} >
                                    <IconAntDesign name='close' size={25} color={'gray'} style={{ alignSelf: 'flex-end', marginRight: 5 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.singleLine} />

                            <View >
                                <InputTextWithCharLimit value={data['reasonInAppropriate']} placeholder={postAnswerConstants.PLEASE_DESCRIBE_WHAT_IS_INAPPROPRIATE} charLimit={data['charLimitAnswer']} name={'reasonInAppropriate'} handleChange={handleChange} titleLabel={''} arraySimilarQuestion={[]} errors={errors} />
                            </View>


                            <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginRight: 5, marginTop: 5, marginBottom: 5 }}>

                                <TouchableOpacity style={[styles.buttonRoundCorner, { marginRight: 10, backgroundColor: 'rgb(51,51,51)' }]} onPress={() => hideInAppropriatePopup()}>
                                    <Text style={styles.buttonTextInappropriate}>{postAnswerConstants.CANCEL_INAPPROPRIATE}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.buttonRoundCorner, { backgroundColor: 'rgb(0, 123, 255)' }]} onPress={() => callApiToMakeNotesInAppropriate()}>
                                    <Text style={styles.buttonTextInappropriate}>{postAnswerConstants.REPORT_INAPPROPRIATE}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableOpacity>

                    : <View />
                }
            </SafeAreaView>
        )
    }
}

