import React, { Component } from 'react';
import { View, FlatList, Text, CheckBox, Image, TouchableOpacity, Button, ActivityIndicator } from 'react-native';
import CategoryConstants from '../constants/CategorySelection'
import commonStyles from '../stylesheet/common/commonStyles.style'
import TableRow from '../commonComponent/TableRow';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
// import 'react-native-gesture-handler';


export default class CategorySelection extends Component {
    constructor(props) {
        super(props);
        // this.arrayData = [CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE,CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE, CategoryConstants.SUBCATEGORY_TITLE];
    }

    renderItem = (item) => {
        // let rowText = item.item.title +"-"+ item.item.description
        return (
            <TableRow rowData={item.item} isShowRightView={true} isShowLeftView={false}  arrayData = {this.props.categoryData} />
        )
    }
    render() {
        
        const { onSuccess, categoryData, errors, handleChange, fetching, saveCategories } = this.props
        return (
            <SafeAreaView style={{ flex: 10 }}>
                <View style={{ flex: 10 }} >
                    <View style={[{ alignItems: 'center', height: '6.5%', flexDirection: 'row' }]}>
                        <View style={{ flex: 1.5 }} />
                        <Text style={[styles.textTitle, { alignSelf: 'center', flex: 7, textAlign: 'center' }]}>{CategoryConstants.ANOTHER_BREATH}</Text>
                        <View style={{ flex: 1.5 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('tabNavigator')}>
                                <Text style={{ color: '#d61515', fontSize: 15 }}>{CategoryConstants.CLOSE}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[styles.singleLine]} />

                    <Text style={[styles.textDesc, { marginLeft: '5%', marginRight: '5%', textAlign: 'center', marginTop: '4.5%' }]}>{CategoryConstants.TOP_DESC1_IT_IS_ALMOST}</Text>
                    <Text style={[styles.textDesc, { marginLeft: '5%', marginRight: '5%', textAlign: 'center', marginBottom: '4.5%', marginTop: '1%' }]}>{CategoryConstants.TOP_DESC2_PLEASE_GIVE_ME}</Text>

                    <View style={[styles.singleLine]} />

                     <FlatList
                        style={{ flex: 7 }}
                        data= {categoryData}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                     {fetching ? <View style={{ justifyContent: 'center', position:'absolute', alignSelf:'center',  width:'100%', height:'100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                    </View>: null}

                <View style={{ alignItems: 'center', alignSelf: 'center', height: '7%', }}>
                        <TouchableOpacity onPress={saveCategories}>
                            <Text style={styles.textStartButton}>{CategoryConstants.START}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = {
    textTitle: {
        fontSize: 15,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDesc: {
        fontSize: 10,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textStartButton: {
        fontSize: 18,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },

}