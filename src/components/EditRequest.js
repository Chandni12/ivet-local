import React, { Component } from 'react';
import { View, FlatList, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import editRequestConstants from '../constants/EditRequest'
import { SafeAreaView } from 'react-navigation';
import SegmentedControlTab from "react-native-segmented-control-tab";
const iconBack = require('../../assets/back.png');
import * as timeConstants from '../constants/TimeConstants';

export default class EditRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            isShowCheckMark:false,
        };
    }
    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };

    renderMyEditRequestItem = (item) => {
        return (
            <View>
                <View style={{ flexDirection: 'row', padding: '4%', alignItems: 'center' }}>
                    <View style={{ marginLeft: '2%', marginRight: '2%' }}>
                        <Text style={styles.textDesc1}>{item.item.questions.title}</Text>
                        <Text style={styles.textDesc1}>{item.item.edit_request}</Text>
                        <Text style={[styles.textData, { width: '20%' }]}>{timeConstants.dateDiffIn_dd_mm_yy(item.item.created_at)}</Text>
                    </View>
                </View>
                <View style={styles.singleLine} />
            </View>
        )
    }

    renderOthersEditRequestData = (item) => {
        return (
            <View>
                <View style={{ flexDirection: 'row', padding: '4%', alignItems: 'center' }}>
                    <View style={{ marginLeft: '2%', marginRight: '2%' }}>
                        <Text style={styles.textDesc1}>{item.item.title}</Text>
                        {/* <Text style={styles.textDesc1}>{}</Text> */}
                        <Text style={[styles.textData, { width: '20%' }]}>{timeConstants.dateDiffIn_dd_mm_yy(item.item.created_at)}</Text>
                    </View>
                </View>
                <View style={styles.singleLine} />
            </View>
        )
    }
    render() {
        const {fetching, editRequestData, otherEditRequestData } = this.props;

        return (
            <SafeAreaView style={{ flex: 10 }}>
                <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' , backgroundColor: 'rgb(61, 160, 248)'}]}>
                    <TouchableOpacity style={{ marginLeft: '3%', flex: 1 }} onPress={() => this.props.navigation.goBack()} >
                        <Image source={iconBack} style={{ width: 16, height: 16 , tintColor:'white'}} />
                    </TouchableOpacity>
                    <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center', color:'white' }]}>{editRequestConstants.EDIT_PAGE}</Text>
                    <View style={{ flex: 1 }}>
                    </View>
                </View>
                <View  style={styles.viewComplete}>
                    <SegmentedControlTab
                        values={[editRequestConstants.MY_EDIT_REQUEST, editRequestConstants.OTHER_EDIT_REQUEST,]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                        tabsContainerStyle={{marginLeft:'5%',marginRight:'5%', marginTop:'3%' ,flex:0.1}}
                    />
                    <View style={[styles.singleLine, {marginTop:'3%'}]} />
                    <FlatList
                        style={{ flex: 5 }}
                        data={this.state.selectedIndex === 0 ? editRequestData : otherEditRequestData}
                        renderItem={this.state.selectedIndex === 0 ? this.renderMyEditRequestItem : this.renderOthersEditRequestData}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%' }}>
                    <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                </View> : null}
            </SafeAreaView>
        )
    }
}

const styles = {
    textTitle: {
        fontSize: 15,
       
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDesc1: {
        fontSize: 13,
        color: '#0a2814',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textDesc: {
        fontSize: 12,
        color: '#898989',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    singleLine: {
        width: '90%',
        height: 1,
        backgroundColor: 'lightgray',
        alignSelf:'center'
    },
    viewComplete:{
         flex: 10, 
         backgroundColor:'white', 
         margin:'3%' ,
         shadowColor: 'gray',
         shadowOpacity: 0.6,
         shadowRadius: 2,
         shadowOffset: {
            height: 5,
            width: 5
          },
          elevation: 1,
          borderColor:'lightgray',
          borderWidth:1
    },
    textData: {
        fontSize: 13,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: '2%',
        width: '75%',

    },
    textTitle: {
        fontSize: 15,
        fontFamily: 'NotoSansCJKjp-Medium',
    },
}