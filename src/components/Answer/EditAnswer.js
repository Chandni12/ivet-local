import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Keyboard, TouchableWithoutFeedback, TextInput, ScrollView, Dimensions, FlatList, ActivityIndicator, Alert } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import postAnswerConstants from '../../constants/PostAnswer';
import CastConstants from '../../constants/Cast'
import * as wordConstants from '../../constants/WordConstants';
import IconFeather from 'react-native-vector-icons/Feather';
import { scale, verticalScale } from 'react-native-size-matters';
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconEntypo from 'react-native-vector-icons/Entypo';
import * as Animatable from 'react-native-animatable';
import ViewPopup from '../../commonComponent/ViewPopup';
import ViewAnswerInstruction from '../../commonComponent/ViewAnswerInstruction';

import PostPhotoSection from '../Question/PostPhotoSection';
import InputTextWithCharLimit from '../../commonComponent/InputTextWithCharLimit';


const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class EditAnswer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowSuggestionView: true,

        }
    }
    componentDidMount() {
        console.log(" Edit answer  ====", this.props.navigation.state.params);
    }
    showSuggestionView = () => {
        this.setState({ isShowSuggestionView: false })
    }
    render() {
        const { postDetail, handleChange, fetching, data, postComment, clickToGiveAnswer, arrayOfImages, handleFileChange, onClickAddImage, onClickDeleteImage, errors, postAnswer, arrayCompleteData } = this.props;

        return (
            <SafeAreaView style={{ height: '100%' }}>
                <View>
                <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                        <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => this.props.navigation.goBack()} >
                            <Image source={require('../../../assets/cross1x.png')} style={{ width: 13, height: 13 }} />
                        </TouchableOpacity>
                      
                        <Text style={{ }}>{CastConstants.PLEASE_POST_YOUR_ANSWER}</Text>
                       
                        <View style={{ flex: 1.5 }}>
                            {/* <TouchableOpacity onPress={this.showActionSheet}>
                                     <Text style={{ color: 'rgb(2, 132, 254)', fontSize: 17 }}>{postAnswerConstants.SEND}</Text>
                                     </TouchableOpacity> */}
                        </View>
                    </View>
                    <View style={styles.singleLine} />
                </View>
                
                <KeyboardAwareScrollView>
                    <View style={{}}>
                   
                        
                        <View style={{ marginBottom:'5%' }}>
                            <InputTextWithCharLimit value={data['answerText']} placeholder={postAnswerConstants.PLEASE_ENTER_YOUR_ANSWER} charLimit={data['charLimitAnswer']} name={'answerText'} handleChange={handleChange} titleLabel={postAnswerConstants.PLEASE_ENTER_YOUR_ANSWER} arraySimilarQuestion={[]} errors={errors} />
                        </View>


                        <View style={{ marginBottom: '5%', marginTop:'5%' }}>
                            <PostPhotoSection arrayOfImages={arrayOfImages} onClickAddImage={onClickAddImage} handleFileChange={handleFileChange} handleChange={handleChange} onClickDeleteImage={onClickDeleteImage} data={data} />
                        </View>

                        <View style={{ marginBottom:'5%' }}>
                            <InputTextWithCharLimit value={data['editNote']} placeholder={postAnswerConstants.WRITE_DOWN_MEMO} charLimit={data['charLimitAnswer']} name={'editNote'} handleChange={handleChange} titleLabel={CastConstants.PLEASE_INCLUDE_NOTE} arraySimilarQuestion={[]} errors={errors} />
                        </View>

                        <View style={ [styles.buttonRoundCorner]}>
                            <TouchableOpacity style={{ alignItems: 'center', }} onPress={() => { postAnswer() }}>
                                <Text style={[styles.textButton]}>{postAnswerConstants.POST_ANSWER}</Text>
                            </TouchableOpacity>
                        </View>

                        {this.state.isShowSuggestionView ?
                            <View>
                                <ViewAnswerInstruction showSuggestionView={() => this.showSuggestionView()} />
                            </View>
                            : null}
                        <View style={{height:50, width:'100%'}}/>

                    </View>
                </KeyboardAwareScrollView>
                 {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: '100%'}}>
                        <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                    </View> : null} 
            </SafeAreaView>
        )
    }
}

const styles = {
    buttonRoundCorner: {
        backgroundColor: 'rgb(0, 123, 255)',
        borderRadius: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        height: verticalScale(50),
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
    viewRow: {
        margin: '2%'
    },
    textButton: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingLeft: '3%',
        paddingRight: '3%'
    },
}