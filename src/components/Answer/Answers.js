import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Dimensions, FlatList } from 'react-native';
import commonStyle from '../../stylesheet/common/commonStyles.style';
import answerConstant from '../../constants/Answers';
import { scale, verticalScale, ScaledSheet } from 'react-native-size-matters';
import { SafeAreaView } from 'react-navigation';
const iconCross = require('../../../assets/cross1x.png')

export default class Answers extends Component {
    render() {
        return (
            <SafeAreaView>
                <View style={{ width: '100%', height: '100%' }}>
                    <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                        <TouchableOpacity style={{ marginLeft: '3%', flex: 2 }} onPress={() => this.props.navigation.goBack()} >
                            <Image source={iconCross} style={{ width: 13, height: 13 }} />
                        </TouchableOpacity>
                        <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center' }]}>{answerConstant.ANSWER}</Text>
                        <View style={{ flex: 2 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Text style={{ color: '#d61515', fontSize: 17 }}>{answerConstant.CLOSE}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={commonStyle.singleLine} />
                    <ScrollView style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity>
                                <Text>{answerConstant.OF_THE}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text>{answerConstant.SETTLE}</Text>
                            </TouchableOpacity>
                            <Text style={styles.textTitleBlackIos}>{answerConstant.QUESTION}</Text>
                        </View>

                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = {
    row: {
        height: verticalScale(58),
        flexDirection: 'row',
    },

    textTitle: {
        fontSize: 20,
        color: 'rgb(153,153,153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    textTitleBlackIos: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'black',
        marginLeft: '5%',
        marginBottom: '5%'
    },
    textTitleBlackAndroid: {
        fontFamily: 'NotoSansCJKjp-Medium',
        fontSize: 13,
        color: 'black',
        marginLeft: '5%',
        marginBottom: '5%'
    },
}