import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Keyboard, TouchableWithoutFeedback, TextInput, ScrollView, Dimensions, FlatList } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import postAnswerConstants from '../../constants/PostAnswer';
import { scale, verticalScale } from 'react-native-size-matters';
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker';
import CommentList from '../Dashboard/CommentList';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const screenWidth = Math.round(Dimensions.get('window').width);


export default class PostAnswer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFirstButtonClick: false,
            imageFirst: false,
            imageSecond: false
        },
            this.arrayComments = [{ commentReputationCount: 2, commentText: postAnswerConstants.COMMENT_TEXT }, { commentReputationCount: 5, commentText: postAnswerConstants.COMMENT_TEXT }, { commentReputationCount: 3, commentText: postAnswerConstants.COMMENT_TEXT }, { commentReputationCount: 1, commentText: postAnswerConstants.COMMENT_TEXT }]
    }


    handleChange = () => {
       
    }
    showActionSheet = () => {
        this.ActionSheet.show()
    }
    openImagePicker(isFirstButtonClick) {
       
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            
            if (this.state.isFirstButtonClick) {
                this.setState({ imageFirst: image.sourceURL })
            } else {
                this.setState({ imageSecond: image.sourceURL })
            }
        });
    }
   

    render() {
        return (
            <SafeAreaView >
                <KeyboardAwareScrollView>
                    <View>
                        {/* Header */}
                        <View>
                            <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                                <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => this.props.navigation.goBack()} >
                                    <Image source={require('../../../assets/cross1x.png')} style={{ width: 13, height: 13 }} />
                                </TouchableOpacity>
                                <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center' }]}>{postAnswerConstants.ANSWER_TITLE}</Text>
                                <View style={{ flex: 1.5 }}>
                                    <TouchableOpacity onPress={this.showActionSheet}>
                                        <Text style={{ color: 'rgb(2, 132, 254)', fontSize: 17 }}>{postAnswerConstants.SEND}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.singleLine} />
                        </View>
                        {/* Header */}
                       
                        <Text style={styles.Question}>{postAnswerConstants.QUESTION}</Text>

                        <Text style={styles.Answer}>{postAnswerConstants.ANSWER}</Text>


                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: '5%' }}>

                            <TouchableOpacity style={styles.buttonPhoto} onPress={() => this.openImagePicker(this.setState({ isFirstButtonClick: true }))}>
                            {this.state.imageFirst ? <Image source={{ uri: this.state.imageFirst }} style={styles.buttonPhoto} />
                                    : null}
                                <View style={styles.titleOnPhoto}>
                                    <Text style={styles.textTitleOnPhoto}>写真1</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.buttonPhoto} onPress={() => this.openImagePicker(this.setState({ isFirstButtonClick: false }))}>
                            {this.state.imageSecond ? <Image source={{ uri: this.state.imageSecond }} style={styles.buttonPhoto} /> : null}

                                <View style={styles.titleOnPhoto}>
                                    <Text style={styles.textTitleOnPhoto}>写真2</Text>
                                </View>

                            </TouchableOpacity>
                        </View>

                        <CommentList />

                        <Text style={[styles.Answer, {marginTop:'10%'} ]}>{postAnswerConstants.PLEASE_ENTER_YOUR_ANSWER}</Text>

                        <View style={[styles.singleLineDouble, {marginTop:'5%'} ]}/>

                         {/* Text field with keyboard handling  */}
                         <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                            <View>
                                <TextInput
                                    placeholder={postAnswerConstants.PLEASE_ENTER_YOUR_ANSWER}
                                   // onChangeText={val => handleChange()}
                                    // value={data.description}
                                    name={"description"}
                                    multiline={true}
                                    numberOfLines={5}
                                    style={styles.inputTextAreaStyle}
                                />
                            </View>
                        </TouchableWithoutFeedback>

                        <View>
                            <ActionSheet
                                ref={o => this.ActionSheet = o}
                                title={'あなたはどれが好きですか ？'}
                                options={[postAnswerConstants.DELETE_DRAFT, postAnswerConstants.SAVE_DRAFT, postAnswerConstants.CANCEL]}
                                cancelButtonIndex={2}
                                destructiveButtonIndex={0}
                                onPress={(index) => { /* do something */ }}
                            />
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}

const styles = {
    row: {
        height: verticalScale(58),
        flexDirection: 'row',
    },

    textTitle: {
        fontSize: 20,
        color: 'rgb(153,153,153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
    singleLineDouble: {
        width: '100%',
        height: 5,
        backgroundColor: 'lightgray'
    },
    inputTextAreaStyle: {
        // marginLeft: '3%',
        marginRight: '3%',
        height: 100,
        backgroundColor: 'white',
        marginTop: '3%',
        // shadowColor: 'rgba(0,0,0,0.7)',
        // shadowOffset: {
        //     width: 2,
        //     height: 4
        // },
        // shadowOpacity: 0.5,
        // shadowRadius: 1,
        // borderRadius: 8,
        fontFamily: 'NotoSansCJKjp-Medium',
        fontWeight: '300',
        fontSize: 16,
        color: '#262626',
        paddingTop: 21,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    Question: {
        fontSize: 18,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    Answer:{
        fontSize: 15,
        color: 'gray',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    buttonPhoto: {
        width: screenWidth / 2 - 30,
        height: screenWidth / 2 - 30,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,
        
    },
    titleOnPhoto: {
        height: '12%',
        width: '40%',
        backgroundColor: 'rgb(219, 254, 224)',
        position:'absolute',
        borderColor: 'green',
        borderWidth: 2,
        margin: '5%',
        justifyContent: 'center', 

    },
    textTitleOnPhoto: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
    }

}