import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Linking, BackHandler } from 'react-native';
import firebase, { Notification, NotificationOpen } from "react-native-firebase";
import * as wordConstants from '../constants/WordConstants';

export default class NotificationService extends Component{

    async componentDidMount() {

    
   }
   async backgroundNotificationHandler (){
    firebase.notifications().getInitialNotification()
      .then((notificationOpen: NotificationOpen) => {
        if (notificationOpen) {

           
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action;
          // Get information about the notification that was opened
          const notification: Notification = notificationOpen.notification;  

          if (notification) {

            // console.log("+================= notificationOpen ", notification, "===========");
             // App was opened by a notification
             // Get the action triggered by the notification being opened
            // const action = notificationOpen.action;
             // Get information about the notification that was opened
            
            
             if (notification._data) {

                 if(notification._data.type){
                     if ( notification._data.type.toUpperCase() !== wordConstants.CONST_ADMIN) {
                       return;
                     }
                   }
                  
                  

                 let notificationStatus = notification._data.status;
                 
     
                 if(notificationStatus.toUpperCase() === wordConstants.CONST_APPROVE){
     
                     this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_CONFIRM, userData: {} });
                 }else{
                     this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_FAILED, userData: {} });
                 }
             } else {
                 
             }
         }
     
        }
      });

   
}

    async foregroundNotificationHandler(){
       

        this.notificationListener = firebase.notifications().onNotification(notification => {

           
            
         //  notification.android.setAutoCancel(false)

           // this.getInspectionUserLogs(this.state.user);

            const channelId = new firebase.notifications.Android.Channel(
                'Default',
                'Default',
                firebase.notifications.Android.Importance.High
            );
            firebase.notifications().android.createChannel(channelId);

            let notification_to_be_displayed = new firebase.notifications.Notification({
                data: notification._android._notification._data,
                sound: 'default',
                show_in_foreground: true,
                title: notification.title,
                body: notification.body,
            });

            if (Platform.OS == 'android') {
                notification_to_be_displayed.android
                    .setPriority(firebase.notifications.Android.Priority.High)
                    .android.setChannelId('Default')
                    .android.setVibrate(1000);
            }

            firebase.notifications().displayNotification(notification_to_be_displayed); 

           
            if (notification._data) {
                if(notification._data.type){
                    if ( notification._data.type.toUpperCase() !== wordConstants.CONST_ADMIN) {
                      return;
                    }
                  }
                 
                let notificationStatus = notification._data.status;
               

                if(notificationStatus.toUpperCase() === wordConstants.CONST_APPROVE){

                    this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_CONFIRM, userData: {} });
                }else{
                    this.props.navigation.navigate('LoginProgress', { status: wordConstants.CONST_FAILED, userData: {} });
                }
            } else {
                
            }
        });
    }


    render(){
        return(
            <View>
                <Text> Notification Service </Text>
            </View>
        )
    }
}