import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, FlatList } from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import postAnswerConstants from '../constants/PostAnswer';


export default class ViewAnswerInstruction extends Component{
    constructor(props){
        super(props);

        this.arrayInstructions = [postAnswerConstants.THANK_YOU_TEXT, postAnswerConstants.PLEASE_ANSWER_THE_QUESTTION_AS_SPECIFIC, postAnswerConstants.BY_ATTTACHING_THE_IMAGE, postAnswerConstants.PLEASE_NOTE_FOLLOWING_POINTS, postAnswerConstants.PLEASE_CLARIFY_AS_MUCH_AS_POSSIBLE, postAnswerConstants.IF_YOU_WANT_SUPPLEMENT_OF_QUESTION, postAnswerConstants.PLEASE_NEVER_DO_ANYTHING];
    }

    renderItemComment = (item, index) => {
        return (
            <View style={styles.viewRow}>
                <Text style={{}}>{item.item}</Text>
            </View>
        )
    }

    render(){
        const {showSuggestionView} = this.props
        return(
            <View style={[{ margin: '2%' }, styles.viewSuggestionMessage]}>
                                <TouchableOpacity style={{ alignSelf: 'flex-end', marginRight: '2%', marginTop: '2%' }} onPress={showSuggestionView}>
                                    <IconAntDesign name="close" style={{ fontSize: 22 }} />
                                </TouchableOpacity>
                                <FlatList
                                style={{}}
                                    data={this.arrayInstructions}
                                    renderItem={this.renderItemComment}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>)
    }
}
const styles = {

    userName: {
        fontSize: 12,
        color: 'rgb(134,134,134)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    viewSuggestionMessage: {
        backgroundColor: 'rgb(255,248,220)',
        borderColor: 'gray',
        borderRadius: 5,
    },
    viewRow: {
        margin: '2%'
    },
}