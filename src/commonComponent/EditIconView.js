import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView } from 'react-native';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import profileConstants from '../constants/Profile';

export default class EditIconView extends Component{
    constructor(props){
        super(props);

    }
    onClickAddIcon = (data) => {
        console.log(" add ---")
        if (this.props.title === profileConstants.EDUCATION) {
            this.props.onClickAddOrEditEducationalDetails(true, {});
        } else {
            this.props.onClickAddOrEditWorkExp(true, {});
        }
       
        
    }
    render(){
        const {isEditIcon} = this.props;

        return(
            <View>
            <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                <View style={{ marginLeft: '3%', flex: 1 }} />
               
                <Text style={[styles.textTitle, { alignSelf: 'center', flex: 5, textAlign: 'center' }]}>{this.props.title}</Text>
                
                <View style={{ flex: 1 }}>
                    <View style={[styles.viewIconPencil]}>
                      {isEditIcon? 
                      <IconMaterial name="edit" style={{ fontSize: 20, color:'rgb(12, 118,214)' }} /> 
                      : 
                      <IconMaterial name="add" style={{ fontSize: 20, color:'rgb(12, 118,214)' }}  onPress={() => this.onClickAddIcon()}/>}  
                    </View>
                </View>
            </View>
        </View>
        )
    }
}

const styles = {
    textTitle: {
        fontSize: 14.5,
        color: 'rgba(10, 40, 65, 255)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center',
    },
    viewIconPencil: {
        width: 30,
        height: 30,
        borderRadius: 15,
        borderColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        borderWidth: 0.5,
        shadowColor: 'gray',
        shadowRadius: 1,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 1,
            height: 1
        },
        marginRight: '10%'
    },
}