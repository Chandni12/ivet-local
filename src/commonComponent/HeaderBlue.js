import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView } from 'react-native';
import CastConstants from '../constants/Cast'
const iconBack = require('../../assets/back.png');

export default class HeaderBlue extends Component {
    constructor(props) {
        super(props);

    }

    onClickBack = () => {
       console.log(" Heder blue --", this.props.viewNameComeFrom); 
       if (this.props.viewNameComeFrom === 'Dashboard' || this.props.viewNameComeFrom === 'DetailPage' || this.props.viewNameComeFrom === 'NoteDetailPage') {
          this.props.navigation.goBack();
       } else {
       // this.props.navigation.navigate(this.props.viewNameComeFrom)
       }
      
    }

    render() {

        return (
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                locations={[0, 0.8, 0.6]}
                colors={['rgb(2, 132, 254)', '#3b5998']}
                style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row', backgroundColor: 'rgb(61, 160, 248)', }]}>
                {this.props.title === CastConstants.CAST_TITLE ? 
                
                <TouchableOpacity style={{ marginLeft: '3%', flex: 1 }} onPress={() => this.onClickBack()} >
                    {this.props.viewNameComeFrom ? <Image source={iconBack} style={{ width: 16, height: 16, tintColor: 'white' }} />: null }
                </TouchableOpacity>
                 : 
                 <TouchableOpacity style={{ marginLeft: '3%', flex: 1 }} onPress={() => this.props.navigation.goBack()} >
                        <Image source={iconBack} style={{ width: 16, height: 16, tintColor: 'white' }} />
                    </TouchableOpacity>}
                <Text style={[styles.textTitle, { alignSelf: 'center', flex: 1, textAlign: 'center', color: 'white' }]}>{this.props.title}</Text>
                <View style={{ flex: 1 }} />
            </LinearGradient>
        )
    }
}

const styles = {
    textTitle: {
        fontSize: 15,
        marginTop: 9,
        fontFamily: 'NotoSansCJKjp-Medium',
    },
}