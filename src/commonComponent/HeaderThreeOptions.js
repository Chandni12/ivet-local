import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView } from 'react-native';
import IconFeather from 'react-native-vector-icons/Feather'
import LinearGradient from 'react-native-linear-gradient';
const iconBack = require('../../assets/back.png');

export default class HeaderBlueThreeOptions extends Component{
    constructor(props){
        super(props);

    }
    render() {
      const {showRightButton} = this.props;
        return (
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                locations={[0, 0.8, 0.6]}
                colors={['rgb(2, 132, 254)', 'rgb(36, 187, 251)']}
                style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row', backgroundColor: 'rgb(61, 160, 248)', }]}>
               
                <TouchableOpacity style={{ marginLeft: '3%', flex: 1, marginTop:'2%' }} onPress={() => this.props.navigation.goBack()} >
                <Image source={iconBack} style={{ width: 16, height: 16, tintColor: 'white' }} />
                </TouchableOpacity>

                <Text style={[styles.textTitle, { alignSelf: 'center', flex: 8, textAlign: 'center', color: 'white' }]}>{this.props.title}</Text>
                  
               {showRightButton?   <View style={{flex: 1, marginTop:'2%'}}>
                        <TouchableOpacity onPress={this.props.onRightButtonPress}>
                           <IconFeather name="more-horizontal" style={styles.iconMore}/>
                        </TouchableOpacity>
        </View> :  <View style={{flex: 1}} />}

            </LinearGradient>
        )
    }

    render1(){
        
        return(
            <View>
            <View style={[{  alignSelf: 'center', height: 40, flexDirection: 'row', backgroundColor: 'rgb(61, 160, 248)', alignItems:'center'}]}>
            <TouchableOpacity style={{ marginLeft: '3%', flex: 1, marginTop:'2%' }} onPress={() => this.props.navigation.goBack()} >
                <Image source={iconBack} style={{ width: 16, height: 16, tintColor: 'white' }} />
            </TouchableOpacity>
            <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center', color: 'white',marginTop:'2%' }]}>{this.props.title}</Text>
            <View style={{flex: 1, marginTop:'2%'}}>
                        <TouchableOpacity onPress={this.props.onRightButtonPress}>
                           <IconFeather name="more-horizontal" style={styles.iconMore}/>
                        </TouchableOpacity>
                    </View>
        </View>
        <View style={styles.singleLine} />
        </View>
        )
    }
}

const styles = {
    textTitle: {
        fontSize: 15,
        color: 'rgb(153, 153, 153)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    iconMore:{
        fontSize:20,
        color:'white',
    }
}