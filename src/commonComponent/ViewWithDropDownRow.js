
import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, FlatList, TextInput, Dimensions, ActivityIndicator } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import commonStyles from '../stylesheet/common/commonStyles.style';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather';
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFontawesome from 'react-native-vector-icons/FontAwesome';
import noteDetailConstants from '../constants/NoteDetail';
import CastConstants from '../constants/Cast';
import dashboardConstants from '../constants/Dashboard';
import othersConstants from '../constants/Others';
import * as timeConstants from '../constants/TimeConstants';
import { SafeAreaView } from 'react-navigation';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL } from '../api/endPoints'

import postAnswerConstants from '../constants/PostAnswer';
import HTML from 'react-native-render-html';
import { ScrollView } from 'react-native-gesture-handler';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const iconCross = require('../../assets/cross1x.png');

export default class ViewWithDropDownRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFontSizeClick: false,
        }
        this.arrayPopup = [CastConstants.SHARE, CastConstants.VIEW_EDIT_HISTORY, CastConstants.REPORT_INAPPROPRIATE];
        this.arrayPopupNotes = [CastConstants.VIEW_EDIT_HISTORY, CastConstants.REPORT_INAPPROPRIATE, CastConstants.EDIT_POST];

    }
    renderModalRow = (item, index) => {
        let viewRow = <View />

        if (item.includes(noteDetailConstants.HISTORY) || item === CastConstants.VIEW_EDIT_HISTORY) {

            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconMaterialCommunity name="history" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>
        } else if (item.includes(noteDetailConstants.INAPPROPRIATE) || item === CastConstants.REPORT_INAPPROPRIATE) {

            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="flag" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>

        } else if (item.includes(noteDetailConstants.EDIT) || item === CastConstants.EDIT_POST) {
            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="edit" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>

        } else if (item.includes(noteDetailConstants.SHARE) || item === CastConstants.SHARE) {
            viewRow = <View style={commonStyles.viewModalRowBackgnd}>
                <IconFontawesome name="share" size={16} style={{ paddingLeft: 5, paddingRight: 5 }} />
                <Text style={commonStyles.styleModalRow}>{item}</Text>
            </View>
        }


        return (
            viewRow
        )
    }

    renderItemTag = (place, index) => {
        return (
            <View key={place.index} style={{ flexDirection: 'row' }} >
                {place.item ?
                    <View style={{ backgroundColor: 'rgb(229, 243, 255)', borderRadius: 20, height: 30, justifyContent: 'center', alignItems: 'center', marginLeft: 2.5, marginRight: 2.5, padding: 2 }}>

                        <Text style={commonStyles.listText}>
                            {place.item}
                        </Text>
                    </View> : null}
            </View>
        )
    };
    renderListItem = (item, index) => {

        let imgNickName = item.item.nickname_icon;
            let nickNameUrl = IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL+imgNickName
            let userImgUrl = { uri: nickNameUrl };
        let viewRow = <View />;
        let questionOrAnswerImage = <Image />

        let categoryValue = ''
        if (item.item.category_id === 1) {
            categoryValue = dashboardConstants.CLINICAL;
        } else if (item.item.category_id === 2) {
            categoryValue = dashboardConstants.LIFE;
        } else {
            categoryValue = dashboardConstants.HOTEL_MANAGEMENT;
        }
        {
           

           let viewWaitingForAnswer = <View />

            if (item.item.type === "question") {

                if (item.item.answers_count > 0) {
                    viewWaitingForAnswer = <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.voteValue}>{noteDetailConstants.ANSWERS}:</Text>
                        <Text style={styles.voteValue}>{item.item.answers_count}</Text>

                    </View>
                } else {
                    viewWaitingForAnswer = <View>
                        <Text style={styles.waitingForVote}>{noteDetailConstants.WAITING_FOR_ANSWER}</Text>
                    </View>
                }
            }

            viewRow = <View>
                <View >

                    {/* Commented temporarily
                    <View style={{ right: 5, position: 'absolute' }}>
                        <ModalDropdown
                            dropdownStyle={{ width: 100, height: 100 }}
                            dropdownTextStyle={{ color: 'rgb(51,51,51)' }}
                            dropdownTextHighlightStyle={{ color: 'rgb(60, 132,255)' }}
                            renderRow={(item, index) => this.renderModalRow(item, index)}
                            options={this.arrayPopup}
                            onSelect={(index, value) => this.props.moveToModalPopupViews(item.item, index, value)}>
                            <IconFeather name='more-horizontal' size={25} />
                        </ModalDropdown>
                    </View> */}

                    <View style={{ marginTop: '5%' }}>
                        <Text style={[styles.titleSuggestedNotes, { marginTop: '5%' }]}>{item.item.title}</Text>
                         {/* <Text style={styles.Answer}>{item.item.summary}</Text> */} 
                        <View style={{margin:10}}>
                        <HTML html={item.item.summary}/>
                        </View>
                        
                    </View>

                    {/* {item.item.updated_at ?
                        <Text style={styles.textGrayTitle}>{wordConst.dateDiffInDays_Months_Years(item.item.updated_at)}</Text> : null} */}

                </View>

                <View style={{ width: '100%', marginLeft: '1%', marginTop: "2%" }}>
                    <FlatList
                        scrollEnabled={true}
                        horizontal={true}
                        data={item.item.tags}
                        renderItem={this.renderItemTag}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

                <View style={{ flexDirection: 'row', marginTop: '5%', alignItems: 'center', justifyContent: 'space-evenly', }}>
                    <View style={[styles.categoryViewInOval]}>
                        <Text style={{ padding: '2%' }} >{categoryValue}</Text>
                    </View>


                    {item.item.total_votes === 0 ?
                        <Text style={styles.waitingForVote}>{noteDetailConstants.WAITING_FOR_VOTE}</Text>
                        : <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.voteValue}>{noteDetailConstants.VOTE}:</Text>
                            <Text style={styles.voteValue}>{item.item.total_votes}</Text>
                        </View>
                    }

                    {viewWaitingForAnswer}
    
                    {this.props.data['viewTitle'] === othersConstants.COLLECTION ? null : <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <IconAntDesign name="star" style={styles.iconHeader} size={19} />
                        <Text style={styles.iconTextHeader}>{noteDetailConstants.COLLECTION}</Text>
                    </View>}

                    <View style={{ flexDirection: 'row' }}>
                        <Text > {noteDetailConstants.VIEWS}:</Text>
                        <Text>{item.item.views}</Text>
                    </View>

                </View>

                <View style={{ backgroundColor: 'rgb(246,246,246)', marginTop: "2%", marginRight: "2%", borderRadius: 10, flexDirection: 'row', alignSelf: 'flex-end', marginBottom: '2%' }}>
                <Image source={userImgUrl} style={[commonStyles.profilePhotoWithoutCircle, {marginLeft:'2%'}]} />
                    <View style={{ padding: '2%' }}>
                        
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ paddingBottom: '1%' }}>{item.item.user.nick_name} </Text>
                            <Text>({item.item.cs_score})</Text>

                        </View>
                    </View>

                </View>
                <View style={styles.singleLine} />
            </View>
        }
        return (
            <SafeAreaView >
                {viewRow}
            </SafeAreaView>
        )
    }

    render() {
        const { data, arrayData, fetching, errors } = this.props;

        return (
            <SafeAreaView >
                <View>
                    <View>
                        <View style={[{ alignItems: 'center', alignSelf: 'center', height: 40, flexDirection: 'row' }]}>
                            <TouchableOpacity style={{ marginLeft: '3%', flex: 1.5 }} onPress={() => this.props.navigation.goBack()} >
                                <Image source={iconCross} style={{ width: 13, height: 13 }} />
                            </TouchableOpacity>
                            <Text style={[styles.textTitle, { alignSelf: 'center', flex: 6, textAlign: 'center' }]}>{data['viewTitle']}</Text>
                            <View style={{ flex: 1.5 }}>
                            </View>
                        </View>
                        <View style={styles.singleLine} />
                    </View>
                    <View>
                    <View style={{ marginTop: '5%' }} />
                            {/* <SegmentedControlTab
                                values={[CastConstants.POSTING_QUESTION, CastConstants.SET_NOTES]}
                                selectedIndex={data['selectedSegmentIndex']}
                                onTabPress={onChangeSegmentedControl}
                                //tabTextStyle={styles.textDesc}
                                tabsContainerStyle={{ marginLeft: '5%', marginRight: '5%' }}
                            /> */}
                            <Text style={{marginLeft:'5%', marginBottom:'5%'}}>{CastConstants.THERE_ARE} {data['totalPosts']} {CastConstants.FAVORITE_ARTICLES}</Text>
                        <View style={styles.singleLine}/>
                    </View>

                   

                   

                        <FlatList
                            style={{height:'85%'}}
                            data={arrayData}
                            renderItem={this.renderListItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                   
                    {fetching ? <View style={{ justifyContent: 'center', position: 'absolute', alignSelf: 'center', width: '100%', height: SCREEN_HEIGHT }}>
                        <ActivityIndicator size="large" color='rgb(2, 132, 254)' style={{ alignSelf: 'center' }} />
                    </View> : null}
                </View>
            </SafeAreaView>)
    }
}

export const styles = {
    titleInappropriate: {
        fontSize: 18,
        color: 'rgb(134,134,134)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    buttonTextInappropriate: {
        padding: 8,
        fontSize: 16,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    textButtonTitle: {
        fontSize: 18,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Medium',
        padding: 5
    },
    buttonClinicalNotes: {
        //justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: 'rgb(66, 77,117)',
    },
    textAuthorName: {
        fontSize: 14,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    iconHeader: {
        paddingRight: 3,
        color: 'rgb(134, 144, 166)'
    },

    iconTextHeader: {
        fontSize: 13,
        color: 'rgb(134, 144, 166)'
    },
    iconHeaderSelected: {
        paddingRight: 3,
        color: 'rgb(236, 188, 2)'
    },

    iconTextHeaderSelected: {
        fontSize: 14,
        color: 'rgb(236, 188, 2)'
    },
    textDescription: {
        fontSize: 14,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    textDescriptionIncreaseSize: {
        fontSize: 17,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    viewCircleComment: {
        backgroundColor: '#0e0e0e',
        height: 20,
        width: 20,
        borderRadius: 10,
        alignItems: 'center',
        // justifyContent: 'center',
        position: 'absolute'
    },
    textComment: {
        fontSize: 12,
        paddingTop: 3,
        color: 'white',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    textAboutVoting: {
        fontSize: 13,
        color: 'rgb(2,132,255)',
        fontFamily: 'NotoSansCJKjp-Medium',
    },
    viewVote: {
        backgroundColor: 'rgb(229, 242,255)',
        width: '90%',
        // height: 100,
        //marginBottom:'2%', 
        alignSelf: 'center',
        borderRadius: 10,
    },
    iconVoteDeselected: {
        color: '#d3d3d3',
        fontSize: 40,
        padding: 5
    },
    iconVoteSelected: {
        color: 'rgb(2, 132, 255)',
        fontSize: 40,
    },
    textTitle: {
        fontSize: 16,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        // alignSelf: 'center',
        // textAlign: 'center',
    },

    textVoteValue: {
        fontSize: 20,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        paddingTop: 5,
        paddingLeft: 5,
        paddingRight: 5
    },
    textVoteTitle: {
        fontSize: 20,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',
        alignSelf: 'center'
    },
    textTime: {
        fontSize: 12,
        color: 'rgb(51,51,51)',
        fontFamily: 'NotoSansCJKjp-Medium',

    },
    viewRow: {
        margin: '2%'
    },
    commentText: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'NotoSansCJKjp-Medium',
        marginLeft: 5,
        marginRight: 5,

    },
    waitingForVote: {
        fontSize: 13,
        color: 'red',
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    voteValue: {
        fontSize: 13,
       
        fontFamily: 'NotoSansCJKjp-Regular',
    },

    titleArticle: {
        fontSize: 13,
        color: 'rgb(116,116,116)',
        fontFamily: 'NotoSansCJKjp-Bold',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 10
    },
    categoryViewInOval: {
        backgroundColor: 'rgb(223,230, 237)', borderRadius: 5, height: 30, justifyContent: 'center', alignItems: 'center'
    },
    titleSuggestedNotes: {
        color: '#0e0e0e',
        fontFamily: 'NotoSansCJKjp-Medium',
        margin: 10
    },
    Answer: {
        fontSize: 13,
        color: 'rgb(116,116,116)',
        fontFamily: 'NotoSansCJKjp-Regular',
        margin: 10
    },
    singleLine: {
        width: '100%',
        height: 1,
        backgroundColor: 'lightgray'
    },
    singleCuttedLine: {
        width: '96%',
        height: 1,
        backgroundColor: '#d3d3d3',
    },
    listText: {
        color: '#0385ff',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        fontSize: 14,
        marginRight: 8,
        marginLeft: 8,
    },
    profilePhotoWithoutCircle: {
        width: 30,
        height: 30,
        borderRadius: 15,
    },
    viewCommentInput: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: '90%',
        borderRadius: 20,
        height: 40,
        backgroundColor: 'rgb(229, 243, 255)'
    },
    viewCommentData: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: '90%',
        borderRadius: 20,
        height: 40,
        backgroundColor: 'rgb(223,230, 237)',

    },
    commentTextRow: {
        paddingLeft: 10,
        fontSize: 13,
        fontFamily: 'NotoSansCJKjp-Regular',
    },
    input: {
        paddingLeft: 10
        // margin: 10,
        // height: 60,
        // borderColor: "#d3d3d3",
        // borderWidth: 2,
        // borderRadius: 5
    },
    thumbSelected: {
        fontSize: 15,
        color: 'rgb(2, 135, 255)',

    },
    thumbNotSelected: {
        fontSize: 15,
        color: 'black',
    },
    textLike: {
        fontSize: 15,
        fontFamily: 'NotoSansCJKjp-Regular',
        marginLeft: 5,
        color: 'rgb(51,51,51)'
    },
    userName: {
        color: '#1b659e',
        fontSize: 15,
        fontFamily: 'NotoSansCJKjp-Regular',
        marginLeft: '2%'
    }
}