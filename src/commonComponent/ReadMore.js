import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, ScrollView, Dimensions, FlatList, Platform } from 'react-native';
// import { SafeAreaView } from 'react-navigation';
// import { ScaledSheet, verticalScale, moderateScale } from 'react-native-size-matters';
//import ProgressBarAnimated from 'react-native-progress-bar-animated';
// import * as Progress from 'react-native-progress';
// import IconAntDesign from 'react-native-vector-icons/AntDesign';
// import IconFeather from 'react-native-vector-icons/Feather';
//import HomeConstant from './constants/Home';
import {ReadMore, RegularText } from 'react-native-read-more-text';
//import TableRowHome  from '../src/commonComponent/TableRowHome';


export class DescriptionCard extends React.Component {
    render() {
      let { text } = this.props;
   
      return (
        <View>
          <View>
            <Text >
              Description
            </Text>
          </View>
   
          <View >
            <View>
              <ReadMore
                numberOfLines={3}
                renderTruncatedFooter={this._renderTruncatedFooter}
                renderRevealedFooter={this._renderRevealedFooter}
                onReady={this._handleTextReady}
                >
                {/* <RegularText>
                  {text}
                </RegularText> */}
              </ReadMore>
            </View>
          </View>
        </View>
      );
    }
   
    _renderTruncatedFooter = (handlePress) => {
      return (
        <RegularText style={{color: Colors.tintColor, marginTop: 5}} onPress={handlePress}>
          Read more
        </RegularText>
      );
    }
   
    _renderRevealedFooter = (handlePress) => {
      return (
        <RegularText style={{color: Colors.tintColor, marginTop: 5}} onPress={handlePress}>
          Show less
        </RegularText>
      );
    }
   
    _handleTextReady = () => {
      // ...
    }
  }