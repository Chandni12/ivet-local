import React, { Component } from 'react';
import { View, FlatList, Text, CheckBox, Image, TouchableOpacity, Button } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';


export default class ViewPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
        }
    }

    handleCheckboxClick(data) {
        this.setState({ checked: !this.state.checked });
        this.props.showPopup();
        this.props.onClickFilterOption(data); 
    }

    renderItem = (item, index) => {
       
        let isShowSingleLine = true;
        // if (item.index === 2) {
        //     isShowSingleLine = true;
        // }
        return (
            <View style={{}}>
                <View style={{ marginLeft: '2%', height: 40, justifyContent: 'center', backgroundColor: 'white' }}>
                    <TouchableOpacity onPress={() => this.handleCheckboxClick(item.item)}>
                        <Text style={styles.textData}>{item.item}</Text>
                    </TouchableOpacity>

                    {/* <Ionicons name="ios-checkmark" fontSize={54} /> */}
                </View>
                {isShowSingleLine ? <View style={styles.viewSingleLine} /> : <View />}
            </View>
        )
    }

    render() {
        
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <FlatList

                    data={this.props.arrayData}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
}

const styles = {
    viewSingleLine: {
        width: '100%',
        height: 0.5,
        backgroundColor: 'rgb(153, 153, 153)',
        // marginTop:'2%', 
        // marginBottom: "2%",
    },
    textData: {
        marginLeft: '5%',
        //height:30, 

        color: 'rgba(155, 155, 155, 255)',
        fontSize: 13
    }
}