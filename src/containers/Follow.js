import React from "react";
import { connect } from "react-redux";
import FollowSelection from '../components/Follow'
import { getFollowersData } from "../actions/follow";

class FollowersPageContainer extends React.PureComponent {

  state = {
    fetching:'',
    arrayFollowData: [],
  };

  componentDidMount = () => {
    this.getFollowersData();
  }

  getFollowersData() {
   this.setState({ fetching: true})
      this.props.getFollowersData({ auth_token: this.props.navigation.state.params.auth_token })
      .then((resData) => {
        this.setState({ fetching: false});
        if (resData.success) {
            this.setState({ 
              arrayFollowData: resData.data
             });
        } else {
          
        }
       
      })
    
  }

  render() {
    const { arrayFollowData, fetching } = this.state;
    return (
      <FollowSelection
      fetching= {fetching}
        followerDetails={arrayFollowData.followersUsers}
        followingUsersDetails={arrayFollowData.followingsUsers}
        navigation={this.props.navigation}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
  };
};

// eslint-disable-next-line
const mapDispatchToProps = (dispatch) => {
  return {
    getFollowersData: (auth_token) => dispatch(getFollowersData(auth_token))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FollowersPageContainer);
