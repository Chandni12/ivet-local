import React from "react";
import { AsyncStorage, View, Dimensions, Text, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import IconEntypo from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconMaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconFeather from 'react-native-vector-icons/Feather';
import DashboardContainer from './Dashboard';
import PostMenu from "../components/PostMenu";
import Others from '../components/Others';
import Questions from './Questions';
import PostQuestionOrNotesContainer from './PostQuestionOrNotes';



const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);


class MainContainer extends React.PureComponent {

    state = {
        navigateItem: 1,
        changeTab: false,
        isShowOthersPopup: false,
        isShowPostMenu: false,
        isModalVisible: true,

    }

    setNavigateState(item) {
        if (item === 4) {
            this.setState({
                changeTab: true,
                isShowOthersPopup: true
            })
        } else {
            this.setState({
                navigateItem: item,
                changeTab: false,
                isShowOthersPopup: false
            })
        }
    }

    getItemToShow(item) {
        if (item === 2) {
            return <Questions
            />
        } else if (item === 1) {
            return <DashboardContainer
                navigation={this.props.navigation} userData={this.props.userData}/>
        } else if (item === 3) {
            return <PostQuestionOrNotesContainer
                navigation={this.props.navigation} />
        }
    }

    showHidePopupView = (viewName) => {
        
        if (viewName === 'postMenu') {
            this.onClickPostMenu(true);
            this.setState({
                isShowOthersPopup: false,
            })
        }
        this.setState({
            isShowOthersPopup: !this.state.isShowOthersPopup,
            isModalVisible: !this.state.isModalVisible
        })
    }

    onClickPostMenu = (isOutSideViewClick) => {
        if (isOutSideViewClick) {
            this.setState({
                isShowPostMenu: !this.state.isShowPostMenu,
                isModalVisible: !this.state.isModalVisible
            })
        } else {
            setTimeout(() => {
                this.setState({
                    isShowPostMenu: !this.state.isShowPostMenu,
                    isModalVisible: !this.state.isModalVisible
                })
            }, 250);
        }
    }

    postMenuOptionClick = (option, isViewName, param) => {

        console.log(" option -----", option, 'isView name --', isViewName, 'param --', param);

        if (option === 'Dashboard') {
            this.props.postMenuOptionClick(option);
            this.onClickPostMenu(false);
            return;
        }

        if (option === 'Tags') {
            this.props.navigation.navigate(option, { auth_token: this.props.auth_token, categoryName: param });
            return;
        }

        if (isViewName) {
            this.props.navigation.navigate(option, { auth_token: this.props.auth_token, param: param });
            return;
        }

        // This will call api with arguments provided.
        this.props.postMenuOptionClick(option);
        this.onClickPostMenu(false);
    }


    render() {
        const { navigateItem, changeTab } = this.state;

        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 9.15 }}>
                    {this.getItemToShow(navigateItem)}
                    {this.state.isShowOthersPopup ?

                        <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', height: '200%', width: "100%", position: 'absolute' }} onPress={() => this.showHidePopupView(false)}>

                            <Others
                                showHidePopupView={this.showHidePopupView}
                                navigation={this.props.navigation}
                                auth_token={this.props.auth_token} />

                        </TouchableOpacity>

                        : <View />
                    }

                    {this.state.isShowPostMenu ?

                        <TouchableOpacity style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: '110%', width: "100%", position: 'absolute' }} onPress={() => this.onClickPostMenu(true)}>

                            <PostMenu
                                onClickPostMenu={() => this.onClickPostMenu(false)}
                                navigation={this.props.navigation}
                                postMenuOptionClick={this.postMenuOptionClick} />
                        </TouchableOpacity>
                        // </Modal>
                        : <View />
                    }

                </View>

<View style={{backgroundColor: '#e5e5e5', height:1}}/>
                <View style={{
                   flex:0.85,
                    elevation: 4,
                    backgroundColor: 'white',
                   
                    justifyContent: 'flex-end',
                    flexDirection: 'row'
                }}>

                    <View style={styles.menuIcon}>
                        <TouchableOpacity onPress={() => this.setNavigateState(1)} style={{ alignItems: "center" }}>
                            <IconEntypo name={'home'} size={30} color={navigateItem === 1 && !changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)'} />
                            <Text style={{ color: navigateItem === 1 && !changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)' }}>ホーム</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.menuIcon, , {marginTop:'1.2%'}]}>
                        <TouchableOpacity onPress={() => this.setNavigateState(2)} style={{ alignItems: "center" }}>
                            <IconMaterialCommunityIcon name={'account-question'} size={27} color={navigateItem === 2 && !changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)'} />
                            <Text style={{ color: navigateItem === 2 && !changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)' }}>回答する</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.menuIcon, {marginTop:'1.45%'} ]}>
                        <TouchableOpacity onPress={() => this.setNavigateState(3)} style={{ alignItems: "center" }}>
                            <IconFontAwesome5 name={'edit'} size={23} color={navigateItem === 3 && !changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)'} />
                            <Text style={{ color: navigateItem === 3 && !changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)', marginTop: 4 }}>質問する</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.menuIcon}>
                        <TouchableOpacity onPress={() => this.setNavigateState(4)} style={{ alignItems: "center" }}>
                            <IconFeather name={'more-horizontal'} size={25} color={changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)'} />
                            <Text style={{ color: changeTab ? 'rgb(2, 132, 254)' : 'rgba(73, 73, 73, 255)' }}>その他</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }

}


const mapStateToProps = state => {

    return {
        state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

const styles = {
    menuIcon:{
        flex: 1, alignItems: "center", marginTop:'1%', 
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)