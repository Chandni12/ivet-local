import React from "react";
import { AsyncStorage } from 'react-native';
import { connect } from "react-redux";
import ViewHistory from '../components/Dashboard/ViewHistory'
import { getQuestionList } from "../actions/questions";
import { getHistoryToCheck, getAnswerHistoryToCheck, getVetNoteHistoryToCheck } from '../actions/history';
import * as wordConstants from '../constants/WordConstants';

class HistoryContainer extends React.PureComponent {

    state = {
        data: {
            search: "",
            username: '',
        },
        fetching: false,
        errors: {},
        onSuccess: false,
        auth_token: '',
        userData: '',
        arrayHistoryData: []
    };

    async _getAuthToken() {
        const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);
        const userData = await AsyncStorage.getItem(wordConstants.CONST_USER_DATA);
        if (auth_token == null) {
        } else {
            this.setState({
                auth_token: auth_token,
                userData: userData
            });
        }
    }
    componentDidMount() {
        console.log('navigation param history-----', this.props.navigation.state.params);
        if (this.props.navigation.state.params.data.answer) {
            this.callApiToGetAnswerHistory();
        } else if (this.props.navigation.state.params.dataOfPost.type === 'case_study') {
            this.callApiToGetVetNoteHistory();
        } else {
            this.callApiToGetQuestionHistory();
        }
    }

    callApiToGetAnswerHistory() {

        if (this.props.navigation.state.params) {
            const { data } = this.state;
            data['username'] = '';
            let parameter = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'id': this.props.navigation.state.params.data.id }
            data['username'] = this.props.navigation.state.params.data.user.nick_name;

            this.props.getAnswerHistoryToCheck(parameter).then((response) => {
                this.setState({ fetching: false });

                if (response) {
                    if (response.data.success) {
                        console.log(" Ans res ---", response.data);
                       
                        if (response.data.data.history) {
                            // this.setState({ arrayHistoryData: response.data.result.history });
                            let objectHistory = response.data.data.history;
                            let arrayUseInAccordion = [];
                            let arrayTemp = Object.keys(objectHistory).map(function (key) {
    
                                let dictTemp = { 'title': key, 'content': objectHistory[key] }
                                arrayUseInAccordion.push(dictTemp);
                                return objectHistory[key];
                            });
                            this.setState({
                                arrayHistoryData: arrayUseInAccordion,
                                data: JSON.parse(JSON.stringify(data))
                            });
                        }
                    } else {
                        if (response.data.message) {
                            alert(response.data.message);
                        }
                    }
                } else {
                    alert (wordConstants.CONST_INTERNET_ISSUE_MESSAGE)
                }
            })
        }
    }
    callApiToGetQuestionHistory() {

        if (this.props.navigation.state.params) {
            let parameter = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'id': this.props.navigation.state.params.data.id }
            this.props.getHistoryToCheck(parameter).then((response) => {
                this.setState({ fetching: false });

                if (response) {
                    if (response.data.success) {
                        const { data } = this.state;
                        data['username'] = response.data.result.question.user.nick_name;

                        if (response.data.result.history) {
                            // this.setState({ arrayHistoryData: response.data.result.history });
                            let objectHistory = response.data.result.history;
                            let arrayUseInAccordion = [];
                            let arrayTemp = Object.keys(objectHistory).map(function (key) {
    
                                let dictTemp = { 'title': key, 'content': objectHistory[key] }
                               
                                arrayUseInAccordion.push(dictTemp);
                                return objectHistory[key];
                            });
                            this.setState({
                                arrayHistoryData: arrayUseInAccordion,
                                data: JSON.parse(JSON.stringify(data))
                            });
                        }
                    } else {
                        if (response.data.message) {
                            alert(response.data.message);
                        }
                    }
                } else {
                    alert (wordConstants.CONST_INTERNET_ISSUE_MESSAGE)
                }
               
            })
        }
    }
    callApiToGetVetNoteHistory() {

        if (this.props.navigation.state.params) {
            let parameter = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'id': this.props.navigation.state.params.data.id }
            this.props.getVetNoteHistoryToCheck(parameter).then((response) => {
                this.setState({ fetching: false });

                if (response) {
                    if (response.data.success) {
                        console.log(" callApiToGetVetNoteHistory ---", response.data);
                        const { data } = this.state;
                        data['username'] = '';
                        if (response.data.data.history) {
                            // this.setState({ arrayHistoryData: response.data.result.history });
                            let objectHistory = response.data.data.history;
                            let arrayUseInAccordion = [];
                            let arrayTemp = Object.keys(objectHistory).map(function (key) {
    
                                let dictTemp = { 'title': key, 'content': objectHistory[key] }
                                arrayUseInAccordion.push(dictTemp);
                                return objectHistory[key];
                            });
                            this.setState({
                                arrayHistoryData: arrayUseInAccordion,
                                data: JSON.parse(JSON.stringify(data))
                            });
                        }
                    } else {
                        if (response.data.message) {
                            alert(response.data.message);
                        }
                    }
                } else {
                    alert (wordConstants.CONST_INTERNET_ISSUE_MESSAGE)
                }
            })
        }
    }

    render() {
        const { arrayHistoryData, fetching, data } = this.state
        return (
            <ViewHistory
                arrayHistoryData={arrayHistoryData}
                fetching={fetching}
                navigation={this.props.navigation}
                data={data}
            />
        )
    }

}

const mapStateToProps = state => {
    let arrayTemp = [];
    return (
        arrayTemp = state.history
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        getHistoryToCheck: (parameter) => dispatch(getHistoryToCheck(parameter)),
        getAnswerHistoryToCheck: (parameter) => dispatch(getAnswerHistoryToCheck(parameter)),
        getVetNoteHistoryToCheck: (parameter) => dispatch(getVetNoteHistoryToCheck(parameter))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryContainer)