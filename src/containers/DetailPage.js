import React from "react";
import { connect } from "react-redux";
import { addCommentsInPost, getDashboardQuestionDetail, getDashboardVetNoteDetail, postAnswerInDetail, makeAnswerInappropriateInDetail, makeQuestionInappropriateInDetail, makeQuestionFavoriteInDetail, markAnswerCorrectInDetail, voteUpDownQuestionInDetail, voteUpDownAnswerInDetail } from "../actions/dashboardPostDetail";
import { getAnswerForEditing, saveEditedAnswer } from "../actions/editAnswer";

import DetailPage from "../components/Dashboard/DetailPage";
import Validator from "../../validator";
import CastConstants from '../constants/Cast';
import postAnswerConstants from '../constants/PostAnswer';


const charLimitDetailForAnswer = 2000;
const maxImageUpload = 8;
const constCharLimitImageTitle = 30;
const constCharLimitImageDescription = 120;

class DetailPageContainer extends React.PureComponent {
  state = {
    data: {
      commentText: '',
      isClickForAnswer: false,
      answerText: '',
      charLimitAnswer: '',
      charLimitImgTitle: '30',
      charLimitImgDesc: '120',
      isAnyAnswerAccepted: false,
      reasonInAppropriate: '',
      isClickToMakeInAppropriate: false,
      selectedQuestionOrAnswerData: '',
      selectedQuestionData:{},
      isStarClicked: false,
      ischeckClicked: false,
      isShowSuggestionView: true,
      category: '',

    },
    fetching: false,
    errors: {},
    arrayData: [],
    onSuccess: false,
    auth_token: '',
    detailData: '',
    arrayOfImages: [],
    arrayCompleteData: [],

  };

  componentDidMount() {
    this.setState({ fetching: true });
    this.props.navigation.addListener(
      'willFocus',
      () => {

        this.callApiToGetData();
      }
    );
    
  }

  showSuggestionView = () => {
    const { data } = this.state;
    data['isShowSuggestionView'] = !data['isShowSuggestionView']
    this.setState({ data: JSON.parse(JSON.stringify(data)) });
  }

  _isValid = (field = null) => {
    const validate = Validator.createValidator(
      {
        answerText: ["required"],
      },
      this.state.data,
      field
    );

    const { isValid, errors } = validate;

    if (field) {
      let oldErrorState = this.state.errors;
      if (!errors[field]) {
        delete oldErrorState[field];
      }
      oldErrorState[field] = errors[field];
      this.setState({ errors: oldErrorState ? JSON.parse(JSON.stringify(oldErrorState)) : oldErrorState });
    } else {
      this.setState({ errors: errors ? JSON.parse(JSON.stringify(errors)) : errors });
    }
    return isValid;
  };

  clickToGiveAnswer = () => {
    const { data } = this.state;
    data['isClickForAnswer'] = true;
    this.setState(
      {
        data: JSON.parse(JSON.stringify(data))
      }
    );
  }


  callApiToGetData = () => {

    console.log(" navigation stte==", this.props.navigation.state.params); 
    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

      this.setState({ auth_token: this.props.navigation.state.params.dataOfPost.auth_token });

      let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'slug': this.props.navigation.state.params.dataOfPost.slug }

      if (this.props.navigation.state.params.dataOfPost.type === 'question') {
        this.props.getDashboardQuestionDetail(parameters).then((resData) => {
 console.log("resData---- ", resData);
          this.setState({ fetching: false })

          if (resData.data.success) {
            this.setState({
              detailData: resData.data.data.question,
            })
            let arrayAnswers = resData.data.data.question.answers;
            arrayAnswers.splice(0, 0, this.state.detailData);
            this.setState({ arrayCompleteData: arrayAnswers });

            const { data } = this.state;

            if (this.state.detailData.is_favoured) {
              data['isStarClicked'] = true;
            }

            for (let index = 0; index < arrayAnswers.length; index++) {
              const element = arrayAnswers[index];

              if (element.is_accepted === 'Y') {

                data['isAnyAnswerAccepted'] = true;
                break;
              }
            }
            this.setState({ data: JSON.parse(JSON.stringify(data)) })
          } else {
            if (resData.data.message) {
              alert(resData.data.message);
            }
          }
        });
      } else {

        this.props.getDashboardVetNoteDetail(parameters).then((resData) => {

          this.setState({ fetching: false });

          if (resData.data.success) {
            this.setState({
              detailData: resData.data.data.question
            })
          } else {
            if (resData.data.message) {
              alert(resData.data.message);
            }
          }
        });

      }
    }
  }

  callApiToPostComment = (itemData) => {

    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
      let typeParam = '';
      if (itemData.answer) {
        typeParam = 'answer';
      } else {
        typeParam = this.props.navigation.state.params.dataOfPost.type;
      }
      let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'comment': this.state.data.commentText, 'type': typeParam, 'id': itemData.id }

      this.props.addCommentsInPost(parameters).then((resData) => {

        this.setState({
          fetching: false,
          // arrayData:resData.data.listing.data
        });
        this.callApiToGetData();
        const { data } = this.state;
        data['commentText'] = '';
        this.setState(
          {
            data: JSON.parse(JSON.stringify(data))
          }
        );
      });
    }
  }

  onClickMakeQuestionFavorite = () => {
    const { data } = this.state;
    if (data['isStarClicked']) {
      return;
    }
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
        this.props.makeQuestionFavoriteInDetail(this.props.navigation.state.params.dataOfPost).then((response) => {

          if (response.data.success) {

            data['isStarClicked'] = true
            this.setState({ data: JSON.parse(JSON.stringify(data)) });
            if (response.data.message) {
              alert(response.data.message);
            }
          } else {
            if (response.data.message) {
              alert(response.data.message);
            }
          }
        })
      }
    }
  }

  onClickAnswerCorrect = (dataItem) => {
    const { data } = this.state;
    if (data['ischeckClicked']) {
      return;
    }
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
        this.props.markAnswerCorrectInDetail(this.props.navigation.state.params.dataOfPost, dataItem).then((response) => {

          if (response.data.success) {

            data['ischeckClicked'] = true
            this.setState({ data: JSON.parse(JSON.stringify(data)) });
            if (response.data.message) {
              alert(response.data.message);
            }
            // To refresh the list 
            this.setState({ fetching: true });
            this.callApiToGetData();
          } else {
            if (response.data.message) {
              alert(response.data.message);
            }
          }
        })
      }
    }
  }


  _handleFileChange = (name, file, index) => {
    let newArray = [...this.state.arrayOfImages];
    if (name === 'imagePhoto') {
      let dict = newArray[index];
      dict.document = file;
      this.setState({ arrayOfImages: newArray });
      return
    }
    if (file) {
      const { data } = this.state;
      data[name] = file;
      this.setState({ data });
    }
  };


  _handleChange = (name, value, index) => {

    const { data } = this.state;
    //***** This is for image title and descriptipn */
    if (index >= 0) {
      var array = [...this.state.arrayOfImages];
      let dict = array[index];

      if (name === 'image_title') {
        dict.image_title = value;

        if (value.length > constCharLimitImageTitle)
          return;

        dict.charLimitImgTitle = constCharLimitImageTitle - value.length;

      } else {
        dict.image_description = value;
        if (value.length > constCharLimitImageDescription)
          return;

        dict.charLimitImgDesc = constCharLimitImageDescription - value.length;
      }
      array[index] = dict;

      this.setState({
        data: JSON.parse(JSON.stringify(data)),
        arrayOfImages: array
      }
      );
      // console.log(" after update ####", this.state.arrayOfImages[index]);
      return;
    }
    //***************//

    data[name] = value;
    this.setState(
      {
        data: JSON.parse(JSON.stringify(data))
      },
      () => this._isValid(name)
    );
  };

  onClickAddImage = () => {
    let dictTemp = { 'image_title': '', 'image_description': '', 'document': '', 'id': '', 'charLimitImgTitle': constCharLimitImageTitle, 'charLimitImgDesc': constCharLimitImageDescription };

    if (this.state.arrayOfImages.length === maxImageUpload) {
      alert(wordConstants.CONST_MAX_PHOTO_UPLOAD_WARNING);
      return;
    }
    this.setState({ arrayOfImages: [...this.state.arrayOfImages, dictTemp] });
  }

  onClickDeleteImage = (index) => {
    var array = [...this.state.arrayOfImages]; // make a separate copy of the array
    if (index > -1) {
      array.splice(index, 1);
      this.setState({ arrayOfImages: array });
    }
  }

  moveToQuestionPopupViews = (data1, index, value) => {
    console.log(" moveToQuestionPopupViews data ----", data1, index);
    if (index === '0') {
      this.props.navigation.navigate('ViewHistory', { data: data1, dataOfPost: this.props.navigation.state.params.dataOfPost });
    }
    else if (index === '1') {
      const { data } = this.state;
      data['isClickToMakeInAppropriate'] = true;
      //data['selectedQuestionOrAnswerData'] = data1,
      this.setState({ data:JSON.parse(JSON.stringify(data)) });

    } else if (index === '2') {

        let dataSendToDetail = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'type': data1.post_type, 'slug': data1.slug, 'id': data1.id };

        this.props.navigation.navigate('PostQuestionOrNotes', { dataOfPost: dataSendToDetail, viewNameComeFrom:'DetailPage' })
  
    }
  }

  moveToAnswerPopupViews = (answerData, index, value) => {
    console.log("--###--", answerData);
    if (index === '0') {
      this.props.navigation.navigate('ViewHistory', { data: answerData, dataOfPost: this.props.navigation.state.params.dataOfPost });
    } else if (index === '1') {
      const { data } = this.state;
      data['isClickToMakeInAppropriate'] = true;
      data['selectedQuestionOrAnswerData'] = answerData,
        this.setState({ data: JSON.parse(JSON.stringify(data)) });

    } else if (index === '2') {
      this.props.navigation.navigate('EditAnswer', { data: answerData, dataOfPost: this.props.navigation.state.params.dataOfPost })

    }
  }
  checkQuestionOrAnswerInAppropriate = () => {
    const { data } = this.state;
    if (data['selectedQuestionOrAnswerData'].answer) {
      this.callApiToMakeAnswerInAppropriate();
    } else
     {
      this.callApiToMakeQuestionInAppropriate();
    }
  }

  callApiToMakeAnswerInAppropriate = () => {
    const { data } = this.state;
    
    if (data['reasonInAppropriate'].length < 30) {
      alert(postAnswerConstants.POST_REASON_ALERT_CHARLIMIT);
      return;
    }

    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

      let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'comment': this.state.data.commentText, 'type': this.props.navigation.state.params.dataOfPost.type, 'id': data['selectedQuestionOrAnswerData'].id }

      let dictTemp = { 'post_report_reason': data['reasonInAppropriate'] }
      this.props.makeAnswerInappropriateInDetail(parameters, dictTemp).then((resData) => {

        this.setState({ fetching: false });

        if (resData.data.success) {
          if (resData.data.message) {
            alert(resData.data.message);
          }
        } else {
          if (resData.data.message) {
            alert(resData.data.message);
          }
        }

        data['reasonInAppropriate'] = '';
        data['isClickToMakeInAppropriate'] = false;
        this.setState({
          data: JSON.parse(JSON.stringify(data))
        });
      });

    }
  }
  hideInAppropriatePopup = () => {
    const { data } = this.state;
    data['isClickToMakeInAppropriate'] = false;
    this.setState({
      data: JSON.parse(JSON.stringify(data))
    });
  }

  callApiToMakeQuestionInAppropriate = () => {

    console.log(" 409 "); 
    const { data } = this.state;
    if (data['reasonInAppropriate'].length < 30) {
      alert(postAnswerConstants.POST_REASON_ALERT_CHARLIMIT);
      return;
    }

    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

      let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'comment': this.state.data.commentText, 'type': this.props.navigation.state.params.dataOfPost.type, 'id': this.props.navigation.state.params.dataOfPost.id }

      let dictTemp = { 'post_report_reason': data['reasonInAppropriate'] }
      this.props.makeQuestionInappropriateInDetail(parameters, dictTemp).then((resData) => {

        this.setState({ fetching: false });

        if (resData.data.success) {
          if (response.data.message) {
            alert(response.data.message);
          }
        } else {
          if (resData.data.message) {
            alert(resData.data.message);
          }
        }

        data['reasonInAppropriate'] = '';
        data['isClickToMakeInAppropriate'] = false;
        this.setState({
          data: JSON.parse(JSON.stringify(data))
        });
      });

    }
  }
  postAnswer = () => {
    // let answerTemp = this.state.data.answerText;
    if (this.state.data.answerText.length < 30 && this.state.data.answerText.length > 0) {
      alert(CastConstants.ANSWER_MUST_BE_30_CHAR);
      return;
    }
    const valid = this._isValid();
    if (valid) {
      this.createPostDataToPostAnswer();
    }
  }

  createPostDataToPostAnswer = () => {

    const { answerText } = this.state.data;

    const postData = new FormData()
    postData.append('answer', answerText.trim());
    postData.append('total_blocks', this.state.arrayOfImages.length)

    for (let i = 0; i < this.state.arrayOfImages.length; i++) {
      let dict = this.state.arrayOfImages[i];
      let incr = i + 1;

      postData.append('image_title-' + incr, dict.image_title.trim())
      postData.append('image_description-' + incr, dict.image_description.trim())


      if (dict.document) {
        postData.append('answer_image-' + incr, {
          uri: dict.document,
          type: 'image/jpeg', // or photo.type
          name: 'answer_image-' + incr
        });
      }
    }
    console.log(" post data -----", postData);
    this.postAnswerApiImplementation(postData);

  }
  postAnswerApiImplementation = (postData) => {
    this.setState({ fetching: true });

    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

      this.props.postAnswerInDetail(this.props.navigation.state.params.dataOfPost, postData).then((response) => {
        this.setState({ fetching: false });
        console.log(" post answer resonse ", response);
        if (response.data.success) {
          if (response.data.message) {
            alert(response.data.message);
          }
          const { data } = this.state;
          data['answerText'] = '';
          this.setState({ arrayOfImages: [] });
          this.setState({ data: JSON.parse(JSON.stringify(data)) })
          this.showSuggestionView();
          this.callApiToGetData()
        } else {
          if (response.data.message) {
            alert(response.data.message);
          }
        }
      })
    }
  }

  voteUpDownQuestion = (action) => {
    console.log(" action-----", action);
    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
      // Upvote
      this.props.voteUpDownQuestionInDetail(this.props.navigation.state.params.dataOfPost, action).then((response) => {
        console.log(" res ----", response);
        if (response.data.success) {
          if (response.data.message) {
            alert(response.data.message);
          }
          this.callApiToGetData();
        } else {
          if (response.data.message) {
            alert(response.data.message);
          }
        }
      })
    }
  }

  voteUpDownAnswer = (itemData, action) => {
    console.log(" action-----", action, 'item data ', itemData);
    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
      // Upvote
      let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'id': itemData.id }
      this.props.voteUpDownAnswerInDetail(parameters, action).then((response) => {
        console.log(" res ----", response);
        if (response.data.success) {
          if (response.data.message) {
            alert(response.data.message);
          }
          this.callApiToGetData();
        } else {
          if (response.data.message) {
            alert(response.data.message);
          }
        }
      })
    }
  }

  render() {
    const { detailData, errors, fetching, data, arrayOfImages } = this.state;
    return (
      <DetailPage
        data={data}
        postDetail={detailData}
        errors={errors}
        fetching={fetching}
        handleChange={this._handleChange}
        navigation={this.props.navigation}
        postComment={this.callApiToPostComment}
        postType={this.props.navigation.state.params.dataOfPost.type}
        clickToGiveAnswer={this.clickToGiveAnswer}
        arrayOfImages={arrayOfImages}
        onClickAddImage={this.onClickAddImage}
        handleFileChange={this.handleFileChange}
        onClickDeleteImage={this.onClickDeleteImage}
        handleFileChange={this._handleFileChange}
        postAnswer={this.postAnswer}
        arrayCompleteData={this.state.arrayCompleteData}
        moveToAnswerPopupViews={this.moveToAnswerPopupViews}
        moveToQuestionPopupViews={this.moveToQuestionPopupViews}
        hideInAppropriatePopup={this.hideInAppropriatePopup}
        callApiToMakeQuestionInAppropriate={this.callApiToMakeQuestionInAppropriate}
        callApiToMakeAnswerInAppropriate={this.callApiToMakeAnswerInAppropriate}
        checkQuestionOrAnswerInAppropriate={this.checkQuestionOrAnswerInAppropriate}
        onClickMakeQuestionFavorite={this.onClickMakeQuestionFavorite}
        onClickAnswerCorrect={this.onClickAnswerCorrect}
        showSuggestionView={this.showSuggestionView}
        voteUpDownQuestion={this.voteUpDownQuestion}
        voteUpDownAnswer={this.voteUpDownAnswer}
      />
    );
  }
}

// eslint-disable-next-line
const mapStateToProps = state => {

  return {
    dataQuestionDetail: state.dashboardPostDetail.data
  };
};

// eslint-disable-next-line
const mapDispatchToProps = (dispatch) => {
  return {
    getDashboardQuestionDetail: (parameters) => dispatch(getDashboardQuestionDetail(parameters)),
    getDashboardVetNoteDetail: (parameters) => dispatch(getDashboardVetNoteDetail(parameters)),
    addCommentsInPost: (parameters) => dispatch(addCommentsInPost(parameters)),
    postAnswerInDetail: (parameters, postData) => dispatch(postAnswerInDetail(parameters, postData)),
    saveEditedAnswer: (parameters) => dispatch(saveEditedAnswer(parameters)),
    getAnswerForEditing: (parameters) => dispatch(getAnswerForEditing(parameters)),
    makeAnswerInappropriateInDetail: (parameters, postData) => dispatch(makeAnswerInappropriateInDetail(parameters, postData)),
    makeQuestionInappropriateInDetail: (parameters, postData) => dispatch(makeQuestionInappropriateInDetail(parameters, postData)),
    makeQuestionFavoriteInDetail: (parameters, postData) => dispatch(makeQuestionFavoriteInDetail(parameters, postData)),
    markAnswerCorrectInDetail: (parameter, postData) => dispatch(markAnswerCorrectInDetail(parameter, postData)),
    voteUpDownQuestionInDetail: (parameter, postData) => dispatch(voteUpDownQuestionInDetail(parameter, postData)),
    voteUpDownAnswerInDetail: (parameter, postData) => dispatch(voteUpDownAnswerInDetail(parameter, postData)),

  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailPageContainer);


/*
async _getAuthToken() {

    const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);

    console.log("************", auth_token);

    if (auth_token == null) {

    } else {
      this.setState({
        auth_token: auth_token
      });
      //  return auth_token
    }
  }

*/