import React from "react";
import { AsyncStorage } from 'react-native';
import { connect } from "react-redux";
import Validator from "../../validator";

import PostQuestionOrNotes from '../components/Question/PostQuestionOrNotes'
import { postQuestion, getDataToPostQuestion, postDraftAsQuestion, getDataToEditQuestion, saveEditedQuestion, getAnimalBreedGender } from "../actions/postQuestion";
import { postNotesApiCall, getDataToPostNotes, getDataToEditVetNotes, saveEditedVetNoteApiCall, saveNoteAfterPreviewApiCall } from '../actions/postNotes';
import { getSpecialityBasisOfCategoryInDetail, getTagsBasisOfCategoryInDetail } from '../actions/dashboardPostDetail';
import TabbarTitleConstant from '../constants/TabbarTitle';
import { getInitialObject, getDefaultStyles, convertToHtmlString, convertToObject } from "react-native-cn-richtext-editor";

import * as wordConstants from '../constants/WordConstants';
import CastConstants from '../constants/Cast'
import { IMAGE_NOTE_DETAIL_NICK_NAME_PREFIX_URL, IMAGE_VETNOTE_PREFIX_URL, IMAGE_DASHBOARD_QUESTION_PREFIX_URL, IMAGE_ANSWER_DETAIL_PREFIX_URL } from '../api/endPoints'


const charLimitTitleMax = 40;
const charLimitDetailMax = 2000;
const charLimitTitleMin = 5;
const constCharLimitImageTitle = 30;
const constCharLimitImageDescription = 120;
const maxImageUpload = 8;
const maxNotesSection = 8;
const maxSummarySection = 8;
const constMaxTagCount = 5;
const searchStartCharLength = 1

// save_post,preview_post,draft_post
//$ivet$summary

const initialState = {
    /* etc */
};

class PostQuestionOrNotesContainer extends React.PureComponent {
    constructor(props) {
        super(props);
        this._initState = {
            data: {

                total_old_documents: 0,
                categoryPickerOpen: false,
                editReasonCommentQuestion: '',
                editReasonCommentNotes: '',

                isAnimalSectionOpen: false,
                selectedSegmentIndex: '',
                // Question posting
                isEditQuestion: false,
                title: "",
                detail: "",//[getInitialObject()],
                category: "", // these contain the id of selected data 
                specialty: "", // these contain the id of selected data 
                tag: "", // these contain the id of selected data 

                animalSpecies: "",  // these contain the name of selected data
                animalVariety: "",  // these contain the id of selected data
                animalVarietyShow: '',

                animalGender: '',
                animalAgeYear: '',
                animalAgeMonth: '',
                charLimitTitle: '',
                charLimitDetail: '',
                charLimitImgTitle: '30',
                charLimitImgDesc: '120',

                //// *******////

                categoryOfQuestion: "", // these contain the actual selected value to show 
                categoryOfVetNote: "", // these contain the actual selected value to show 

                //// *******////

                // Notes posting
                isEditVetNote: false,
                titleNotes: "",
                detailNotes: "",
                categoryNotes: "",
                specialtyNotes: "",
                tagNotes: "",
                charLimitTitleNotes: '',
                charLimitDetailNotes: '',
                summary: '',  // For showing summary validation this string create by arraySummarynotes.
                textImageContent: '',  // For showing text image content validation this string create by arraySummarynotes.

            },
            savedVetNoteId: '',
            isDraftClickedNotes: false,
            isPreviewClickedNotes: false,
            isDraftClicked: false,
            is_previewClicked: false,
            fetching: false,
            question_id: '',
            errors: {},
            onSuccess: false,
            auth_token: '',
            arrayAnimalSpecies: [],
            titleArrayAnimalSpecies: [],

            categoryObject: {},
            specialityObject: {},

            titleArrayCategory: [],

            tagObject: {},
            titleArrayTag: [],

            arrayBreed: [],
            titleArrayBreed: [],

            arrayGender: [],
            titleArrayGender: [],

            titleArraySpecialityMaster: [],
            arraySelectedSpecialities: [],

            arraySelectedSpecialitiesNotes: [],

            arraySimilarQuestions: [],

            arrayOfImages: [],
            question_documents: [],

            arraySelectedTags: [],
            arraySelectedTagsOfNotes: [],

            arrayImageTextOfNotes: [],
            arraySummaryNotes: [''],
            // from where we are showing this view by tab bar click or Dashboard edit or popup menu 
            arraySearchTags: [] // When user type show suggestions 

        };
        this.state = this._initState;
        this.isViewShowByTabClick = ''
    }

    resetVetNoteData() {

        const { data } = this.state;
        data['editReasonCommentNotes'] = '';
        data['titleNotes'] = '';
        data['detailNotes'] = "",//[getInitialObject()];
            data['category'] = '';
        data['categoryNotes'] = '';
        data['specialtyNotes'] = '';
        data['tagNotes'] = '';
        data['animalSpecies'] = '';
        data['animalVariety'] = '';
        data['animalVarietyShow'] = '';
        data['animalGender'] = '';
        data['animalAgeYear'] = '';
        data['animalAgeMonth'] = '';
        data['charLimitTitleNotes'] = '';
        data['summary'] = '';
        data['charLimitDetailNotes'] = '';
        data['charLimitImgTitle'] = 30;
        data['charLimitImgDesc'] = 120;
        data['categoryOfVetNote'] = '';
        data['textImageContent'] = '';

        this.setState({
            data: JSON.parse(JSON.stringify(data)),
            isDraftClickedNotes: false,
            isPreviewClickedNotes: false,
            isDraftClicked: false,
            is_previewClicked: false,
            fetching: false,
            errors: {},
            onSuccess: false,
            arraySelectedSpecialitiesNotes: [],
            arrayOfImages: [],
            arraySelectedTagsOfNotes: [],
            arrayImageTextOfNotes: [],
            arraySummaryNotes: [''],
        });
    }

    resetQuestionData() {
        const { data } = this.state;
        data['editReasonCommentQuestion'] = '';
        data['title'] = '';
        data['detail'] = "",//[getInitialObject()];
            data['category'] = '';
        data['specialty'] = '';
        data['tag'] = '';
        data['animalSpecies'] = '';
        data['animalVariety'] = '';
        data['animalVarietyShow'] = '';
        data['animalGender'] = '';
        data['animalAgeYear'] = '';
        data['animalAgeMonth'] = '';
        data['charLimitTitle'] = '';
        data['charLimitDetail'] = '';
        data['charLimitImgTitle'] = 30;
        data['charLimitImgDesc'] = 120;
        data['categoryOfQuestion'] = '';
        data['textImageContent'] = '';


        this.setState({
            data: JSON.parse(JSON.stringify(data)),
            isDraftClickedNotes: false,
            isPreviewClickedNotes: false,
            isDraftClicked: false,
            is_previewClicked: false,
            fetching: false,
            errors: {},
            onSuccess: false,
            arraySelectedSpecialities: [],
            arrayOfImages: [],
            arraySelectedTags: [],

            arrayOfImages: [],
            question_documents: [],
        });
    }

    componentDidMount = () => {

        this.props.navigation.addListener(
            'willFocus',
            () => {
                if (this.props.navigation.state.routeName === TabbarTitleConstant.POST_ANSWER) {
                    this.isViewShowByTabClick = true;
                } else {
                    this.isViewShowByTabClick = false;
                }
            });

        // When a user click on edit opttion in Dashboard row.
        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
            this.getBasicDetailForEditing();
        } else {
            if (this.props.navigation.state.params) {
                // When user select option from postmenu to post a new question/notes 
                if (this.props.navigation.state.params.param) {
                    if (this.props.navigation.state.params.param.isQuestion) {
                        this.onChangeSegmentedControl(0);
                    } else {
                        this.onChangeSegmentedControl(1);
                    }
                } else {
                    // When user comes by clicking on 3rd tab
                    this.onChangeSegmentedControl(0);
                }
            }
        }
    }



    onChangeSegmentedControl = (index) => {

        const { data } = this.state;

        if (index === 0) {
            data['selectedSegmentIndex'] = 0;
            // this.getBasicDetailToPostQuestion();
            this.setState({ data: JSON.parse(JSON.stringify(data)) });
        } else {
            data['selectedSegmentIndex'] = 1;
            // this.getBasicDetailToPostNotes();
            this.setState({ data: JSON.parse(JSON.stringify(data)) });
        }
    }

    getBasicDetailForEditing() {
        // Edit scenario

        const { data } = this.state;

        if (this.props.navigation.state.params.dataOfPost.type === 'question') {

            this.setState({ fetching: true });
            data['isEditQuestion'] = true;
            data['selectedSegmentIndex'] = 0;
            this.setState({ data });


            this.props.getDataToEditQuestion(this.props.navigation.state.params.dataOfPost, this.state.auth_token).then((response) => {
                this.setState({ fetching: false });

                if (response) {
                    if (response.success) {

                        let responseData = response.data;
                        this.setState({ question_id: responseData.question.id })
                        // let valueInHtml = '';

                        // if (name === 'detail' || name === 'detailNotes' ) {
                        //     valueInHtml = convertToObject(responseData.question.description);
                        // }

                        data['title'] = responseData.question.title;
                        data['charLimitTitle'] = charLimitTitleMax - data['title'].length;
                        data['detail'] = responseData.question.description;
                        data['charLimitDetail'] = charLimitDetailMax - data['detail'].length;
                        data['animalSpecies'] = responseData.question.animal_detail && responseData.question.animal_detail.animal_type ? responseData.question.animal_detail.animal_type.title : '';
                        data['animalVarietyShow'] = responseData.question.animal_detail && responseData.question.animal_detail.animal_breed ? responseData.question.animal_detail.animal_breed.title : '';
                        data['animalVariety'] = responseData.question.animal_breed_type_id;
                        data['animalGender'] = responseData.question.gender;
                        { responseData.question.age_year ? data['animalAgeYear'] = responseData.question.age_year.toString() : data['animalAgeYear'] = '' };
                        { responseData.question.age_month ? data['animalAgeMonth'] = responseData.question.age_month.toString() : data['animalAgeMonth'] = '' };
                        data['tag'] = responseData.question.tag_id;
                        data['category'] = responseData.question.category_id;
                        data['specialty'] = responseData.question.speciality_id;

                        if (responseData.question.animal_type_id) {
                            data['isAnimalSectionOpen'] = true;
                        }

                        data['total_old_documents'] = responseData.question.documents.length && responseData.question.documents.length > 0 ? responseData.question.documents.length : 0;

                        for (let index = 0; index < responseData.question.documents.length; index++) {
                            let dictGet = responseData.question.documents[index];

                            let imageName = dictGet.document
                            let completeUrl = IMAGE_DASHBOARD_QUESTION_PREFIX_URL + dictGet.documentable_id + '-thumb_' + imageName;


                            let dictSave = { 'image_title': dictGet.title, 'image_description': dictGet.description, 'document': completeUrl, 'imageId': dictGet.documentable_id }
                            this.setState(prevState => ({ arrayOfImages: [...prevState.arrayOfImages, dictSave] }));

                        }

                        this.setState({
                            data: JSON.parse(JSON.stringify(data)),
                        });
                        this.initializeDataWithInformation(response, true);

                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })
        } else {
            this.setState({ fetching: true });
            this.props.getDataToEditVetNotes(this.props.navigation.state.params.dataOfPost, this.state.auth_token).then((response) => {
                data['isEditVetNote'] = true;
                data['selectedSegmentIndex'] = 1;
                this.setState({ data });
                this.setState({ fetching: false });

                if (response) {
                    if (response.success) {

                        console.log(" in basic detail for editing ---", response.data);
                        let responseData = response.data;
                        this.setState({ savedVetNoteId: responseData.case_study_post.id })
                        data['titleNotes'] = responseData.case_study_post.title;
                        data['charLimitTitleNotes'] = charLimitTitleMax - responseData.case_study_post.title.length;
                        data['tagNotes'] = responseData.case_study_post.tag_id;
                        data['categoryNotes'] = responseData.case_study_post.category_id;
                        data['specialtyNotes'] = responseData.case_study_post.speciality_id;

                        // for (let index = 0; index < responseData.case_study_post.content.length; index++) {
                        //     let dictGet = responseData.question.content[index];

                        //     let dictSave = { 'image_title': dictGet.title, 'image_description': dictGet.description, 'document': dictGet.document, 'imageId': dictGet.documentable_id }
                        //     this.setState(prevState => ({ arrayOfImages: [...prevState.arrayOfImages, dictSave] }));

                        // }

                        this.initializeDataWithInformationVetNote(response, true);


                        this.setState({ data: JSON.parse(JSON.stringify(data)) });
                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })

            data['selectedSegmentIndex'] = 1;

            this.setState({ data });

        }
    }
    getVetNoteDetailForEditing() {
        // Edit scenario

        const { data } = this.state;

        if (this.props.navigation.state.params.dataOfPost.type === 'question') {

            this.setState({ fetching: true });
            data['selectedSegmentIndex'] = 0;
            this.setState({ data });

            this.props.getDataToEditQuestion(this.props.navigation.state.params.dataOfPost, this.state.auth_token).then((response) => {
                this.setState({ fetching: false });

                if (response) {
                    if (response.success) {

                        let responseData = response.data;
                        this.setState({ question_id: responseData.question.id })

                        data['titleNotes'] = responseData.question.title;
                        data['detailNotes'] = responseData.question.description;
                        data['tagNotes'] = responseData.question.tag_id;
                        data['categoryNotes'] = responseData.question.category_id;
                        data['specialtyNotes'] = responseData.question.speciality_id;
                        this.initializeDataWithInformation(response, true);
                        this.setState({ data });
                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })
        } else {
            data['selectedSegmentIndex'] = 1;
            this.setState({ data });

        }
    }
    getBasicDetailToPostNotes = () => {

        if (this.props.navigation.state.params) {

            let auth_tokenTemp = '';
            { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

            this.setState({ fetching: true });
            this.props.getDataToPostNotes(auth_tokenTemp).then((response) => {

                this.setState({ fetching: false });


                if (this.state.data['isEditVetNote']) {
                    if (response) {
                        if (response.success && response.data) {
                            this.initializeDataWithInformationVetNote(response, true);
                        } else { }
                    } else {
                        alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                    }
                } else {
                    if (response) {

                        if (response.success && response.data) {

                            this.initializeDataWithInformationVetNote(response, false);
                        } else { }
                    } else {
                        alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                    }
                }

            })
        }
    }

    initializeDataWithInformationVetNote(response, isEditVetNote) {
        let responseData = response.data;

        const { data } = this.state;
        {
            {
                if (responseData.case_study_post) {
                    if (responseData.case_study_post.content) {
                        responseData.case_study_post.content.map((data, index) => {
                            if (data.text) {
                                let dictTemp = { 'sentence': data.text, 'isText': true, 'id': data.id }
                                this.setState({ arrayImageTextOfNotes: [...this.state.arrayImageTextOfNotes, dictTemp] });

                            } else {

                                let imgUrl = IMAGE_VETNOTE_PREFIX_URL + data.case_study_post_id + '-thumb_' + data.image_name;
                                console.log(" data in edit vetnote", imgUrl);

                                let dictTemp = { 'id': data.id, 'image_title': data.image_title, 'image_description': data.image_description, 'document': imgUrl, 'isText': false, 'charLimitImgTitle': constCharLimitImageTitle - data.image_title.length, 'charLimitImgDesc': constCharLimitImageDescription - data.image_description.length };
                                this.setState({ arrayImageTextOfNotes: [...this.state.arrayImageTextOfNotes, dictTemp] });

                            }
                        })
                    }
                }
                if (responseData.category_master) {

                    let object = responseData.category_master;

                    let arrayTemp = Object.keys(object).map(function (key) {

                        if (isEditVetNote) {
                            if (key.includes(data['categoryNotes'])) {
                                data['categoryOfVetNote'] = object[key];
                            }
                        }
                        return object[key];
                    });
                    this.setState({
                        titleArrayCategory: arrayTemp,
                        categoryObject: responseData.category_master,
                        data
                    });
                }
                else {
                    null
                };
                this.initializeDataTagsVetNote(isEditVetNote, responseData);

                this.initializeDataSpecialitiesVetNote(isEditVetNote, responseData);

                if (responseData.case_study_post) {
                    if (responseData.case_study_post.summary) {
                        responseData.case_study_post.summary.map((data, index) => {

                            this.setState(prevState => ({ arraySummaryNotes: [...prevState.arraySummaryNotes, data] }));
                        })
                        this.removeEmptyFromArray();
                        data['summary'] = this.state.arraySummaryNotes.toString();
                        this.setState({ data })
                    }
                }
            }
        }
    }
    initializeDataTagsVetNote = (isEditVetNote, responseData) => {
        const { data } = this.state;
        if (responseData.tag_master) {
            let object = responseData.tag_master;
            let arrayTempTagsName = [];

            let arrayTemp = Object.keys(object).map(function (key) {
                if (isEditVetNote) {
                    if (data['tagNotes'].includes(key)) {
                        arrayTempTagsName.push(object[key]);
                    }
                }
                return object[key];
            });
            if (isEditVetNote) {
                this.setState({
                    titleArrayTag: arrayTemp,
                    tagObject: responseData.tag_master,
                    arraySelectedTagsOfNotes: arrayTempTagsName,
                    data
                });
            } else {
                this.setState({
                    titleArrayTag: arrayTemp,
                    tagObject: responseData.tag_master,
                });
            }
        } else {
            null
        };
    }

    initializeDataSpecialitiesVetNote = (isEditVetNote, responseData) => {
        const { data } = this.state;
        if (responseData.speciality_master) {
            let objectTemp = responseData.speciality_master;
            let arrayTempSelectedSpeciality = [];
            let arrayTempContainSplitedData = [];

            { isEditVetNote ? arrayTempContainSplitedData = data['specialtyNotes'].split(',') : null }

            let arrayTemp = Object.keys(objectTemp).map(function (key) {
                if (isEditVetNote) {

                    if (data['specialtyNotes'].includes(key)) {
                        if (arrayTempSelectedSpeciality.indexOf(objectTemp[key]) === -1) {
                            arrayTempSelectedSpeciality.push(objectTemp[key]);
                        }
                    }
                }
                return objectTemp[key];
            })
            if (isEditVetNote) {
                this.setState({
                    specialityObject: responseData.speciality_master,
                    arraySelectedSpecialitiesNotes: arrayTempSelectedSpeciality,
                    titleArraySpecialityMaster: arrayTemp,
                    data
                })
            } else {
                this.setState({
                    specialityObject: responseData.speciality_master,
                    titleArraySpecialityMaster: arrayTemp,
                })
            }

        } else {
            null
        };
    }
    onClickToOpenAnimalSection = () => {

        { this.state.titleArrayCategory.length > 0 ? null : this.getBasicDetailToPostQuestion() };
        const { data } = this.state;
        data['isAnimalSectionOpen'] = !data['isAnimalSectionOpen'];
        this.setState({ data: JSON.parse(JSON.stringify(data)) });

    }
    onClickCategoryToLoadData = () => {
        { this.state.titleArrayCategory.length > 0 ? null : this.getBasicDetailToPostQuestion() };
        const { data } = this.state;
        data['categoryPickerOpen'] = true;
        this.setState({ data: JSON.parse(JSON.stringify(data)) });

    }

    removeEmptyFromArray() {
        let array = this.state.arraySummaryNotes;
        var filtered = array.filter(function (el) {
            return el != "";
        });
        this.setState({ arraySummaryNotes: filtered })


    }
    getBasicDetailToPostQuestion = () => {

        let auth_tokenTemp = '';
        {
            this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token
        }
        this.setState({ fetching: true });
        if (this.props.navigation.state.params && auth_tokenTemp) {

            this.props.getDataToPostQuestion(auth_tokenTemp).then((response) => {
                this.setState({ fetching: false });

                if (response) {
                    if (response.success && response.data) {
                        this.initializeDataWithInformation(response, false)
                    } else {
                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })
        }
    }

    initializeDataWithInformation(response, isEditQuestion) {
        const { data } = this.state;

        if (response) {

            if (response.success && response.data) {
                let responseData = response.data;

                if (responseData.category_master) {

                    // Initialize category by default 1st value 

                    let object = responseData.category_master;
                    let arrayTemp = Object.keys(object).map(function (key) {

                        if (isEditQuestion) {
                            if (key.includes(data['category'])) {
                                data['categoryOfQuestion'] = object[key];
                            }
                        }
                        return object[key];
                    });

                    //     let selectedCat = arrayTemp[0];
                    //     data['categoryOfQuestion'] = selectedCat;
                    //     let arrayTemp1 = Object.keys(object).map(function (key) {

                    //         if (object[key] === selectedCat) {
                    //             keyFirst = key;
                    //             console.log(" selected key ====", keyFirst)
                    //         }
                    //         return object[key];
                    //     });

                    //    this._handleChange('category', keyFirst, -1);

                    this.setState({
                        titleArrayCategory: arrayTemp,
                        categoryObject: responseData.category_master,
                        data
                    });
                }
                else {
                    null
                };
                this.initializeDataTagsQuestion(isEditQuestion, responseData);

                this.initializeDataSpecialitiesQuestion(isEditQuestion, responseData);

                if (responseData.animal_type_master) {
                    responseData.animal_type_master.map((data, index) => {


                        this.setState(prevState => ({
                            titleArrayAnimalSpecies: [...prevState.titleArrayAnimalSpecies, data.title]
                        }))
                    })
                    this.setState({
                        arrayAnimalSpecies: responseData.animal_type_master,
                        data
                    })
                } else {
                    null
                };

                this.initializeDataBreedAndGenderQuestion(isEditQuestion, responseData);

                if (responseData.related_questions) {
                    this.setState({ arraySimilarQuestions: responseData.related_questions })

                } else {
                    null
                };

            } else {
            }
        }
    }

    initializeDataSpecialitiesQuestion = (isEditQuestion, responseData) => {

        const { data } = this.state;
        if (responseData.speciality_master) {

            let objectTemp = responseData.speciality_master;
            let arrayTempSelectedSpeciality = [];
            let arrayTemp = Object.keys(objectTemp).map(function (key) {

                if (isEditQuestion) {
                    if (data['specialty'].includes(key)) {
                        arrayTempSelectedSpeciality.push(objectTemp[key]);
                    }
                }
                return objectTemp[key];
            })
            console.log("isEditQuestion ---", isEditQuestion, " speciality data === selected data", arrayTempSelectedSpeciality);
            if (isEditQuestion) {
                this.setState({
                    specialityObject: responseData.speciality_master,
                    arraySelectedSpecialities: arrayTempSelectedSpeciality,
                    titleArraySpecialityMaster: arrayTemp,
                })
            } else {

                this.setState({
                    specialityObject: responseData.speciality_master,
                    titleArraySpecialityMaster: arrayTemp,
                    arraySelectedSpecialities: [],
                    // data
                })
            }


        } else {
            null
        };
    }
    initializeDataTagsQuestion = (isEditQuestion, responseData) => {
        const { data } = this.state;
        if (responseData.tag_master) {
            let object = responseData.tag_master;
            let arrayTempTagsName = [];

            let arrayTemp = Object.keys(object).map(function (key) {
                
                if (isEditQuestion) {
                    if (data['tag'].includes(key)) {
                        arrayTempTagsName.push(object[key]);
                    }
                }
                return object[key];
            });
            if (isEditQuestion) {
                this.setState({
                    titleArrayTag: arrayTemp,
                    tagObject: responseData.tag_master,
                    arraySelectedTags: arrayTempTagsName,
                    arraySearchTags: arrayTemp,
                    data
                });
            } else {
                this.setState({
                    titleArrayTag: arrayTemp,
                    arraySearchTags: arrayTemp,
                    tagObject: responseData.tag_master,
                });
            }

        } else {
            null
        };
    }
    initializeDataBreedAndGenderQuestion = (isEditQuestion, responseData) => {
        const { data } = this.state;

        //*** */ Clear all picker data so that we can initialize it with new data ****//
        this.setState({
            arrayBreed: [],
            titleArrayBreed: [],
            titleArrayGender: [],
            animalGender: []
        })

        if (typeof responseData.breed_master === 'string') {
            const JSONString = responseData.breed_master;
            let object = JSON.parse(JSONString);
            let arrayTemp = Object.keys(object).map(function (k) {

                return object[k];
            });
            this.setState({ arrayBreed: arrayTemp });
            arrayTemp.map((data, index) => {
                this.setState(prevState => ({ titleArrayBreed: [...prevState.titleArrayBreed, data.title] }))
            })
        } else {
            let arrayTempBreed = [];
            for (let index = 0; index < responseData.breed_master.length; index++) {
                const data1 = responseData.breed_master[index];
                arrayTempBreed.push(data1.title);
            }
            this.setState({ titleArrayBreed: arrayTempBreed });
            this.setState({ arrayBreed: responseData.breed_master });
        };

        //******** this is for gender *********//

        if (typeof responseData.animal_gender === 'string') {
            const JSONString = responseData.animal_gender;
            let object = JSON.parse(JSONString);
            let arrayTemp = Object.keys(object).map(function (k) {
                return object[k];
            });
            this.setState({ arrayGender: arrayTemp });
            arrayTemp.map((data, index) => {
                this.setState(prevState => ({ titleArrayGender: [...prevState.titleArrayGender, data.title] }))
            })
        } else {
            if (responseData.gender_type_master) {

                responseData.gender_type_master.map((data1, index) => {

                    if (String(data['animalGender']) === String(data1.id)) {
                        data['animalGender'] = data1.title;
                    }
                    this.setState(prevState => ({ titleArrayGender: [...prevState.titleArrayGender, data1.title] }))
                })
                this.setState({ arrayGender: responseData.gender_type_master });
            } else {
                null
            };
        };
    }

    async _getAuthToken() {
        const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);
        const userData = await AsyncStorage.getItem(wordConstants.CONST_USER_DATA);
        if (auth_token == null) {
        } else {
            this.setState({
                auth_token: auth_token,
                userData: userData
            });
        }
    }

    _isValidPostNotesData = (field = null) => {

        const validate = Validator.createValidator(
            {
                titleNotes: ["required"],
                specialtyNotes: ["required"],
                categoryNotes: ["required"],
                summary: ["required"],
                // textImageContent:["required"]
            },
            this.state.data,
            field
        );

        const { isValid, errors } = validate;

        if (field) {
            let oldErrorState = this.state.errors;
            if (!errors[field]) {
                delete oldErrorState[field];
            }
            oldErrorState[field] = errors[field];
            this.setState({ errors: oldErrorState ? JSON.parse(JSON.stringify(oldErrorState)) : oldErrorState });
        } else {
            this.setState({ errors: errors ? JSON.parse(JSON.stringify(errors)) : errors });
        }
        return isValid;
    };


    _isValid = (field = null) => {

        const validate = Validator.createValidator(
            {
                title: ["required"],
                detail: ["required"],
                specialty: ["required"],
                category: ["required"],
            },
            this.state.data,
            field
        );

        const { isValid, errors } = validate;

        if (field) {
            let oldErrorState = this.state.errors;
            if (!errors[field]) {
                delete oldErrorState[field];
            }
            oldErrorState[field] = errors[field];
            this.setState({ errors: oldErrorState ? JSON.parse(JSON.stringify(oldErrorState)) : oldErrorState });
        } else {
            this.setState({ errors: errors ? JSON.parse(JSON.stringify(errors)) : errors });
        }
        return isValid;
    };

    //**** Add Image or Text of notes. */

    /**
   * Multi Select Handler
   *
   * @param Object selectedOption
   * @param String name
   */
    onClickDeleteMultiSelect = (name, index) => {

        if (name === 'tags') {
            var array = [...this.state.arraySelectedTags]; // make a separate copy of the array


            if (index > -1) {
                array.splice(index, 1);
                this.setState({ arraySelectedTags: array });
            }

        } else if (name === 'tagNotes') {
            var array = [...this.state.arraySelectedTagsOfNotes]; // make a separate copy of the array


            if (index > -1) {
                array.splice(index, 1);
                this.setState({ arraySelectedTagsOfNotes: array });
            }
        } else if (name === 'specialty') {
            var array = [...this.state.arraySelectedSpecialities]; // make a separate copy of the array


            if (index > -1) {
                array.splice(index, 1);
                this.setState({ arraySelectedSpecialities: array });
            }
        } else {
            var array = [...this.state.arraySelectedSpecialitiesNotes]; // make a separate copy of the array


            if (index > -1) {
                array.splice(index, 1);
                this.setState({ arraySelectedSpecialitiesNotes: array });
            }
        }
    }
    removeAllSpeciality = (isQuestion) => {
        if (name === 'specialty') {
            var array = [... this.state.arraySelectedSpecialities];
            array = [];
            //  this.setState(prevState => ({ arraySelectedSpecialities: [...prevState.arraySelectedSpecialities, array] }));

            this.setState({ arraySelectedSpecialities: [] });
        } else {
            var array = [... this.state.arraySelectedSpecialitiesNotes];
            array = [];
            this.setState({ arraySelectedSpecialitiesNotes: array });
        }
        const { data } = this.state;
        this.setState({ data: JSON.parse(JSON.stringify(data)) });
    }

    _handleMultiSelect = (name, selectedOption) => {

        let value = '';
        let isValueAdd = false;

        if (name === 'tags') {

            if (this.state.arraySelectedTags.indexOf(selectedOption) === -1) {
                if (this.state.arraySelectedTags.length === constMaxTagCount) {
                    alert(wordConstants.CONST_MAX_TAGS_WARNING);
                    return;
                }
                value = selectedOption;
                this.setState(prevState => ({ arraySelectedTags: [...prevState.arraySelectedTags, selectedOption] }));

                isValueAdd = true;
            } else {

                isValueAdd = false;
                var array = [...this.state.arraySelectedTags]; // make a separate copy of the array
                var index = array.indexOf(selectedOption)
                array.splice(index, 1);
                this.setState({ arraySelectedTags: array });

            }
            this.showTagSearchData(selectedOption);

        } else if (name === 'tagNotes') {


            var index = this.state.arraySelectedTagsOfNotes.indexOf(selectedOption);

            if (index === -1) {
                if (this.state.arraySelectedTagsOfNotes.length === constMaxTagCount) {
                    alert(wordConstants.CONST_MAX_TAGS_WARNING);
                    //return;
                }
                value = selectedOption;
                isValueAdd = true;
                this.setState(prevState => ({ arraySelectedTagsOfNotes: [...prevState.arraySelectedTagsOfNotes, selectedOption] }));
            } else {
                isValueAdd = false;
                var array = [... this.state.arraySelectedTagsOfNotes];
                array.splice(index, 1);
                this.setState({ arraySelectedTagsOfNotes: array });
            }
        }
        else if (name === 'specialty') {

            var index = this.state.arraySelectedSpecialities.indexOf(selectedOption);

            if (index === -1) {
                isValueAdd = true;
                value = selectedOption;
                this.setState(prevState => ({ arraySelectedSpecialities: [...prevState.arraySelectedSpecialities, selectedOption] }));

            } else {
                isValueAdd = false;
                var array = [... this.state.arraySelectedSpecialities];
                array.splice(index, 1);
                this.setState({ arraySelectedSpecialities: array });
            }
        } else {
            var index = this.state.arraySelectedSpecialitiesNotes.indexOf(selectedOption);

            if (index === -1) {
                isValueAdd = true;
                value = selectedOption;
                this.setState(prevState => ({ arraySelectedSpecialitiesNotes: [...prevState.arraySelectedSpecialitiesNotes, selectedOption] }));

            } else {
                isValueAdd = false;
                var array = [... this.state.arraySelectedSpecialitiesNotes];
                array.splice(index, 1);
                this.setState({ arraySelectedSpecialitiesNotes: array });
            }
        }
        if (isValueAdd) {
            const { data } = this.state;
            data[name] = value;
            if (name === 'specialtyNotes' || name === 'tagNotes') {
                this.setState(
                    {
                        data: JSON.parse(JSON.stringify(data))
                    },
                    () => this._isValidPostNotesData(name)
                );
            } else {
                this.setState({
                    data: JSON.parse(JSON.stringify(data))
                }, () => this._isValid(name));
            }
        }
    };

    onSelectedTagsChange = selectedItems => {
        // this.setState({ arraySelectedTags: selectedItems });
        this.setState(prevState => ({ arraySelectedTags: [...prevState.arraySelectedTags, selectedItems] }))

    };

    ///********** Post Question  */

    _handleChange = (name, value, index) => {

        const { data } = this.state;

        if (name === 'editReasonCommentQuestion') {
            data[name] = value;
            this.setState({
                data: JSON.parse(JSON.stringify(data))
            });
            return;
        }

        //***** This is for image title and descriptipn */
        if (index >= 0) {
            var array = [...this.state.arrayOfImages];
            let dict = array[index];
            if (name === 'image_title') {
                dict.image_title = value;
                if (value.length > constCharLimitImageTitle)
                    return;

                dict.charLimitImgTitle = constCharLimitImageTitle - value.length;

            } else {
                dict.image_description = value;
                if (value.length > constCharLimitImageDescription)
                    return;

                dict.charLimitImgDesc = constCharLimitImageDescription - value.length;
            }
            array[index] = dict;
            this.setState({ arrayOfImages: array });

            return;
        }
        //***************//

        data[name] = value;
        if (name === 'title') {
            if (value.length > charLimitTitleMax) {
                return;
            }
            data['charLimitTitle'] = charLimitTitleMax - value.length;
        } else {
            if (name === 'detail') {
                if (value.length > charLimitDetailMax) {
                    return;
                }
                data['charLimitDetail'] = charLimitDetailMax - value.length;
            }
        }
        this.setState(
            {
                data: JSON.parse(JSON.stringify(data))
            },
            () => this._isValid(name)
        );

        //   For speciality Data get 
        if (name === 'category') {

            data['specialty'] = '';
            data['tag'] = '';
            this.setState({
                specialityObject: {},
                // tagObject: {},
                // titleArrayTag: [],
                // arraySelectedTags: [],
                titleArraySpecialityMaster: [],
                arraySelectedSpecialities: [],
               // arraySearchTags:[],

                data: JSON.parse(JSON.stringify(data))
            })

            this.specialityAndTagApiCalling(value);
        } else {
            if (name === 'animalSpecies') {

                data['animalVariety'] = ''
                data['animalVarietyShow'] = ''
                data['animalGender'] = ''
                this.setState({ data: JSON.parse(JSON.stringify(data)) })
                this.callApiToGetAnimalBreedAndGender(value);
            }
        }
    };

    specialityAndTagApiCalling = (catId) => {
        this.callApiToGetSpecialityBasisOfCategory(catId);
      // Moto wants all tags   this.callApiToGetTagsBasisOfCategory(catId);
    }

    _handleFileChange = (name, file, index) => {
        let newArray = [...this.state.arrayOfImages];
        if (name === 'imagePhoto') {
            let dict = newArray[index];
            dict.document = file;
            this.setState({ arrayOfImages: newArray });
            return
        }
        if (file) {
            const { data } = this.state;
            data[name] = file;
            this.setState({ data });
        }
    };

    // Chandni
    onClickAddImage = () => {
        let dictTemp = { 'image_title': '', 'image_description': '', 'document': '', 'id': '', 'charLimitImgTitle': constCharLimitImageTitle, 'charLimitImgDesc': constCharLimitImageDescription, };

        if (this.state.arrayOfImages.length === maxImageUpload) {
            alert(wordConstants.CONST_MAX_PHOTO_UPLOAD_WARNING);
            return;
        }
        this.setState({ arrayOfImages: [...this.state.arrayOfImages, dictTemp] });
    }

    onClickDeleteImage = (index) => {

        var array = [...this.state.arrayOfImages]; // make a separate copy of the array

        if (index > -1) {
            array.splice(index, 1);
            this.setState({ arrayOfImages: array });
        }
    }

    onClickPostQuestion = () => {

        const valid = this._isValid();
        if (valid) {

            if (this.state.isDraftClicked) {
                this.createPostDataToPostQuestion(true, true);
            } else {
                this.createPostDataToPostQuestion(false, false);
            }
        }
    }

    onClickPreview = () => {
        const valid = this._isValid();
        if (valid) {
            this.setState({ is_previewClicked: true })
        }
    }

    onClickSaveDraft = () => {

        const valid = this._isValid();
        if (valid) {
            this.setState({ isDraftClicked: true });
            this.createPostDataToPostQuestion(true, false);
        }
    }

    createPostDataToPostQuestion = (isDraft, isPostQuestionAfterDraft) => {

        const { editReasonCommentQuestion, title, animalSpecies, animalVariety, animalGender, animalAgeYear, animalAgeMonth, detail, category, specialty, isEditQuestion } = this.state.data;

        let strSelectedSpeciality = this.state.arraySelectedSpecialities.toString();
        let objectTemp = this.state.specialityObject;
        let arrayTempSelectedSpecialityId = [];
        let arrayTemp = Object.keys(objectTemp).map(function (key) {

            if (strSelectedSpeciality.includes(objectTemp[key])) {
                arrayTempSelectedSpecialityId.push(key);
            }
            return objectTemp[key];
        })

        let strSelectedTags = this.state.arraySelectedTags.toString();

        // *****  Animal species 
        let selectedIdOfAnimal = ''
        for (let index = 0; index < this.state.arrayAnimalSpecies.length; index++) {
            if (this.state.arrayAnimalSpecies[index].title === animalSpecies) {
                selectedIdOfAnimal = this.state.arrayAnimalSpecies[index].id;
                break;
            }
        }


        // ********Animal gender 
        let selectedIdOfAnimalGender = ''
        for (let index = 0; index < this.state.arrayGender.length; index++) {
            if (this.state.arrayGender[index].title === animalGender) {
                selectedIdOfAnimalGender = this.state.arrayGender[index].id;
                break;
            }
        }


        const postData = new FormData()
        postData.append('title', title.trim());
        postData.append('animal_type', selectedIdOfAnimal);
        postData.append('animal_breed_type', animalVariety);
        postData.append('animal_gender', selectedIdOfAnimalGender);
        postData.append('animal_age_year', animalAgeYear);
        postData.append('animal_age_month', animalAgeMonth);
        postData.append('description', detail.trim());
        postData.append('category', category);
        postData.append('specialities', arrayTempSelectedSpecialityId.toString());
        postData.append('tags', strSelectedTags);
        postData.append('total_blocks', this.state.arrayOfImages.length);

        for (let index = 0; index < this.state.arrayOfImages.length; index++) {
            let dict = this.state.arrayOfImages[index];
            console.log(" img dict ----", dict);
            let incr = index + 1;

            { dict.imageId ? postData.append('image_id-' + incr, dict.imageId) : null }

            postData.append('image_title-' + incr, dict.image_title.trim())
            postData.append('image_description-' + incr, dict.image_description.trim())

            if (dict.document) {
                postData.append('documents-' + incr, {
                    uri: dict.document,
                    type: 'image/jpeg', // or photo.type
                    name: 'documents-' + incr
                });
            }
        }



        console.log(" post ata isEditQ --- ", this.state.question_id, isEditQuestion);

        if (isEditQuestion && this.state.question_id) {

            postData.append('question_id', this.state.question_id);
            postData.append('is_preview', 0);

            postData.append('question_comment', editReasonCommentQuestion);

            console.log(" post data --- ", postData);

            this.postEditedQuestionWithData(postData);
            return;
        }
        let isPreviewTemp = 0
        { isDraft && this.state.question_id === '' ? null : postData.append('question_id', this.state.question_id) };

        if (isDraft) {
            isPreviewTemp = 1;
            postData.append('is_preview', 1)
        } else {
            isPreviewTemp = 0
            postData.append('is_preview', 0)
        }

        if (isPostQuestionAfterDraft && isDraft) {
            this.postDraftAsQuestionCall(postData)
        } else {
            this.postQuestionWithData(postData, isPreviewTemp);
        }
    }

    postQuestionWithData = (postData, isPreviewTemp) => {

        let auth_tokenTemp = '';
        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

        const valid = this._isValid();

        if (valid) {

            if (auth_tokenTemp && this.props.navigation.state.params) {
                this.setState({ fetching: true });
                console.log(" post Q parameter ---", postData);

                this.props.postQuestion(postData, auth_tokenTemp).then((response) => {
                    this.setState({ fetching: false });
                    if (response) {
                        if (response.success) {
                            console.log(" postRes ---", response);
                            if (response.message) {
                                alert(response.message)
                            }
                            if (response.question_id) {
                                if (response.question_id > 0) {
                                    this.setState({ question_id: response.question_id })
                                }
                            }
                            if (isPreviewTemp === 0) {
                                this.resetQuestionData();
                            }
                        }
                    } else {
                        alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                    }
                })
            }
        }
    }


    postDraftAsQuestionCall = () => {

        const postData = new FormData();

        { this.state.question_id === '' ? '' : postData.append('question_id', this.state.question_id) };

        let auth_tokenTemp = '';
        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

        const valid = this._isValid();
        if (valid) {

            if (auth_tokenTemp && this.props.navigation.state.params) {
                this.setState({ fetching: true });
                this.props.postDraftAsQuestion(postData, auth_tokenTemp).then((response) => {
                    this.setState({ fetching: false });
                    if (response) {
                        if (response.success) {
                            if (response.message) {
                                alert(response.message)
                            }
                            if (response.question_id) {
                                if (response.question_id > 0) {
                                    this.setState({ question_id: response.question_id })
                                }
                            }
                            this.resetQuestionData();
                        }
                    } else {
                        alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                    }
                })
            }
        }
    }

    // Edited question save 

    postEditedQuestionWithData = (postData) => {

        let auth_tokenTemp = '';
        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

        const valid = this._isValid();
        if (valid) {

            if (this.props.navigation.state.params) {

                this.setState({ fetching: true });
                this.props.saveEditedQuestion(postData, auth_tokenTemp).then((response) => {
                    this.setState({ fetching: false });

                    if (response) {
                        if (response.success) {
                            if (response.message) {
                                alert(response.message)
                            }
                            this.resetQuestionData();
                        }
                    } else {
                        alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                    }
                })
            }
        }
    }

    ///*****///********** Post Notes  ///*****///***********/

    _handleChangeNotes = (name, value, index) => {

        const { data } = this.state;

        data[name] = value;
        //***** This is for image title and descriptipn *////
        if (index >= 0 && name != 'summary') {
            var array = [...this.state.arrayImageTextOfNotes];
            let dict = array[index];

            if (name === 'sentence') {
                dict.sentence = value;
            } else
                if (name === 'image_title') {
                    dict.image_title = value;

                    if (value.length > constCharLimitImageTitle)
                        return;

                    dict.charLimitImgTitle = constCharLimitImageTitle - value.length;

                }
                else {
                    dict.image_description = value;
                    if (value.length > constCharLimitImageDescription)
                        return;

                    dict.charLimitImgDesc = constCharLimitImageDescription - value.length;

                }
            array[index] = dict;
            this.setState({ arrayImageTextOfNotes: array });
            data['textImageContent'] = dict;
            this.setState(
                {
                    data: JSON.parse(JSON.stringify(data))
                },
                () => this._isValidPostNotesData(name)
            );
            return;
        }

        //***************//

        if (name === 'summary') {
            if (index >= 0) {
                var array = [...this.state.arraySummaryNotes];

                array[index] = value;
                this.setState({ arraySummaryNotes: array });

                data['summary'] = this.state.arraySummaryNotes.toString();
            }
        }
        if (name === 'titleNotes') {
            if (value.length > charLimitTitleMax) {
                return;
            }
            data['charLimitTitleNotes'] = charLimitTitleMax - value.length;
        } else {
            if (name === 'detailNotes') {
                if (value.length > charLimitDetailMax) {
                    return;
                }
                data['charLimitDetailNotes'] = charLimitDetailMax - value.length;
            }
        }

        this.setState(
            {
                data: JSON.parse(JSON.stringify(data))
            },
            () => this._isValidPostNotesData(name)
        );

        //   For speciality Data get 
        if (name === 'categoryNotes') {

            data['specialtyNotes'] = '';
            data['tagNotes'] = '';
            this.setState({
                specialityObject: {},
                tagObject: {},
                titleArrayTag: [],
                arraySelectedTagsOfNotes: [],
                titleArraySpecialityMaster: [],
                arraySelectedSpecialitiesNotes: [],
                data: JSON.parse(JSON.stringify(data))
            })

            this.specialityAndTagApiCalling(value);
        }
    };

    _handleFileChangeNotes = (name, file, index) => {

        let newArray = [...this.state.arrayImageTextOfNotes];
        if (name === 'imagePhoto') {
            let dict = newArray[index];
            dict.document = file;
            this.setState({ arrayImageTextOfNotes: newArray });
            return
        }
        if (file) {
            const { data } = this.state;
            data[name] = file;
            this.setState({ data });
        }
    };

    onClickAddImageOrTextOfNotes = (isText) => {
        if (isText) {

            let dictTemp = { 'sentence': '', 'isText': true };
            if (this.state.arrayImageTextOfNotes.length === maxNotesSection) {
                alert(wordConstants.CONST_MAX_PHOTO_UPLOAD_WARNING);
                return;
            }
            this.setState({ arrayImageTextOfNotes: [...this.state.arrayImageTextOfNotes, dictTemp] });

        }
        else {
            let dictTemp = { 'image_title': '', 'image_description': '', 'document': '', 'isText': false, 'id': '', 'charLimitImgTitle': constCharLimitImageTitle, 'charLimitImgDesc': constCharLimitImageDescription };

            if (this.state.arrayImageTextOfNotes.length === maxImageUpload) {
                alert(wordConstants.CONST_MAX_PHOTO_UPLOAD_WARNING);
                return;
            }
            this.setState({ arrayImageTextOfNotes: [...this.state.arrayImageTextOfNotes, dictTemp] });

        }
    }

    // ************* Summary ************ //

    onClickDeleteImageOrTextOfNotes = (index) => {

        var array = [...this.state.arrayImageTextOfNotes]; // make a separate copy of the array


        if (index > -1) {
            array.splice(index, 1);
            this.setState({ arrayImageTextOfNotes: array });
        }
    }

    onClickAddSummaryNotes = () => {

        let dictTemp = '';
        if (this.state.arraySummaryNotes.length === maxSummarySection) {
            alert(wordConstants.CONST_MAX_SUMMARY_ADD_WARNING);
            return;
        }
        this.setState({ arraySummaryNotes: [...this.state.arraySummaryNotes, dictTemp] });


    }

    onClickDeleteSummaryNotes = (index) => {

        var array = [...this.state.arraySummaryNotes]; // make a separate copy of the array


        if (index > -1) {
            array.splice(index, 1);
            this.setState({ arraySummaryNotes: array });
        }
    }
    onClickPostNotesAsDraft = () => {

        this.createPostDataToPostNotes(true, false, false);

        if (this.state.arrayImageTextOfNotes.length === 0) {
            alert(wordConstants.CONST_EMPTY_CONTENT_NOTES);
        }
        const valid = this._isValidPostNotesData();
        if (valid) {
            this.setState({ isDraftClickedNotes: true });
            this.createPostDataToPostNotes(true, false, false);
        }
    }
    onClickPostNotesAsPreview = () => {

        if (this.state.arrayImageTextOfNotes.length === 0) {
            alert(wordConstants.CONST_EMPTY_CONTENT_NOTES);
        }
        const valid = this._isValidPostNotesData();
        if (valid) {
            this.createPostDataToPostNotes(false, true, false);
        }
    }

    onClickPostNotes = () => {

        if (this.state.arrayImageTextOfNotes.length === 0) {
            alert(wordConstants.CONST_EMPTY_CONTENT_NOTES);
        }
        const valid = this._isValidPostNotesData();
        if (valid) {
            this.createPostDataToPostNotes(false, false, true);
        }
    }

    createPostDataToPostNotes = (isDraft, isPreview, isPostNotes) => {

        const { titleNotes, categoryNotes, specialtyNotes, isEditVetNote, editReasonCommentNotes } = this.state.data;

        let strSelectedTagsNotes = this.state.arraySelectedTagsOfNotes.join(',');
        let strSelectedSpeciality = this.state.arraySelectedSpecialitiesNotes.toString(',');


        let objectTemp = this.state.specialityObject;
        let arrayTempSelectedSpecialityId = [];
        let arrayTemp = Object.keys(objectTemp).map(function (key) {

            if (strSelectedSpeciality.includes(objectTemp[key])) {
                arrayTempSelectedSpecialityId.push(key);
            }
            return objectTemp[key];
        })

        const postData = new FormData()
        postData.append('post_title', titleNotes.trim());
        postData.append('category', categoryNotes);
        postData.append('specialities', arrayTempSelectedSpecialityId.toString());
        postData.append('tags', strSelectedTagsNotes);
        { this.state.arraySummaryNotes.length > 0 ? postData.append('summary', this.state.arraySummaryNotes.join('$ivet$summary')) : null }
        postData.append('total_blocks', this.state.arrayImageTextOfNotes.length)

        for (let i = 0; i < this.state.arrayImageTextOfNotes.length; i++) {
            let dict = this.state.arrayImageTextOfNotes[i];
            let incr = i + 1;
            if (dict.isText) {
                postData.append('description-' + incr, dict.sentence.trim())
                { dict.id ? postData.append('block_id-' + incr, dict.id) : null }
            } else {
                postData.append('image_title-' + incr, dict.image_title.trim())
                postData.append('image_description-' + incr, dict.image_description.trim())
                { dict.id ? postData.append('block_id-' + incr, dict.id) : null }

                if (dict.document) {
                    postData.append('image-' + incr, {
                        uri: dict.document,
                        type: 'image/jpeg', // or photo.type
                        name: 'image-' + incr
                    });
                }
            }

        }

        console.log(" edit vet note 1676---", isEditVetNote, this.state.savedVetNoteId, this.state.savedVetNoteId.length);

        if (isEditVetNote && this.state.savedVetNoteId) {

            postData.append('submit_from', 'save_post')
            postData.append('post_id', this.state.savedVetNoteId);
            postData.append('edit_comment', editReasonCommentNotes);

        } else {
            if (isDraft) {
                postData.append('submit_from', 'draft_post')
            } else if (isPreview) {
                postData.append('submit_from', 'preview_post')
            } else {
                postData.append('submit_from', 'save_post')
            }
        }
        console.log(" edit vet note ---", postData);

        if (isEditVetNote && this.state.savedVetNoteId) {
            this.postNotesApiImplementationForEdit(postData);
        } else {
            this.postNotesApiImplementation(isDraft, isPreview, isPostNotes, postData);
        }
    }


    postNotesApiImplementation = (isDraft, isPreview, isPostNotes, postData) => {
        let auth_tokenTemp = '';
        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

        if (auth_tokenTemp && this.props.navigation.state.params) {

            if (this.state.savedVetNoteId != '' && isPreview) {
                { this.state.savedVetNoteId != '' ? postData.append('id', this.state.savedVetNoteId) : null };

                this.setState({ fetching: true });
                this.props.saveNoteAfterPreviewApiCall(postData, auth_tokenTemp).then((response) => {

                    this.setState({ fetching: false });

                    if (response) {
                        if (response.success) {
                            if (response.id) {
                                this.setState({ savedVetNoteId: response.id })
                            }
                            if (isPreview) {
                                this.setState({ isPreviewClickedNotes: true });
                            } else {
                                this.resetVetNoteData();
                            }

                            if (response.message) {
                                alert(response.message)
                            }

                        } else {
                            if (response.message) {
                                if (response.message.post_title && response.message.post_title.length > 0)
                                    alert(response.message.post_title[0])

                                alert(response.message);
                            }
                        }
                    } else {
                        alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                    }
                })
            } else {

                this.setState({ fetching: true });
                this.props.postNotesApiCall(postData, auth_tokenTemp).then((response) => {

                    this.setState({ fetching: false });

                    if (response) {
                        if (response.success) {
                            if (response.id) {
                                this.setState({ savedVetNoteId: response.id })
                            }
                            if (isPreview) {
                                this.setState({ isPreviewClickedNotes: true });
                            } else {
                                this.resetVetNoteData();
                            }

                            if (response.message) {
                                alert(response.message)
                            }

                        } else {
                            if (response.message) {
                                if (response.message.post_title && response.message.post_title.length > 0)
                                    alert(response.message.post_title[0])

                                alert(response.message);
                            }
                        }
                    } else {
                        alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                    }
                })
            }
        }
    }

    callApiToGetSpecialityBasisOfCategory = (category_id) => {
        let auth_tokenTemp = '';
        const { data } = this.state;

        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }



        if (auth_tokenTemp && this.props.navigation.state.params) {
            this.setState({ fetching: true });
            this.props.getSpecialityBasisOfCategoryInDetail(auth_tokenTemp, category_id).then((response) => {

                this.setState({ fetching: false });

                if (response) {
                    if (response.data.success) {

                        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
                            if (data['selectedSegmentIndex'] === 0) {
                                this.initializeDataSpecialitiesQuestion(true, response.data)
                            } else {
                                this.initializeDataSpecialitiesVetNote(true, response.data)
                            }
                        } else {
                            if (data['selectedSegmentIndex'] === 0) {
                                this.initializeDataSpecialitiesQuestion(false, response.data)
                            } else {
                                this.initializeDataSpecialitiesVetNote(false, response.data);
                            }
                        }
                    } else {
                        if (response.message) {
                            alert(response.message);
                        }
                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })
        }

    }
    callApiToGetTagsBasisOfCategory = (category_id) => {
        let auth_tokenTemp = '';
        const { data } = this.state;
        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

        if (auth_tokenTemp && this.props.navigation.state.params) {
            this.setState({ fetching: true });
            this.props.getTagsBasisOfCategoryInDetail(auth_tokenTemp, category_id).then((response) => {

                this.setState({ fetching: false });

                if (response) {
                    if (response.data.success) {
                        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
                            if (data['selectedSegmentIndex'] === 0) {
                                this.initializeDataTagsQuestion(true, response.data)
                            } else {
                                this.initializeDataTagsVetNote(true, response.data)
                            }
                        } else {
                            if (data['selectedSegmentIndex'] === 0) {
                                this.initializeDataTagsQuestion(false, response.data)
                            } else {
                                this.initializeDataTagsVetNote(false, response.data)
                            }
                        }
                    } else {
                        if (response.message) {
                            alert(response.message);
                        }
                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })
        }
    }


    callApiToGetAnimalBreedAndGender = (animalName) => {

        let selectedIdOfAnimal = ''
        for (let index = 0; index < this.state.arrayAnimalSpecies.length; index++) {
            if (this.state.arrayAnimalSpecies[index].title === animalName) {
                selectedIdOfAnimal = this.state.arrayAnimalSpecies[index].id;
                break;
            }
        }
        let auth_tokenTemp = '';
        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

        if (auth_tokenTemp && this.props.navigation.state.params) {
            this.setState({ fetching: true });

            this.props.getAnimalBreedGender(auth_tokenTemp, selectedIdOfAnimal).then((response) => {

                this.setState({ fetching: false });

                if (response) {
                    if (response.success) {


                        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
                            this.initializeDataBreedAndGenderQuestion(true, response.data);
                        } else {
                            this.initializeDataBreedAndGenderQuestion(false, response.data);
                        }
                    } else {
                        if (response.message) {
                            if (response.message.post_title && response.message.post_title.length > 0)
                                alert(response.message.post_title[0])

                            alert(response.message);
                        }
                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })
        }

    }


    postNotesApiImplementationForEdit = (postData) => {

        let auth_tokenTemp = '';
        { this.state.data['isEditVetNote'] || this.state.data['isEditQuestion'] ? auth_tokenTemp = this.props.navigation.state.params.dataOfPost.auth_token : auth_tokenTemp = this.props.navigation.state.params.auth_token }

        if (this.props.navigation.state.params) {
            this.setState({ fetching: true });
            this.props.saveEditedVetNoteApiCall(postData, auth_tokenTemp).then((response) => {

                this.setState({ fetching: false });

                if (response) {
                    if (response.success) {

                        if (response.message) {
                            alert(response.message)
                        }
                        this.resetVetNoteData();
                    } else {
                        if (response.message) {
                            if (response.message.post_title && response.message.post_title.length > 0)
                                alert(response.message.post_title[0])
                            alert(response.message);
                        }
                    }
                } else {
                    alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
                }
            })

        }
    }

    showTagSearchData = (text1) => {

        if (text1.length> searchStartCharLength) {
            console.log(" text1 in search ===", text1)
            let searchText = text1;
    
            searchText = searchText.trim().toLowerCase();
    
           let arrayFiltered = this.state.titleArrayTag.filter(temp => {
                console.log(" -------", temp)
                return temp.toLowerCase().match(searchText);
            });
    
            this.setState({
                arraySearchTags: arrayFiltered
            });
        }
    }

    render() {
        const { arraySearchTags, arraySelectedSpecialitiesNotes, arraySelectedSpecialities, arrayImageTextOfNotes, fetching, data, errors, arrayOfImages, arrayAnimalSpecies, categoryObject, arrayTag, arraySimilarQuestions, titleArraySpecialityMaster, specialityObject, titleArrayBreed, titleArrayGender, arrayBreed, arrayGender, titleArrayAnimalSpecies, titleArrayCategory, titleArrayTag, tagObject, arraySelectedTags, onSelectedTagsChange, arraySummaryNotes, isPreviewClickedNotes, arraySelectedTagsOfNotes } = this.state
        return (
            <PostQuestionOrNotes
                showTagSearchData={this.showTagSearchData}
                arraySearchTags={arraySearchTags}
                onClickCategoryToLoadData={this.onClickCategoryToLoadData}
                onClickToOpenAnimalSection={this.onClickToOpenAnimalSection}
                removeAllSpeciality={this.removeAllSpeciality}
                onClickAddImageOrTextOfNotes={this.onClickAddImageOrTextOfNotes}
                data={data}
                onClickPreview={this.onClickPreview}
                is_previewClicked={this.state.is_previewClicked}
                onClickSaveDraft={this.onClickSaveDraft}
                titleArrayAnimalSpecies={titleArrayAnimalSpecies}
                arrayAnimalSpecies={arrayAnimalSpecies}
                arraySelectedSpecialitiesNotes={arraySelectedSpecialitiesNotes}
                titleArraySpecialityMaster={titleArraySpecialityMaster}
                specialityObject={specialityObject}
                categoryObject={categoryObject}
                titleArrayCategory={titleArrayCategory}
                titleArrayTag={titleArrayTag}
                tagObject={tagObject}
                arraySimilarQuestions={this.state.arraySimilarQuestions}
                handleFileChange={this._handleFileChange}
                errors={errors}
                fetching={fetching}
                onSubmit={this.postQuestion}
                handleChange={this._handleChange}
                navigation={this.props.navigation}
                arrayOfImages={arrayOfImages}
                onClickAddImage={this.onClickAddImage}
                onClickDeleteImage={this.onClickDeleteImage}
                arrayBreed={arrayBreed}
                titleArrayBreed={titleArrayBreed}
                arrayGender={arrayGender}
                titleArrayGender={titleArrayGender}
                postQuestion={this.onClickPostQuestion}
                arraySelectedTags={arraySelectedTags}
                arraySelectedTagsOfNotes={arraySelectedTagsOfNotes}
                onSelectedTagsChange={this.onSelectedTagsChange}
                arraySelectedSpecialities={arraySelectedSpecialities}
                handleMultiSelect={this._handleMultiSelect}
                arrayImageTextOfNotes={arrayImageTextOfNotes}
                onClickDeleteImageOrTextOfNotes={this.onClickDeleteImageOrTextOfNotes}
                handleChangeNotes={this._handleChangeNotes}
                handleFileChangeNotes={this._handleFileChangeNotes}
                onChangeSegmentedControl={this.onChangeSegmentedControl}
                arraySummaryNotes={arraySummaryNotes}
                onClickAddSummaryNotes={this.onClickAddSummaryNotes}
                onClickDeleteSummaryNotes={this.onClickDeleteSummaryNotes}
                onClickPostNotesAsDraft={this.onClickPostNotesAsDraft}
                onClickPostNotesAsPreview={this.onClickPostNotesAsPreview}
                onClickPostNotes={this.onClickPostNotes}
                isPreviewClickedNotes={isPreviewClickedNotes}
                onClickDeleteMultiSelect={this.onClickDeleteMultiSelect}
                getBasicDetailToPostQuestion={this.getBasicDetailToPostQuestion}
                getBasicDetailToPostNotes={this.getBasicDetailToPostNotes}
            />
        )
    }
}

const mapStateToProps = state => {

    let dataToPostQuestion = '';
    return (
        dataToPostQuestion = state.questions
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        getDataToPostQuestion: (param) => dispatch(getDataToPostQuestion(param)),
        postQuestion: (param, auth_token) => dispatch(postQuestion(param, auth_token)),
        postDraftAsQuestion: (param, auth_token) => dispatch(postDraftAsQuestion(param, auth_token)),

        getDataToPostNotes: (param) => dispatch(getDataToPostNotes(param)),
        postNotesApiCall: (param, auth_token) => dispatch(postNotesApiCall(param, auth_token)),
        saveNoteAfterPreviewApiCall: (param, auth_token) => dispatch(saveNoteAfterPreviewApiCall(param, auth_token)),

        getDataToEditVetNotes: (param, auth_token) => dispatch(getDataToEditVetNotes(param, auth_token)),
        getDataToEditQuestion: (param, auth_token) => dispatch(getDataToEditQuestion(param, auth_token)),

        getSpecialityBasisOfCategoryInDetail: (param, postData) => dispatch(getSpecialityBasisOfCategoryInDetail(param, postData)),
        getTagsBasisOfCategoryInDetail: (param, postData) => dispatch(getTagsBasisOfCategoryInDetail(param, postData)),

        saveEditedVetNoteApiCall: (param, auth_token) => dispatch(saveEditedVetNoteApiCall(param, auth_token)),
        saveEditedQuestion: (param, auth_token) => dispatch(saveEditedQuestion(param, auth_token)),
        getAnimalBreedGender: (token, animalId) => dispatch(getAnimalBreedGender(token, animalId)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostQuestionOrNotesContainer)