import React from "react";
import { AsyncStorage } from 'react-native';
import { connect } from "react-redux";
import NotificationSelection from '../components/Notification'
import { getNotificationData, deleteNotification } from "../actions/notifications";
import * as wordConstants from '../constants/WordConstants';


class NotificationContainer extends React.PureComponent {

  state = {
    notificationData: [],
    auth_token: undefined,
    fetching: ''
  };

  componentDidMount = () => {
    console.log(" his.props.navigation.state.paramshis.props.navigation.state.params", this.props.navigation.state.params);
    this.getNotificationData();
  }

  getNotificationData() {

    if (this.props.navigation.state.params) {
      this.setState({ fetching: true });
      this.props.getNotificationData(this.props.navigation.state.params.auth_token)
        .then((resData) => {
          this.setState({ fetching: false });

          if (resData.success) {
            console.log("Response ====", resData);
            let notifData = resData.data.notifications
            this.setState({ notificationData: notifData.data });
          }
        })
    }
  }

  deleteItemById = (id) => {
    this.props.deleteNotification({ auth_token: this.props.navigation.state.params.auth_token, id: id })
      .then((resData) => {
        if (resData && resData.success) {
         
          let arrayTemp = [];
          for (let index = 0; index < this.state.notificationData.length; index++) {
            const element = this.state.notificationData[index];
            if (element.id === id) {
            } else {
              arrayTemp.push(element)
            }
          }
                this.setState({ notificationData: arrayTemp });
        }
        else {
        }
      })
  }

  render() {
    const { notificationData, fetching } = this.state;
    return (
      <NotificationSelection
        notificationData={notificationData}
        navigation={this.props.navigation}
        fetching={fetching}
        deleteItemById={this.deleteItemById}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
  };
};

// eslint-disable-next-line
const mapDispatchToProps = (dispatch) => {
  return {
    getNotificationData: (auth_token) => dispatch(getNotificationData(auth_token)),
    deleteNotification: (auth_token, id) => dispatch(deleteNotification(auth_token, id))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationContainer);
