import React from "react";
import { AsyncStorage, View, Dimensions, Text, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import TagComponent from '../components/TagComponent';




const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);


class TagContainer extends React.PureComponent {

    render() {
        return (
            <TagComponent></TagComponent>
        )
    }

}


const mapStateToProps = state => {

    return {
        state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TagContainer)