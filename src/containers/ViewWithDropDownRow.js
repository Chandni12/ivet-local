import React from 'react';
import { connect } from 'react-redux';
import ViewWithDropDownRow from '../commonComponent/ViewWithDropDownRow';
import { getCollectionData } from '../actions/collection';
import otherConstants from '../constants/Others';


class ViewWithDropDownRowContainer extends React.PureComponent {

    state = {
        fetching: false,
        errors: {},

        data: {
            viewTitle: '',
            totalPosts: 0
        },
        arrayData: []
    }

    componentDidMount = () => {
        console.log(" navigation data ---", this.props.navigation.state.params);
        const { data } = this.state;

        // if (this.props.navigation.state.params.dataToPass.viewName === otherConstants.COLLECTION)
         {
            data['viewTitle'] = otherConstants.COLLECTION;
            this.setState({data: JSON.parse(JSON.stringify(data))});
            this.callApiToGetCollectionData(this.props.navigation.state.params.auth_token);

        }
    }

    callApiToGetCollectionData(auth_token) {

        this.setState({ fetching: true });

        this.props.getCollectionData(auth_token)
            .then((resData) => {
                this.setState({ fetching: false });
                console.log(" response ====", resData);
                if (resData.success) {

                    const { data } = this.state;

                 let totalCount = resData.data.posts.total;
                
                     data['totalPosts'] = totalCount;

                    let arrayTemp = resData.data.posts.data;
                    this.setState({
                         data: JSON.parse(JSON.stringify(data)),
                        arrayData: arrayTemp
                    });
                }
            })
    }

    moveToModalPopupViews = () => {

    }
    render() {
        const { arrayData, data, errors, fetching } = this.state
        return (
            <ViewWithDropDownRow
                arrayData={arrayData}
                data={data}
                errors={errors}
                fetching={fetching}
                moveToModalPopupViews={this.moveToModalPopupViews}
                navigation={this.props.navigation}
            />
        );

    }
}

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCollectionData: (token) => dispatch(getCollectionData(token)),
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViewWithDropDownRowContainer);