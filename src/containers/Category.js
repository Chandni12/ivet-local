import React from "react";
import { connect } from "react-redux";
import CategorySelection from '../components/CategorySelection'
import { getCategoryData, saveCategoryData } from "../actions/category";
import * as wordConstants from '../constants/WordConstants';



class CategoryContainer extends React.PureComponent {

  state = {
    data: {
      search: "",
    },
    fetching: false,
    errors: {},
    onSuccess: false,
    arrayCategoryData: [],
   
  };


  componentDidMount = () => {

    this.setState({ fetching: true });
    console.log(" token-in container -------", this.props.loginData.data.auth_token);

    this.callApiToGetCategoryData(this.props.navigation.state.params.auth_token);
   
  }

  callApiToGetCategoryData(auth_token) {

    this.props.getCategoryData(auth_token)
      .then((resData) => {
        this.setState({ fetching: false });
       
        if (resData.success > 0) {
           console.log(" response ====", resData); 
          let arrayCategoriesSubCategories = resData.categoriesSubCategories;
          [this.setState({ arrayCategoryData: arrayCategoriesSubCategories })];
        }
      })
  }
  
  _handleChange = (name, value) => {
    const { data } = this.state;
    data[name] = value;
    this.setState(
      {
        data: JSON.parse(JSON.stringify(data))
      }
    );
  };

  _saveCategorySelection = () => {
    
   
    this.setState({ fetching: true });
   
    let arrayCat = [];
    for (let i = 0; i < this.state.arrayCategoryData.length; i++) {
      if (this.state.arrayCategoryData[i].isChecked) {
        let catIdCurrent = this.state.arrayCategoryData[i].id;

        if (!arrayCat.includes(catIdCurrent)) {
          arrayCat.push(catIdCurrent);
        }
      }
    }

    let data = {'category_id':arrayCat.toString()}

    this.props.saveCategoryData(this.props.navigation.state.params.auth_token, data).then((response) => {
   
       this.setState({ fetching: false });
        console.log(" Category response  ===",response ); 
      if (response.success) {

       this.props.navigation.navigate('tabNavigator', 
       { auth_token: this.props.navigation.state.params.auth_token, arrayCategory: arrayCat });

      } else {
        alert (wordConstants.CONST_CATEGORY_ERROR_MESSAGE);
      }

    })

  }

  render() {

    const { data, errors, fetching, onSuccess, arrayCategoryData } = this.state;
    // const { categoryData } = this.props;

    // console.log(" categroy data in render ", categoryData.data.subCategories);

    return (
      <CategorySelection
        fetching={fetching}
        onSuccess={onSuccess}
        categoryData={this.state.arrayCategoryData}
        errors={errors}
        saveCategories={this._saveCategorySelection}
        handleChange={this._handleChange}
        navigation={this.props.navigation}
      />
    );
  }

}

const mapStateToProps = state => {
  return {
    categoryData: state.category,
    loginData: state.login
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCategoryData: (token) => dispatch(getCategoryData(token)),
    saveCategoryData: (token, data) => dispatch(saveCategoryData(token, data))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryContainer);


// async _getAuthToken() {

//   const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);
//   const userData = await AsyncStorage.getItem(wordConstants.CONST_USER_DATA);
//   if (auth_token == null) {
//   } else {
//     this.setState({
//       auth_token: auth_token
//     });
   
//   }
// }