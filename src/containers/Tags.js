import React from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import Tags from '../components/Tags'
import dashboardConstants from '../constants/Dashboard';
import tagsConstants from '../constants/Tags';

import { getAllTagsData, getFollowingTagsData, getFollowingTagsBasedOnCategoryData, addRemoveTagFromTagfeedData } from '../actions/tags';


class TagContainer extends React.PureComponent {

  state = {
    fetching: false,
    errors: {},
    onSuccess: false,
    searchText: '',
    selectedIndex: 0,
    arrayTags: [],
    data: {
      isFindOtherTagClick: true,
      isManageFollowingTags: false
    },
    arraySelectedTags: []
  }

  componentDidMount() {

    // Comes from Dashboard to show data of all tags 
    console.log(" navigation params ---", this.props.navigation.state.params.categoryName); 

    if (this.props.navigation.state.params.categoryName) {
      let index = '';
      if (this.props.navigation.state.params.categoryName === dashboardConstants.CLINICAL) {
        index = 0;
      } else if (this.props.navigation.state.params.categoryName === dashboardConstants.LIFE) {
        index = 1;
      } else {
        index = 2
      }
      const {data} = this.state;
      data['isManageFollowingTags']= true;
      data['isFindOtherTagClick']= false;
      this.setState({ data: JSON.parse(JSON.stringify(data))}); 

      this.onChangeSegmentedControl(index, '', false, true)
    } else {
      // Comes from others to show data of following tags 
      const {data} = this.state;
      data['isManageFollowingTags']= false;
      data['isFindOtherTagClick']= true;
      this.setState({ data: JSON.parse(JSON.stringify(data))}); 
      this.onChangeSegmentedControl(0, '', false, true)
    }

  }
  getCategoryName = (index) => {
    let categoryName = ''
    if (index === 0) {
      categoryName = dashboardConstants.CLINICAL;
    } else if (index === 1) {
      categoryName = dashboardConstants.LIFE
    } else {
      categoryName = dashboardConstants.HOTEL_MANAGEMENT
    }
    return categoryName;
  }

  onChangeSegmentedControl = (index, searchText, isManageFollowingTags, isFindOtherTagClick) => {
    let categoryName = this.getCategoryName(index);
    this.setState({ selectedIndex: index });

    console.log(' ttags ---', "manage following tags --", this.state.data['isManageFollowingTags']);

    if (this.state.data['isFindOtherTagClick']) {
      this.callApiToGetTagFollowingTagsBasedOnCategoryName(categoryName, searchText);
    }

    if (this.state.data['isManageFollowingTags']) {
      this.callApiToGetAllTags(categoryName, searchText);
    }

  }

  onChangeSearchText = (name, value) => {
    this.setState({ searchText: value });
    this.onChangeSegmentedControl(this.state.selectedIndex, value);
  }

  onClickStar = (tagId) => {
    let alertTitle = '';
    if (this.state.arraySelectedTags.indexOf(tagId) === -1) {
      alertTitle = tagsConstants.ADD_THIS_TAG
    } else {
      alertTitle = tagsConstants.REMOVE_THIS_TAG
    }
    Alert.alert(
      tagsConstants.TAG_FEED,
      alertTitle,
      [
        {
          text: tagsConstants.CANCEL,
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: tagsConstants.OK, onPress: () => {
            this.callApiToAddTagInTagFeed(tagId);
          }
        },
      ],
      { cancelable: false },
    );
  }

  onClickFindOtherTag = () => {
    const { data } = this.state;
    data['isFindOtherTagClick'] = false;
    data['isManageFollowingTags'] = true;
    this.setState({ data: JSON.parse(JSON.stringify(data)) });
    this.onChangeSegmentedControl(this.state.selectedIndex, '', false, true);
  }

  onClickManageFollowingTag = () => {
    const { data } = this.state;
    data['isFindOtherTagClick'] = true;
    data['isManageFollowingTags'] = false;
    this.setState({ data: JSON.parse(JSON.stringify(data)) });
    this.onChangeSegmentedControl(this.state.selectedIndex, '', true, false);
  }


  callApiToAddTagInTagFeed = (tagId) => {

    this.props.addRemoveTagFromTagfeedData(this.props.navigation.state.params.auth_token, tagId).then((resData) => {
      console.log(" response $$$$$$$", resData);
      this.setState({ fetching: false });
      if (resData.success) {

        // if (this.state.arraySelectedTags.indexOf(tagId) === -1) {
        //   //  console.log(" in -1 ", selectedOption, this.state.arraySelectedTags);
        //   this.setState(prevState => ({ arraySelectedTags: [...prevState.arraySelectedTags, tagId] }));

        // } else {

        //   // console.log(" in remove ----  ", selectedOption, this.state.arraySelectedTags);

        //   var array = [...this.state.arraySelectedTags]; // make a separate copy of the array
        //   var index = array.indexOf(tagId)
        //   array.splice(index, 1);
        //   this.setState({ arraySelectedTags: array });

        // }
        this.onChangeSegmentedControl(this.state.selectedIndex, '');
      } else {
        if (resData.message) {
          alert(resData.message);
        }
      }
    });

  }

  callApiToGetTagFollowingTagsBasedOnCategoryName = (categoryName, searchText) => {
    if (this.props.navigation.state.params.auth_token) {
      this.setState({ fetching: true });

      this.props.getFollowingTagsBasedOnCategoryData(this.props.navigation.state.params.auth_token, categoryName, searchText).then((resData) => {
        console.log(" response following---", resData);
        this.setState({ fetching: false });

        if (resData.success) {
          if (resData.data.tags.data.length > 0) {
            this.setState({
              arrayTags: resData.data.tags.data
            });
          } else {
            this.setState({ arrayTags: [] });
            alert(dashboardConstants.THERE_IS_NO_RECORD_TO_DISPLAY);
          }
        } else {
          if (resData.message) {
            alert(resData.message);
          }
        }
      });
    }
  }

  callApiToGetAllTags = (categoryName, searchText) => {
    let postData = {
      'limit': 20,
      'paginate': 1,
      'category': categoryName,
      'search': searchText
    };

    if (this.props.navigation.state.params.auth_token) {
      this.setState({ fetching: true });
      this.props.getAllTagsData(this.props.navigation.state.params.auth_token, postData).then((resData) => {

        this.setState({ fetching: false });
        console.log(" responsee ----", resData);
        if (resData.success) {
          if (resData.data.tags.data.length > 0) {
            this.setState({
              arrayTags: resData.data.tags.data
            });
          } else {
            this.setState({ arrayTags: [] });
            alert(dashboardConstants.THERE_IS_NO_RECORD_TO_DISPLAY);
          }

        } else {
          if (resData.message) {
            alert(resData.message);
          }
        }
      });
    }
  }

  callApiToGetFollowingTags = () => {

    let postData = {
      "limit": 20,
      "paginate": 1,

    }
    if (this.props.navigation.state.params.auth_token) {
      this.setState({ fetching: true });

      this.props.getFollowingTagsData(this.props.navigation.state.params.auth_token).then((resData) => {

        this.setState({ fetching: false });
        if (resData.success) {

          if (resData.data.tags.data.length > 0) {
            this.setState({
              arrayTags: resData.data.tags.data
            });
          } else {
            this.setState({ arrayTags: [] });
            alert(dashboardConstants.THERE_IS_NO_RECORD_TO_DISPLAY);
          }
        } else {
          if (resData.message) {
            alert(resData.message);
          }
        }
      });
    }
  }

  render() {
    const { searchText, selectedIndex, arrayTags, errors, fetching, data, arraySelectedTags } = this.state;
    return (
      <Tags
        onClickManageFollowingTag={this.onClickManageFollowingTag}
        arraySelectedTags={arraySelectedTags}
        data={data}
        searchText={searchText}
        selectedIndex={selectedIndex}
        arrayTags={arrayTags}
        errors={errors}
        fetching={fetching}
        onChangeSegmentedControl={this.onChangeSegmentedControl}
        onChangeSearchText={this.onChangeSearchText}
        onClickStar={this.onClickStar}
        onClickFindOtherTag={this.onClickFindOtherTag}
        navigation={this.props.navigation}
      />
    );
  }
}

const mapStateToProps = state => {
  return {

  };
};

const mapDisptatchToProps = (dispatch) => {
  return {
    getAllTagsData: (token, postData) => dispatch(getAllTagsData(token, postData)),
    getFollowingTagsData: (token) => dispatch(getFollowingTagsData(token)),
    getFollowingTagsBasedOnCategoryData: (token, categoryName) => dispatch(getFollowingTagsBasedOnCategoryData(token, categoryName)),
    addRemoveTagFromTagfeedData: (token, tagId) => dispatch(addRemoveTagFromTagfeedData(token, tagId))

  }

}

export default connect(
  mapStateToProps,
  mapDisptatchToProps
)(TagContainer);