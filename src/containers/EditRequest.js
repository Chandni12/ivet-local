import React from "react";
import { connect } from "react-redux";
import EditRequest from '../components/EditRequest'
import { getEditRequestData, getEditOtherRequestData } from "../actions/editRequest";

class EditRequestContainer extends React.PureComponent {

  state = {
    fetching:'',
    editRequestData: [],
    otherEditRequestData: []
  };

  componentDidMount = () => {
    this.getEditRequestData();
    this.getEditOtherRequestData();
  }

  getEditRequestData() {
   this.setState({ fetching: true})
      this.props.getEditRequestData({ auth_token: this.props.navigation.state.params.auth_token })
      .then((resData) => {
        this.setState({ fetching: false});
        if (resData.success) {
            this.setState({ 
                editRequestData: resData.data
             });
        } else {
        }
      })
  }

  getEditOtherRequestData() {
    this.setState({ fetching: true})
       this.props.getEditOtherRequestData({ auth_token: this.props.navigation.state.params.auth_token })
       .then((resData) => {
         this.setState({ fetching: false});
         if (resData.success) {
             this.setState({ 
                otherEditRequestData: resData.data
              });
         } else {
         }
       })
   }

    render() {
        const { editRequestData, otherEditRequestData, fetching} = this.state;
        return (
            <EditRequest
                fetching={fetching}
                editRequestData={editRequestData.data}
                otherEditRequestData={otherEditRequestData.data}
                navigation={this.props.navigation}
            />
        );
    }
}

const mapStateToProps = state => {
  return {
  };
};

// eslint-disable-next-line
const mapDispatchToProps = (dispatch) => {
  return {
    getEditRequestData: (auth_token) => dispatch(getEditRequestData(auth_token)),
    getEditOtherRequestData: (auth_token) => dispatch(getEditOtherRequestData(auth_token))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditRequestContainer);
