import React from "react";
import { connect } from "react-redux";
import { addCommentsInPost, getDashboardQuestionDetail, getDashboardVetNoteDetail, postAnswerInDetail } from "../actions/dashboardPostDetail";
import { getAnswerForEditing, saveEditedAnswer } from "../actions/editAnswer";
import EditAnswer from "../components/Answer/EditAnswer";
import Validator from "../../validator";
import CastConstants from '../constants/Cast';
import { IMAGE_EDIT_ANSWER_PREFIX_URL } from '../api/endPoints';


const charLimitDetailForAnswer = 2000;
const maxImageUpload = 8;
const constCharLimitImageTitle = 30;
const constCharLimitImageDescription = 120;

class EditAnswerContainer extends React.PureComponent {
  state = {
    data: {
      
      editNote:'',
      answerText: '',
      charLimitAnswer: '',
      charLimitImgTitle: '30',
      charLimitImgDesc: '120',
      isAnyAnswerAccepted: false
    },
    fetching: false,
    errors: {},
    arrayData: [],
    onSuccess: false,
    auth_token: '',
    detailData: '',
    arrayOfImages: [],
    arrayCompleteData: []
  };

  componentDidMount() {
    this.setState({ fetching: true });
    this.callApiToGetData();
    console.log(" navigation =======", this.props.navigation.state.params);
  }

  _isValid = (field = null) => {
    const validate = Validator.createValidator(
      {
        answerText: ["required"],
        editNote:['required']
      },
      this.state.data,
      field
    );

    const { isValid, errors } = validate;

    if (field) {
      let oldErrorState = this.state.errors;
      if (!errors[field]) {
        delete oldErrorState[field];
      }
      oldErrorState[field] = errors[field];
      this.setState({ errors: oldErrorState ? JSON.parse(JSON.stringify(oldErrorState)) : oldErrorState });
    } else {
      this.setState({ errors: errors ? JSON.parse(JSON.stringify(errors)) : errors });
    }
    return isValid;
  };

  callApiToGetData = () => {

    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

      this.setState({ auth_token: this.props.navigation.state.params.dataOfPost.auth_token });

      let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'slug': this.props.navigation.state.params.dataOfPost.slug, 'id': this.props.navigation.state.params.data.id }

      // if (this.props.navigation.state.params.dataOfPost.type === 'question') 
      {
        this.props.getAnswerForEditing(parameters).then((resData) => {

         
          this.setState({ fetching: false })

          if (resData.data.success) {
            this.setState({
              detailData: resData.data.data,
            })
            const { data } = this.state

            if (this.state.detailData.is_accepted === 'Y') {
              data['isAnyAnswerAccepted'] = true;
            }

            data['answerText'] = this.state.detailData.answer;

            this.setState({ data: JSON.parse(JSON.stringify(data)) })
            this.state.detailData.documents.map((data, index) => {
              console.log(" image name --------", data);

              let imgUrl = ''; //IMAGE_EDIT_ANSWER_PREFIX_URL+data.id+'-thumb_'+data.document; Temp 
              let dictTemp = { 'id': data.id, 'image_title': data.title, 'image_description': data.description, 'document': imgUrl , 'charLimitImgTitle': constCharLimitImageTitle-data.title.length, 'charLimitImgDesc': constCharLimitImageDescription-data.description.length};

              this.setState({ arrayOfImages: [...this.state.arrayOfImages, dictTemp] });

            })

          }
        });
      }
      // else {

      //   this.props.getDashboardVetNoteDetail(parameters).then((resData) => {

      //     this.setState({ fetching: false });

      //     if (resData.data.success) {
      //       this.setState({
      //         detailData: resData.data.data.question
      //       })
      //     } else {
      //       if (resData.data.message) {
      //         alert(resData.data.message);
      //       }
      //     }
      //   });

      // }
    }
  }

  _handleFileChange = (name, file, index) => {
    let newArray = [...this.state.arrayOfImages];
    if (name === 'imagePhoto') {
      let dict = newArray[index];
      dict.document = file;
      this.setState({ arrayOfImages: newArray });
      return
    }
    if (file) {
      const { data } = this.state;
      data[name] = file;
      this.setState({ data });
    }
  };


  _handleChange = (name, value, index) => {

    const { data } = this.state;
    //***** This is for image title and descriptipn */
    if (index >= 0) {
      var array = [...this.state.arrayOfImages];
      let dict = array[index];

      if (name === 'image_title') {
        dict.image_title = value;

        if (value.length > constCharLimitImageTitle)
          return;

        dict.charLimitImgTitle = constCharLimitImageTitle - value.length;

      } else {
        dict.image_description = value;
        if (value.length > constCharLimitImageDescription)
          return;

       dict.charLimitImgDesc = constCharLimitImageDescription - value.length;
      }
      array[index] = dict;

      this.setState({
        data: JSON.parse(JSON.stringify(data)),
        arrayOfImages: array
      }
      );
      // console.log(" after update ####", this.state.arrayOfImages[index]);
      return;
    }
    //***************//

    data[name] = value;
    this.setState(
      {
        data: JSON.parse(JSON.stringify(data))
      },
      () => this._isValid(name)
    );
  };

  onClickAddImage = () => {
    let dictTemp = { 'image_title': '', 'image_description': '', 'document': '', 'id':'' , 'charLimitImgTitle':constCharLimitImageTitle, 'charLimitImgDesc':constCharLimitImageDescription};

    if (this.state.arrayOfImages.length === maxImageUpload) {
      alert(wordConstants.CONST_MAX_PHOTO_UPLOAD_WARNING);
      return;
    }
    this.setState({ arrayOfImages: [...this.state.arrayOfImages, dictTemp] });
  }

  onClickDeleteImage = (index) => {
    var array = [...this.state.arrayOfImages]; // make a separate copy of the array
    if (index > -1) {
      array.splice(index, 1);
      this.setState({ arrayOfImages: array });
    }
  }

  postAnswer = () => {
    let answerTemp = this.state.data.answerText;
    if (this.state.data.answerText.length < 30 && this.state.data.answerText.length > 0) {
      alert(CastConstants.ANSWER_MUST_BE_30_CHAR);
      return;
    }
    const valid = this._isValid();
    if (valid) {
      this.createPostDataToPostAnswer();
    }
  }

  createPostDataToPostAnswer = () => {

    const { answerText, editNote } = this.state.data;

    const postData = new FormData()
    postData.append('answer', answerText.trim());
    postData.append('edit_comment', editNote.trim());
    postData.append('total_blocks', this.state.arrayOfImages.length)

    for (let i = 0; i < this.state.arrayOfImages.length; i++) {
      let dict = this.state.arrayOfImages[i];
      let incr = i + 1;

      postData.append('image_title-' + incr, dict.image_title.trim())
      postData.append('image_description-' + incr, dict.image_description.trim())

      postData.append('doc_id-' + incr, dict.id)

      if (dict.document) {
        postData.append('answer_image-' + incr, {
          uri: dict.document,
          type: 'image/jpeg', // or photo.type
          name: 'answer_image-' + incr
        });
      }
    }
    console.log(" post data -----", postData);
    this.postAnswerApiImplementation(postData);

  }
  postAnswerApiImplementation = (postData) => {
    this.setState({ fetching: true });
   
    if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
      let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'slug': this.props.navigation.state.params.dataOfPost.slug, 'id': this.props.navigation.state.params.data.id }


      this.props.saveEditedAnswer(parameters, postData).then((response) => {
        this.setState({ fetching: false });
        console.log(" post answer resonse ", response);
        if (response.data.success) {
          if (response.data.message) {
           // alert(response.data.message);
           alert(CastConstants.YOUR_ANSWER_HAS_BEEN_POSTED);
          }
          this.props.navigation.goBack();
          
        } else {
          if (response.data.message) {
            alert(response.data.message);
          }
        }
      })
    }
  }

  render() {
    const { detailData, errors, fetching, data, arrayOfImages } = this.state;
    return (
      <EditAnswer
        data={data}
        postDetail={detailData}
        errors={errors}
        fetching={fetching}
        handleChange={this._handleChange}
        navigation={this.props.navigation}
        postComment={this.callApiToPostComment}
        postType={this.props.navigation.state.params.dataOfPost.type}
        clickToGiveAnswer={this.clickToGiveAnswer}
        arrayOfImages={arrayOfImages}
        onClickAddImage={this.onClickAddImage}
        onClickDeleteImage={this.onClickDeleteImage}
        handleFileChange={this._handleFileChange}
        postAnswer={this.postAnswer}
        arrayCompleteData={this.state.arrayCompleteData}

      />
    );
  }

}

// eslint-disable-next-line
const mapStateToProps = state => {

  return {
    editedData: state.editAnswer.data
  };
};

// eslint-disable-next-line
const mapDispatchToProps = (dispatch) => {
  return {
    getDashboardQuestionDetail: (parameters) => dispatch(getDashboardQuestionDetail(parameters)),
    getDashboardVetNoteDetail: (parameters) => dispatch(getDashboardVetNoteDetail(parameters)),
    addCommentsInPost: (parameters) => dispatch(addCommentsInPost(parameters)),
    postAnswerInDetail: (parameters, postData) => dispatch(postAnswerInDetail(parameters, postData)),
    saveEditedAnswer: (parameters, postData) => dispatch(saveEditedAnswer(parameters, postData)),
    getAnswerForEditing: (parameters) => dispatch(getAnswerForEditing(parameters))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditAnswerContainer);
