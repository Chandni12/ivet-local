import React from "react";
import { Platform } from 'react-native';
import { connect } from "react-redux";
import LoginProgress from "../components/LoginProgress"
import * as wordConstants from '../constants/WordConstants';
import firebase, { Notification, NotificationOpen } from "react-native-firebase";


class LoginProgressContainer extends React.PureComponent {
  state = {

    status: wordConstants.CONST_CHECKING, // checking/confirmed/failed
    userData: ''
  }

  async componentDidMount() {
    let statusReceived = this.props.navigation.state.params.status;
    console.log(" string  ====", this.props.navigation.state.params);
    this.setState({ status: statusReceived })
    
    if (this.props.navigation.state.params.userData) {
      this.setState({ userData: this.props.navigation.state.params.userData });
    }

    // Foreground app work well   
    if (Platform.OS === 'ios') {
      this.createNotificationListenersIos();
    } else {
      // Background 
      this.backgroundNotificationHandler();
      this.foregroundNotificationHandler();
    }

  }

  

  async createNotificationListenersIos() {



    /* Triggered when a particular notification has been received in foreground
*/
   const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
   .setDescription('My apps test channel');

 // Create the channel
 firebase.notifications().android.createChannel(channel);
 this.notificationListener = firebase.notifications().onNotification((notification) => {

   const { title, body } = notification;
   notification
     .android.setChannelId('test-channel')
     .android.setSmallIcon('ic_launcher');
   firebase.notifications()
     .displayNotification(notification);

     this.checkForData(notification);

 });

 /*
 * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
*/
 this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
   const { title, body } = notificationOpen.notification;
   // this.props.navigation.navigate("Booking")


   this.checkForData(notificationOpen.notification);
 });

 /*
 * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
*/
 const notificationOpen = await firebase.notifications().getInitialNotification();
 if (notificationOpen) {
   const { title, body } = notificationOpen.notification;



    this.checkForData(notificationOpen.notification);
 }
 /*
 * Triggered for data only payload in foreground
*/
 this.messageListener = firebase.messaging().onMessage((message) => {
   //process data message

 });
}

////***** These 2 methods are using for Android foreground and background tasks //////////******/

async backgroundNotificationHandler() {
  firebase.notifications().getInitialNotification()
    .then((notificationOpen: NotificationOpen) => {
      if (notificationOpen) {

        console.log(" 39 ===== ", notificationOpen);
        // App was opened by a notification
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification;

        if (notification) {

          // console.log("+================= notificationOpen ", notification, "===========");
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          // const action = notificationOpen.action;
          // Get information about the notification that was opened

          console.log("54 ^^^^^^^^^^^^^^^^^^", notification._data);
          this.checkForData(notification);
        }

      }
    });
}
  async foregroundNotificationHandler() {
    console.log(" foregroundNotificationHandler ");

    this.notificationListener = firebase.notifications().onNotification(notification => {

      console.log(" createNotification notification is ===", notification);

      //  notification.android.setAutoCancel(false)

      // this.getInspectionUserLogs(this.state.user);

      const channelId = new firebase.notifications.Android.Channel(
        'Default',
        'Default',
        firebase.notifications.Android.Importance.High
      );
      firebase.notifications().android.createChannel(channelId);

      let notification_to_be_displayed = new firebase.notifications.Notification({
        data: notification._android._notification._data,
        sound: 'default',
        show_in_foreground: true,
        title: notification.title,
        body: notification.body,
      });

      if (Platform.OS == 'android') {
        notification_to_be_displayed.android
          .setPriority(firebase.notifications.Android.Priority.High)
          .android.setChannelId('Default')
          .android.setVibrate(1000);
      }

      firebase.notifications().displayNotification(notification_to_be_displayed);

      console.log("^^^^^^^^^", notification._data, "Only noti======", notification);
      this.checkForData(notification);

    });
  }

  checkForData(notification) {

    console.log("165 ^^^^^^^^^", notification._data, "notification._data.type ===",notification._data.type, "notification._data.type.toUpperCase() ==", notification._data.type.toUpperCase());
   
    if (notification._data) {

     // If notification type is admin then no need to process notfication. 

      if (notification._data.type) {
        if (notification._data.type.toUpperCase() !== wordConstants.CONST_ADMIN) {
          return;
        }
      }

      console.log("39 ^^^^^^^^^", notification._data);
      let statusData = notification._data.status

      if (statusData.toUpperCase() === wordConstants.CONST_APPROVE) {
        // Chandni
        this.setState({ status: wordConstants.CONST_CONFIRM });

        console.log("======= in Confirm situation ");
      } else {
        // Chandni
        this.setState({ status: wordConstants.CONST_FAILED });
        console.log("======= in failed situatuon ");
      }
    }
  }

  moveToCategory =() =>{
    if(this.props.navigation.state.params.auth_token){
      this.props.navigation.navigate('CategorySelection', {auth_token:this.props.navigation.state.params.auth_token});
    }
  }

  componentWillUnmount() {

    this.notificationListener();

    if (Platform.OS === 'ios') {
      this.notificationOpenedListener();
      this.messageListener();
    }


  }
  render() {
    const { loginData } = this.props;

    return (
      <LoginProgress
        login={loginData}
        status={this.state.status}
        userData={this.state.userData}
        navigation={this.props.navigation}
        moveToCategory = {this.moveToCategory}
      />
    );

  }
}

const mapStateToProps = state => {
  return {
    loginData: state.login,
  };
};


export default connect(
  mapStateToProps,
  null
)(LoginProgressContainer);

/*

 if (Platform.OS === 'ios') {
      this.createNotificationListenersIos();
    } else {
      this.createNotificationListenersAndroid();
    }

iOS

async createNotificationListenersIos() {

    console.log("createNotificationListenersIOs ");

    * Triggered when a particular notification has been received in foreground

   const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
   .setDescription('My apps test channel');
 // Create the channel
 firebase.notifications().android.createChannel(channel);
 this.notificationListener = firebase.notifications().onNotification((notification) => {

   const { title, body } = notification;
   notification
     .android.setChannelId('test-channel')
     .android.setSmallIcon('ic_launcher');
   firebase.notifications()
     .displayNotification(notification);

 });

 /*
 * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:

 this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
   const { title, body } = notificationOpen.notification;
   // this.props.navigation.navigate("Booking")
   this.checkForData(notificationOpen);
 });

 /*
 * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:

 const notificationOpen = await firebase.notifications().getInitialNotification();
 if (notificationOpen) {
   const { title, body } = notificationOpen.notification;
    this.checkForData(notificationOpen);
 }
 /*
 * Triggered for data only payload in foreground

 this.messageListener = firebase.messaging().onMessage((message) => {
   //process data message

 });
}


Android

async createNotificationListenersAndroid() {
    console.log(" createNotificationListeners ");

    this.notificationListener = firebase.notifications().onNotification(notification => {

      //notification.android.setAutoCancel(false)

      const channelId = new firebase.notifications.Android.Channel(
        'Default',
        'Default',
        firebase.notifications.Android.Importance.High
      );
      firebase.notifications().android.createChannel(channelId);

      let notification_to_be_displayed = new firebase.notifications.Notification({
        data: notification._android._notification._data,
        sound: 'default',
        show_in_foreground: true,
        title: notification.title,
        body: notification.body,
      });

      if (Platform.OS == 'android') {
        notification_to_be_displayed.android
          .setPriority(firebase.notifications.Android.Priority.High)
          .android.setChannelId('Default')
          .android.setVibrate(1000);
      }
      firebase.notifications().displayNotification(notification_to_be_displayed);

      this.checkForData(notification);
    });
  }


*/