import React, { useRef } from "react";
import { connect } from "react-redux";
import { AsyncStorage } from 'react-native';
import { getDashboardData, getDashboardSearchData, getDashboardQandA, getDashboardClinicalNotes, getDashboardTagFeed, getDashboardTimeLineData, getDashboardSelectedTag, callApiToFollowUnfollowPost } from "../actions/dashboard";
import { getNotificationData, deleteNotification } from "../actions/notifications";

import { getMyProfileData } from '../actions/myProfile';
import Dashboard from "../components/Dashboard/Dashboard";
import postMenuConstants from '../constants/PostMenu';
import dashboardConstants from '../constants/Dashboard';
import * as wordConstants from '../constants/WordConstants';

export class DashboardContainer extends React.PureComponent {
  state = {
    data: {
      search: "",
      notificationCount: 0,
      
    },
    fetching: false,
    errors: {},
    arrayData: ['', '', ''], //// Add blank for start 3 row to show progress bar and other views in flat list 
    onSuccess: false,
    auth_token: '',
    myProfileData: '',
    selectedCategoryOption: '',
    paginate: 1,
    currentPostData: '',
    userNickNameIcon: '',
    userNickName: '',
  };


  componentDidMount() {
    let postData = {
      "limit": 20,
      "paginate": 1,
    }
    // when coming back after navigation call api 
    this.props.navigation.addListener(
      'willFocus',
      () => {


        if (this.props.navigation.state.params && this.props.navigation.state.params.auth_token) {
          this.setState({
            auth_token: this.props.navigation.state.params.auth_token,
          });
        }
        this.setState({
          currentPostData: postData,
          arrayData: ['', '', '']
        });
        // this.callDashboardApi(postData);
        this.getNotificationData();
        this.retrieveItem('userData');
      }
    );
    // After changing the tab again call api 
    // this.callDashboardApi(postData);
    this.getNotificationData();

    
    this.retrieveItem('userData');
    
    }

  onScreenFocus = () => {


    // Screen was focused, our on focus logic goes here
    let postData = {
      "limit": 20,
      "paginate": 1,
    }

    this.callDashboardApi(postData);
  }

  async retrieveItem(key) {
    try {
      const retrievedItem =  await AsyncStorage.getItem(key);
      const userDataValueArray = JSON.parse(retrievedItem);
      
      console.log(" 84----", userDataValueArray);
      if (userDataValueArray.length>0) {

        let userDataValue = userDataValueArray[0]

        if (userDataValue.other && userDataValue.other.nick_name) {

          this.setState({ 
            userNickName: userDataValue.other.nick_name, 
          userNickNameIcon:userDataValue.other.nick_name_icon? userDataValue.other.nick_name_icon:''
          });

          // const { data } = this.state;
          // let nickName = userDataValue.other.email;
          // data['userNickName'] = nickName
          // this.setState({ data: JSON.stringify(data) });
          // console.log(" 89----",nickName);
        } 
      }
     
  
    } catch (error) {
      console.log(error.message);
    }
    return
  }

  getNotificationData() {

    if (this.props.navigation.state.params) {

      this.props.getNotificationData(this.props.navigation.state.params.auth_token)
        .then((resData) => {

          if (resData.success) {

            const { data } = this.state;
            data['notificationCount'] = resData.data.notifications.total
            this.setState({ data })

          }
        })
    }
  }

  async _getAuthToken() {
    const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);

    if (auth_token == null) {
    } else {
      this.setState({
        auth_token: auth_token
      });
      //  return auth_token
    }
  }

  _handleChange = (name, value) => {
    const { data } = this.state;
    data[name] = value;
    this.setState(
      {
        data: JSON.parse(JSON.stringify(data))
      }
    );
  };

  onClickSearchIcon = () => {
    let dataSend = { 'auth_token': this.props.navigation.state.params.auth_token }
    this.props.navigation.navigate('Questions', { param: dataSend })
  }
  postMenuOptionClick = (option) => {
    let postData = {
      "limit": 20,
      "paginate": this.state.paginate,
    }
    if (this.props.navigation.state.params && this.props.navigation.state.params.auth_token) {
      this.setState({ fetching: true });

      if (option === postMenuConstants.Q_A_FORUM) {

        this.props.getDashboardQandA(postData, this.props.navigation.state.params.auth_token).then((resData) => {

          this.setState({ fetching: false });

          if (resData.data.success) {
            this.setState({
              arrayData: resData.data.data.all_posts.data
            });
          } else {
            if (resData.message) {
              alert(resData.message);
            }
          }
        });
      } else if (option === postMenuConstants.CLINICAL_NOTES) {
        this.props.getDashboardClinicalNotes(postData, this.props.navigation.state.params.auth_token).then((resData) => {


          this.setState({ fetching: false });

          if (resData.data.success) {
            this.setState({
              arrayData: resData.data.data.data
            });
          } else {
            if (resData.message) {
              alert(resData.message);
            }
          }
        });
      } else if (option === postMenuConstants.TIME_LINE) {
        this.props.getDashboardTimeLineData(postData, this.props.navigation.state.params.auth_token).then((resData) => {

          this.setState({ fetching: false });

          if (resData.data.success) {
            this.setState({
              arrayData: resData.data.data.posts.data
            });
          } else {
            if (resData.message) {
              alert(resData.message);
            }
          }
        });
      } else if (option === postMenuConstants.TAG_FEED) {
        this.props.getDashboardTagFeed(postData, this.props.navigation.state.params.auth_token).then((resData) => {

          this.setState({ fetching: false });
          if (resData.data.success) {
            this.setState({
              arrayData: resData.data.data.questions.data
            });
          } else {
            if (resData.message) {
              alert(resData.message);
            }
          }
        });
      } else if (option === postMenuConstants.LIFE || postMenuConstants.HOTEL_MANAGEMENT || postMenuConstants.CLINICAL) {
        this.setState({ fetching: false });
        let postData = {
          "limit": 20,
          "paginate": this.state.paginate,
          category: option
        }
        this.props.getDashboardSelectedTag(postData, this.props.navigation.state.params.auth_token).then((resData) => {

          this.setState({ fetching: false });
          if (resData.data.success) {
            if (resData.data.data.tags.data.length > 0) {
              this.setState({
                arrayData: resData.data.data.tags.data
              });
            } else {
              alert(dashboardConstants.THERE_IS_NO_RECORD_TO_DISPLAY);
            }

          } else {
            if (resData.message) {
              alert(resData.message);
            }
          }
        });
      }
    }
  }

  //* Calling Apis  *//

  callMyProfileApi = () => {
    if (this.props.navigation.state.params && this.props.navigation.state.params.auth_token) {
      this.props.getMyProfileData(this.props.navigation.state.params.auth_token).then((resData) => {

        this.setState({
          fetching: false,
          myProfileData: resData.data
        });
      });
    }
  }

  onClickFilterOption = (option) => {
    let sort = ''

    if (option === dashboardConstants.THIS_WEEK) {
      sort = 'this-week'
    } else if (option === dashboardConstants.THIS_MONTH) {
      sort = "this-month"
    } else if (option === dashboardConstants.CALL_FOR_ANSWERS) {
      sort = "waitingans"
    } else {
      sort = "attention"
    }

    let postData = {
      "limit": 20,
      "paginate": this.state.paginate,
      "sort": sort
    }
    this.callDashboardApi(postData);
  }
  onClickAddTo = () => {

    //this.props.navigation.navigate('PostQuestionOrNotes', { auth_token: this.props.navigation.state.params.auth_token, param: {'isQuestion':true}, viewNameComeFrom:'Dashboard'  });
    this.props.navigation.navigate('notification', { auth_token: this.props.navigation.state.params.auth_token, param: { 'isQuestion': true }, viewNameComeFrom: 'Dashboard' });

  }

  loadMoreData = () => {
    // console.log(" in load more --- pagination", this.state.paginate, this.state.currentPostData);
    if (this.state.paginate > 0) {
      this.callDashboardApi(this.state.currentPostData);
    }
  }

  callDashboardApi = (postData) => {

    postData.paginate = this.state.paginate;
    // postData.limit = 20;
    // 
    // postData.category_id = 2;

    if (this.props.navigation.state.params && this.props.navigation.state.params.auth_token) {

      this.setState({ fetching: true })
      this.props.getDashboardData(postData, this.props.navigation.state.params.auth_token).then((response) => {

        this.setState({ fetching: false })
        let resData = response.data;

        console.log(" resData of Dashboard --", resData); 

        if (response) {
          if (response.status === 200) {

            // getting nickname icon
            if (resData.all_posts.data.length > 0) {
              let dict = resData.all_posts.data[0];
             this.setState({userNickNameIcon: dict.nickname_icon})
            }
           
            if (resData.all_posts.last_page < this.state.paginate) {
              this.setState({ paginate: 0 })

            } else {
              this.setState({ paginate: resData.all_posts.current_page + 1 })
              this.setState(prevState => ({
                arrayData: [...prevState.arrayData, ...resData.all_posts.data]
              }))
            }


          } else {
            if (resData.data.message) {
              alert(resData.data.message);
            } else {
              // alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE);
            }
          }
        } else {

          alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE)
        }
      });
    }
  }

  handleFollowUnfollowPost = (idOfPost, index) => {


    if (this.props.navigation.state.params && this.props.navigation.state.params.auth_token) {
      this.setState({ fetching: true });

      this.props.callApiToFollowUnfollowPost(idOfPost, this.props.navigation.state.params.auth_token).then((resData) => {


        if (resData) {
          if (resData.data.success) {
            // Update perticluar row 
            let arrayTemp = this.state.arrayData;
            let dictTemp = this.state.arrayData[index];

            if (resData.data.action_name.toUpperCase() === 'FOLLOW') {
              dictTemp.isFollowByUser = true
            } else {
              dictTemp.isFollowByUser = false
            }
            arrayTemp[index] = dictTemp;
            this.setState({ arrayData: arrayTemp });

            this.setState({ fetching: false });
            alert(resData.data.message);
          } else {
            this.setState({ fetching: false });
            if (resData.data.message) {
              alert(resData.data.message);
            } else
              alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE)
          }
        } else {
          alert(wordConstants.CONST_INTERNET_ISSUE_MESSAGE)
        }
      });
    }
  }
  render() {
    const { userNickName, userNickNameIcon,data, errors, fetching, onSuccess, auth_token } = this.state;

    return (
      <Dashboard
      userNickName={userNickName}
      userNickNameIcon={userNickNameIcon}
        loadMoreData={this.loadMoreData}
        data={data}
        onClickAddTo={this.onClickAddTo}
        onClickFilterOption={this.onClickFilterOption}
        formData={data}
        onSuccess={onSuccess}
        dashboard={this.state.arrayData}
        //myProfileData={this.state.myProfileData}
        errors={errors}
        fetching={fetching}
        handleChange={this._handleChange}
        navigation={this.props.navigation}
        auth_token={this.props.navigation.state.params.auth_token}
        postMenuOptionClick={this.postMenuOptionClick}
        selectedCategoryOption={this.state.selectedCategoryOption}
        handleFollowUnfollow={this.handleFollowUnfollowPost}
        onClickSearchIcon={this.onClickSearchIcon}
      />
    );
  }
}

// eslint-disable-next-line
export const mapStateToProps = state => {

  return {
    dashboardData: state.dashboard,
    loginData: state.login
    //myProfileData: state.myProfile
  };
};

// eslint-disable-next-line
export const mapDispatchToProps = (dispatch) => {
  return {
    getDashboardData: (data, token) => dispatch(getDashboardData(data, token)),
    getDashboardQandA: (data, token) => dispatch(getDashboardQandA(data, token)),
    getDashboardTagFeed: (data, token) => dispatch(getDashboardTagFeed(data, token)),
    getDashboardTimeLineData: (data, token) => dispatch(getDashboardTimeLineData(data, token)),
    getDashboardClinicalNotes: (data, token) => dispatch(getDashboardClinicalNotes(data, token)),
    getDashboardSelectedTag: (data, token) => dispatch(getDashboardSelectedTag(data, token)),
    callApiToFollowUnfollowPost: (data, token) => dispatch(callApiToFollowUnfollowPost(data, token)),
    getNotificationData: (auth_token) => dispatch(getNotificationData(auth_token)),

    // getDashboardSearchData: (data, token) => dispatch(getDashboardSearchData(data, token))
    // getMyProfileData:(data) => dispatch(getMyProfileData(data))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardContainer);
