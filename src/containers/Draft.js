import React from 'react';
import { connect } from 'react-redux';
import Draft from '../components/Draft';
import { getDraftData } from '../actions/draft';
import otherConstants from '../constants/Others';


class DraftContainer extends React.PureComponent {

    state = {
        fetching: false,
        errors: {},
        data: {
            viewTitle: '',
            totalDraft: 0
        },
        arrayData: []
    }

    componentDidMount = () => {
        const { data } = this.state;
        {
            data['viewTitle'] = otherConstants.DRAFT + otherConstants.POST_LIST;
            this.getDraftData(this.props.navigation.state.params.auth_token);
        }
    }

    getDraftData(auth_token) {
        this.setState({ fetching: true });
        this.props.getDraftData(auth_token)
            .then((resData) => {
                this.setState({ fetching: false });
                if (resData.success) {
                    const { data } = this.state;
                    let totalCount = resData.result.questions.data.length;
                    data['totalDraft'] = totalCount;
                    let arrayTemp = resData.result.questions.data;
                    this.setState({
                        arrayData: arrayTemp
                    });
                }
            })
    }

    render() {
        const { arrayData, data, fetching } = this.state
        return (
            <Draft
                arrayData={arrayData}
                data={data}
                fetching={fetching}
                navigation={this.props.navigation}
            />
        );

    }
}

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDraftData: (token) => dispatch(getDraftData(token)),
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DraftContainer);