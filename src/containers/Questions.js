import React from "react";
import { AsyncStorage } from 'react-native';
import { connect } from "react-redux";
import QuestionsList from '../components/Question/QuestionsList'
import { getQuestionList } from "../actions/questions";
import { getDashboardSearchData } from '../actions/dashboard';
import * as wordConstants from '../constants/WordConstants';

export class Questions extends React.PureComponent {

    state = {
        data: {
            search: "",
        },
        fetching: false,
        errors: {},
        onSuccess: false,
        auth_token: '',
        userData: '',
        arrayQuestionData: []
    };

    async _getAuthToken() {
        const auth_token = await AsyncStorage.getItem(wordConstants.CONST_AUTH_TOKEN);
        const userData = await AsyncStorage.getItem(wordConstants.CONST_USER_DATA);
        if (auth_token == null) {
        } else {
            this.setState({
                auth_token: auth_token,
                userData: userData
            });
        }
    }
    callApiToGetQuestionList() {

        // We will get category id from user data 
        let param = { 'category_id': 2 }
        // let auth_token = "d6RJIk9ig_s:APA91bHddUeyMWb49ziwuL-1HQy9B8rtXWQQua71QAOBonJ--sLUzmzz-SOxPNgw7XZ65Y3YU-Z2dx0i48nN2Sz8pycJW7DniSLTHsOmtWj4_53iL-ld8W2ya6jjzF_secIsNclVuKXU1VFGB654HY"

        this.props.getQuestionList(param, this.state.auth_token).then((response) => {
            this.setState({ fetching: false });

            if (response.success) {
                if (response.Question) {
                    this.setState({ arrayQuestionData: response.Question });
                }
            } else {

            }
        })

    }
    onClickSearchButton = ()=>{
        this.callApiToGetSearchData(this.state.data['search'], true);
    }

    _handleChange = (name, value) => {
        const { data } = this.state;
        data[name] = value;
        this.setState(
            {
                data: JSON.parse(JSON.stringify(data))
            }
        );
        this.callApiToGetSearchData(value, false);
    };

    callApiToGetSearchData(searchText, isSearchButtonClick) {

        if (this.props.navigation.state.params.param.auth_token) {
            this.setState({fetching: true});
            this.props.getDashboardSearchData(searchText, this.props.navigation.state.params.param.auth_token, isSearchButtonClick).then((response) => {
                this.setState({fetching: false});
                
                if (response.data.success) {
                    console.log(" response is ----", response.data);
                    this.setState({ arrayQuestionData: response.data.result.questions.data });
                } else {
                    if (response.message)
                        alert(response.message);
                }
            })
        }
    }

    render() {
        const { arrayQuestionData, fetching, data } = this.state
        return (
            <QuestionsList
            onClickSearchButton ={this.onClickSearchButton}
                arrayQuestionData={arrayQuestionData}
                fetching={fetching}
                navigation={this.props.navigation}
                handleChange={this._handleChange}
                data={data}
            />
        )
    }

}

export const mapStateToProps = state => {

    return (
        questionList = state.questions
    )
}

export const mapDispatchToProps = (dispatch) => {
    return {
        getQuestionList: (token, param) => dispatch(getQuestionList(token, param)),
        getDashboardSearchData: (data, token, isSearchButtonClick) => dispatch(getDashboardSearchData(data, token, isSearchButtonClick))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Questions)