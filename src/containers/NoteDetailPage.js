import React from 'react';
import { Linking, Share } from 'react-native';
import { connect } from 'react-redux';
import NoteDetailPage from '../components/Dashboard/NoteDetailPage'
import { getDashboardVetNoteDetail } from "../actions/dashboardPostDetail";
import { getNotesHistory, voteUpDownNotesInDetail, makeNotesInappropriateInDetail, likeNotesComment, addNoteToCollection,addCommentsInPost } from "../actions/dashboardNotesDetail";
import dashboardConstants from '../constants/Dashboard';
import postAnswerConstants from '../constants/PostAnswer';

class NoteDetailPageContainer extends React.PureComponent {

    state = {

        data: {
            reasonInAppropriate: '',

            isClickToMakeInAppropriate: false,
            selectedQuestionOrAnswerData: '',
            commentText: '', // for inappropriate user enter comment
            commentForPost: '',
            isRecentCommentPost: false,
            isAddedInCollection: false,
            noteTextBlocks: '',
            category: '',
            isUpVote: false,
            isDownVote: false, 
            specialty:''

        },
        detailData: '',
        arrayRelatedArticles: [],
        arrayComments: [],
        fetching: false,
        errors: {},
        onSuccess: false,
        arrayTags: [],
    }

    componentDidMount() {
        console.log(" param data ===", this.props.navigation.state.params);
        this.setState({ fetching: true });
        this.props.navigation.addListener(
            'willFocus',
            () => {
      
              this.callApiToGetData();
            }
          );
        this.callApiToGetData();
    }

    callApiToGetData = () => {

        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

            this.setState({ auth_token: this.props.navigation.state.params.dataOfPost.auth_token });

            let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'slug': this.props.navigation.state.params.dataOfPost.slug }

            this.props.getDashboardVetNoteDetail(parameters).then((response) => {

                this.setState({ fetching: false });
                console.log(" response vet note =-=====", response.data.data);

                if (response.data.success) {
                    const { data } = this.state;
                    let tempArray = [];

                    if (response.data.data.case_study_post.content) {
                        response.data.data.case_study_post.content.map((data, index) => {
                            let str = data.text;
                            tempArray.push(str);
                        })
                    }
                    data['noteTextBlocks'] = tempArray.join(" ");
                    if (response.data.data.case_study_post.upvotes === 1) {
                        data['isUpVote'] = true
                    } else if (response.data.data.case_study_post.downvotes === 1) {
                        data['isDownVote'] = true
                    }
                    let categoryValue = ''
                    if (response.data.data.case_study_post.category_id === 1) {
                        categoryValue = dashboardConstants.CLINICAL;
                    } else if (response.data.data.case_study_post.category_id === 2) {
                        categoryValue = dashboardConstants.LIFE;
                    } else {
                        categoryValue = dashboardConstants.HOTEL_MANAGEMENT;
                    }
                    data['category'] = categoryValue;

                    let specialitiesValue = '';
                    if (response.data.data.case_study_post.specialities.length > 0) {
                        for (let index = 0; index < response.data.data.case_study_post.specialities.length; index++) {
                            let dict = response.data.data.case_study_post.specialities[index];
                           
                            specialitiesValue = specialitiesValue + dict.title+', ';
                        }
                    }
                    specialitiesValue = specialitiesValue.replace(/,\s*$/, "");
                     data['specialty'] = specialitiesValue;
                     

                    if (response.data.data.case_study_post.current_user_favourite && response.data.data.case_study_post.current_user_favourite > 0) {
                        data['isAddedInCollection'] = true;
                    }

                    this.setState({
                        data: JSON.parse(JSON.stringify(data)),
                        detailData: response.data.data.case_study_post,
                    });

                    if (response.data.data.suggested_posts) {
                        this.setState({ arrayRelatedArticles: response.data.data.suggested_posts })
                    }
                    if (response.data.data.case_study_post.comments) {

                        let dictTemp = { 'user_nick_name': response.data.data.user.nick_name }
                        let arrayTemp = [];
                        arrayTemp = response.data.data.case_study_post.comments;
                        arrayTemp.unshift(dictTemp);
                        this.setState({ arrayComments: arrayTemp });
                    }

                } else {
                    if (resData.data.message) {
                        alert(resData.data.message);
                    }
                }
            });
        }
    }

    moveToNotesPopupViews = (index, value) => {
        if (index === '0') {
            this.props.navigation.navigate('ViewHistory', { data: this.state.detailData, dataOfPost: this.props.navigation.state.params.dataOfPost });
        } else if (index === '1') {
            const { data } = this.state;
            data['isClickToMakeInAppropriate'] = true;
            data['selectedQuestionOrAnswerData'] = this.state.detailData,
                this.setState({ data: JSON.parse(JSON.stringify(data)) });

        } else if (index === '2') {
            let dataSendToDetail = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'type': this.state.detailData.post_type, 'slug': this.state.detailData.slug, 'id': this.state.detailData.id };
            this.props.navigation.navigate('PostQuestionOrNotes', { dataOfPost: dataSendToDetail, viewNameComeFrom:'NoteDetailPage' })
        }
    }

    callApiToMakeNotesInAppropriate = () => {
        const { data } = this.state;

        if (data['reasonInAppropriate'].length < 30) {
            alert(postAnswerConstants.POST_REASON_ALERT_CHARLIMIT);
            return;
        }

        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

            let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'comment': this.state.data.commentText, 'type': this.props.navigation.state.params.dataOfPost.type, 'id': data['selectedQuestionOrAnswerData'].id }

            let dictTemp = { 'post_report_reason': data['reasonInAppropriate'] }
            this.props.makeNotesInappropriateInDetail(parameters, dictTemp).then((resData) => {

                this.setState({ fetching: false });
                console.log(" response ainn---", resData);
                if (resData.data.success) {
                    if (resData.data.message) {
                        alert(resData.data.message);
                    }
                } else {
                    if (resData.data.message) {
                        alert(resData.data.message);
                    }
                }
                data['reasonInAppropriate'] = '';
                data['isClickToMakeInAppropriate'] = false;
                this.setState({
                    data: JSON.parse(JSON.stringify(data))
                });
            });

        }
    }
    hideInAppropriatePopup = () => {
        const { data } = this.state;
        data['isClickToMakeInAppropriate'] = false;
        this.setState({
            data: JSON.parse(JSON.stringify(data))
        });
    }

    callApiToPostComment = () => {

        console.log("post comment  iem data ====", this.state.data.commentText);

        if (this.state.data.commentForPost.trim().length === 0) {
            return;
        }

        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {

            let parameters = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'comment': this.state.data.commentForPost, 'user_id': this.state.detailData.user_id, 'post_id': this.state.detailData.id }

            this.props.addCommentsInPost(parameters).then((resData) => {

                this.setState({
                    fetching: false,
                    // arrayData:resData.data.listing.data
                });
                this.callApiToGetData();
                const { data } = this.state;
                data['commentForPost'] = '';
               
                this.setState({ data: JSON.parse(JSON.stringify(data)) }
                );
            });
        }
    }
    onClickAddNoteToCollection = () => {
        const { data } = this.state;

        if (this.props.navigation.state.params) {
            if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
                this.props.addNoteToCollection(this.props.navigation.state.params.dataOfPost).then((response) => {
                    console.log(" res ----", response);
                    if (response.data.success) {
                        if (response.data.starCount>0) {
                            data['isAddedInCollection'] = true;
                        } else {
                            data['isAddedInCollection'] = false;
                        }
                       
                        this.setState({ data: JSON.parse(JSON.stringify(data)) });
                        if (response.data.message) {
                            alert(response.data.message);
                        }
                    } else {
                        if (response.data.message) {
                            alert(response.data.message);
                        }
                    }
                })
            }
        }
    }

    onClickLikeNotesComment = (commentId) => {

        if (this.props.navigation.state.params.dataOfPost) {
            let parameter = { 'auth_token': this.props.navigation.state.params.dataOfPost.auth_token, 'id': commentId }

            this.props.likeNotesComment(parameter).then((response) => {

                if (response.data.success) {
                    if (response.data.message) {
                        alert(response.data.message);
                    }
                    this.callApiToGetData();
                } else {
                    if (response.data.message) {
                        alert(response.data.message);
                    }
                }
            })
        }
    }
    voteUpDownNotes = (action) => {
       
        if (this.props.navigation.state.params && this.props.navigation.state.params.dataOfPost) {
            // Upvote
            this.props.voteUpDownNotesInDetail(this.props.navigation.state.params.dataOfPost, action).then((response) => {
               
                if (response.data.success) {
                    if (response.data.message) {
                        alert(response.data.message);
                    }
                    this.callApiToGetData();
                } else {
                    if (response.data.message) {
                        alert(response.data.message);
                    }
                }
            })
        }
    }
    _handleChange = (name, value) => {

        const { data } = this.state;
        data[name] = value;
        this.setState(
            {
                data: JSON.parse(JSON.stringify(data))
            },
        );
    };

    moveToAnswerPopupViews = (answerData, index, value) => {
        console.log("--###--", answerData);
        if (index === '1') {
            this.props.navigation.navigate('ViewHistory', { data: answerData, dataOfPost: this.props.navigation.state.params.dataOfPost });
        }
        else if (index === '2') {
            const { data } = this.state;
            data['isClickToMakeInAppropriate'] = true;
            data['selectedQuestionOrAnswerData'] = answerData,
                this.setState({ data: JSON.parse(JSON.stringify(data)) });

        } else if (index === '0') {
            this.onShare();
            // Share functionality
            //  Linking.openURL('mailto:chandni.kkpl@gmail.com?subject=SendMail&body=Description') 

        }
    }

    onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    'React Native | A framework for building native apps using React',
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    }
    render() {
        const { data, arrayRelatedArticles, arrayComments, fetching, errors, onSuccess, detailData } = this.state;
        return (
            <NoteDetailPage
                data={data}
                detailData={detailData}
                onSuccess={onSuccess}
                arrayRelatedArticles={arrayRelatedArticles}
                arrayComments={arrayComments}
                fetching={fetching}
                errors={errors}
                callApiToGetData={this.callApiToGetData}
                hideInAppropriatePopup={this.hideInAppropriatePopup}
                callApiToPostComment={this.callApiToPostComment}
                onClickAddNoteToCollection={this.onClickAddNoteToCollection}
                onClickLikeNotesComment={this.onClickLikeNotesComment}
                voteUpDownNotes={this.voteUpDownNotes}
                navigation={this.props.navigation}
                handleChange={this._handleChange}
                moveToAnswerPopupViews={this.moveToAnswerPopupViews}
                moveToNotesPopupViews={this.moveToNotesPopupViews}
                hideInAppropriatePopup={this.hideInAppropriatePopup}
                callApiToMakeNotesInAppropriate={this.callApiToMakeNotesInAppropriate}
                onShare= {this.onShare}
            />
        );
    }
}

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDashboardVetNoteDetail: (parameters) => dispatch(getDashboardVetNoteDetail(parameters)),
        likeNotesComment: (parameters) => dispatch(likeNotesComment(parameters)),
        getNotesHistory: (parameters) => dispatch(getNotesHistory(parameters)),
        addNoteToCollection: (parameters) => dispatch(addNoteToCollection(parameters)),

        addCommentsInPost: (parameters) => dispatch(addCommentsInPost(parameters)),
        voteUpDownNotesInDetail: (parameter, postData) => dispatch(voteUpDownNotesInDetail(parameter, postData)),
        makeNotesInappropriateInDetail: (parameter, postData) => dispatch(makeNotesInappropriateInDetail(parameter, postData)),

    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NoteDetailPageContainer);