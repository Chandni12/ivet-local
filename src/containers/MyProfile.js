import React from 'react';
import { connect } from 'react-redux';
import { setNickName, getNickName, getMyProfileData, myProfileUpdateData, addWorkExperienceProfileData, updateWorkExperienceProfileData, addEducationProfileData, updateEducationProfileData } from '../actions/myProfile';
import Profile from '../components/profile/Profile';
import Validator from "../../validator";
import moment from 'moment';
import wordConstant from '../constants/WordConstants';

const minCharInWorkExpDescription = 30
const defaultClinicNameValue = 'あさ動物病院';

class MyProfileContainer extends React.PureComponent {
   constructor (props){
      super(props);
      this.state = {
         selectedNickName: false,
         fetching: false,
         errors: {},
         onSuccess: false,
         data: {
            isEditWorkExperience: false,
            isAddWorkExperience: false,
            isEditEducationalDetails: false,
            isAddEducationDetails: false,
            isEditIconClick: false,
         },
         workExperience: {
            id: '',
            title: '',
            clinic_id: '',
            hospitalAddress: '',
            clinic_name:'',
            start_year: '',
            end_year: '',
            start_month: '',
            end_month: '',
            currently_working: '',
            isPublishWithiVetTeacher: '',
            description: '',
            hospitalNames: [],
            public: ''
         },
         user: {},
         basicInfoData: {
            first_name: "",
            last_name: '',
            last_kana_name: '',
            first_kana_name: '',
            veterinarian_type: '',
            email: "",
            phone_number: "",
            dob: '',
         },

         educationData: {
            id: '',
            user_id: '',
            institute: "",
            degree: "",
            study_field: "",
            start_year: '',
            end_year: '',
            public: ""
         },

         yearList: [],
         monthList: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'],

         workExperienceArray:[],
         educationArray:[],
         veterinarian_type_name_display: '',
         veterinarian_types_complete_data: [],
         veterinarian_types_name: []
      }
   }

   componentDidMount() {
      this.callApiToGetMyProfileData();
   }

   // ******** Validator *******//

   _isValidBasicInfo = (field = null) => {
      const validate = Validator.createValidator(
         {
            first_kana_name: ["required"],
            last_kana_name: ["required"],
            email: ["required", "email"],
            phone_number: ["required"],
            first_name: ["required"],
            last_name: ["required"],
            veterinarian_type: ["required"],
            dob: ["required"],
         },
         this.state.basicInfoData,
         field
      );
      const { isValid, errors } = validate;

      if (field) {
         let oldErrorState = this.state.errors;
         if (!errors[field]) {
            delete oldErrorState[field];
         }
         oldErrorState[field] = errors[field];
         this.setState({ errors: oldErrorState ? JSON.parse(JSON.stringify(oldErrorState)) : oldErrorState });
      } else {
         this.setState({ errors: errors ? JSON.parse(JSON.stringify(errors)) : errors });
      }
      return isValid;
   };

   _isValidWorkExp = (field = null) => {
      const validate = Validator.createValidator(
         {

            title: ["required"],
            clinic_name: ["required"],
            start_year: ["required"],
            end_year: ["required"],
            start_month: ["required"],
            // end_month: ["required"],
            // currently_working: ["required"],
            description: ["required"],
            public: ["required"],
         },
         this.state.workExperience,
         field
      );
      const { isValid, errors } = validate;

      if (field) {
         let oldErrorState = this.state.errors;
         if (!errors[field]) {
            delete oldErrorState[field];
         }
         oldErrorState[field] = errors[field];
         this.setState({ errors: oldErrorState ? JSON.parse(JSON.stringify(oldErrorState)) : oldErrorState });
      } else {
         this.setState({ errors: errors ? JSON.parse(JSON.stringify(errors)) : errors });
      }
      return isValid;
   };

   _isValidEducationData = (field = null) => {
      const validate = Validator.createValidator(
         {
            institute: ["required"],
            study_field: ["required"],
            // clinic_id: ["required"],
            degree: ["required"],
            start_year: ["required"],
            end_year: ["required"],
            public: ["required"],
         },
         this.state.educationData,
         field
      );
      const { isValid, errors } = validate;

      if (field) {
         let oldErrorState = this.state.errors;
         if (!errors[field]) {
            delete oldErrorState[field];
         }
         oldErrorState[field] = errors[field];
         this.setState({ errors: oldErrorState ? JSON.parse(JSON.stringify(oldErrorState)) : oldErrorState });
      } else {
         this.setState({ errors: errors ? JSON.parse(JSON.stringify(errors)) : errors });
      }
      return isValid;
   };


   // ******  Handle Change ********//
   _handleChangeBasicInfo = (name, value) => {

      console.log(" name --", name,value);
      const { basicInfoData } = this.state;
      basicInfoData[name] = value;
      this.setState({
         basicInfoData: JSON.parse(JSON.stringify(basicInfoData))
      },
         () => this._isValidBasicInfo(name)
      );
   }

   _handleChangeWorkExp = (name, value) => {
      const { workExperience } = this.state;

      workExperience[name] = value;


      this.setState(
         {
            workExperience: JSON.parse(JSON.stringify(workExperience))
         },
         () => this._isValidWorkExp(name)
      );
   };

   _handleChangeEducation = (name, value) => {
      //console.log(" name ==== value ==== ", name, value);
      const { educationData } = this.state;
      educationData[name] = value;

      this.setState({
         educationData: JSON.parse(JSON.stringify(educationData))
      },
         () => this._isValidEducationData(name)
      );
   };

   //*****  Basic info ******//
   onClickEditIconUserView = (isShowEditView) => {
      const { data } = this.state
      data['isEditIconClick'] = isShowEditView;
      this.setState({ data: JSON.parse(JSON.stringify(data)) });
   }

   //******* Education */
   closeEducationPopup = () => {
      const { data } = this.state
      data['isAddEducationDetails'] = false;
      data['isEditEducationalDetails'] = false;
      this.setState({ data: JSON.parse(JSON.stringify(data)) });
   }

   onClickAddOrEditEducationalDetails = (isAdd, itemData) => {
      console.log("---- item data ---", itemData);
      const { data, educationData } = this.state;

      if (isAdd) {

         data['isAddEducationDetails'] = true;
         data['isEditEducationalDetails'] = false;

         educationData['id'] = '';
         educationData['start_year'] = '';
         educationData['end_year'] = '';
         educationData['institute'] = '';
         educationData['degree'] = '',
            educationData['study_field'] = '';
         educationData['public'] = '';

      } else {

         data['isAddEducationDetails'] = false;
         data['isEditEducationalDetails'] = true;

         if (itemData) {
            educationData['id'] = itemData.id;
            educationData['start_year'] = itemData.start_year;
            educationData['end_year'] = itemData.end_year;
            educationData['institute'] = itemData.institute;
            educationData['degree'] = itemData.degree,
               educationData['study_field'] = itemData.study_field;
            educationData['public'] = itemData.public;
         }
      }
      this.setState({
         data: JSON.parse(JSON.stringify(data)),
         educationData: JSON.parse(JSON.stringify(educationData)),
      });
   }

   //*****  Wrok Exp ******//

   closeWorkExpPopup = () => {

      const { data } = this.state
      data['isAddWorkExperience'] = false;
      data['isEditWorkExperience'] = false;
      this.setState({ data: JSON.parse(JSON.stringify(data)) });
   }


   // moveToNotesPopupViews = (index, value) => {
   _onClickAddOrEditWorkExp = (isAdd, itemData) => {

      const { data, workExperience } = this.state;

      if (isAdd) {

         data['isAddWorkExperience'] = true;
         data['isEditWorkExperience'] = false;

         workExperience['id'] = '';
         workExperience['description'] = '';
         workExperience['title'] = '';
         workExperience['start_year'] = '';
         workExperience['end_year'] = '';
         workExperience['start_month'] = '';
         workExperience['end_month'] = '',
            workExperience['currently_working'] = '';
         workExperience['clinic_id'] = '';
         workExperience['public'] = '';
         workExperience['clinic_name'] = '';
      } else {

         data['isAddWorkExperience'] = false;
         data['isEditWorkExperience'] = true;

         if (itemData) {
            workExperience['id'] = itemData.id;
            workExperience['description'] = itemData.description;
            workExperience['title'] = itemData.title;
            workExperience['start_year'] = itemData.start_year;
            workExperience['end_year'] = itemData.end_year
            workExperience['start_month'] = moment().month(itemData.start_month - 1).format('MMMM')
            workExperience['end_month'] = moment().month(itemData.end_month - 1).format('MMMM')
            workExperience['currently_working'] = itemData.currently_working
            workExperience['clinic_id'] = itemData.clinic_id
            workExperience['public'] = itemData.public
            workExperience['clinic_name'] = itemData.clinic_name? itemData.clinic_name: defaultClinicNameValue;
         }
      }
      this.setState({
         data: JSON.parse(JSON.stringify(data)),
         workExperience: JSON.parse(JSON.stringify(workExperience)),
      });
   }

   //********* Api Implementation */
   callApiToGetMyProfileData = ()=>{
      this.setState({fetching: true}); 
      if (this.props.navigation.state.params.auth_token) {
         this.props.getMyProfileData(this.props.navigation.state.params.auth_token).then((response) => {
            this.setState({fetching: false}); 
            console.log(" response ----", response);
            if (response.success) {

               let arrayTemp = [];
               let nick_name_Temp = [];
               var selectedVeterianName = '';

               response.data.veterinarian_types.map((data) => {

                  if (String(data.id) === String(response.data.user.veterinarian_type)) {
                     console.log(" data ----", data.id, response.data.user.veterinarian_type);
                     selectedVeterianName = data.veterinarian_type_name
                  }
                  arrayTemp.push(data.veterinarian_type_name)
               })

               response.data.nick_name_type.map((data) => {
                  nick_name_Temp.push(data.title)
               })

               let monthObject = response.data.months;

               let tempArrayMonth = Object.keys(monthObject).map(function(key) {
                  return monthObject[key];
               })

               this.setState({
                  veterinarian_type_name_display: selectedVeterianName,
                  veterinarian_types_name: arrayTemp,
                  basicInfoData: response.data.user,
                  user: response.data.user,
                  veterinarian_types_complete_data: response.data.veterinarian_types,
                  workExperienceArray:response.data.experienceDetails,
                  educationArray: response.data.educationDetails,
                  monthList:tempArrayMonth,
                  categories: Object.values(response.data.categories),
                  nickNameType: nick_name_Temp,
               })

               let array1 = [];
               for (let index = 1950; index < 2030; index++) {
                  array1.push(index.toString());
               }
               this.setState({ yearList: array1 });

            } else {
               if (response.message)
                  alert(response.message);
            }
         })
      }
   }

   callApiToUpdateProfile =() => {

      const { basicInfoData } = this.state;
      console.log(" callApiToUpdateProfile basic data ---", this.state);


      if (this._isValidBasicInfo) {
         let dataPost = {
            'first_name': this.state.basicInfoData['first_name'],
            'last_name': this.state.basicInfoData['last_name'],
            'last_kana_name': this.state.basicInfoData['last_kana_name'],
            "first_kana_name": this.state.basicInfoData["first_kana_name"],
            "nick_name": this.state.basicInfoData["nick_name"],
            "gender": this.state.basicInfoData["gender"],
            "dob": this.state.basicInfoData["dob"],
            "phone_number": this.state.basicInfoData["phone_number"],
            "veterinarian_type": this.state.basicInfoData['veterinarian_type'],
            'email':this.state.basicInfoData['email'],
         }
         console.log(" data ---post 000000-", dataPost);

         this.setState({fetching: true}); 
      this.props.myProfileUpdateData(this.props.navigation.state.params.auth_token, dataPost).then((response) => {
         this.setState({fetching: false}); 
            console.log(" response ---", response);
            if (response.success) {
               this.onClickEditIconUserView(false);
               this.callApiToGetMyProfileData();
            } else {

            }
         })
      } else {

      }
   }

   callApiToUpdateWorkExperience =() =>{

      const { workExperience, data } = this.state;


      if (this._isValidWorkExp)
      {
         if (workExperience['description'].length< minCharInWorkExpDescription) {
            alert("descriptionは、30文字以上にしてください。");
            return; 
         }

         let dataPost = {
            'title': workExperience['title'],
            'clinic_name': workExperience['clinic_name'] === ''? defaultClinicNameValue: workExperience['clinic_name'],
            'currently_working': workExperience['currently_working'],
            "start_month": moment().month(workExperience["start_month"]).format("M"),
            "start_year": workExperience["start_year"],
            "end_year": workExperience["end_year"],
            "end_month": moment().month(workExperience["end_month"]).format("M"),
            "description":workExperience['description'], 
            'public':workExperience['public'],
         }
   
         if (data['isEditWorkExperience']) {
            dataPost['exp_id'] = workExperience['id']
            this.setState({fetching: true}); 
   
            console.log(" callApiToUpdateWorkExperience basic data ---", dataPost);
           
            this.props.updateWorkExperienceProfileData(this.props.navigation.state.params.auth_token, dataPost).then((response) => {
               console.log(" response ---", response);
   
               this.setState({fetching: false}); 

               if (response.success) {
                  this.callApiToGetMyProfileData();
                  this.closeWorkExpPopup();
                  if (response.message) {
                     alert(response.message)
                  }

               } else { }
            })
         } else {
            console.log(" Add work  basic data ---", dataPost);
            this.props.addWorkExperienceProfileData(this.props.navigation.state.params.auth_token, dataPost).then((response) => {
               console.log(" response ---", response);
               if (response.success) {
                  this.callApiToGetMyProfileData();
                  this.closeWorkExpPopup();
                  if (response.message) {
                     alert(response.message)
                  }

               } else { }
            })
         }
      }

   }

   callApiToAddUpdateEducation =() =>{
      const { educationData, data } = this.state;
      console.log(" callApiToeducation  basic data ---", this.state);
   
       if (this._isValidEducationData)
      {
         let dataPost = {
            'institute_name':educationData['institute'],
            'degree':educationData['degree'],
            'field':educationData['study_field'],
            'start_year':String(educationData['start_year']) ,
            'end_year':String(educationData['end_year']) ,
            'public':educationData['public']
   
         }
   
         if (data['isEditEducationalDetails']) {
   
            dataPost['education_id'] = educationData['id']
            this.setState({fetching: true}); 
   
            this.props.updateEducationProfileData(this.props.navigation.state.params.auth_token, dataPost).then((response) => {
               console.log(" response ---", response);
               this.setState({fetching: false}); 

               if (response.success) {
                  this.callApiToGetMyProfileData();
                  this.closeEducationPopup();
                  if (response.message) {
                     alert(response.message);
                  }
               } else { }
            })
         } else {
            this.props.addEducationProfileData(this.props.navigation.state.params.auth_token, dataPost).then((response) => {
               console.log(" response ---", response);
               if (response.success) {
                  this.callApiToGetMyProfileData();
                  this.closeEducationPopup();
                  if (response.message) {
                     alert(response.message);
                  }
               } else { }
            })
         }
      }
   }

   callApiTosetNickName = (name) => {
      this.setState({ fetching: true });
      this.props.setNickName(this.props.navigation.state.params.auth_token, "dog").then((response) => {
         this.setState({ fetching: false,nick_name: "" });
         if (response.success) {
            this.setState({ selectedNickName : name})
         }
      })
   }

   callApiTogetNickName = (name) => {
      this.setState({ fetching: true });
      this.props.getNickName(this.props.navigation.state.params.auth_token, name).then((response) => {
         this.setState({ fetching: false });
         if (response.success) {
            this.setState({ nick_name: response.nicknames })
         } else {

         }
      })
   }

   render() {
      const { veterinarian_type_name_display, user, veterinarian_types_complete_data, veterinarian_types_name, data, workExperience, basicInfoData, errors, fetching, yearList, monthList, educationData, workExperienceArray, educationArray, categories, nickNameType, nick_name, selectedNickName } = this.state;
      return (
         <Profile
            workExperienceArray= {workExperienceArray}
            educationArray={educationArray}
            educationData={educationData}
            closeEducationPopup={this.closeEducationPopup}
            handleChangeEducation={this._handleChangeEducation}
            callApiToUpdateWorkExperience={this.callApiToUpdateWorkExperience}
            callApiToAddUpdateEducation = {this.callApiToAddUpdateEducation}
            yearList={yearList}
            monthList={monthList}
            closeWorkExpPopup={this.closeWorkExpPopup}
            veterinarian_type_name_display={veterinarian_type_name_display}
            errors={errors}
            fetching={fetching}
            data={data}
            basicInfoData={basicInfoData}
            workExperience={workExperience}
            user={user}
            navigation={this.props.navigation}
            onClickAddOrEditWorkExp={this._onClickAddOrEditWorkExp}
            onClickAddOrEditEducationalDetails={this.onClickAddOrEditEducationalDetails}
            onClickEditIconUserView={this.onClickEditIconUserView}
            handleChangeBasicInfo={this._handleChangeBasicInfo}
            handleChangeWorkExp={this._handleChangeWorkExp}
            callApiToUpdateProfile={this.callApiToUpdateProfile}
            veterinarian_types_complete_data={veterinarian_types_complete_data}
            veterinarian_types_name={veterinarian_types_name}
            categories={categories}
            nickNameType={nickNameType}
            callApiTogetNickName={this.callApiTogetNickName}
            callApiTosetNickName={this.callApiTosetNickName}
            nick_names={nick_name}
            setNickName={selectedNickName}
         />
      );
   }
}



const mapStateToProps = state => {
   return {

   };
};

const mapDispatchToProps = (dispatch) => {
   return {
      setNickName: (token, data) => dispatch(setNickName(token, data)),
      getNickName: (token, data) => dispatch(getNickName(token, data)),
      getMyProfileData: (token) => dispatch(getMyProfileData(token)),
      myProfileUpdateData: (token, params) => dispatch(myProfileUpdateData(token, params)),
      addWorkExperienceProfileData: (token, params) => dispatch(addWorkExperienceProfileData(token, params)),
      updateWorkExperienceProfileData: (token, params) => dispatch(updateWorkExperienceProfileData(token, params)),
      addEducationProfileData: (token, params) => dispatch(addEducationProfileData(token, params)),
      updateEducationProfileData: (token, params) => dispatch(updateEducationProfileData(token, params))
   }
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(MyProfileContainer);