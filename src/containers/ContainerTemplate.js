import React from 'react';
import { connect } from 'react-redux';
import NoteDetailPage from '../components/Dashboard/NoteDetailPage'

class NoteDetailPageContainer extends React.PureComponent {

    state = {
        fetching: false,
        errors: {},
        onSuccess: false,
        data: {}
    }

    render() {
      
        return (
            <NoteDetailPage
            navigation={this.props.navigation}/>
        );

    }


}

const mapStateToProps = state => {
    return {
        
    };
};


export default connect(
    mapStateToProps,
    null
)(NoteDetailPageContainer);