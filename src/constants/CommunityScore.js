const communityScoreConstants = {
   MY_PAGE:'My Page',
   COMMUNITY_SCORE:'Community score',
   TOP:'Top',
   BY_CATEGORY:'By category',
   CLINICAL: 'Clinical',
   LIFE:'Life',
   HOSPITAL_MANAGEMENT:'Hospital management',
   BLOOD_IMMUNITY:'Blood immunity',
   VOTE_FOR_ANSWERS:'Vote for answers',
   VOTE_FOR_QUESTION: "Vote for question",
   CASE: "Case",
   BEST_ANSWERE: "Best answer",
   NOTES_0:'0 Notes',
   POST_TO_NOTE: "Post to note",
   ORTHOPEDICS: "Orthopedics",
   LIVER_BILIARY_SYSTEM: "Liver / biliary system",
   HELP: "Help",
   WHAT_IS_COMMUNITY_SCORE: "What is a community score?",
   COMMUNITY_SCORE_DESC1: "Show how much best answer you got if you voted in the community One indicator is Community Scores (CS: Community Scores)Show how much you can contribute to creating a community that is useful to as many people as possibleTo do.",
   COMMUNITY_SCORE_DESC2: "Community scores are aggregated by the total for each posting category.If you earn a high CS, you can earn a special (subcategory) badge.Earn the right to do spot consulting.",
   LIST_OF_COMMUNITY_SCORE:'List of community score',
   AN_EVENT: "An Event",
   VOTE_FOR_ANSWERS2: "Vote for answere",
   VOTE_FOR_NOTES: "Vote for notes",
   VOTE_FOR_QUESTION2: "Vote for question",
   ANSWERE_SELECTED_AS_BEST_ANSWERE: "Answer selected as best answer",
   SELECTED_BEST_ANSWERE: "Select best answer",
   POST: "Post",
   TEXT_ORANGE:'Text Orange',
   CS355:'355cs',
   CS120:'120cs',
   FOREST_TO_1:'Forest to 00',
   FOREST_TO_2: 'Forest to 01',

}

const communityScoreConstants1 = {
    MY_PAGE:'マイベージ',
    COMMUNITY_SCORE:'コミュニティスコア',
    TOP:'上位',
    BY_CATEGORY:'カテゴリ別',
    CLINICAL: '臨床',
    LIFE:'ライフ',
    HOSPITAL_MANAGEMENT:'病院経営',
    BLOOD_IMMUNITY:'血液免疫',
    VOTE_FOR_ANSWERS:'回答への投票',
    VOTE_FOR_QUESTION: "質問への投票",
    CASE: "件",
    BEST_ANSWERE: "ベストアンサー",
    POST_TO_NOTE: "ノートへの投稿",
    ORTHOPEDICS: "整形外科",
    LIVER_BILIARY_SYSTEM: "肝・胆道系",
    HELP: "ヘルプ",
    WHAT_IS_COMMUNITY_SCORE: "コミュニティスコアとは?",
    COMMUNITY_SCORE_DESC1: "コミュニティで投票ゃべストアンサーがどれたけ得られたかを示す一つの指標がコミュニティスコア(CS:CommunityScores)で、より多くの方に役立つコミュニティづくりにどれだけ貢献できたかを示します。",
    COMMUNITY_SCORE_DESC2: "コミュニティスコアは各投稿カテゴリの合計によって集計されます。高いCSを獲得されると、専門性(サブカテゴリ)バッジを獲得でき、スポットコンサルを行う権利を獲得します。",
    LIST_OF_COMMUNITY_SCORE: "コミューティスコア一覧表",
    AN_EVENT: "イベント",
    VOTE_FOR_ANSWERS2: "回答に対する投票",
    VOTE_FOR_NOTES: "ノートに対する投票",
    VOTE_FOR_QUESTION2: "質問に対する投票",
    ANSWERE_SELECTED_AS_BEST_ANSWERE: "回答がベストアンサー選定",
    SELECTED_BEST_ANSWERE: "ベストアンサーを選択",
    POST: "投稿",
    FOREST_TO_1:'再生医療 40件/年',
    FOREST_TO_2: '腎臓透析 80件/年',
    NOTES_0:'0メモ',
 }

export default communityScoreConstants1;