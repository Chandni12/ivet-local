const History ={
   HISTORY:'History',
   COMING_SOON:'Coming Soon',
   BACK_TO_PREVIOUS_PAGE:'Back to previous page',
   EDITED_BY:'Edited by',
   BEFORE_CHANGE:'Before Change',
   AFTER_CHANGE:'After Change',
   TARGET:'Target',
   CHANGE_TARGET:'Change Target',
   CHANGE_OBJECT:'Change Object',
   ADDITIONAL_CONTENT:'Additional content',
   COMMENT:'Comment',
   CREATED_AT:'Created at',
ANIMAL_TYPE:'Animal type', 
ANIMAL_BREED_TYPE:'Animal breed type ',
GENDER:'Gender',
AGE_YEAR:'Age year',
AGE_MONTH:'Age month',
TITLE:'Title',
TAG:'Tag',
DESCRIPTION:'Description',
SPECIALITY:'Speciality',
CATEGORY:'Category',
REMARKS:'Remarks',
PHOTO_NUMBER:'Photo number',
IMAGE_TO_BE_CHANGED:'Image to be changed', 
CHANGE_THE_SUBJECT:'Change the subject',

}

const HistoryJapanese ={
    HISTORY:'歴史',
    COMING_SOON:' 近日公開',
    BACK_TO_PREVIOUS_PAGE:'前のページへ戻る',
    EDITED_BY:'によって編集',
    BEFORE_CHANGE:'変更前',
    AFTER_CHANGE:'変更後',
    TARGET:'対象',
    CHANGE_TARGET:'変更対象',
    CHANGE_OBJECT:'オブジェクトを変更',
    ADDITIONAL_CONTENT:'追加コンテンツ',
    COMMENT:'コメント',
    CREATED_AT:'で作成',
    ANIMAL_TYPE:'', 
ANIMAL_BREED_TYPE:'',
GENDER:'',
AGE_YEAR:'',
AGE_MONTH:'',
TITLE:'',
TAG:'',
DESCRIPTION:'',
SPECIALITY:'',
CATEGORY:'',
REMARKS:'',
PHOTO_NUMBER:'',
IMAGE_TO_BE_CHANGED:'',
CHANGE_THE_SUBJECT:'',
}

export default History