const editRequestConstants = {
    MY_EDIT_REQUEST: 'My edit request',
    OTHER_EDIT_REQUEST: 'Other edit request',
    EDIT_PAGE:'Edit page'
}

const editRequestConstants1 = {
    MY_EDIT_REQUEST: '私の編集リクエスト',
    OTHER_EDIT_REQUEST: 'その他の編集リクエスト',
    EDIT_PAGE:'ページを編集'
}

export default editRequestConstants1;