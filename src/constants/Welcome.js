const welcomeConstants = {
    TITLE: 'Dr. Vet',
    DESCRIPTION: 'There is also a world where veterinary medicine and sharing speed.',
    WELCOME_TEXT:'Welcome to iVet',
    REGISTRATION_TITLE:'Register for free or log in.',
    REGISTRATION_BUTTON_TITLE:'Free registration (only for veterinarians/veterinary student)',
    LOGIN_BUTTON_TITLE: 'Login',
    BY_CONTINUEING_USAGE_RULES:'By continueing the usage rules for iVet' ,
   
    IT_IS_CONSIDERED: 'it is considered',
    THAT_YOU_HAVE_AGREED_PRIVACY_POLICY: 'that you have agreed privacy policy',
    COPYRIGHT_TEXT: '@ MOTOCLE, Inc. 2019',
    // Privacy Policy url 
    PRIVACY_POLICY_URL: "http://54.250.243.234/privacy",

    TERMS_AND_CONDITION_URL: "http://54.250.243.234/term",

}

const welcomeConstants1 = {
    TITLE: 'iVet',
    TITLE_BLACK:'先生',
    DESCRIPTION: '獣医療、共有で広がる世界もある。' ,
    WELCOME_TEXT:'iVet先生へようこそ。',
    REGISTRATION_TITLE:'無料登録またはログインしてください。',

    REGISTRATION_BUTTON_TITLE:'無料登録',
    REGISTRATION_BUTTON_TITLE_2:'(獣医師 / 獣医学生のみ)', 
    LOGIN_BUTTON_TITLE: 'ログイン',
    BY_CONTINUEING_USAGE_RULES: '続行する事で、iVet先生の',
    IT_IS_CONSIDERED: '利用規定',
    IT_IS_CONSIDERED_2: 'と',
    THAT_YOU_HAVE_AGREED_PRIVACY_POLICY: 'プライバシー規定',
    PRIVACY_POLICY_2:'に同意したものとみなされます',
    COPYRIGHT_TEXT: '@ MOTOCLE, Inc. 2019',

    PRIVACY_POLICY_URL: "http://54.250.243.234/privacy",

    TERMS_AND_CONDITION_URL: "http://54.250.243.234/term",
}
export default welcomeConstants1;
