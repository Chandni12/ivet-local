import CastConstants from './Cast';

// Word constants 
const wordConstants ={
    CONST_CHECKING: "checking",
    CONST_YEAR : "Year",
    CONST_CONFIRM : "confirm",
    CONST_FAILED : "failed",
    CONST_APPROVE : "APPROVE",
    CONST_REJECT : "REJECT",
    CONST_IS_REGISTER : "IS_REGISTER",
    CONST_ADMIN : "ADMIN",
    CONST_AUTH_TOKEN : "auth_token",
    CONST_FCM_TOKEN : "fcm_token",
    CONST_USER_DATA : "userData ",
    CONST_MONTH : "MONTH",
    CONST_DAY : "DAY",
    CONST_YEARS : "YEARS",
    CONST_MONTHS : "MONTHS",
    CONST_DAYS : "DAYS",
    CONST_AGO : "AGO",
    CONST_BEFORE : "BEFORE",
    CONST_REGISTER_ERROR_MESSAGE : "REGISTER_ERROR_MESSAGE" ,
    CONST_MAX_TAGS_WARNING:"Up to five tags can be added.",
    CONST_USER_DATA:'userData',

}
    
const wordContantsJapanese ={
    CONST_USER_DATA:'userData',
    CONST_MAX_TAGS_WARNING:"最大5つのタグを追加できます。",

    CONST_CHECKING: "checking",
    CONST_CONFIRM : "confirm",
    CONST_FAILED : "failed",
    CONST_APPROVE : "APPROVE",
    CONST_REJECT : "REJECT",
    CONST_IS_REGISTER : "IS_REGISTER",
    CONST_ADMIN : "ADMIN",
    CONST_AUTH_TOKEN : "auth_token",
    CONST_FCM_TOKEN : "fcm_token",
    CONST_USER_DATA : "userData ",
    CONST_YEAR : "年",
    CONST_MONTH : "月",
    CONST_DAY : "日",
    CONST_YEARS : "年",
    CONST_MONTHS : "ヶ月",
    CONST_DAYS : "日々",
    CONST_AGO : "前",
    CONST_BEFORE : "前",
    CONST_REGISTER_ERROR_MESSAGE : "情報をもう一度確認してください" ,

}
export const CONST_CHECKING = "checking";
export const CONST_CONFIRM = "confirm";
export const CONST_FAILED = "failed";
export const CONST_APPROVE = "APPROVE";
export const CONST_REJECT = "REJECT";
export const CONST_IS_REGISTER = "IS_REGISTER";
export const CONST_ADMIN = "ADMIN";
export const CONST_AUTH_TOKEN = "auth_token"; 
export const CONST_FCM_TOKEN = "fcm_token";
export const CONST_USER_DATA = "userData ";

export const CONST_YEAR = "年"
export const CONST_MONTH = "月"
export const CONST_DAY = "日"

// greater than 1 
export const CONST_YEARS = "年"
export const CONST_MONTHS = "ヶ月"
export const CONST_DAYS = "日々"

export const CONST_AGO = "前"

export const CONST_BEFORE = "前"


// Error messages 
export const CONST_REGISTER_ERROR_MESSAGE = "情報をもう一度確認してください" ;
export const CONST_CATEGORY_ERROR_MESSAGE = "category idは、必ず指定してください。" ;

export const CONST_REGISTER_ERROR_PHOTO_NATIONAL_QUALIFICATION = "" ;
export const CONST_REGISTER_ERROR_PHOTO_STUDENT_ID = "" ;
export const CONST_VETENARY = '1';

export const CONST_VETENARY_STUDENT = '2';

export const CONST_INTERNET_ISSUE_MESSAGE = "インターネット接続を確認してください";
export const CONST_MAX_PHOTO_UPLOAD_WARNING = "最大8枚の写真を追加できます。"
export const CONST_MAX_SUMMARY_ADD_WARNING = "追加できるセクションは8つだけです。"

export const CONST_EMPTY_CONTENT_NOTES = "選択した合計ブロックは無効です。";
export const CONST_MAX_TAGS_WARNING = "最大5つのタグを追加できます。";

export const CONST_WORK_EXP_DESCRIPTION_ERROR_MESSAGE = "descriptionは、30文字以上にしてください。" ;
