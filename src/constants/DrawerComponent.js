const DrawerComponentConstants = {
    DASHBOARD: "Dashboard",
    CANCEL:'Cancel',
    OK:'OK',
    LOGOUT:'Logout',
    ARE_YOU_SURE_TO_LOGOUT:'Are you sure to logout?',
    FOLLOW:'Follow',
    DRAFT:'Draft',
    EDIT_REQUEST_LIST: 'Edit request list'
}

const DrawerComponentConstants1 = {
    DASHBOARD: "ホーム",
   CANCEL:'キャンセル',
    OK:'OK',
    LOGOUT:'ログアウト',
    ARE_YOU_SURE_TO_LOGOUT:'ログアウトしますか?',
    FOLLOW:'フォロー',
    DRAFT:'ドラフト',
    EDIT_REQUEST_LIST: 'リクエストリストを編集'
}

export default DrawerComponentConstants1