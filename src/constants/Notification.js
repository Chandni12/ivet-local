const notificationConstants = {
    NOTIFICATION: 'Notification',
    ALL_NOTIFICATIONS: 'All notifications',
    NOTIFICATION_DESC1: 'I have posted a question. Features Vapecia with symptoms such as fever, mucous pallor, splenomegaly, bilirubin urine (brown urine)',
    NOTIFICATION_DESC2: '[Important notice] December 15, 2019, 12: 00-15: 00 Perform system maintenance',
    NOTIFICATION_DESC3: '[Other Information] A new function has been added to Dr. Ivet.',
    NOTIFICATION_DESC4: '[Other notices] "Program revision of the next standard mutual aid system Regarding public notice (public offering) related to `` consignment of training (responding to change of certificate management method etc.)Please.',
    LOAD_MORE: 'Load more',
    POST_NOTIFICATION: "Post notification",
    IMPORTANT_NOTICE: "Important notice",
    OTHER_NOTIFICATIONS: "Other notifications"
}

const notificationConstants1 = {
    NOTIFICATION: '通知',
    ALL_NOTIFICATIONS: '全ての通知',
    NOTIFICATION_DESC1: '質問を投稿しました。『発熱、粘膜蒼白、脾腫、ビリルビン尿(茶色い尿)などの症状でバペシアを特…』',
    NOTIFICATION_DESC2: '【重要なお知らせ】2019年12月15日12:00-15:00でシステムメンテナンスを行います。',
    NOTIFICATION_DESC3: '【その他のお知らせ】ivet先生に新機能が追加されました。',
    NOTIFICATION_DESC4: '【その他のお知らせ】「次期標準共済システムのプログラム改修の委託(証管理方法変更対応等)」に係る公示(公募)について。',
    LOAD_MORE: 'さらに読み込む',
    POST_NOTIFICATION: "投稿の通知",
    IMPORTANT_NOTICE: "重要な通知",
    OTHER_NOTIFICATIONS: "その他の通知"

}

export default notificationConstants1;