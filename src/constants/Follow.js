const followConstants = {
    MY_PAGE: 'My Page',
    FOLLOW_ME: 'Follow me',
    I_FOLLOW: 'I Follow',
    USER_DESC: 'Specialty / Expertise: Cardiovascular Surgery, Oncology Surgery',
    CLINICAL: 'Clinical',
    LIFE: 'Life',
    HOSPITAL_MANAGEMENT: 'Hospital management',
    LOAD_MORE: 'Load more'
}

const followConstants1 = {
    MY_PAGE: 'マイベージ',
    FOLLOW_ME: '私がフォロ-',
    I_FOLLOW: '私をフォロー',
    USER_DESC: '専門/得意:循環器外科、腫瘍外科',
    CLINICAL: '臨床',
    LIFE: 'ライフ',
    HOSPITAL_MANAGEMENT: '病院経営',
    LOAD_MORE: 'さらに読み込む'

}

export default followConstants1;