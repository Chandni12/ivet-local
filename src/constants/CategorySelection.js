const CategoryConstants = {
  ANOTHER_BREATH:'Another Breath',
  TOP_DESC1_IT_IS_ALMOST: 'It is almost complete, select atleast 10 topics that interest you.',
  TOP_DESC2_PLEASE_GIVE_ME: 'Please give me. Related topics will be displayed',
  SUBCATEGORY_TITLE: 'Subcategory title', 
  START: 'Start', 
  CLOSE:'Close',
}

const CategoryConstants1 = {
  ANOTHER_BREATH:'もう一息です',
  TOP_DESC1_IT_IS_ALMOST: 'もうすぐ完了です。 興味があるトピックを10件以上選択して',
  TOP_DESC2_PLEASE_GIVE_ME: 'ください。 関連するトピックが表示されるようになります。',
  SUBCATEGORY_TITLE: '臨床内科 - Subcategory title here', 
  START: '開始する', 
  CLOSE:'閉じる',
}

export default CategoryConstants1;


