const TagConstants ={

    FEED_TAG:'Feed tags',
    FIND_OTHER_TAGS:'Find other tags',
    TAGS_DESCRIPTION:'When you add a feed tag, any questions or notes related to that tag will appear in the tag feed when there are posts related to the tag displayed here . New tags can be added on the tag page .',
SEARCH_TAGS:"Search Tags ...",
TAG_FEED:'Tag Feed',
    CANCEL:'Cancel',
    OK:'Ok',
    ADD_THIS_TAG:'Add this tag to your feed tags? Articles related to this tag will be added to the feed.',
    REMOVE_THIS_TAG:'Remove this tag from feed tags? Articles related to this tag will be removed from the feed.',
MANAGE_FOLLOWING_TAGS:'Manage following tags',
}

const TagConstantsJapanese ={
    FEED_TAG:'フィード タグ',
    FIND_OTHER_TAGS:'他のタグを探す',
    TAGS_DESCRIPTION:'フィード タグを追加すると、そのタグに関連した質問やノートが投稿された際ここに表示されたタグに関連する投稿があった際にはタグフィードに表示されます。新しいタグの追加は タグページで追加することができます。',
    SEARCH_TAGS:"タグを検索する...",
    TAG_FEED:'タグ・フィード',
    CANCEL:'キャンセル',
    OK:'Ok',
    ADD_THIS_TAG:'このタグをフィードタグに追加しますか？ このタグに関連する記事がフィードに追加されます。',
    REMOVE_THIS_TAG:'このタグをフィードタグから外しますか？ このタグに関連する記事がフィードから外されます。',
    MANAGE_FOLLOWING_TAGS:'次のタグを管理する',

}

export default TagConstantsJapanese; 