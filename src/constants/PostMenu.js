const PostMenu = {
    POST_MENU: "Post Menu",
    POST_A_QUESTION: "Post a Question",
    POST_CLINICAL_NOTES: "Post clinical notes",
    TOPICS_YOU_ARE_FOLLOWING: "Topics you are following",
    LIST_OF_MY_POST: "List of my posts",
    LIST_OF_DRAFTS: "List of drafts",
    POST_TYPE: "Post Type",
    Q_A_FORUM: "Q & A Forum",
    CLINICAL_NOTES: "Clinical notes",
    CATEGORY: "Category",
    CLINICAL: "Clinical",
    LIFE: "Life",
    HOTEL_MANAGEMENT: "Hospital Management",
    TIME_LINE:'Time Line',
    TAG_FEED: 'Tag Feed',
    NOTE:'Note',
}


const PostMenu1 = {
    NOTE:'ノート',
    POST_MENU: "投稿メニュー",
    POST_A_QUESTION: "質問を投稿する",
    POST_CLINICAL_NOTES: "臨床ノートを投稿する",
    TOPICS_YOU_ARE_FOLLOWING: "フォローしているトピックス",
    LIST_OF_MY_POST: "私の投稿一覧",
    LIST_OF_DRAFTS: "下書き一覧",
    POST_TYPE: "投稿タイプ",
    Q_A_FORUM: "Q&Aフォーラム",
    CLINICAL_NOTES: "臨床ノート",
    CATEGORY: "カテゴリ",
    CLINICAL: "臨床",
    LIFE: "ライフ",
    TIME_LINE:'タイムライン',
    HOTEL_MANAGEMENT: "病院経営",
    TAG_FEED: 'タグフィード',

}

export default PostMenu1;