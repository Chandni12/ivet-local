import CastConstants from './Cast';
import moment from 'moment';


const TimeConstant1 = {
    CONST_YEAR: "Year",
    CONST_MONTH: "Month",
    CONST_DAY: "Day",
    CONST_YEARS: "Years",
    CONST_MONTHS: "Months",
    CONST_DAYS: "Days",
    CONST_MINUTES: "Minutes",
    CONST_MINUTE: "Minute",
    CONST_HOUR: 'Hour',
    CONST_HOURS: 'Hours',
    CONST_SECONDS: 'Seconds',
    CONST_SECOND: 'Second',
    CONST_AGO: "Ago",
    CONST_BEFORE: "Before",
}

const TimeConstant = {
    CONST_SECOND:'第二',
    CONST_YEAR: "年",
    CONST_MONTH: "月",
    CONST_DAY: "日",
    CONST_YEARS: "年",
    CONST_MONTHS: "ヶ月",
    CONST_DAYS: "日々",
    CONST_AGO: "前",
    CONST_BEFORE: "前",
    CONST_SECONDS: '秒',
    CONST_HOUR: '時',
    CONST_HOURS: '時間',
    CONST_MINUTES: "議事録",
    CONST_MINUTE: "分",
}

export default TimeConstant;

export function showTimeWithMonthYearAndDate(timeUpdate) {
    moment.locale('en');
    var dt = timeUpdate;
    return (moment().format('MMMM Do YYYY')) //basically you can do all sorts of the formatting and others

}

export function showMonthYearAndDate(timeUpdate) {
    moment.locale('en');
    var dt = timeUpdate;
    return (moment().format('MMMM Do YYYY')) //basically you can do all sorts of the formatting and others

}

export function dateDiffInDays_Months_Years(timeUpdate) {
    var m1 = new Date(timeUpdate);
    const a = moment();
    const b = moment(timeUpdate);

    const dDiff = a.diff(b, 'days');
   
    const mDiff = a.diff(b, 'months');
   
    const hourDiff = a.diff(b, 'hours');
   
    const minDiff = a.diff(b, 'minutes');

    const secDiff = a.diff(b, "seconds" )
   
    const yDiff = a.diff(b, 'years');
   

    let timeShowOnPost = '';

    if (yDiff !== 0) {
        if (yDiff > 1) {
            timeShowOnPost = yDiff + ' ' + TimeConstant.CONST_YEARS
        } else {
            timeShowOnPost = yDiff + ' ' + TimeConstant.CONST_YEAR
        }
    } else if (mDiff !== 0) {
        if (mDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + mDiff + ' ' + TimeConstant.CONST_MONTHS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + mDiff + TimeConstant.CONST_MONTH
        }
    } else if (dDiff != 0) {
        if (dDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + dDiff + TimeConstant.CONST_DAYS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + dDiff + TimeConstant.CONST_DAY
        }
    } else if (hourDiff != 0) {

        if (hourDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + hourDiff + TimeConstant.CONST_HOURS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + hourDiff + TimeConstant.CONST_HOUR
        }
    } else if (minDiff != 0) {

        if (minDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + minDiff + TimeConstant.CONST_MINUTES
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + minDiff + TimeConstant.CONST_MINUTE
        }
    } else if(secDiff != 0) {
       
        if (secDiff>1) {
            timeShowOnPost = timeShowOnPost + ' ' + secDiff + TimeConstant.CONST_SECONDS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + secDiff + TimeConstant.CONST_SECONDS
        }
       
    }

    timeShowOnPost = timeShowOnPost + ' ' + TimeConstant.CONST_AGO;

  //  timeShowOnPost = timeShowOnPost + ' ' + TimeConstant.CONST_AGO + CastConstants.ANSWER;

   
    return (timeShowOnPost);
}

export function dateDiffIn_dd_mm_yy(timeUpdate) {
    var m1 = new Date(timeUpdate);
    const a = moment();
    const b = moment(timeUpdate);

    const dDiff = a.diff(b, 'days');
   
    const mDiff = a.diff(b, 'months');
   
    const hourDiff = a.diff(b, 'hours');
   
    const minDiff = a.diff(b, 'minutes');

    const secDiff = a.diff(b, "seconds" )
   
    const yDiff = a.diff(b, 'years');
   

    let timeShowOnPost = '';

    if (yDiff !== 0) {
        if (yDiff > 1) {
            timeShowOnPost = yDiff + ' ' + TimeConstant.CONST_YEARS
        } else {
            timeShowOnPost = yDiff + ' ' + TimeConstant.CONST_YEAR
        }
    } else if (mDiff !== 0) {
        if (mDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + mDiff + ' ' + TimeConstant.CONST_MONTHS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + mDiff + TimeConstant.CONST_MONTH
        }
    } else if (dDiff != 0) {
        if (dDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + dDiff + TimeConstant.CONST_DAYS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + dDiff + TimeConstant.CONST_DAY
        }
    } else if (hourDiff != 0) {

        if (hourDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + hourDiff + TimeConstant.CONST_HOURS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + hourDiff + TimeConstant.CONST_HOUR
        }
    } else if (minDiff != 0) {

        if (minDiff > 1) {
            timeShowOnPost = timeShowOnPost + ' ' + minDiff + TimeConstant.CONST_MINUTES
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + minDiff + TimeConstant.CONST_MINUTE
        }
    } else if(secDiff != 0) {
       
        if (secDiff>1) {
            timeShowOnPost = timeShowOnPost + ' ' + secDiff + TimeConstant.CONST_SECONDS
        } else {
            timeShowOnPost = timeShowOnPost + ' ' + secDiff + TimeConstant.CONST_SECONDS
        }
       
    }

    timeShowOnPost = timeShowOnPost + ' ' + TimeConstant.CONST_AGO;

   
    return (timeShowOnPost);
}

// export function dateDiffInDays_Months_Years1(timeUpdate) {
//     var m1 = new Date(timeUpdate);
//     var m2 = new Date();
//     var yDiff = m2.getFullYear() - m1.getFullYear();
//     var mDiff = m2.getMonth() - m1.getMonth();
//     var dDiff = m2.getDate() - m1.getDate();
//     var minDiff = m2.getMinutes() - m1.getMinutes();
//     var secDiff = m2.getSeconds() - m1.getSeconds();

//     if (dDiff < 0) {

//         var d = new Date(timeUpdate);
//         var lastDayOfMonth = new Date(d.getFullYear(), d.getMonth() + 1, 0);
//         // return lastDayOfMonth.getDate();

//         var daysInLastFullMonth = lastDayOfMonth.getDate();
//         if (daysInLastFullMonth < m1.getDate()) {
//             dDiff = daysInLastFullMonth + dDiff + (m1.getDate() - daysInLastFullMonth);
//         } else {
//             dDiff = daysInLastFullMonth + dDiff;
//         }
//         mDiff--;
//     }
//     if (mDiff < 0) {
//         mDiff = 12 + mDiff;
//         yDiff--;
//     }
//     // let timeShowOnPost = '';

//     // if (yDiff !== 0) {
//     //     if (yDiff > 1) {
//     //         timeShowOnPost = yDiff + ' ' + TimeConstant.CONST_YEARS
//     //     } else {
//     //         timeShowOnPost = yDiff + ' ' + TimeConstant.CONST_YEAR
//     //     }
//     // } else {
//         // if (mDiff !== 0) {
//         //     if (mDiff > 1) {
//         //         timeShowOnPost = timeShowOnPost + ' ' + mDiff + ' ' + TimeConstant.CONST_MONTHS
//         //     } else {
//         //         timeShowOnPost = timeShowOnPost + ' ' + mDiff + TimeConstant.CONST_MONTH
//         //     }
//         // } else {
//             // if (dDiff != 0) {
//             //     if (dDiff > 1) {
//             //         timeShowOnPost = timeShowOnPost + ' ' + dDiff + TimeConstant.CONST_DAYS
//             //     } else {
//             //         timeShowOnPost = timeShowOnPost + ' ' + dDiff + TimeConstant.CONST_DAY
//             //     }
//             // } else {
//                 // if (minDiff != 0) {
//                 //     console.log(" min Diff ", minDiff);
//                 //     if (minDiff > 1) {
//                 //         timeShowOnPost = timeShowOnPost + ' ' + minDiff + TimeConstant.CONST_MINUTES
//                 //     } else {
//                 //         timeShowOnPost = timeShowOnPost + ' ' + minDiff + TimeConstant.CONST_MINUTE
//                 //     }
//                 // } else {
//                 //     console.log(" SECONDS  Diff ", secDiff);
//                 //     timeShowOnPost = timeShowOnPost + ' ' + secDiff + TimeConstant.CONST_SECONDS
//                 // }

//             }
//         }
//     }

//     timeShowOnPost = timeShowOnPost + ' ' + TimeConstant.CONST_AGO + CastConstants.ANSWER;
//     return timeShowOnPost;
// }

export function getDaysInLastFullMonth(day) {
    var d = new Date(day);
    var lastDayOfMonth = new Date(d.getFullYear(), d.getMonth() + 1, 0);
    return lastDayOfMonth.getDate();
}