const Others = {
  OTHER_MENU: "Other menu",
  INFORMATION: 'Information',
  COLLECTION: 'Collection',
  COMMUNITY_SCORE:'Community Score',
  MY_PAGE: "My page",
  POSTING_MENU: 'Posting menu',
  HELP: 'Help',
  SETTINGS: 'Settings',
  TERMS_OF_USE_AND_PRIVACY: 'Terms of user and privacy',
  FOLLOWING_TAGS:'Following Tags',
  DRAFT:'Draft: ',
  POST_LIST:'Post List'
}


const Others1 = {
  OTHER_MENU: "その他メニユー",
  INFORMATION: 'お知らせ',
  COLLECTION: 'コレクション',
  COMMUNITY_SCORE:'コミュニティスコア',
  MY_PAGE: "マイベージ",
  POSTING_MENU: '投稿メニュー',
  HELP: 'ヘルプ',
  SETTINGS: '設定',
  TERMS_OF_USE_AND_PRIVACY: '利用規約・プライバシ-規約',
  FOLLOWING_TAGS:'フォロー中のタグ',
  DRAFT:'ドラフト',
  POST_LIST:'ドラフト：'
}

export default Others1;