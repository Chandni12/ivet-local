const CastConstants = {
  CAST_TITLE: "Post",
  POSTING_QUESTION: "Posting Question",
  SET_NOTES: "Set Notes",
  SUBTITLE: "",
  ADD_MARK_AT_THE_END_TITLE: "Add ? at the end of the question title",
  CHARACTER_LIMIT: "Characters",
  CHARACTER_LIMIT_2000: '2000 Characters',
  ENTER_YOUR_QUESTION: "Enter your question", // need to find correct translation from screen shot 
  BASIC_INFORMATION: 'Basic Information',
  MALE: 'Male',
  FEMALE: 'Female',
  NOT_KNOW: 'Not Know',
  AGE: 'Age',
  GENDER: 'Gender',
  PLEASE_ENTER_DETAILS_OF_QUESTION: 'Please enter the details of the question.',
  PLEASE_FILL_DETAILS_CORRECTLY: 'Please fill details correctly.',
  DO_YOU_HAVE_IMAGES: 'Do you have images',
  IMAGE: 'Image',
  IMAGE2: 'Image2',
  ADD: 'Add',
  PLEASE_ENTER_IMAGE_TITLE: 'Please enter the image title',
  SET_THE_POST_CATEGORY: 'Set the post category',
  MORE_ANSWER: "More answers if you setup specialized areas and tags appropriately. ",
  IT_WILL_BE_EASIER: "It will be easier.",
  QUESTION_CATEGORY: 'Question category',
  SPECIALITY_AREA: 'Speciality Area',
  TAG: 'Tag',
  DESCRIPTION_CATEGORY: 'Description',
  SAVE_DRAFT: 'Save as draft',
  PREVIEW_CONFIRMATION: 'Preview Confirmation',
  RED_TEXT_PHOTO_SECTION: 'Delete Image',
  RED_DELETE_TEXT: 'Delete Text',
  BASIC_INFO_ANIMAL_SPECIES: 'Animal Species',
  BASIC_INFO_VARIETY: "Variety",
  BASIC_INFO_SEX: 'Sex',
  BASIC_INFO_AGE: 'Age',
  PLEASE_SELECT_AN_ANIMAL_SPECIES: 'Please select an animal species',
  PLEASE_SELECT_A_BREED: 'Please select a breed',
  PLEASE_SELECT_A_GENDER: 'Please select a gender',
  PLEASE_SELECT_A_CATEGORY_OF_QUESTION: 'Please select a category of the question',
  PLEASE_SELECT_SUB_CATEGORY: 'Please select subcategory (required)',
  ADD_TAG: '  Add Tag',
  DRAFT_CREATED: 'Draft created! Its a bit more to post',
  DISCARD_DRAFT: 'Save as a draft',
  POST_A_QUESTION: 'Post a question',

  YOU_CAN_SELECT_MULTIPLE_TAGS: 'You can select multiple tags',
  PLEASE_PROVIDE_IMAGE_DESCRIPTION: 'Please provide a description of the image',
  QUESTION_CREATED: 'Question Created',
  //** Notes  */
  PLEASE_ENTER_CLINICAL_NOTES_TITLE: 'Enter clinical notes title',
  PLEASE_LEAVE_A_NOTE_ABOUT_CONTENT_SUMMARY: 'Please leave a note about the edited content. This content will be published.',
  ENTER_SUMMARY: "Enter Summary",
  SAVE_POST: 'Save post',
  GIVE_YOUR_NOTEBOOK_A_TITLE: "Give your notebook a title",
  ADD_TEXT: 'Add Text',
  ADD_IMAGE: 'Add Image',
  PLEASE_GIVE_A_DESCRIPTIVE_TITLE: "Please give a descriptive title",
  TEXT: "Text",
  SUMMARY: "Summary",
  RED_DELETE: "Delete",
  DETERMINE_THE_BEST_ANSWER: 'Determine the best answer?',
  YES: 'Yes',
  NO: 'No',
  ANSWER_MUST_BE_SPECIFIED: 'Answer must be specified.',
  ANSWER_MUST_BE_30_CHAR: 'answer must be at least 30 characters.',
  ANSWERS: 'Answers',
  ANSWER: 'Answer',
  UPDATED_ON: 'Updated on',
  VIEW_EDIT_HISTORY: 'View edit history',
REPORT_INAPPROPRIATE: 'Report as inappropriate',
HIDE_AUTHORS_NAME:'Hide authors name',
EDIT_QUESTION:'Edit question',
PLEASE_POST_YOUR_ANSWER:'Please post your answer.',
PLEASE_INCLUDE_NOTE:'Please include a note about the change (if necessary) (content will be published)',
WRITE_DOWN_MEMO: 'Write down the memo, including the reason for the change.',
YOUR_ANSWER_HAS_BEEN_POSTED:'Your answer has been posted',
EDIT_POST:'Edit Post',
SHARE:'Share',
THERE_ARE:'There are',
FAVORITE_ARTICLES:'favorite articles.',
POSTS:'Posts',
DRAFT:'Draft',
PHOTO:'Photo'
}


  


const CastConstants1 = {
  PHOTO:'写真',
  PLEASE_INCLUDE_NOTE :'（必要に応じて）変更内容に関するメモを記載してください（内容は公開されます）',
  WRITE_DOWN_MEMO  :'変更した理由などメモを記入してください。',

  POSTS:'投稿',
  FAVORITE_ARTICLES:'お気に入りの記事。',
  THERE_ARE:'がある',
  EDIT_POST:'投稿を編集',
  SHARE:'共有',
  YOUR_ANSWER_HAS_BEEN_POSTED:'あなたの回答が投稿されました',
  PLEASE_INCLUDE_NOTE:'（必要に応じて）変更内容に関するメモを記載してください（内容は公開されます）',
  WRITE_DOWN_MEMO:'変更した理由などメモを記入してください。',
  PLEASE_POST_YOUR_ANSWER:'回答を投稿してください。',
  EDIT_QUESTION:'質問を編集',
  HIDE_AUTHORS_NAME:'著者名を非表示',
  VIEW_EDIT_HISTORY: '編集履歴を表示',
  REPORT_INAPPROPRIATE: '不適切として報告',

  UPDATED_ON: '更新日',
  ANSWER: '回答',
  ANSWERS: '件の回答',
  ANSWER_MUST_BE_SPECIFIED: 'answerは、必ず指定してください。',
  ANSWER_MUST_BE_30_CHAR: 'answerは、30文字以上にしてください。',
  DETERMINE_THE_BEST_ANSWER: 'ベストアンサーを決定しますか？',
  YES: 'はい',
  NO: '番号',
  RED_DELETE_TEXT: "削除する",
  SUMMARY: "概要",
  ENTER_SUMMARY: '要約を入力してください',
  QUESTION_CREATED: '質問を作成しました',
  PLEASE_ENTER_IMAGE_TITLE: '画像 タ イ ト ル を ご 記 入 く だ さ い',
  PLEASE_PROVIDE_IMAGE_DESCRIPTION: '画像の説明をご記入ください',
  CAST_TITLE: "投稿",
  POSTING_QUESTION: "質問の投稿",
  SET_NOTES: "ノ-トを投稿",
  SUBTITLE: "",
  RED_TEXT_PHOTO_SECTION: '画像を削除',
  ADD_MARK_AT_THE_END_TITLE: "質問タイトルの最後に「？」をつけて下さい。",
  CHARACTER_LIMIT: "キャラクター",
  CHARACTER_LIMIT_2000: '残り 2000 文字',
  ENTER_YOUR_QUESTION: "どんなタイトルをつけますか?", // need to find correct translation from screen shot 
  BASIC_INFORMATION: '動物の基本情報',
  MALE: '男性',
  FEMALE: '雌',
  NOT_KNOW: 'わからない',
  AGE: '年齢',
  GENDER: '性別',
  PLEASE_ENTER_DETAILS_OF_QUESTION: '投稿の分類を設定してください。',
  PLEASE_FILL_DETAILS_CORRECTLY: '専門領域やタグを適正に設定するとより多くの回答を得やすくなります。',
  DO_YOU_HAVE_IMAGES: '画像はありますか?',
  IMAGE: '画像',
  IMAGE2: '画像2',
  ADD: '画像を追加',
  PLEASE_SELECT_IMAGE: '画像を追加する',
  SET_THE_POST_CATEGORY: '投稿の分類を設定してください',
  MORE_ANSWER: "専門領域やタグを適正に設定するとより多くの回答",
  IT_WILL_BE_EASIER: "を得やすくなります。",
  QUESTION_CATEGORY: '質問カテゴリ',
  SPECIALITY_AREA: '専門領域',
  DESCRIPTION_CATEGORY: '質問カテゴリ',
  SAVE_DRAFT: '下きを保存',
  PREVIEW_CONFIRMATION: 'プレビュー確認',
  BASIC_INFO_ANIMAL_SPECIES: '動物種:',
  BASIC_INFO_VARIETY: "品種:",
  BASIC_INFO_SEX: '性別:',
  BASIC_INFO_AGE: '年齢:',
  PLEASE_SELECT_AN_ANIMAL_SPECIES: '動物種を選択してください',
  PLEASE_SELECT_A_BREED: '品種を選択してください',
  PLEASE_SELECT_A_GENDER: '性別を選択してください',
  PLEASE_SELECT_A_CATEGORY_OF_QUESTION: '質問のカテゴリを選択してください',
  PLEASE_SELECT_SUB_CATEGORY: 'サブカテゴリを選択（必須）',
  ADD_TAG: 'タグを追加',
  YOU_CAN_SELECT_MULTIPLE_TAGS: '複数のタグを選択できます',
  TAG:'タグ',
  DRAFT_CREATED_MESSAGE: 'ドラフトを作成しました！投稿まであと少しです♬',
  DISCARD_DRAFT: '下書きを破棄',
  POST_A_QUESTION: '質問を投稿する',

  //** Notes  */
  PLEASE_ENTER_CLINICAL_NOTES_TITLE: 'ケーススタディのタイトルを入力',
  PLEASE_LEAVE_A_NOTE_ABOUT_CONTENT_SUMMARY: '編集した内容に関するメモを残してください。この内容は公開されます。',
  SAVE_POST: '投稿を保存します',
  GIVE_YOUR_NOTEBOOK_A_TITLE: "ノートブックにタイトルを付ける",
  ADD_TEXT: 'テキストを追加',
  ADD_IMAGE: '画像を追加',
  TEXT: 'テキスト',
  PLEASE_GIVE_A_DESCRIPTIVE_TITLE: "わかりやすいタイトルを入力してください",
  DRAFT:'ドラフト'

}

export default CastConstants1;