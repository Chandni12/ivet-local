/**
 * @format
 */

import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import { IVetApp} from './src/Navigator/Navigator';
import Tags from './src/components/Tags'; 
import InputRichTextEditor from './src/commonComponent/InputRichTextEditor';
import {name as appName} from './app.json';
import Example from './src/commonComponent/DraggableBox';
import NewTest from './NewTest';


 AppRegistry.registerComponent(appName, () => IVetApp);

//AppRegistry.registerComponent(appName, () => NewTest );